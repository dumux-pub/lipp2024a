// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Assembly
 * \ingroup StaggeredDiscretization
 * \ingroup MultiDomain
 * \brief A multidomain assembler for Jacobian and residual contribution per element (staggered method)
 */
#ifndef DUMUX_MY_MULTIDOMAIN_STAGGERED_LOCAL_ASSEMBLER_HH
#define DUMUX_MY_MULTIDOMAIN_STAGGERED_LOCAL_ASSEMBLER_HH

#include <dune/common/reservedvector.hh>
#include <dune/grid/common/gridenums.hh> // for GhostEntity
#include <dune/istl/matrixindexset.hh>

#include <dumux/common/reservedblockvector.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/numericdifferentiation.hh>
#include <dumux/common/typetraits/utility.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/assembly/fvlocalassemblerbase.hh>

#include <dumux/discretization/staggered/freeflow/mygridvolumevariables.hh>
#include <dumux/discretization/staggered/freeflow/gridandelemvolvarsfunctions.hh>

namespace Dumux {

namespace Properties{
    template<class TypeTag, class MyTypeTag>
    struct DualCVsVolVars { using type = UndefinedProperty; };

    template<class TypeTag, class MyTypeTag>
    struct PrimalCVsVolVars { using type = UndefinedProperty; };
}

/*!
 * \ingroup Assembly
 * \ingroup StaggeredDiscretization
 * \ingroup MultiDomain
 * \brief A base class for all multidomain local assemblers (staggered)
 * \tparam id the id of the sub domain
 * \tparam TypeTag the TypeTag
 * \tparam Assembler the assembler type
 * \tparam Implementation the actual assembler implementation
 * \tparam implicit whether the assembly is explicit or implicit in time
 */
template<std::size_t id, class TypeTag, class Assembler, class Implementation, bool isImplicit = true>
class MySubDomainStaggeredLocalAssemblerBase : public FVLocalAssemblerBase<TypeTag, Assembler,Implementation, isImplicit>
{
    using ParentType = FVLocalAssemblerBase<TypeTag, Assembler,Implementation, isImplicit>;

    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using LocalResidual = GetPropType<TypeTag, Properties::LocalResidual>;
    using ElementResidualVector = typename LocalResidual::ElementResidualVector;
    using JacobianMatrix = GetPropType<TypeTag, Properties::JacobianMatrix>;
    using SolutionVector = typename Assembler::SolutionVector;
    using SubSolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using ElementBoundaryTypes = GetPropType<TypeTag, Properties::ElementBoundaryTypes>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using GridVolumeVariables = typename GridVariables::GridVolumeVariables;
    using ElementVolumeVariables = typename GridVolumeVariables::LocalView;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using Scalar = typename GridVariables::Scalar;

    using ElementFaceVariables = typename GetPropType<TypeTag, Properties::GridFaceVariables>::LocalView;
    using CellCenterResidualValue = typename LocalResidual::CellCenterResidualValue;
    using FaceResidualValue = typename LocalResidual::FaceResidualValue;

    using GridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;

    using CouplingManager = typename Assembler::CouplingManager;

    static constexpr auto numEq = GetPropType<TypeTag, Properties::ModelTraits>::numEq();

public:
    static constexpr auto domainId = typename Dune::index_constant<id>();
    static constexpr auto cellCenterId = GridGeometry::cellCenterIdx();
    static constexpr auto faceId = GridGeometry::faceIdx();

    static constexpr auto faceOffset = getPropValue<TypeTag, Properties::NumEqCellCenter>();

    using ParentType::ParentType;

    explicit MySubDomainStaggeredLocalAssemblerBase(const Assembler& assembler,
                                                  const Element& element,
                                                  const SolutionVector& curSol,
                                                  CouplingManager& couplingManager)
    : ParentType(assembler,
                 element,
                 curSol,
                 localView(assembler.gridGeometry(domainId)),
                 localView(assembler.gridVariables(domainId).curGridVolVars()),
                 localView(assembler.gridVariables(domainId).prevGridVolVars()),
                 localView(assembler.gridVariables(domainId).gridFluxVarsCache()),
                 assembler.localResidual(domainId),
                 (element.partitionType() == Dune::GhostEntity))
    , curElemFaceVars_(localView(assembler.gridVariables(domainId).curGridFaceVars()))
    , prevElemFaceVars_(localView(assembler.gridVariables(domainId).prevGridFaceVars()))
    , couplingManager_(couplingManager)
    {}

    /*!
     * \brief Computes the derivatives with respect to the given element and adds them
     *        to the global matrix. The element residual is written into the right hand side.
     */
    template<class JacobianMatrixRow, class SubSol, class GridVariablesTuple>
    void assembleJacobianAndResidual(JacobianMatrixRow& jacRow, SubSol& res, GridVariablesTuple& gridVariables)
    {
        this->asImp_().bindLocalViews();
        this->elemBcTypes().update(problem(), this->element(), this->fvGeometry());

        assembleJacobianAndResidualImpl_(domainId, jacRow, res, gridVariables);
    }

    /*!
     * \brief Assemble the residual only
     */
    template<class SubSol>
    void assembleResidual(SubSol& res)
    {
        this->asImp_().bindLocalViews();
        this->elemBcTypes().update(problem(), this->element(), this->fvGeometry());

        assembleResidualImpl_(domainId, res);
    }

    /*!
     * \brief Convenience function to evaluate the complete local residual for the current element. Automatically chooses the the appropriate
     *        element volume variables.
     */
    CellCenterResidualValue evalLocalResidualForCellCenter() const
    {
        if (!isImplicit)
            if (this->assembler().isStationaryProblem())
                DUNE_THROW(Dune::InvalidStateException, "Using explicit jacobian assembler with stationary local residual");

        if (this->elementIsGhost())
        {
            return CellCenterResidualValue(0.0);
        }

        return isImplicit ? evalLocalResidualForCellCenter(this->curElemVolVars(), this->curElemFaceVars())
                          : evalLocalResidualForCellCenter(this->prevElemVolVars(), this->prevElemFaceVars());
    }

    /*!
     * \brief Evaluates the complete local residual for the current cell center.
     * \param elemVolVars The element volume variables
     * \param elemFaceVars The element face variables
     */
    CellCenterResidualValue evalLocalResidualForCellCenter(const ElementVolumeVariables& elemVolVars,
                                                           const ElementFaceVariables& elemFaceVars) const
    {
        auto residual = evalLocalFluxAndSourceResidualForCellCenter(elemVolVars, elemFaceVars);

        if (!this->assembler().isStationaryProblem())
            residual += evalLocalStorageResidualForCellCenter();

        this->localResidual().evalBoundaryForCellCenter(residual, this->problem(), this->element(), this->fvGeometry(), elemVolVars, elemFaceVars, this->elemBcTypes(), this->elemFluxVarsCache());

        return residual;
    }

    /*!
     * \brief Convenience function to evaluate the flux and source terms (i.e, the terms without a time derivative)
     *        of the local residual for the current element. Automatically chooses the the appropriate
     *        element volume and face variables.
     */
    CellCenterResidualValue evalLocalFluxAndSourceResidualForCellCenter() const
    {
        return isImplicit ? evalLocalFluxAndSourceResidualForCellCenter(this->curElemVolVars(), this->curElemFaceVars())
                          : evalLocalFluxAndSourceResidualForCellCenter(this->prevElemVolVars(), this->prevElemFaceVars());
     }

    /*!
     * \brief Evaluates the flux and source terms (i.e, the terms without a time derivative)
     *        of the local residual for the current element.
     *
     * \param elemVolVars The element volume variables
     * \param elemVolVars The element face variables
     */
    CellCenterResidualValue evalLocalFluxAndSourceResidualForCellCenter(const ElementVolumeVariables& elemVolVars, const ElementFaceVariables& elemFaceVars) const
    {
        return this->localResidual().evalFluxAndSourceForCellCenter(this->element(), this->fvGeometry(), elemVolVars, elemFaceVars, this->elemBcTypes(), this->elemFluxVarsCache());
    }

    /*!
     * \brief Convenience function to evaluate storage term (i.e, the term with a time derivative)
     *        of the local residual for the current element. Automatically chooses the the appropriate
     *        element volume and face variables.
     */
    CellCenterResidualValue evalLocalStorageResidualForCellCenter() const
    {
        return this->localResidual().evalStorageForCellCenter(this->element(), this->fvGeometry(), this->prevElemVolVars(), this->curElemVolVars());
    }

    /*!
     * \brief Convenience function to evaluate the  local residual for the current face. Automatically chooses the the appropriate
     *        element volume and face variables.
     * \param scvf The sub control volume face
     */
    FaceResidualValue evalLocalResidualForFace(const SubControlVolumeFace& scvf, bool writeOut=false) const
    {
        if (!isImplicit)
            if (this->assembler().isStationaryProblem())
                DUNE_THROW(Dune::InvalidStateException, "Using explicit jacobian assembler with stationary local residual");

        if (this->elementIsGhost())
        {
            return FaceResidualValue(0.0);
        }

        return isImplicit ? evalLocalResidualForFace(scvf, this->curElemVolVars(), this->curElemFaceVars(), writeOut)
                          : evalLocalResidualForFace(scvf, this->prevElemVolVars(), this->prevElemFaceVars(), writeOut);
    }

    /*!
     * \brief Evaluates the complete local residual for the current face.
     * \param scvf The sub control volume face
     * \param elemVolVars The element volume variables
     * \param elemFaceVars The element face variables
     */
    FaceResidualValue evalLocalResidualForFace(const SubControlVolumeFace& scvf,
                                               const ElementVolumeVariables& elemVolVars,
                                               const ElementFaceVariables& elemFaceVars, bool writeOut=false) const
    {
        auto residual = evalLocalFluxAndSourceResidualForFace(scvf, elemVolVars, elemFaceVars, writeOut);

        if (!this->assembler().isStationaryProblem())
            residual += evalLocalStorageResidualForFace(scvf, writeOut);

        static constexpr auto domainId = Dune::index_constant<id>();
        const auto& curSol = this->curSol()[domainId];

        this->localResidual().evalBoundaryForFace(residual, this->problem(), this->element(), this->fvGeometry(), elemVolVars, elemFaceVars, this->elemBcTypes(), this->elemFluxVarsCache(), scvf, curSol, writeOut);

        return residual;
    }

    /*!
     * \brief Convenience function to evaluate the flux and source terms (i.e, the terms without a time derivative)
     *        of the local residual for the current element. Automatically chooses the the appropriate
     *        element volume and face variables.
     * \param scvf The sub control volume face
     */
    FaceResidualValue evalLocalFluxAndSourceResidualForFace(const SubControlVolumeFace& scvf, bool writeOut) const
    {
        return isImplicit ? evalLocalFluxAndSourceResidualForFace(scvf, this->curElemVolVars(), this->curElemFaceVars(), writeOut)
                          : evalLocalFluxAndSourceResidualForFace(scvf, this->prevElemVolVars(), this->prevElemFaceVars(), writeOut);
    }

    /*!
     * \brief Evaluates the flux and source terms (i.e, the terms without a time derivative)
     *        of the local residual for the current face.
     * \param scvf The sub control volume face
     * \param elemVolVars The element volume variables
     * \param elemFaceVars The element face variables
     */
    FaceResidualValue evalLocalFluxAndSourceResidualForFace(const SubControlVolumeFace& scvf,
                                                            const ElementVolumeVariables& elemVolVars,
                                                            const ElementFaceVariables& elemFaceVars, bool writeOut) const
    {
        return this->localResidual().evalFluxAndSourceForFace(this->element(), this->fvGeometry(), elemVolVars, elemFaceVars, this->elemBcTypes(), this->elemFluxVarsCache(), scvf, this->curSol(), writeOut);
    }

    /*!
     * \brief Convenience function to evaluate storage term (i.e, the term with a time derivative)
     *        of the local residual for the current face. Automatically chooses the the appropriate
     *        element volume and face variables.
     * \param scvf The sub control volume face
     */
    FaceResidualValue evalLocalStorageResidualForFace(const SubControlVolumeFace& scvf, bool writeOut) const
    {
        return this->localResidual().evalStorageForFace(this->element(), this->fvGeometry(), this->prevElemVolVars(), this->curElemVolVars(), this->prevElemFaceVars(), this->curElemFaceVars(), scvf);
    }

    const Problem& problem() const
    { return this->assembler().problem(domainId); }

    //! The current element volume variables
    ElementFaceVariables& curElemFaceVars()
    { return curElemFaceVars_; }

    //! The element volume variables of the provious time step
    ElementFaceVariables& prevElemFaceVars()
    { return prevElemFaceVars_; }

    //! The current element volume variables
    const ElementFaceVariables& curElemFaceVars() const
    { return curElemFaceVars_; }

    //! The element volume variables of the provious time step
    const ElementFaceVariables& prevElemFaceVars() const
    { return prevElemFaceVars_; }

    CouplingManager& couplingManager()
    { return couplingManager_; }

private:

    //! Assembles the residuals for the cell center dofs.
    template<class SubSol>
    void assembleResidualImpl_(Dune::index_constant<cellCenterId>, SubSol& res)
    {
        const auto cellCenterGlobalI = problem().gridGeometry().elementMapper().index(this->element());
        res[cellCenterGlobalI] = this->asImp_().assembleCellCenterResidualImpl();
    }

    //! Assembles the residuals for the face dofs.
    template<class SubSol>
    void assembleResidualImpl_(Dune::index_constant<faceId>, SubSol& res)
    {
        for(auto&& scvf : scvfs(this->fvGeometry()))
        {
            res[scvf.dofIndex()] +=  this->asImp_().assembleFaceResidualImpl(scvf);

        }
    }

    //! Assembles the residuals and derivatives for the cell center dofs.
    template<class JacobianMatrixRow, class SubSol, class GridVariablesTuple>
    auto assembleJacobianAndResidualImpl_(Dune::index_constant<cellCenterId>, JacobianMatrixRow& jacRow, SubSol& res, GridVariablesTuple& gridVariables)
    {
        auto& gridVariablesI = *std::get<domainId>(gridVariables);
        const auto cellCenterGlobalI = problem().gridGeometry().elementMapper().index(this->element());
        const auto residual = this->asImp_().assembleCellCenterJacobianAndResidualImpl(jacRow[domainId], gridVariablesI);
        res[cellCenterGlobalI] = residual;


        // for the coupling blocks
        using namespace Dune::Hybrid;
        static constexpr auto otherDomainIds = makeIncompleteIntegerSequence<JacobianMatrixRow::size(), domainId>{};
        forEach(otherDomainIds, [&](auto&& domainJ)
        {
            this->asImp_().assembleJacobianCellCenterCoupling(domainJ, jacRow[domainJ], residual, gridVariablesI);
        });
    }

    //! Assembles the residuals and derivatives for the face dofs.
    template<class JacobianMatrixRow, class SubSol, class GridVariablesTuple>
    void assembleJacobianAndResidualImpl_(Dune::index_constant<faceId>, JacobianMatrixRow& jacRow, SubSol& res, GridVariablesTuple& gridVariables)
    {
        auto& gridVariablesI = *std::get<domainId>(gridVariables);
        const auto residual = this->asImp_().assembleFaceJacobianAndResidualImpl(jacRow[domainId], gridVariablesI);

        for(auto&& scvf : scvfs(this->fvGeometry()))
            res[scvf.dofIndex()] += residual[scvf.localFaceIdx()];

        // for the coupling blocks
        using namespace Dune::Hybrid;
        static constexpr auto otherDomainIds = makeIncompleteIntegerSequence<JacobianMatrixRow::size(), domainId>{};
        forEach(otherDomainIds, [&](auto&& domainJ)
        {
            this->asImp_().assembleJacobianFaceCoupling(domainJ, jacRow[domainJ], residual, gridVariablesI);
        });
    }

    ElementFaceVariables curElemFaceVars_;
    ElementFaceVariables prevElemFaceVars_;
    CouplingManager& couplingManager_; //!< the coupling manager
};

/*!
 * \ingroup Assembly
 * \ingroup StaggeredDiscretization
 * \ingroup MultiDomain
 * \brief A base class for all implicit multidomain local assemblers (staggered)
 * \tparam id the id of the sub domain
 * \tparam TypeTag the TypeTag
 * \tparam Assembler the assembler type
 * \tparam Implementation the actual assembler implementation
 */
template<std::size_t id, class TypeTag, class Assembler, class Implementation>
class MySubDomainStaggeredLocalAssemblerImplicitBase : public MySubDomainStaggeredLocalAssemblerBase<id, TypeTag, Assembler, Implementation>
{
    using ParentType = MySubDomainStaggeredLocalAssemblerBase<id, TypeTag, Assembler, Implementation>;
    static constexpr auto domainId = Dune::index_constant<id>();
public:
    using ParentType::ParentType;

    void bindLocalViews()
    {
        // get some references for convenience
        auto& couplingManager = this->couplingManager();
        const auto& element = this->element();
        const auto& curSol = this->curSol()[domainId];
        auto&& fvGeometry = this->fvGeometry();
        auto&& curElemVolVars = this->curElemVolVars();
        auto&& curElemFaceVars = this->curElemFaceVars();
        auto&& elemFluxVarsCache = this->elemFluxVarsCache();

        // bind the caches
        couplingManager.bindCouplingContext(domainId, element, this->assembler());
        fvGeometry.bind(element);
        curElemVolVars.bind(element, fvGeometry, curSol);
        curElemFaceVars.bind(element, fvGeometry, curSol);
        elemFluxVarsCache.bind(element, fvGeometry, curElemVolVars);
        if (!this->assembler().isStationaryProblem())
        {
            this->prevElemVolVars().bindElement(element, fvGeometry, this->assembler().prevSol()[domainId]);
            this->prevElemFaceVars().bindElement(element, fvGeometry, this->assembler().prevSol()[domainId]);
        }
    }

};

/*!
 * \ingroup Assembly
 * \ingroup StaggeredDiscretization
 * \ingroup MultiDomain
 * \brief The staggered multidomain local assembler
 * \tparam id the id of the sub domain
 * \tparam TypeTag the TypeTag
 * \tparam Assembler the assembler type
 * \tparam DM the numeric differentiation method
 * \tparam implicit whether the assembler is explicit or implicit in time
 */
template<std::size_t id, class TypeTag, class Assembler, DiffMethod DM = DiffMethod::numeric, bool implicit = true>
class MySubDomainStaggeredLocalAssembler;

/*!
 * \ingroup Assembly
 * \ingroup StaggeredDiscretization
 * \ingroup MultiDomain
 * \brief Staggered scheme local assembler using numeric differentiation and implicit time discretization
 */
template<std::size_t id, class TypeTag, class Assembler>
class MySubDomainStaggeredLocalAssembler<id, TypeTag, Assembler, DiffMethod::numeric, /*implicit=*/true>
: public MySubDomainStaggeredLocalAssemblerImplicitBase<id, TypeTag, Assembler,
            MySubDomainStaggeredLocalAssembler<id, TypeTag, Assembler, DiffMethod::numeric, true> >
{
    using ThisType = MySubDomainStaggeredLocalAssembler<id, TypeTag, Assembler, DiffMethod::numeric, /*implicit=*/true>;
    using ParentType = MySubDomainStaggeredLocalAssemblerImplicitBase<id, TypeTag, Assembler, ThisType>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using LocalResidual = GetPropType<TypeTag, Properties::LocalResidual>;
    using CellCenterResidualValue = typename LocalResidual::CellCenterResidualValue;
    using FaceResidualValue = typename LocalResidual::FaceResidualValue;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GridFaceVariables = GetPropType<TypeTag, Properties::GridFaceVariables>;
    using ElementFaceVariables = typename GetPropType<TypeTag, Properties::GridFaceVariables>::LocalView;
    using FaceVariables = typename ElementFaceVariables::FaceVariables;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using GridVolumeVariables = typename GridVariables::GridVolumeVariables;
    using ElementVolumeVariables = typename GridVolumeVariables::LocalView;
    using PrimalCVsVolVars = GetPropType<TypeTag, Properties::PrimalCVsVolVars>;
    using DualCVsVolVars = GetPropType<TypeTag, Properties::DualCVsVolVars>;
    using CellCenterPrimaryVariables = GetPropType<TypeTag, Properties::CellCenterPrimaryVariables>;
    using FacePrimaryVariables = GetPropType<TypeTag, Properties::FacePrimaryVariables>;
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;

    static constexpr bool enableGridFluxVarsCache = getPropValue<TypeTag, Properties::EnableGridFluxVariablesCache>();
    static constexpr int maxNeighbors = 4*(2*ModelTraits::dim());
    static constexpr auto domainI = Dune::index_constant<id>();
    static constexpr auto cellCenterId = GridGeometry::cellCenterIdx();
    static constexpr auto faceId = GridGeometry::faceIdx();

    static constexpr auto numEq = ModelTraits::numEq();
    static constexpr auto numEqCellCenter = CellCenterPrimaryVariables::dimension;
    static constexpr auto numEqFace = FacePrimaryVariables::dimension;

public:
    using ParentType::ParentType;

    CellCenterResidualValue assembleCellCenterResidualImpl()
    {
        return this->evalLocalResidualForCellCenter();
    }

    FaceResidualValue assembleFaceResidualImpl(const SubControlVolumeFace& scvf)
    {
        return this->evalLocalResidualForFace(scvf, false);
    }

    /*!
     * \brief Computes the derivatives with respect to the given element and adds them
     *        to the global matrix.
     *
     * \return The element residual at the current solution.
     */
    template<class JacobianMatrixDiagBlock, class GridVariables>
    CellCenterResidualValue assembleCellCenterJacobianAndResidualImpl(JacobianMatrixDiagBlock& A, GridVariables& gridVariables)
    {
        assert(domainI == cellCenterId);
        //////////////////////////////////////////////////////////////////////////////////////////////////
        // Calculate derivatives of all dofs in stencil with respect to the dofs in the element. In the //
        // neighboring elements we do so by computing the derivatives of the fluxes which depend on the //
        // actual element. In the actual element we evaluate the derivative of the entire residual.     //
        //////////////////////////////////////////////////////////////////////////////////////////////////

        // get some aliases for convenience
        const auto& element = this->element();
        const auto& fvGeometry = this->fvGeometry();
        auto&& curElemVolVars = this->curElemVolVars();
        const auto& gridGeometry = this->problem().gridGeometry();
        const auto& curSol = this->curSol()[domainI];

        // get the vecor of the acutal element residuals
        // const auto origResiduals = this->evalLocalResidual();

        const auto cellCenterGlobalI = gridGeometry.elementMapper().index(element);
        const auto origResidual = this->evalLocalResidualForCellCenter();

        //////////////////////////////////////////////////////////////////////////////////////////////////
        //                                                                                              //
        // Calculate derivatives of all dofs in stencil with respect to the dofs in the element. In the //
        // neighboring elements we do so by computing the derivatives of the fluxes which depend on the //
        // actual element. In the actual element we evaluate the derivative of the entire residual.     //
        //                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////

       // build derivatives with for cell center dofs w.r.t. cell center dofs

       const auto& connectivityMap = gridGeometry.connectivityMap();

       // create the vector storing the partial derivatives
       CellCenterResidualValue partialDeriv(0.0);

        for(const auto& globalJ : connectivityMap(cellCenterId, cellCenterId, cellCenterGlobalI))
        {
            for(int pvIdx = 0; pvIdx < numEqCellCenter; ++pvIdx)
            {
                using PrimaryVariables = typename VolumeVariables::PrimaryVariables;
                auto cellCenterPriVars = curSol[globalJ];

                const auto& cellCenterPriVarsVar = curSol[globalJ];
                PrimaryVariables priVarsVar = makePriVarsFromCellCenterPriVars<PrimaryVariables>(cellCenterPriVarsVar);

                constexpr auto offset = numEq - numEqCellCenter;
                partialDeriv = 0.0;

                auto evalResidual = [&](Scalar priVar)
                {
                    auto&& scvJ = fvGeometry.scv(globalJ);
                    const auto elementJ = fvGeometry.gridGeometry().element(globalJ);

                    // get the volVars of the element with respect to which we are going to build the derivative
                    auto& curPrimalVolVars =  this->getVolVarAccess(gridVariables.curGridVolVars(), curElemVolVars, cellCenterGlobalI);
                    auto& curVolVarsVar =  this->getVolVarAccess(gridVariables.curGridVolVars(), curElemVolVars, scvJ);

                    const auto origPrimalVolVars(curPrimalVolVars);
                    const auto origVolVarsVar(curVolVarsVar);

                    // update the volume variables
                    priVarsVar[pvIdx + offset] = priVar;
                    auto elemSol = elementSolution<FVElementGeometry>(std::move(priVarsVar));

                    using ElementSolution = std::decay_t<decltype(elemSol)>;
                    using Element = std::decay_t<decltype(elementJ)>;
                    using SubControlVolume = std::decay_t<decltype(scvJ)>;
                    std::vector<ElementSolution> elemSols;
                    elemSols.push_back(elemSol);
                    std::vector<Element> elementJs;
                    elementJs.push_back(elementJ);
                    std::vector<SubControlVolume> scvJs;
                    scvJs.push_back(scvJ);
                    std::vector<Scalar> interpolationFactors;
                    interpolationFactors.push_back(1.);

                    curVolVarsVar.adaptiveUpdate(elemSols, this->problem(), elementJs, scvJs, interpolationFactors);

                    // update the volume variables
                    cellCenterPriVars[pvIdx] = priVar;
                    primalCVsVolVarsUpdate_(curPrimalVolVars, this->curSol(), globalJ, this->problem(), fvGeometry, cellCenterPriVars);

                    //compute element residual
                    auto retVal = this->evalLocalResidualForCellCenter();

                    //restore the original volVars
                    curPrimalVolVars = origPrimalVolVars;
                    curVolVarsVar = origVolVarsVar;

                    return retVal;
                };

                // derive the residuals numerically
                const auto& paramGroup = this->problem().paramGroup();
                static const int numDiffMethod = getParamFromGroup<int>(paramGroup, "Assembly.NumericDifferenceMethod");
                static const auto eps = this->couplingManager().numericEpsilon(domainI, paramGroup);
                NumericDifferentiation::partialDerivative(evalResidual, priVarsVar[pvIdx + offset], partialDeriv, origResidual,
                                                            eps(priVarsVar[pvIdx + offset], pvIdx), numDiffMethod);

                // update the global jacobian matrix with the current partial derivatives
                updateGlobalJacobian_(A, cellCenterGlobalI, globalJ, pvIdx, partialDeriv);
            }
       }

        return origResidual;
    }

    /*!
     * \brief Computes the derivatives with respect to the given element and adds them
     *        to the global matrix.
     *
     * \return The element residual at the current solution.
     */
    template<class JacobianMatrixDiagBlock, class GridVariables>
    auto assembleFaceJacobianAndResidualImpl(JacobianMatrixDiagBlock& A, GridVariables& gridVariables)
    {
        assert(domainI == faceId);
        //////////////////////////////////////////////////////////////////////////////////////////////////
        // Calculate derivatives of all dofs in stencil with respect to the dofs in the element. In the //
        // neighboring elements we do so by computing the derivatives of the fluxes which depend on the //
        // actual element. In the actual element we evaluate the derivative of the entire residual.     //
        //////////////////////////////////////////////////////////////////////////////////////////////////

        // get some aliases for convenience
        const auto& problem = this->problem();
        const auto& element = this->element();
        const auto& fvGeometry = this->fvGeometry();
        const auto& gridGeometry = this->problem().gridGeometry();
        const auto& curSol = this->curSol()[domainI];

        using FaceSolutionVector = GetPropType<TypeTag, Properties::FaceSolutionVector>; // TODO: use reserved vector
        FaceSolutionVector origResiduals;
        origResiduals.resize(fvGeometry.numScvf());
        origResiduals = 0.0;

        // get the vecor of the acutal element residuals
        // const auto origResiduals = this->evalLocalResidual();
        // treat the local residua of the face dofs:
        for(auto&& scvf : scvfs(fvGeometry))
        {
            origResiduals[scvf.localFaceIdx()] =  this->evalLocalResidualForFace(scvf, true);
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////
        //                                                                                              //
        // Calculate derivatives of all dofs in stencil with respect to the dofs in the element. In the //
        // neighboring elements we do so by computing the derivatives of the fluxes which depend on the //
        // actual element. In the actual element we evaluate the derivative of the entire residual.     //
        //                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////

       // build derivatives with for cell center dofs w.r.t. cell center dofs

       const auto& connectivityMap = gridGeometry.connectivityMap();
       const auto& fluxCorrectionGeometry = gridGeometry.fluxCorrectionGeometry();

       FaceResidualValue partialDeriv(0.0);

       for(auto&& scvf : scvfs(fvGeometry))
       {
            // set the actual dof index
            const auto faceGlobalI = scvf.dofIndex();

           // build derivatives with for face dofs w.r.t. cell center dofs
           for(const auto& globalJ : connectivityMap(faceId, faceId, scvf.index()))
           {
                for(int pvIdx = 0; pvIdx < numEqFace; ++pvIdx)
                {
                   partialDeriv = 0;

                   using FaceSolution = GetPropType<TypeTag, Properties::StaggeredFaceSolution>;
                    const auto origFaceSolution = FaceSolution(scvf, curSol, gridGeometry);
                    auto& faceVars = getFaceVarAccess(gridVariables.curGridFaceVars(), this->curElemFaceVars(), scvf);
                    const auto origFaceVars(faceVars);
                    auto faceSolution = origFaceSolution;

                   auto evalResidual = [&](Scalar priVar)
                   {
                        const auto& firstOppoFace = fluxCorrectionGeometry.firstOpposingFace(scvf.index());
                        const auto& secondOppoFace = fluxCorrectionGeometry.secondOpposingFace(scvf.index());
                        const auto& firstNeighFace = fluxCorrectionGeometry.firstNeighborFace(scvf.index());
                        const auto& secondNeighFace = fluxCorrectionGeometry.secondNeighborFace(scvf.index());
                        const auto& thirdNeighFace = fluxCorrectionGeometry.thirdNeighborFace(scvf.index());
                        const auto& forthNeighFace = fluxCorrectionGeometry.forthNeighborFace(scvf.index());

                        // get the faceVars of the face with respect to which we are going to build the derivative
                        auto& firstOppoCurFaceVars = this->getFaceVarAccess(gridVariables.curGridFaceVars(), this->curElemFaceVars(), firstOppoFace);
                        auto& secondOppoCurFaceVars = this->getFaceVarAccess(gridVariables.curGridFaceVars(), this->curElemFaceVars(), secondOppoFace);
                        auto& firstNeighCurFaceVars = this->getFaceVarAccess(gridVariables.curGridFaceVars(), this->curElemFaceVars(), firstNeighFace);
                        auto& secondNeighCurFaceVars = this->getFaceVarAccess(gridVariables.curGridFaceVars(), this->curElemFaceVars(), secondNeighFace);
                        auto& thirdNeighCurFaceVars = this->getFaceVarAccess(gridVariables.curGridFaceVars(), this->curElemFaceVars(), thirdNeighFace);
                        auto& forthNeighCurFaceVars = this->getFaceVarAccess(gridVariables.curGridFaceVars(), this->curElemFaceVars(), forthNeighFace);

                        const auto firstOppoOrigFaceVars(firstOppoCurFaceVars);
                        const auto secondOppoOrigFaceVars(secondOppoCurFaceVars);
                        const auto firstNeighOrigFaceVars(firstNeighCurFaceVars);
                        const auto secondNeighOrigFaceVars(secondNeighCurFaceVars);
                        const auto thirdNeighOrigFaceVars(thirdNeighCurFaceVars);
                        const auto forthNeighOrigFaceVars(forthNeighCurFaceVars);

                        const auto& firstNeighElement = fluxCorrectionGeometry.firstNeighElement(scvf.index());
                        const auto& secondNeighElement = fluxCorrectionGeometry.secondNeighElement(scvf.index());
                        const auto& thirdNeighElement = fluxCorrectionGeometry.thirdNeighElement(scvf.index());
                        const auto& forthNeighElement = fluxCorrectionGeometry.forthNeighElement(scvf.index());

                        faceVarsUpdate_(firstOppoCurFaceVars, firstOppoFace, curSol, globalJ, pvIdx, priVar, problem, element);
                        faceVarsUpdate_(secondOppoCurFaceVars, secondOppoFace, curSol, globalJ, pvIdx, priVar, problem, element);

                        faceVarsUpdate_(firstNeighCurFaceVars, firstNeighFace, curSol, globalJ, pvIdx, priVar, problem, firstNeighElement);
                        faceVarsUpdate_(secondNeighCurFaceVars, secondNeighFace, curSol, globalJ, pvIdx, priVar, problem, secondNeighElement);
                        faceVarsUpdate_(thirdNeighCurFaceVars, thirdNeighFace, curSol, globalJ, pvIdx, priVar, problem, thirdNeighElement);
                        faceVarsUpdate_(forthNeighCurFaceVars, forthNeighFace, curSol, globalJ, pvIdx, priVar, problem, forthNeighElement);

                        // update the volume variables and compute element residual
                        faceSolution[globalJ][pvIdx] = priVar;
                        faceVars.update(faceSolution, problem, element, fvGeometry, scvf);

                        auto retVal = this->evalLocalResidualForFace(scvf, false);

                        // restore the original faceVars
                        faceVars = origFaceVars;
                        firstOppoCurFaceVars = firstOppoOrigFaceVars;
                        secondOppoCurFaceVars = secondOppoOrigFaceVars;
                        firstNeighCurFaceVars = firstNeighOrigFaceVars;
                        secondNeighCurFaceVars = secondNeighOrigFaceVars;
                        thirdNeighCurFaceVars = thirdNeighOrigFaceVars;
                        forthNeighCurFaceVars = forthNeighOrigFaceVars;

                        return retVal;
                   };

                   // derive the residuals numerically
                   const auto& paramGroup = problem.paramGroup();
                   static const int numDiffMethod = getParamFromGroup<int>(paramGroup, "Assembly.NumericDifferenceMethod");
                   static const auto eps = this->couplingManager().numericEpsilon(domainI, paramGroup);
                   NumericDifferentiation::partialDerivative(evalResidual, faceSolution[globalJ][pvIdx], partialDeriv, origResiduals[scvf.localFaceIdx()],
                                                             eps(faceSolution[globalJ][pvIdx], pvIdx), numDiffMethod);//temporary note: I think this line should be fine in the flux correction context (18.2.2019)

                   // update the global jacobian matrix with the current partial derivatives
                   updateGlobalJacobian_(A, faceGlobalI, globalJ, pvIdx, partialDeriv);
               }
           }
       }

        return origResiduals;
    }

    /*!
     * \brief Computes the derivatives with respect to the given element and adds them
     *        to the global matrix.
     *
     * \return The element residual at the current solution.
     */
    template<class JacobianBlock, class GridVariables>
    void assembleJacobianCellCenterCoupling(Dune::index_constant<faceId> domainJ, JacobianBlock& A,
                                            const CellCenterResidualValue& origResidual, GridVariables& gridVariables)
    {
        // ////////////////////////////////////////////////////////////////////////////////////////////////////////
        // // Calculate derivatives of all dofs in the element with respect to all dofs in the coupling stencil. //
        // ////////////////////////////////////////////////////////////////////////////////////////////////////////

        // get some aliases for convenience
        const auto& element = this->element();
        const auto& fvGeometry = this->fvGeometry();
        const auto& gridGeometry = this->problem().gridGeometry();
        // const auto& curSol = this->curSol()[domainI];
        // build derivatives with for cell center dofs w.r.t. cell center dofs
        const auto cellCenterGlobalI = gridGeometry.elementMapper().index(element);

        // create the vector storing the partial derivatives
        CellCenterResidualValue partialDeriv(0.0);

        for(const auto& scvfJ : scvfs(fvGeometry))
        {
             const auto globalJ = scvfJ.dofIndex();

            // get the faceVars of the face with respect to which we are going to build the derivative
            auto& faceVars = getFaceVarAccess(gridVariables.curGridFaceVars(), this->curElemFaceVars(), scvfJ);
            const auto origFaceVars(faceVars);

            for(int pvIdx = 0; pvIdx < numEqFace; ++pvIdx)
            {
                auto facePriVars(this->curSol()[faceId][globalJ]);
                partialDeriv = 0.0;

                auto evalResidual = [&](Scalar priVar)
                {
                    // update the volume variables and compute element residual
                    facePriVars[pvIdx] = priVar;
                    faceVars.updateOwnFaceOnly(facePriVars);
                    return this->evalLocalResidualForCellCenter();
                };

                // derive the residuals numerically
                const auto& paramGroup = this->assembler().problem(domainJ).paramGroup();
                static const int numDiffMethod = getParamFromGroup<int>(paramGroup, "Assembly.NumericDifferenceMethod");
                static const auto epsCoupl = this->couplingManager().numericEpsilon(domainJ, paramGroup);
                NumericDifferentiation::partialDerivative(evalResidual, facePriVars[pvIdx], partialDeriv, origResidual,
                                                          epsCoupl(facePriVars[pvIdx], pvIdx), numDiffMethod);

                // update the global jacobian matrix with the current partial derivatives
                updateGlobalJacobian_(A, cellCenterGlobalI, globalJ, pvIdx, partialDeriv);

                // restore the original faceVars
                faceVars = origFaceVars;
            }
        }

    }

    template<std::size_t otherId, class JacobianBlock, class GridVariables>
    void assembleJacobianCellCenterCoupling(Dune::index_constant<otherId> domainJ, JacobianBlock& A,
                                            const CellCenterResidualValue& res, GridVariables& gridVariables)
    {
        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Calculate derivatives of all dofs in the element with respect to all dofs in the coupling stencil. //
        ////////////////////////////////////////////////////////////////////////////////////////////////////////

        // get some aliases for convenience
        const auto& element = this->element();

        // get stencil informations
        const auto& stencil = this->couplingManager().couplingStencil(domainI, element, domainJ);

        if(stencil.empty())
            return;

        for (const auto globalJ : stencil)
        {
            const auto origResidual = this->couplingManager().evalCouplingResidual(domainI, *this, domainJ, globalJ);
            const auto& curSol = this->curSol()[domainJ];
            const auto origPriVarsJ = curSol[globalJ];

            for (int pvIdx = 0; pvIdx < JacobianBlock::block_type::cols; ++pvIdx)
            {
                auto evalCouplingResidual = [&](Scalar priVar)
                {
                    auto deflectedPriVarsJ = origPriVarsJ;
                    deflectedPriVarsJ[pvIdx] = priVar;
                    this->couplingManager().updateCouplingContext(domainI, *this, domainJ, globalJ, deflectedPriVarsJ, pvIdx);
                    //TODO make sure everything that need to be deflected is deflected for adaptive
                    return this->couplingManager().evalCouplingResidual(domainI, *this, domainJ, globalJ);
                };

                CellCenterResidualValue partialDeriv(0.0);

                // derive the residuals numerically
                const auto& paramGroup = this->assembler().problem(domainJ).paramGroup();
                static const int numDiffMethod = getParamFromGroup<int>(paramGroup, "Assembly.NumericDifferenceMethod");
                static const auto epsCoupl = this->couplingManager().numericEpsilon(domainJ, paramGroup);
                NumericDifferentiation::partialDerivative(evalCouplingResidual, origPriVarsJ[pvIdx], partialDeriv, origResidual,
                                                          epsCoupl(origPriVarsJ[pvIdx], pvIdx), numDiffMethod);

                // update the global stiffness matrix with the current partial derivatives
                const auto cellCenterGlobalI = this->problem().gridGeometry().elementMapper().index(element);
                updateGlobalJacobian_(A, cellCenterGlobalI, globalJ, pvIdx, partialDeriv);

                // restore the undeflected state of the coupling context
                this->couplingManager().updateCouplingContext(domainI, *this, domainJ, globalJ, origPriVarsJ, pvIdx);
            }
        }
    }

    /*!
     * \brief Computes the derivatives with respect to the given element and adds them
     *        to the global matrix.
     *
     * \return The element residual at the current solution.
     */
    template<class JacobianBlock, class ElementResidualVector, class GridVariables>
    void assembleJacobianFaceCoupling(Dune::index_constant<cellCenterId> domainJ, JacobianBlock& A,
                                      const ElementResidualVector& origResiduals, GridVariables& gridVariables)
    {
        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Calculate derivatives of all dofs in the element with respect to all dofs in the coupling stencil. //
        ////////////////////////////////////////////////////////////////////////////////////////////////////////

        // get some aliases for convenience
        const auto& problem = this->problem();
        const auto& fvGeometry = this->fvGeometry();
        const auto& gridGeometry = this->problem().gridGeometry();
        const auto& connectivityMap = gridGeometry.connectivityMap();
        const auto& fluxCorrectionGeometry = gridGeometry.fluxCorrectionGeometry();
        FaceResidualValue partialDeriv;

        // build derivatives with for cell center dofs w.r.t. cell center dofs
        for(auto&& scvf : scvfs(fvGeometry))
        {
            // build derivatives with for face dofs w.r.t. cell center dofs
            for(const auto& globalJ : connectivityMap(faceId, cellCenterId, scvf.index()))
            {
                for(int pvIdx = 0; pvIdx < numEqCellCenter; ++pvIdx)
                {
                    using PrimaryVariables = typename VolumeVariables::PrimaryVariables;
                    const auto& cellCenterPriVars = this->curSol()[cellCenterId][globalJ];
                    auto CCPriVars = this->curSol()[cellCenterId][globalJ];
                    PrimaryVariables priVars = makePriVarsFromCellCenterPriVars<PrimaryVariables>(cellCenterPriVars);

                    constexpr auto offset = PrimaryVariables::dimension - CellCenterPrimaryVariables::dimension;

                    partialDeriv = 0;

                    // set the actual dof index
                    const auto faceGlobalI = scvf.dofIndex();

                    auto evalResidual = [&](Scalar priVar)
                    {
                        // update the volume variables and compute element residual
                        // get the volVars of the element with respect to which we are going to build the derivative
                        auto&& scvJ = fvGeometry.scv(globalJ);

                        auto& curVolVars = this->getVolVarAccess(gridVariables.curGridVolVars(), this->curElemVolVars(), scvJ);
                        priVars[pvIdx + offset] = priVar;
                        auto elemSol = elementSolution<FVElementGeometry>(std::move(priVars));
                        const auto elementJ = fvGeometry.gridGeometry().element(globalJ);

                        using ElementSolution = std::decay_t<decltype(elemSol)>;
                        using Element = std::decay_t<decltype(elementJ)>;
                        using SubControlVolume = std::decay_t<decltype(scvJ)>;
                        std::vector<ElementSolution> elemSols;
                        elemSols.push_back(elemSol);
                        std::vector<Element> elementJs;
                        elementJs.push_back(elementJ);
                        std::vector<SubControlVolume> scvJs;
                        scvJs.push_back(scvJ);
                        std::vector<Scalar> interpolationFactors;
                        interpolationFactors.push_back(1.);

                        const auto& firstOppoFace = fluxCorrectionGeometry.firstOpposingFace(scvf.index());
                        const auto& secondOppoFace = fluxCorrectionGeometry.secondOpposingFace(scvf.index());
                        const auto& firstNeighFace = fluxCorrectionGeometry.firstNeighborFace(scvf.index());
                        const auto& secondNeighFace = fluxCorrectionGeometry.secondNeighborFace(scvf.index());
                        const auto& thirdNeighFace = fluxCorrectionGeometry.thirdNeighborFace(scvf.index());
                        const auto& forthNeighFace = fluxCorrectionGeometry.forthNeighborFace(scvf.index());

                        auto& curFaceVolVars = this->getVolVarAccess(gridVariables.curGridVolVars(), this->curElemVolVars(), scvf, gridGeometry);//purposely took scvf as the attached scv is the only one with respect to which a derivative has to be made, if I take random scvfs corresponding to the scvJs, the curElemVolVars get updated for the stuff that is not called by evalLocalResidualForFace
                        auto& firstOppoCurFaceVolVars = this->getVolVarAccess(gridVariables.curGridVolVars(), this->curElemVolVars(), firstOppoFace, gridGeometry);
                        auto& secondOppoCurFaceVolVars = this->getVolVarAccess(gridVariables.curGridVolVars(), this->curElemVolVars(), secondOppoFace, gridGeometry);
                        auto& firstNeighCurFaceVolVars = this->getVolVarAccess(gridVariables.curGridVolVars(), this->curElemVolVars(), firstNeighFace, gridGeometry);
                        auto& secondNeighCurFaceVolVars = this->getVolVarAccess(gridVariables.curGridVolVars(), this->curElemVolVars(), secondNeighFace, gridGeometry);
                        auto& thirdNeighCurFaceVolVars = this->getVolVarAccess(gridVariables.curGridVolVars(), this->curElemVolVars(), thirdNeighFace, gridGeometry);
                        auto& forthNeighCurFaceVolVars = this->getVolVarAccess(gridVariables.curGridVolVars(), this->curElemVolVars(), forthNeighFace, gridGeometry);

                        const auto origVolVars(curVolVars);
                        const auto origFaceVolVars(curFaceVolVars);
                        const auto firstOppoOrigFaceVolVars(firstOppoCurFaceVolVars);
                        const auto secondOppoOrigFaceVolVars(secondOppoCurFaceVolVars);
                        const auto firstNeighOrigFaceVolVars(firstNeighCurFaceVolVars);
                        const auto secondNeighOrigFaceVolVars(secondNeighCurFaceVolVars);
                        const auto thirdNeighOrigFaceVolVars(thirdNeighCurFaceVolVars);
                        const auto forthNeighOrigFaceVolVars(forthNeighCurFaceVolVars);

                        CCPriVars[pvIdx] = priVar;

                        curVolVars.adaptiveUpdate(elemSols, problem, elementJs, scvJs, interpolationFactors);
                        dualCVsVolVarsUpdate_(curFaceVolVars, this->curSol(), scvf, globalJ, problem, CCPriVars);
                        dualCVsVolVarsUpdate_(firstOppoCurFaceVolVars, this->curSol(), firstOppoFace, globalJ, problem, CCPriVars);
                        dualCVsVolVarsUpdate_(secondOppoCurFaceVolVars, this->curSol(), secondOppoFace, globalJ, problem, CCPriVars);
                        dualCVsVolVarsUpdate_(firstNeighCurFaceVolVars, this->curSol(), firstNeighFace, globalJ, problem, CCPriVars);
                        dualCVsVolVarsUpdate_(secondNeighCurFaceVolVars, this->curSol(), secondNeighFace, globalJ, problem, CCPriVars);
                        dualCVsVolVarsUpdate_(thirdNeighCurFaceVolVars, this->curSol(), thirdNeighFace, globalJ, problem, CCPriVars);
                        dualCVsVolVarsUpdate_(forthNeighCurFaceVolVars, this->curSol(), forthNeighFace, globalJ, problem, CCPriVars);

                        auto retVal = this->evalLocalResidualForFace(scvf, false);

                        // restore the original volVars
                        curVolVars = origVolVars;
                        curFaceVolVars = origFaceVolVars;
                        firstOppoCurFaceVolVars = firstOppoOrigFaceVolVars;
                        secondOppoCurFaceVolVars = secondOppoOrigFaceVolVars;
                        firstNeighCurFaceVolVars = firstNeighOrigFaceVolVars;
                        secondNeighCurFaceVolVars = secondNeighOrigFaceVolVars;
                        thirdNeighCurFaceVolVars = thirdNeighOrigFaceVolVars;
                        forthNeighCurFaceVolVars = forthNeighOrigFaceVolVars;

                        return retVal;
                    };

                    // derive the residuals numerically
                    const auto& paramGroup = this->assembler().problem(domainJ).paramGroup();
                    static const int numDiffMethod = getParamFromGroup<int>(paramGroup, "Assembly.NumericDifferenceMethod");
                    static const auto epsCoupl = this->couplingManager().numericEpsilon(domainJ, paramGroup);

                    NumericDifferentiation::partialDerivative(evalResidual, priVars[pvIdx + offset], partialDeriv, origResiduals[scvf.localFaceIdx()],
                                                              epsCoupl(priVars[pvIdx + offset], pvIdx), numDiffMethod);

                    // update the global jacobian matrix with the current partial derivatives
                    updateGlobalJacobian_(A, faceGlobalI, globalJ, pvIdx, partialDeriv);
                }
            }
        }

    }

    template<std::size_t otherId, class JacobianBlock, class ElementResidualVector, class GridVariables>
    void assembleJacobianFaceCoupling(Dune::index_constant<otherId> domainJ, JacobianBlock& A,
                                      const ElementResidualVector& res, GridVariables& gridVariables)
    {
        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Calculate derivatives of all dofs in the element with respect to all dofs in the coupling stencil. //
        ////////////////////////////////////////////////////////////////////////////////////////////////////////

        // get some aliases for convenience
        const auto& fvGeometry = this->fvGeometry();
        const auto& curSol = this->curSol()[domainJ];
        FaceResidualValue partialDeriv;

        // build derivatives with for cell center dofs w.r.t. cell center dofs
        for(auto&& scvf : scvfs(fvGeometry))
        {

            // set the actual dof index
            const auto faceGlobalI = scvf.dofIndex();

            // get stencil informations
            const auto& stencil = this->couplingManager().couplingStencil(domainI, scvf, domainJ);

            if(stencil.empty())
                continue;

            // build derivatives with for face dofs w.r.t. cell center dofs
            for(const auto& globalJ : stencil)
            {
                const auto origPriVarsJ = curSol[globalJ];
                const auto origResidual = this->couplingManager().evalCouplingResidual(domainI, scvf, *this, domainJ, globalJ);

                for (int pvIdx = 0; pvIdx < JacobianBlock::block_type::cols; ++pvIdx)
                {
                    auto evalCouplingResidual = [&](Scalar priVar)
                    {
                        auto deflectedPriVars = origPriVarsJ;
                        deflectedPriVars[pvIdx] = priVar;
                        this->couplingManager().updateCouplingContext(domainI, *this, domainJ, globalJ, deflectedPriVars, pvIdx);
                        //TODO make sure everything that need to be deflected is deflected for adaptive
                        return this->couplingManager().evalCouplingResidual(domainI, scvf, *this, domainJ, globalJ);
                    };

                    // derive the residuals numerically
                    FaceResidualValue partialDeriv(0.0);
                    const auto& paramGroup = this->assembler().problem(domainJ).paramGroup();
                    static const int numDiffMethod = getParamFromGroup<int>(paramGroup, "Assembly.NumericDifferenceMethod");
                    static const auto epsCoupl = this->couplingManager().numericEpsilon(domainJ, paramGroup);
                    NumericDifferentiation::partialDerivative(evalCouplingResidual, origPriVarsJ[pvIdx], partialDeriv, origResidual,
                                                              epsCoupl(origPriVarsJ[pvIdx], pvIdx), numDiffMethod);

                    // update the global stiffness matrix with the current partial derivatives
                    updateGlobalJacobian_(A, faceGlobalI, globalJ, pvIdx, partialDeriv);

                    // restore the undeflected state of the coupling context
                    this->couplingManager().updateCouplingContext(domainI, *this, domainJ, globalJ, origPriVarsJ, pvIdx);
                }
            }
        }


    }

    template<class JacobianMatrixDiagBlock, class GridVariables>
    void evalAdditionalDerivatives(const std::vector<std::size_t>& additionalDofDependencies,
                                   JacobianMatrixDiagBlock& A, GridVariables& gridVariables)
    {

    }

        /*!
     * \brief Updates the current global Jacobian matrix with the
     *        partial derivatives of all equations in regard to the
     *        primary variable 'pvIdx' at dof 'col'. Specialization for cc methods.
     */
    template<class SubMatrix, class CCOrFacePrimaryVariables>
    static void updateGlobalJacobian_(SubMatrix& matrix,
                                      const int globalI,
                                      const int globalJ,
                                      const int pvIdx,
                                      const CCOrFacePrimaryVariables &partialDeriv)
    {
        for (int eqIdx = 0; eqIdx < partialDeriv.size(); eqIdx++)
        {
            // A[i][col][eqIdx][pvIdx] is the rate of change of
            // the residual of equation 'eqIdx' at dof 'i'
            // depending on the primary variable 'pvIdx' at dof
            // 'col'.
            assert(pvIdx >= 0);
            assert(eqIdx < matrix[globalI][globalJ].size());
            assert(pvIdx < matrix[globalI][globalJ][eqIdx].size());
            matrix[globalI][globalJ][eqIdx][pvIdx] += partialDeriv[eqIdx];
        }
    }

private:
    template<class FaceVariables, class SubControlVolumeFace, class SolutionVector, class GlobalIndexType, class IndexType, class PrimaryVariables, class Problem, class Element>
    void faceVarsUpdate_(FaceVariables& toBeUpdatedFaceVars,
                         const SubControlVolumeFace& face,
                         const SolutionVector& curSol,
                         GlobalIndexType globalJ,
                         IndexType pvIdx,
                         const PrimaryVariables& priVar,
                         const Problem& problem,
                         const Element& element)
    {
        const auto& gridGeometry = problem.gridGeometry();
        using FaceSolution = GetPropType<TypeTag, Properties::StaggeredFaceSolution>;
        FVElementGeometry fvGeometry(gridGeometry);
        fvGeometry.bind(element);

        const auto origFaceSolution = FaceSolution(face, curSol, gridGeometry);
        auto faceSolution = origFaceSolution;
        if(faceSolution.exists(globalJ))
            faceSolution[globalJ][pvIdx] = priVar;
        toBeUpdatedFaceVars.update(faceSolution, problem, element, fvGeometry, face);
    }

    template<class DualCVsVolVars, class SolutionVector, class SubControlVolumeFace, class IndexType, class Problem, class CellCenterPrimaryVariables>
    void dualCVsVolVarsUpdate_(DualCVsVolVars& toBeUpdatedDualCVsVolVars,
                            const SolutionVector& curSol,
                            const SubControlVolumeFace& face,
                            IndexType globalJ,
                            const Problem& problem,
                            const CellCenterPrimaryVariables& CCPriVars)
    {
        if (face.insideScvIdx() != face.volVarsData().innerVolVarsDofs[0])
        {
            DUNE_THROW(Dune::InvalidStateException, "Expected face.insideScvIdx() = face.volVarsData().innerVolVarsDofs[0].");
        }

        std::vector<CellCenterPrimaryVariables> insideCCPriVarsVector;
        for (unsigned int i = 0; i < face.volVarsData().innerVolVarsDofs.size(); ++i)
        {
            const auto scvDof = face.volVarsData().innerVolVarsDofs[i];

            if(scvDof == globalJ)
            {
                insideCCPriVarsVector.push_back(CCPriVars);
            }
            else if (scvDof < 0)
            {
                const auto& scvfBuildingData = face.volVarsData().innerVolVarsScvfBuildingData[i];
                SubControlVolumeFace boundaryFace = problem.gridGeometry().scvf(scvfBuildingData.eIdx, scvfBuildingData.localScvfIdx);
                insideCCPriVarsVector.push_back(curSol[cellCenterId][boundaryFace.insideScvIdx()]);
            }
            else
            {
                insideCCPriVarsVector.push_back(curSol[cellCenterId][scvDof]);
            }
        }

        std::array<std::vector<CellCenterPrimaryVariables>, SubControlVolumeFace::numPairs> normalFaceOutsideCCPriVars;
        for (unsigned int localSubFaceIdx = 0; localSubFaceIdx < SubControlVolumeFace::numPairs; ++localSubFaceIdx)
        {
            for (unsigned int i = 0; i < face.volVarsData().normalVolVarsDofs[localSubFaceIdx].size(); ++i)
            {
                const auto scvDof = face.volVarsData().normalVolVarsDofs[localSubFaceIdx][i];

                if(scvDof == globalJ)
                {
                    normalFaceOutsideCCPriVars[localSubFaceIdx].push_back(CCPriVars);
                }
                else if (scvDof < 0)
                {
                    const auto& scvfBuildingData = face.volVarsData().normalVolVarsScvfBuildingData[localSubFaceIdx][i];
                    SubControlVolumeFace boundaryFace = problem.gridGeometry().scvf(scvfBuildingData.eIdx, scvfBuildingData.localScvfIdx);
                    normalFaceOutsideCCPriVars[localSubFaceIdx].push_back(curSol[cellCenterId][boundaryFace.insideScvIdx()]);
                }
                else
                {
                    normalFaceOutsideCCPriVars[localSubFaceIdx].push_back(curSol[cellCenterId][scvDof]);
                }
            }
        }

        toBeUpdatedDualCVsVolVars.update(problem.gridGeometry(), insideCCPriVarsVector, normalFaceOutsideCCPriVars, face, problem);
    }

    template<class PrimalCVsVolVars, class SolutionVector, class Problem, class FVElementGeometry, class CellCenterPrimaryVariables>
    void primalCVsVolVarsUpdate_(PrimalCVsVolVars& toBeUpdatedPrimalCVsVolVars,
                            const SolutionVector& curSol,
                            std::size_t globalJ,
                            const Problem& problem,
                            const FVElementGeometry& fvGeometry,
                            const CellCenterPrimaryVariables& CCPriVars)
    {
        for (auto&& face : scvfs(fvGeometry))
        {
            if (face.insideScvIdx() != face.volVarsData().innerVolVarsDofs[0])
            {
                DUNE_THROW(Dune::InvalidStateException, "Expected face.insideScvIdx() = face.volVarsData().innerVolVarsDofs[0].");
            }

            std::vector<CellCenterPrimaryVariables> insideCCPriVarsVector;
            for (unsigned int i = 0; i < face.volVarsData().innerVolVarsDofs.size(); ++i)
            {
                const auto scvDof = face.volVarsData().innerVolVarsDofs[i];

                if(scvDof == globalJ)
                {
                    insideCCPriVarsVector.push_back(CCPriVars);
                }
                else if (scvDof < 0)
                {
                    const auto& scvfBuildingData = face.volVarsData().innerVolVarsScvfBuildingData[i];
                    SubControlVolumeFace boundaryFace = fvGeometry.gridGeometry().scvf(scvfBuildingData.eIdx, scvfBuildingData.localScvfIdx);
                    insideCCPriVarsVector.push_back(curSol[cellCenterId][boundaryFace.insideScvIdx()]);
                }
                else
                {
                    insideCCPriVarsVector.push_back(curSol[cellCenterId][scvDof]);
                }
            }

            std::vector<CellCenterPrimaryVariables> outsideCCPriVarsVector;
            for (unsigned int i = 0; i < face.volVarsData().outerVolVarsDofs.size(); ++i)
            {
                const auto scvDof = face.volVarsData().outerVolVarsDofs[i];

                if(scvDof == globalJ)
                {
                    outsideCCPriVarsVector.push_back(CCPriVars);
                }
                else if (scvDof < 0)
                {
                    const auto& scvfBuildingData = face.volVarsData().outerVolVarsScvfBuildingData[i];
                    SubControlVolumeFace boundaryFace = fvGeometry.gridGeometry().scvf(scvfBuildingData.eIdx, scvfBuildingData.localScvfIdx);
                    outsideCCPriVarsVector.push_back(curSol[cellCenterId][boundaryFace.insideScvIdx()]);
                }
                else
                {
                    outsideCCPriVarsVector.push_back(curSol[cellCenterId][scvDof]);
                }
            }

            updatePrimal(toBeUpdatedPrimalCVsVolVars[face.localFaceIdx()], problem.gridGeometry(), insideCCPriVarsVector, outsideCCPriVarsVector, face, problem);
        }
    }

    template<class T = TypeTag>
    static typename std::enable_if<!getPropValue<T, Properties::EnableGridFaceVariablesCache>(), FaceVariables&>::type
    getFaceVarAccess(GridFaceVariables& gridFaceVariables, ElementFaceVariables& elemFaceVars, const SubControlVolumeFace& scvf)
    { return elemFaceVars[scvf]; }

    template<class T = TypeTag>
    static typename std::enable_if<getPropValue<T, Properties::EnableGridFaceVariablesCache>(), FaceVariables&>::type
    getFaceVarAccess(GridFaceVariables& gridFaceVariables, ElementFaceVariables& elemFaceVars, const SubControlVolumeFace& scvf)
    { return gridFaceVariables.faceVars(scvf.index()); }

    using ParentType::getVolVarAccess;

    template<class T = TypeTag, typename std::enable_if_t<!getPropValue<T, Properties::EnableGridVolumeVariablesCache>(), int> = 0>
    DualCVsVolVars& getVolVarAccess(GridVolumeVariables& gridVolVars, ElementVolumeVariables& elemVolVars, const SubControlVolumeFace& scvf, const GridGeometry& gridGeometry)
    { return elemVolVars[scvf]; }

    template<class T = TypeTag, typename std::enable_if_t<getPropValue<T, Properties::EnableGridVolumeVariablesCache>(), int> = 0>
    DualCVsVolVars& getVolVarAccess(GridVolumeVariables& gridVolVars, ElementVolumeVariables& elemVolVars, const SubControlVolumeFace& scvf, const GridGeometry& gridGeometry)
    { return gridVolVars.volVars(scvf); }

    template<class T = TypeTag, typename std::enable_if_t<!getPropValue<T, Properties::EnableGridVolumeVariablesCache>(), int> = 0>
    PrimalCVsVolVars& getVolVarAccess(GridVolumeVariables& gridVolVars, ElementVolumeVariables& elemVolVars, unsigned int scvIdx)
    { return elemVolVars[scvIdx]; }

    template<class T = TypeTag, typename std::enable_if_t<getPropValue<T, Properties::EnableGridVolumeVariablesCache>(), int> = 0>
    PrimalCVsVolVars& getVolVarAccess(GridVolumeVariables& gridVolVars, ElementVolumeVariables& elemVolVars, unsigned int  scvIdx)
    { return gridVolVars.volVars(scvIdx); }
};

} // end namespace Dumux

#endif
