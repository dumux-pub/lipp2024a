// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Flux variables cache for 1p PNM
 */
#ifndef DUMUX_PNM_1P_FLUXVARIABLESCACHE_HH
#define DUMUX_PNM_1P_FLUXVARIABLESCACHE_HH

namespace Dumux
{

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//! The cache is dependent on the active physical processes (advection, diffusion, heat conduction)
//! For each type of process there is a base cache storing the data required to compute the respective fluxes
//! Specializations of the overall cache are provided for combinations of processes
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*!
 * \ingroup ImplicitModel
 * \brief The flux variables cache classes for porous media.
 *        Store data required for flux calculation. For each type of physical process (advection, diffusion, heat conduction)
 *        there is a base cache storing the data required to compute the respective fluxes. Specializations of the overall
 *        cache class are provided for different combinations of processes.
 */

//! We only store discretization-related quantities for the box method.
//! Thus, we need no physics-dependent specialization.
template<class Scalar>
class PNMOnePFluxVariablesCache
{
public:

    template<class Problem, class Element, class FVElementGeometry,
             class ElementVolumeVariables, class SubControlVolumeFace>
    void update(const Problem& problem,
                const Element& element,
                const FVElementGeometry& fvGeometry,
                const ElementVolumeVariables& elemVolVars,
                const SubControlVolumeFace& scvf)
    {
        throatCrossSectionalArea_ = problem.spatialParams().throatCrossSectionalArea(element, elemVolVars);
        throatLength_ = problem.spatialParams().throatLength(element, elemVolVars);
        throatRadius_ = problem.spatialParams().throatRadius(element, elemVolVars);
        aspectRatio_ = problem.spatialParams().throatAspectRatio(element, elemVolVars);
        transmissibility_ = problem.spatialParams().transmissibility(element, fvGeometry, scvf, elemVolVars, *this, /*phaseIdx*/0);
    }

    Scalar transmissibility(const int phaseIdx = 0) const
    { return transmissibility_; }

    Scalar throatCrossSectionalArea(const int phaseIdx = 0) const
    { return throatCrossSectionalArea_; }

    Scalar throatLength() const
    { return throatLength_; }

    Scalar throatRadius() const
    { return throatRadius_; }

    Scalar throatAspectRatio() const
    { return aspectRatio_; }

private:
    Scalar transmissibility_;
    Scalar throatCrossSectionalArea_;
    Scalar throatLength_;
    Scalar throatRadius_;
    Scalar aspectRatio_;
};

} // end namespace

#endif
