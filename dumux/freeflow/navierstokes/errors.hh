// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \copydoc Dumux::NavierStokesTestError
 */
#ifndef DUMUX_TEST_FREEFLOW_NAVIERSTOKES_REFINED_ERRORS_HH
#define DUMUX_TEST_FREEFLOW_NAVIERSTOKES_REFINED_ERRORS_HH

#include <vector>
#include <cmath>
#include <dumux/discretization/extrusion.hh>
#include <dumux/io/format.hh>
#include <dumux/freeflow/navierstokes/derivatives.hh>

namespace Dumux {

/*!
 * \brief Compute errors between an analytical solution and the numerical approximation
 */
template<class Problem, class Scalar = double>
class NavierStokesRefinedErrors
{
    using GridGeometry = std::decay_t<decltype(std::declval<Problem>().gridGeometry())>;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using Extrusion = Extrusion_t<GridGeometry>;
    using Indices = typename Problem::Indices;
    using Element = typename GridGeometry::LocalView::Element;
    using PrimaryVariables = std::decay_t<decltype(
        std::declval<Problem>().dirichlet(std::declval<Element>(), std::declval<SubControlVolume>())
    )>;

    static constexpr int dim = GridGeometry::GridView::dimension;

public:
    NavierStokesRefinedErrors(std::shared_ptr<const Problem> problem)
    : problem_(problem)
    { }

    void setCoarseVelocityDiscretization(bool coarseVelocityDiscretization)
    {
        coarseVelocityDiscretization_ = coarseVelocityDiscretization;
    }

    /*!
     * \brief Computes errors between an analytical solution and the numerical approximation
     *
     * \param curSol The current solution vector
     * \param time The current time
     */
    template<class SolutionVector, class CCSolutionVector, class FaceSolutionVector>
    void calc(const SolutionVector& curSol,
                const CCSolutionVector& ccResidualWithAnalyticalSol,
                const FaceSolutionVector& faceResidualWithAnalyticalSol,
                Scalar time,
                unsigned int timeIndex)
    { calculateErrors_(curSol,
        ccResidualWithAnalyticalSol,
        faceResidualWithAnalyticalSol,
        [&](const SubControlVolume& scv) { return problem_->instationaryAnalyticalPressureSolutionAtPos(scv, time); },
        [&](const SubControlVolumeFace& scvf) { return problem_->instationaryAnalyticalVelocitySolutionAtPos(scvf, time);},
        time,
        timeIndex); }

    /*!
     * \brief Computes errors between an analytical solution and the numerical approximation
     *
     * \param curSol The current solution vector
     */
    template<class SolutionVector, class CCSolutionVector, class FaceSolutionVector>
    void calc(const SolutionVector& curSol,
                const CCSolutionVector& ccResidualWithAnalyticalSol,
                const FaceSolutionVector& faceResidualWithAnalyticalSol,
                unsigned int refinementStep = 0)
    { calculateErrors_(curSol,
        ccResidualWithAnalyticalSol,
        faceResidualWithAnalyticalSol,
        [&](const SubControlVolume& scv) { return problem_->analyticalPressureSolutionAtPos(scv); },
        [&](const SubControlVolumeFace& scvf) { return problem_->analyticalVelocitySolutionAtPos(scvf);},
        0.0,
        refinementStep); }

    //! The relative discrete l1 error (relative to the discrete l1 norm of the reference solution)
    const PrimaryVariables& l1SolRelative() const { return l1SolRelative_; }

    //! The relative discrete l2 error (relative to the discrete l2 norm of the reference solution)
    const PrimaryVariables& l2SolRelative() const { return l2SolRelative_; }

    //! The relative discrete l-infinity error (relative to the discrete loo norm of the reference solution)
    const PrimaryVariables& lInfSolRelative() const { return lInfSolRelative_; }

    //! The (absolute) discrete l1 error
    const PrimaryVariables& l1SolAbsolute() const { return l1SolAbsolute_; }

    //! The (absolute) discrete l2 error
    const PrimaryVariables& l2SolAbsolute() const { return l2SolAbsolute_; }

    //! The (absolute) discrete l-infinity error
    const PrimaryVariables& lInfSolAbsolute() const { return lInfSolAbsolute_; }

    const PrimaryVariables& l1TruncErrMom() const { return l1TruncErrMom_; }
    const PrimaryVariables& l2TruncErrMom() const { return l2TruncErrMom_; }
    const PrimaryVariables& lInfTruncErrMom() const { return lInfTruncErrMom_; }

    //! The relative discrete l2 error for the du/dx derivative
    const Scalar& l2DxURelative() const { return l2DxURelative_; }

    //! The relative discrete l2 error for the dv/dy derivative
    const Scalar& l2DyVRelative() const { return l2DyVRelative_; }

    //! The absolute discrete l2 error for the divergence of velocity
    const Scalar& l2DivVelAbsolute() const { return l2DivVelAbsolute_; }

    //! Time corresponding to the error (returns 0 per default)
    Scalar time() const { return time_; }
    unsigned int vtuSuffix() const { return vtuSuffix_; }

private:
    template<class SolutionVector, class CCSolutionVector, class FaceSolutionVector, class LambdaA, class LambdaB>
    void calculateErrors_(const SolutionVector& curSol,
                const CCSolutionVector& ccResidualWithAnalyticalSol,
                const FaceSolutionVector& faceResidualWithAnalyticalSol,
                const LambdaA& instationaryAnalyticalPressureSolutionAtPosLambdaFunction,
                const LambdaB& instationaryAnalyticalVelocitySolutionAtPosLambdaFunction,
                Scalar time,
                unsigned int timeIndexOrRefinementStep)
    {
        // store time information
        time_ = time;
        vtuSuffix_ = timeIndexOrRefinementStep+1;

        // calculate helping variables
        PrimaryVariables sumCellWeightedSolError(0.0);
        PrimaryVariables sumCellWeightedSolReference(0.0);

        PrimaryVariables sumSquaredCellWeightedSolError(0.0);
        PrimaryVariables sumSquaredCellWeightedSolReference(0.0);

        PrimaryVariables maxSolError(0.0);
        PrimaryVariables maxSolReference(0.0);

        Scalar sumSquaredCellWeightedDxUError = 0.;
        Scalar sumSquaredCellWeightedDxUReference = 0.;

        Scalar sumSquaredCellWeightedDyVError = 0.;
        Scalar sumSquaredCellWeightedDyVReference = 0.;

        Scalar sumSquaredCellWeightedDivError = 0.;

        PrimaryVariables sumCellWeightedTruncErr(0.0);
        PrimaryVariables sumSquaredCellWeightedTruncErr(0.0);
        PrimaryVariables maxTruncErr(0.0);

        PrimaryVariables totalVolume(0.0);

        auto fvGeometry = localView(problem_->gridGeometry());
        for (const auto& element : elements(problem_->gridGeometry().gridView()))
        {
            fvGeometry.bindElement(element);

            for (const auto& scv : scvs(fvGeometry))
            {
                totalVolume[Indices::pressureIdx] += Extrusion::volume(scv);

                //primal-grid based
                addToSumsQuantityPressure_(scv,
                                           curSol,
                                            instationaryAnalyticalPressureSolutionAtPosLambdaFunction,
                                            sumCellWeightedSolError[Indices::pressureIdx],
                                            sumCellWeightedSolReference[Indices::pressureIdx],
                                            sumSquaredCellWeightedSolError[Indices::pressureIdx],
                                            sumSquaredCellWeightedSolReference[Indices::pressureIdx],
                                            maxSolError[Indices::pressureIdx],
                                            maxSolReference[Indices::pressureIdx]);

                addToSumsQuantityLocalTruncErrMass_(scv,
                                            ccResidualWithAnalyticalSol,
                                            sumCellWeightedTruncErr[Indices::pressureIdx],
                                            sumSquaredCellWeightedTruncErr[Indices::pressureIdx],
                                            maxTruncErr[Indices::pressureIdx]);

                addToSumsQuantitiesGradientsAndDivergence_(fvGeometry,
                                                           scv,
                                                           curSol,
                                                            instationaryAnalyticalVelocitySolutionAtPosLambdaFunction,
                                                            sumSquaredCellWeightedDxUError,
                                                            sumSquaredCellWeightedDxUReference,
                                                            sumSquaredCellWeightedDyVError,
                                                            sumSquaredCellWeightedDyVReference,
                                                            sumSquaredCellWeightedDivError);

                for (const auto& scvf : scvfs(fvGeometry))
                {
                    // dual-grid based
                    addToSumsQuantityVelocity_(element,
                                               scvf,
                                                curSol,
                                                instationaryAnalyticalVelocitySolutionAtPosLambdaFunction,
                                                sumCellWeightedSolError[Indices::velocity(scvf.directionIndex())],
                                                sumCellWeightedSolReference[Indices::velocity(scvf.directionIndex())],
                                                sumSquaredCellWeightedSolError[Indices::velocity(scvf.directionIndex())],
                                                sumSquaredCellWeightedSolReference[Indices::velocity(scvf.directionIndex())],
                                                maxSolError[Indices::velocity(scvf.directionIndex())],
                                                maxSolReference[Indices::velocity(scvf.directionIndex())],
                                                totalVolume[Indices::velocity(scvf.directionIndex())]);

                    addToSumsQuantityLocalTruncErrMom_(scvf,
                                                faceResidualWithAnalyticalSol,
                                                sumCellWeightedTruncErr[Indices::velocity(scvf.directionIndex())],
                                                sumSquaredCellWeightedTruncErr[Indices::velocity(scvf.directionIndex())],
                                                maxTruncErr[Indices::velocity(scvf.directionIndex())]);
                }
            }
        }

        // calculate errors
        for (int i = 0; i < PrimaryVariables::size(); ++i)
        {
            l1SolAbsolute_[i] = sumCellWeightedSolError[i] / totalVolume[i];
            l1SolRelative_[i] = sumCellWeightedSolError[i] / sumCellWeightedSolReference[i];

            l2SolAbsolute_[i] = std::sqrt(sumSquaredCellWeightedSolError[i] / totalVolume[i]);
            l2SolRelative_[i] = std::sqrt(sumSquaredCellWeightedSolError[i] / sumSquaredCellWeightedSolReference[i]);

            lInfSolAbsolute_[i] = maxSolError[i];
            lInfSolRelative_[i] = maxSolError[i] / maxSolReference[i];

            l1TruncErrMom_[i] = sumCellWeightedTruncErr[i] / totalVolume[i];
            l2TruncErrMom_[i] = std::sqrt(sumSquaredCellWeightedTruncErr[i] / totalVolume[i]);
            lInfTruncErrMom_[i] = maxTruncErr[i];
        }

        l2DxURelative_ = std::sqrt(sumSquaredCellWeightedDxUError / sumSquaredCellWeightedDxUReference);
        l2DyVRelative_ = std::sqrt(sumSquaredCellWeightedDyVError / sumSquaredCellWeightedDyVReference);
        l2DivVelAbsolute_ = std::sqrt(sumSquaredCellWeightedDivError);
    }

    template<class SolutionVector, class LambdaA>
    void addToSumsQuantityPressure_(const SubControlVolume& scv,
                                    const SolutionVector& curSol,
                                    const LambdaA& instationaryAnalyticalPressureSolutionAtPosLambdaFunction,
                                    Scalar& l1ErrSum,
                                    Scalar& l1RefSum,
                                    Scalar& l2ErrSum,
                                    Scalar& l2RefSum,
                                    Scalar& lInfErrSum,
                                    Scalar& lInfRefSum) const
    {
        const Scalar analyticalPCC = instationaryAnalyticalPressureSolutionAtPosLambdaFunction(scv);
        const Scalar numericalPCC = curSol[GridGeometry::cellCenterIdx()][scv.dofIndex()][Indices::pressureIdx - dim];

        const Scalar pError = absDiff_(analyticalPCC, numericalPCC);
        const Scalar pReference = absDiff_(analyticalPCC, 0.0);

        l1ErrSum += pError * Extrusion::volume(scv);
        l1RefSum += pReference * Extrusion::volume(scv);

        l2ErrSum += pError * pError * Extrusion::volume(scv);
        l2RefSum += pReference * pReference * Extrusion::volume(scv);

        lInfErrSum = std::max(lInfErrSum, pError);
        lInfRefSum = std::max(lInfRefSum, pReference);
    }

    template<class CCSolutionVector>
    void addToSumsQuantityLocalTruncErrMass_(const SubControlVolume& scv,
                                            const CCSolutionVector& ccResidualWithAnalyticalSol,
                                            Scalar& l1ErrSum,
                                            Scalar& l2ErrSum,
                                            Scalar& lInfErrSum)
    {
        const Scalar pError = std::abs(ccResidualWithAnalyticalSol[scv.dofIndex()]);

        l1ErrSum += pError * Extrusion::volume(scv);
        l2ErrSum += pError * pError * Extrusion::volume(scv);
        lInfErrSum = std::max(lInfErrSum, pError);
    }

    template<class SolutionVector, class LambdaB>
    void addToSumsQuantityVelocity_(const Element& element,
                                    const SubControlVolumeFace& scvf,
                                    const SolutionVector& curSol,
                                    const LambdaB& instationaryAnalyticalVelocitySolutionAtPosLambdaFunction,
                                    Scalar& l1ErrSum,
                                    Scalar& l1RefSum,
                                    Scalar& l2ErrSum,
                                    Scalar& l2RefSum,
                                    Scalar& lInfErrSum,
                                    Scalar& lInfRefSum,
                                    Scalar& totalVolume) const
    {
        Scalar staggeredHalfVolume = 0.5 * scvf.inside().geometry().volume();
        if (scvf.neighbor() && !coarseVelocityDiscretization_ && scvf.inside().level() < scvf.outside().level())
        {
            staggeredHalfVolume *= 0.5;
        }

        const Scalar analyticalSolutionFace = instationaryAnalyticalVelocitySolutionAtPosLambdaFunction(scvf);
        const Scalar numericalSolutionFace = curSol[GridGeometry::faceIdx()][scvf.dofIndex()][0];

        const Scalar velError = absDiff_(analyticalSolutionFace, numericalSolutionFace);
        const Scalar velReference = absDiff_(analyticalSolutionFace, 0.0);

        l1ErrSum += velError * staggeredHalfVolume;
        l1RefSum += velReference * staggeredHalfVolume;

        l2ErrSum += velError * velError * staggeredHalfVolume;
        l2RefSum += velReference * velReference * staggeredHalfVolume;

        lInfErrSum = std::max(lInfErrSum, velError);
        lInfRefSum = std::max(lInfRefSum, velReference);

        if (!(scvf.boundary() && problem_->boundaryTypes(element, scvf).isDirichlet(Indices::velocity(scvf.directionIndex()))))
            totalVolume += staggeredHalfVolume;
    }

    template<class FaceSolutionVector>
    void addToSumsQuantityLocalTruncErrMom_(const SubControlVolumeFace& scvf,
                                    const FaceSolutionVector& faceResidualWithAnalyticalSol,
                                    Scalar& l1ErrSum,
                                    Scalar& l2ErrSum,
                                    Scalar& lInfErrSum) const
    {
        Scalar staggeredHalfVolume = 0.5 * scvf.inside().geometry().volume();
        if (scvf.neighbor() && !coarseVelocityDiscretization_ && scvf.inside().level() < scvf.outside().level())
        {
            staggeredHalfVolume *= 0.5;
        }

        const Scalar velError = std::abs(faceResidualWithAnalyticalSol[scvf.dofIndex()]);

        l1ErrSum += velError * staggeredHalfVolume;
        l2ErrSum += velError * velError * staggeredHalfVolume;
        lInfErrSum = std::max(lInfErrSum, velError);
    }

    template<class FVElementGeometry, class SolutionVector, class LambdaB>
    void addToSumsQuantitiesGradientsAndDivergence_(const FVElementGeometry& fvGeometry,
                                                    const SubControlVolume& scv,
                                                    const SolutionVector& curSol,
                                                    const LambdaB& instationaryAnalyticalVelocitySolutionAtPosLambdaFunction,
                                                    Scalar& sumSquaredCellWeightedDxUError,
                                                    Scalar& sumSquaredCellWeightedDxUReference,
                                                    Scalar& sumSquaredCellWeightedDyVError,
                                                    Scalar& sumSquaredCellWeightedDyVReference,
                                                    Scalar& sumSquaredCellWeightedDivError) const
    {
        Derivatives<GridGeometry> derivatives;
        derivatives.update(fvGeometry, scv, curSol, instationaryAnalyticalVelocitySolutionAtPosLambdaFunction);

        // compute the cc based errors
        const Scalar dxuError = absDiff_(derivatives.getAnalyticalDxUCC(), derivatives.getNumericalDxUCC());//I have interpreted the error norms in [Boyer, Franck, and Pascal Omnes. "Benchmark Proposal for the FVCA8 Conference: Finite Volume Methods for the Stokes and Navier–Stokes Equations." International Conference on Finite Volumes for Complex Applications. Springer, Cham, 2017.] as grad(u_ex) rather than (grad u)_ex, Bernd agreed that this is (at least a possible) interpretation
        const Scalar dxUReference = absDiff_(derivatives.getAnalyticalDxUCC(), 0.0);

        sumSquaredCellWeightedDxUError += dxuError * dxuError * Extrusion::volume(scv);
        sumSquaredCellWeightedDxUReference += dxUReference * dxUReference * Extrusion::volume(scv);

        const Scalar dyvError = absDiff_(derivatives.getAnalyticalDyVCC(), derivatives.getNumericalDyVCC());
        const Scalar dyVReference = absDiff_(derivatives.getAnalyticalDyVCC(), 0.0);

        sumSquaredCellWeightedDyVError += dyvError * dyvError * Extrusion::volume(scv);
        sumSquaredCellWeightedDyVReference += dyVReference * dyVReference * Extrusion::volume(scv);

        const Scalar divVelError = absDiff_(derivatives.getAnalyticalDivVelCC(), derivatives.getNumericalDivVelCC());

        sumSquaredCellWeightedDivError += divVelError * divVelError * Extrusion::volume(scv);
    }

    template<class T>
    T absDiff_(const T& a, const T& b) const
    { using std::abs; return abs(a-b); }

    std::shared_ptr<const Problem> problem_;
    bool coarseVelocityDiscretization_ = false;

    PrimaryVariables l1SolRelative_;
    PrimaryVariables l2SolRelative_;
    PrimaryVariables lInfSolRelative_;

    PrimaryVariables l1SolAbsolute_;
    PrimaryVariables l2SolAbsolute_;
    PrimaryVariables lInfSolAbsolute_;

    PrimaryVariables l1TruncErrMom_;
    PrimaryVariables l2TruncErrMom_;
    PrimaryVariables lInfTruncErrMom_;

    // those quantities are inspired by [Boyer, Franck, and Pascal Omnes. "Benchmark Proposal for the FVCA8 Conference: Finite Volume Methods for the Stokes and Navier–Stokes Equations." International Conference on Finite Volumes for Complex Applications. Springer, Cham, 2017] errgu and errdivu
    Scalar l2DxURelative_;
    Scalar l2DyVRelative_;
    Scalar l2DivVelAbsolute_;

    Scalar time_;
    unsigned int vtuSuffix_;
};

template<class Problem, class SolutionVector>
NavierStokesRefinedErrors(std::shared_ptr<Problem>, SolutionVector&&)
-> NavierStokesRefinedErrors<Problem>;

template<class Problem, class SolutionVector>
NavierStokesRefinedErrors(std::shared_ptr<Problem>, SolutionVector&&, unsigned int)
-> NavierStokesRefinedErrors<Problem>;

template<class Problem, class SolutionVector, class Scalar>
NavierStokesRefinedErrors(std::shared_ptr<Problem>, SolutionVector&&, Scalar, unsigned int)
-> NavierStokesRefinedErrors<Problem, Scalar>;

/*!
 * \brief An error CSV file writer
 */
template<class Problem, class Scalar = double>
class NavierStokesRefinedErrorCSVWriter
{
    using GridGeometry = std::decay_t<decltype(std::declval<Problem>().gridGeometry())>;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using Indices = typename Problem::Indices;
    using Element = typename GridGeometry::LocalView::Element;
    using PrimaryVariables = std::decay_t<decltype(
        std::declval<Problem>().dirichlet(std::declval<Element>(), std::declval<SubControlVolume>())
    )>;

    static constexpr int dim = GridGeometry::GridView::dimension;

public:
    NavierStokesRefinedErrorCSVWriter(std::shared_ptr<const Problem> problem, const std::string& suffix = "")
    : problem_(problem),
      name_(problem->name() + (suffix.empty() ? "_error" : "_error_" + suffix))
    {
        // clear error file
        std::ofstream logFile(name_ + ".csv", std::ios::trunc);

        // write header
        logFile << "time,corresponding paraview files,cc dofs, face dofs, all dofs";
        using ErrorNames = std::vector<std::string>;

        for (const std::string e : { "L1Rel", "L2Rel", "LinfRel", "L1Abs", "L2Abs", "LinfAbs", "L1LocalTruncErr", "L2LocalTruncErr", "LinfLocalTruncErr" })
        {
            ErrorNames errorNames;
            errorNames.resize(PrimaryVariables::size());

            errorNames[Indices::pressureIdx] = e + "(p)";
            errorNames[Indices::velocity(0)] = e + "(u)";
            if (dim > 1)
                errorNames[Indices::velocity(1)] = e + "(v)";
            if (dim > 2)
                errorNames[Indices::velocity(2)] = e + "(w)";

            printPriVarError_(logFile, errorNames, "{:s}");
        }
        printScalarError_(logFile, "L2Rel(dxu)", "{:s}");
        printScalarError_(logFile, "L2Rel(dyv)", "{:s}");
        printScalarError_(logFile, "L2(divVel)", "{:s}");

        logFile << "\n";
    }

    void printErrors(const NavierStokesRefinedErrors<Problem>& errors) const
    {
        // append to error file
        std::ofstream logFile(name_ + ".csv", std::ios::app);

        const int numCCDofs = problem_->gridGeometry().numCellCenterDofs();
        const int numFaceDofs = problem_->gridGeometry().numFaceDofs();

        std::string vtuName = " , 0000" + std::to_string(errors.vtuSuffix()) + ".vtu, ";

        logFile << Fmt::format("{:.5e}{}{},{},{}", errors.time(), vtuName, numCCDofs, numFaceDofs, numCCDofs + numFaceDofs);

        printPriVarError_(logFile, errors.l1SolRelative());
        printPriVarError_(logFile, errors.l2SolRelative());
        printPriVarError_(logFile, errors.lInfSolRelative());

        printPriVarError_(logFile, errors.l1SolAbsolute());
        printPriVarError_(logFile, errors.l2SolAbsolute());
        printPriVarError_(logFile, errors.lInfSolAbsolute());

        printPriVarError_(logFile, errors.l1TruncErrMom());
        printPriVarError_(logFile, errors.l2TruncErrMom());
        printPriVarError_(logFile, errors.lInfTruncErrMom());

        printScalarError_(logFile, errors.l2DxURelative());
        printScalarError_(logFile, errors.l2DyVRelative());
        printScalarError_(logFile, errors.l2DivVelAbsolute());

        logFile << "\n";
    }

private:
    template<class Error>
    void printPriVarError_(std::ofstream& logFile, const Error& error, const std::string& format = "{:.5e}") const
    {
        logFile << Fmt::format(", " + format, error[Indices::pressureIdx]);
        for (int dirIdx = 0; dirIdx < dim; ++dirIdx)
            logFile << Fmt::format(", " + format, error[Indices::velocity(dirIdx)]);
    }

    template<class Error>
    void printScalarError_(std::ofstream& logFile, const Error& error, const std::string& format = "{:.5e}") const
    {
        logFile << Fmt::format(", " + format, error);
    }

    std::shared_ptr<const Problem> problem_;
    std::string name_;
};

template<class Problem>
NavierStokesRefinedErrorCSVWriter(std::shared_ptr<Problem>)
-> NavierStokesRefinedErrorCSVWriter<Problem>;

template<class Problem>
NavierStokesRefinedErrorCSVWriter(std::shared_ptr<Problem>, const std::string&)
-> NavierStokesRefinedErrorCSVWriter<Problem>;
} // end namespace Dumux

#endif
