// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Provides a grid creator builds a pore network grid from a host grid
 */
#ifndef DUMUX_CONTROL_VOLUME_GRID_HH
#define DUMUX_CONTROL_VOLUME_GRID_HH

#include <iostream>
#include <fstream>
#include <iomanip>

#include <dumux/discretization/staggered/freeflow/mystaggeredgeometryhelper.hh>

namespace Dumux {
template<class GlobalPosition>
struct ContainerCmpClass {
    bool operator() (const GlobalPosition& lhs, const GlobalPosition& rhs) const
    {
        double epsilon = 1e-5;

        auto scalarLess = [&] (double left, double right) { return (std::abs(left - right) > epsilon) && (left < right); };

        return (scalarLess(lhs[0], rhs[0]) || ((scalarCmp(lhs[0],rhs[0],epsilon)) && (scalarLess(lhs[1], rhs[1]))));
    }
};

template<class SubControlVolumeFace, class GridGeometry>
void fillNonDirCoord_(const SubControlVolumeFace& scvf,
                    const GridGeometry& gridGeometry,
                    double& nonDirCoord0,
                    double& nonDirCoord1,
                    bool hangingNodesAreCVCenters)
{
    if (hangingNodesAreCVCenters && !scvf.boundary() && (scvf.inside().level() != scvf.outside().level()))
    {
        SubControlVolumeFace coarseSideScvf;

        if (!scvf.boundary() && (scvf.inside().level() < scvf.outside().level()))
        {
            coarseSideScvf = scvf;
        }
        else if (!scvf.boundary() && (scvf.inside().level() > scvf.outside().level()))
        {
            auto outsideFvGeometry = localView(gridGeometry);
            outsideFvGeometry.bindElement(scvf.outside());

            for (auto&& myscvf : scvfs(outsideFvGeometry))
            {
                auto minusNormal = scvf.unitOuterNormal();
                minusNormal *= -1.;

                if (containerCmp(myscvf.unitOuterNormal(),minusNormal))
                {
                    if (containerCmp(myscvf.center(),scvf.center()))
                    {
                        coarseSideScvf = myscvf;
                    }
                }
            }
        }

        unsigned int nonDirIdx = (scvf.directionIndex() == 0) ? 1 : 0;
        const SubControlVolumeFace& normalFace0 = gridGeometry.scvf(coarseSideScvf.insideScvIdx(), coarseSideScvf.localNormalFaceBuildingIdx(0));
        nonDirCoord0 = normalFace0.center()[nonDirIdx];

        const SubControlVolumeFace& normalFace1 = gridGeometry.scvf(coarseSideScvf.insideScvIdx(), coarseSideScvf.localNormalFaceBuildingIdx(1));
        nonDirCoord1 = normalFace1.center()[nonDirIdx];
    }
    else
    {
        unsigned int dirIdx = scvf.directionIndex();
        unsigned int nonDirIdx = (dirIdx == 0) ? 1 : 0;

        nonDirCoord0 = scvf.corner(0)[nonDirIdx];
        nonDirCoord1 = scvf.corner(1)[nonDirIdx];
    }
}

template<class GlobalPosition, class SubControlVolumeFace, class GridGeometry>
std::array<GlobalPosition,4> localCVVertices(const SubControlVolumeFace& scvf, const GridGeometry& gridGeometry, bool hangingNodesAreCVCenters)
{
    double nonDirCoord0;
    double nonDirCoord1;

    fillNonDirCoord_(scvf, gridGeometry, nonDirCoord0, nonDirCoord1, hangingNodesAreCVCenters);

    unsigned int dirIdx = scvf.directionIndex();
    unsigned int nonDirIdx = (dirIdx == 0) ? 1 : 0;

    std::array<GlobalPosition,4> localVertices;

    //fill local vertices inside
    GlobalPosition innerNormalCenter0;
    innerNormalCenter0[dirIdx] = scvf.inside().geometry().center()[dirIdx];
    innerNormalCenter0[nonDirIdx] = nonDirCoord0;

    GlobalPosition innerNormalCenter1;
    innerNormalCenter1[dirIdx] = scvf.inside().geometry().center()[dirIdx];
    innerNormalCenter1[nonDirIdx] = nonDirCoord1;

    localVertices[0] = innerNormalCenter0;
    localVertices[1] = innerNormalCenter1;

    //fill local vertices outside
    if (scvf.boundary())
    {
        GlobalPosition scvfCorner0 = scvf.corner(0);
        GlobalPosition scvfCorner1 = scvf.corner(1);

        localVertices[2] = scvfCorner0;
        localVertices[3] = scvfCorner1;
    }
    else
    {
        GlobalPosition outerNormalCenter0;
        outerNormalCenter0[dirIdx] = scvf.outside().geometry().center()[dirIdx];
        outerNormalCenter0[nonDirIdx] = nonDirCoord0;

        GlobalPosition outerNormalCenter1;
        outerNormalCenter1[dirIdx] = scvf.outside().geometry().center()[dirIdx];
        outerNormalCenter1[nonDirIdx] = nonDirCoord1;

        localVertices[2] = outerNormalCenter0;
        localVertices[3] = outerNormalCenter1;
    }

    return localVertices;
}


template<class GlobalPosition, class SubControlVolumeFace>
GlobalPosition localCVCenter(const SubControlVolumeFace& scvf, bool hangingNodesAreCVCenters)
{
    unsigned int dirIdx = scvf.directionIndex();
    unsigned int nonDirIdx = (dirIdx == 0) ? 1 : 0;

    GlobalPosition center;
    double insideCoord = scvf.inside().geometry().center()[dirIdx];
    double outsideCoord = scvf.boundary()?scvf.center()[dirIdx]:scvf.outside().geometry().center()[dirIdx];
    center[dirIdx] = (insideCoord + outsideCoord)*0.5;

    if (hangingNodesAreCVCenters && !scvf.boundary() && (scvf.inside().level() != scvf.outside().level()))
    {
        if (scvf.inside().level() < scvf.outside().level())
            center[nonDirIdx] = scvf.inside().geometry().center()[nonDirIdx];
        else
            center[nonDirIdx] = scvf.outside().geometry().center()[nonDirIdx];
    }
    else
        center[nonDirIdx] = scvf.center()[nonDirIdx];

    return center;
}

template<class GridGeometry, class GlobalPosition>
auto fillCVsGridInfo(bool hangingNodesAreCVCenters,
                const GridGeometry& gridGeometry,
                int dirIdx,
                std::map<int, GlobalPosition>& vertices,
                std::vector<std::array<int,4>>& cubes,
                std::vector<std::array<int,2>>& boundarySegments)
{
    using namespace Dumux;
    unsigned int vertexNumber = 0;
    std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>> cvCenterScvfIndicesMap;

    std::vector<unsigned int> visitedScvfDofs;

    for (const auto& element : elements(gridGeometry.gridView()))
    {
        auto fvGeometry = localView(gridGeometry);
        fvGeometry.bindElement(element);

        for (auto&& scvf : scvfs(fvGeometry))
        {
            GlobalPosition cvCenter = localCVCenter<GlobalPosition>(scvf,hangingNodesAreCVCenters);

            if ( std::find(visitedScvfDofs.begin(), visitedScvfDofs.end(), scvf.dofIndex()) != visitedScvfDofs.end())
            {
                auto it = cvCenterScvfIndicesMap.find(cvCenter);
                if (it != cvCenterScvfIndicesMap.end())
                    (*it).second.push_back(scvf.index());

                continue;
            }

            bool transitionFace = hangingNodesAreCVCenters && !scvf.boundary() && (scvf.inside().level() != scvf.outside().level());

            if (transitionFace)
            {
                //fill visited dof
                using SubControlVolumeFace =  std::decay_t<decltype(scvf)>;
                SubControlVolumeFace otherFace;
                //fill other face
                if (!scvf.boundary() && (scvf.inside().level() < scvf.outside().level()))
                {
                    for (auto&& myscvf : scvfs(fvGeometry))
                    {
                        if (containerCmp(myscvf.unitOuterNormal(),scvf.unitOuterNormal()) && !containerCmp(myscvf.center(),scvf.center()))
                        {
                            otherFace = myscvf;
                        }
                    }
                }
                else if (!scvf.boundary() && (scvf.inside().level() > scvf.outside().level()))
                {
                    auto outsideFvGeometry = localView(gridGeometry);
                    outsideFvGeometry.bindElement(scvf.outside());

                    for (auto&& myscvf : scvfs(outsideFvGeometry))
                    {
                        auto minusNormal = scvf.unitOuterNormal();
                        minusNormal *= -1.;

                        if (containerCmp(myscvf.unitOuterNormal(),minusNormal))
                        {
                            if (!containerCmp(myscvf.center(),scvf.center()))
                            {
                                otherFace = myscvf;
                            }
                        }
                    }
                }

                //fill visitedDof
                if (std::find(visitedScvfDofs.begin(), visitedScvfDofs.end(), otherFace.dofIndex()) != visitedScvfDofs.end())
                {
                    visitedScvfDofs.push_back(scvf.dofIndex());

                    auto it = cvCenterScvfIndicesMap.find(cvCenter);
                    if (it != cvCenterScvfIndicesMap.end())
                        (*it).second.push_back(scvf.index());

                    continue;
                }
            }

            visitedScvfDofs.push_back(scvf.dofIndex());

            if (scvf.directionIndex() == dirIdx)
            {
                std::array<GlobalPosition,4> localVertices = localCVVertices<GlobalPosition>(scvf, gridGeometry, hangingNodesAreCVCenters);

                std::vector<unsigned int> scvfIndices;
                scvfIndices.push_back(scvf.index());

                cvCenterScvfIndicesMap.insert(std::pair<GlobalPosition, std::vector<unsigned int>>(cvCenter,scvfIndices));

                std::array<int,4> localCube;

                std::array<int,2> parallelLocalBoundarySegment;
                std::array<int,2> perpendicularLocalBoundarySegment;

                for (unsigned int i = 0; i < 4; ++i)
                {
                    const GlobalPosition& vertex = localVertices[i];

                    int alreadyThereVertexNumber = -1;
                    for (auto it = vertices.begin(); it != vertices.end(); ++it)
                    {
                        if (containerCmp(it->second, vertex))
                        {
                            alreadyThereVertexNumber = it->first;
                        }
                    }

                    if (!(alreadyThereVertexNumber < 0))
                    {
                        localCube[i] = alreadyThereVertexNumber;
                    }
                    else
                    {
                        localCube[i] = vertexNumber;
                        vertices.insert(std::pair<unsigned int, GlobalPosition>(vertexNumber, vertex));
                        ++vertexNumber;
                    }
                }

                for (unsigned int someIndex = 0; someIndex < 2; ++someIndex)
                {
                    unsigned int otherIdx = (someIndex==0)?1:0;

                    if (!scvf.hasParallelNeighbor(someIndex,0))
                    {
                        if (containerCmp(scvf.pairData(someIndex).virtualFirstParallelFaceDofPos, scvf.corner(someIndex)))
                        {
                            if (someIndex==0)
                                perpendicularLocalBoundarySegment = {localCube[0],localCube[2]};
                            else if (someIndex==1)
                                perpendicularLocalBoundarySegment = {localCube[1],localCube[3]};
                        }
                        else if (containerCmp(scvf.pairData(someIndex).virtualFirstParallelFaceDofPos, scvf.corner(otherIdx)))
                        {
                            if (otherIdx==0)
                                perpendicularLocalBoundarySegment = {localCube[0],localCube[2]};
                            else if (otherIdx==1)
                                perpendicularLocalBoundarySegment = {localCube[1],localCube[3]};
                        }
                        else
                            DUNE_THROW(Dune::InvalidStateException, "");
                    }
                }

                if (scvf.boundary())
                    parallelLocalBoundarySegment = {localCube[2],localCube[3]};

                cubes.push_back(localCube);

                if (scvf.boundary())
                    boundarySegments.push_back(parallelLocalBoundarySegment);

                if (!scvf.hasParallelNeighbor(0,0) || !scvf.hasParallelNeighbor(1,0))
                    boundarySegments.push_back(perpendicularLocalBoundarySegment);
            }
        }
    }

    return cvCenterScvfIndicesMap;
}

template<class GlobalPosition>
void printCVsGridInfo(const std::string& name,
                    const std::map<int, GlobalPosition>& vertices,
                    const std::vector<std::array<int,4>>& cubes,
                    const std::vector<std::array<int,2>>& boundarySegments)
{
    std::ofstream dgfFile;
    dgfFile.open(name + ".dgf");
    dgfFile << "DGF" << std::endl;
    dgfFile << "% Elements = " << cubes.size() << "  |  Vertices = " << vertices.size() << std::endl;
    dgfFile << std::endl;
    dgfFile << "VERTEX" << std::endl;
    auto i = vertices.begin();
    while (i != vertices.end())
    {
        dgfFile << (i->second)[0] << " " << (i->second)[1] << std::endl;
        ++i;
    }
    dgfFile << "#" << std::endl;
    dgfFile << std::endl;
    dgfFile << "CUBE" << std::endl;
    for (const auto& cube : cubes)
        dgfFile << cube[0] << " " << cube[1] << " " << cube[2] << " "  << cube[3] << std::endl;
    dgfFile << "#" << std::endl;
    dgfFile << std::endl;
    dgfFile << "BOUNDARYSEGMENTS" << std::endl;
    for (const auto& boundarySegment : boundarySegments)
        dgfFile << "1   " << boundarySegment[0] << " " << boundarySegment[1] << std::endl;
    dgfFile << "#" << std::endl;
    dgfFile << std::endl;
    dgfFile << "#" << std::endl;
    dgfFile << "BOUNDARYDOMAIN" << std::endl;
    dgfFile << "default 1     % all other boundary segments have id 1" << std::endl; //I had to add this as for some reason it seems that the hanging node stuff (faces at hanging nodes from both sides) is also treated as boundary, if I not set anything the boundary there has id 0 which is not allowed
    dgfFile << "#" << std::endl;
    dgfFile.close();
}

template<class GridGeometry, class GlobalPosition>
auto generateCVsDGFfile(const std::string& paramGroup, const GridGeometry& gridGeometry, int dirIdx, bool hangingNodesAreCVCenters)
{
    std::map<int, GlobalPosition> vertices;
    std::vector<std::array<int,4>> cubes;
    std::vector<std::array<int,2>> boundarySegments;

    std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>> cvCenterScvfIndicesMap = fillCVsGridInfo<GridGeometry, GlobalPosition>(hangingNodesAreCVCenters, gridGeometry,dirIdx, vertices, cubes, boundarySegments);
    printCVsGridInfo<GlobalPosition>(paramGroup, vertices, cubes, boundarySegments);

    return cvCenterScvfIndicesMap;
}
}

#endif
