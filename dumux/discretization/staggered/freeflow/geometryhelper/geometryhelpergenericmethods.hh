// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::CVDGeometryHelperUtilities
 */
#ifndef DUMUX_GH_GENERIC_METHODS_HH
#define DUMUX_GH_GENERIC_METHODS_HH

#include <algorithm>
#include <type_traits>

namespace Dumux {
template <class SomeType>
bool containerCmp(const SomeType& a, const SomeType& b, double eps) {
    if (a.size() != b.size()) {
        std::cout << "containerCmp with different sizes" << std::endl;
        return false;
    } else {
        bool retVal = true;
        for (unsigned int i = 0; i < a.size(); ++i) {
            if (!((a[i] > b[i] - eps) && (a[i] < b[i] + eps))) {
                retVal = false;
                break;
            }
        }

        return retVal;
    }
}

template <class SomeType>
bool containerCmp(const SomeType& a, const SomeType& b) {
    return containerCmp(a,b,1e-10);
}

template <class SomeType>
bool scalarCmp(const SomeType& a, const SomeType& b, double eps) {
    return a > b - eps && a < b + eps;
}

template <class SomeType>
bool scalarCmp(const SomeType& a, const SomeType& b) {
    return scalarCmp(a,b,1e-10);
}

template <class InputIt, class T>
constexpr InputIt containerFind(InputIt first, InputIt last, const T& value) {
    for (; first != last; ++first) {
        if (containerCmp(*first, value)) {
            return first;
        }
    }
    return last;
}

template <class InputIt, class T>
constexpr InputIt pairSecondFind(InputIt first, InputIt last, const T& value) {
    for (; first != last; ++first) {
        if (containerCmp((*first).second, value)) {
            return first;
        }
    }
    return last;
}

template <class GeometricType1, class GeometricType2>
bool haveCommonCorner(const GeometricType1& geometry1, const GeometricType2& geometry2) {
    bool commonCorners = false;

    using GlobalPosition = typename std::decay_t<decltype(geometry1.geometry().corner(0))>;

    std::vector<GlobalPosition> geometry1Corners;
    for (unsigned int i = 0; i < geometry1.geometry().corners(); ++i) {
        geometry1Corners.push_back(geometry1.geometry().corner(i));
    }

    for (unsigned int i = 0; i < geometry2.geometry().corners(); ++i) {
        const auto geometry2Corner = geometry2.geometry().corner(i);

        if (containerFind(geometry1Corners.begin(), geometry1Corners.end(), geometry2Corner) != geometry1Corners.end()) {
            commonCorners = true;
        }
    }

    return commonCorners;
}

template <class InputIt, class T>
constexpr InputIt scalarFind_(InputIt first, InputIt last, const T& value) {
    for (; first != last; ++first) {
        if (scalarCmp(*first, value)) {
            return first;
        }
    }
    return last;
}

}  // namespace Dumux
#endif
