// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::InterpolationHelper
 */
#ifndef DUMUX_INTERPOLATION_HELPER_HH
#define DUMUX_INTERPOLATION_HELPER_HH

#include <algorithm>
#include <type_traits>


namespace Dumux {
template <class GridView>
class InterpolationHelper {
    using Scalar = typename GridView::ctype;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    using ScvGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim, dimWorld>;
    using ScvfGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim - 1, dimWorld>;

    using GlobalPosition = typename ScvGeometry::GlobalCoordinate;

    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;

    using ReferenceElements = typename Dune::ReferenceElements<Scalar, dim>;
    using GridIndexType = typename GridView::IndexSet::IndexType;

   private:
    const GridView gridView_;
    std::vector<std::pair<GridIndexType, GlobalPosition>> velocityXPositions_;
    std::vector<std::pair<GridIndexType, GlobalPosition>> velocityYPositions_;

   public:
    InterpolationHelper(const GridView& gridView,
     const std::vector<std::pair<GridIndexType, GlobalPosition>>& velocityXPositions,
     const std::vector<std::pair<GridIndexType, GlobalPosition>>& velocityYPositions)
        : gridView_(gridView),
        velocityXPositions_(velocityXPositions),
        velocityYPositions_(velocityYPositions) {}

    Scalar distance(const GlobalPosition& a, const GlobalPosition& b)
    {
        return std::sqrt((a[0]-b[0])*(a[0]-b[0]) + (a[1]-b[1])*(a[1]-b[1]));
    }

    void replaceDofsHigherOrder_(std::vector<int>& dofs, std::vector<Scalar>& interpFactors, const GlobalPosition& pos, unsigned int directionIndex)
    {
        //note: if I want to come close to the boundary with this, I had a note that dof=-1 should be ignored and that in the case of oppo -1 can be accompanied by an actual dof
        if (dofs.size() > 1)
        {
            auto velocityPositions = (directionIndex==0)?velocityXPositions_:velocityYPositions_;
            bool isDof = (pairSecondFind(velocityPositions.begin(), velocityPositions.end(), pos) != velocityPositions.end());

            if (!isDof)
            {
                dofs.clear();
                interpFactors.clear();

                sort(velocityPositions.begin(), velocityPositions.end(), [&](const std::pair<GridIndexType, GlobalPosition> &a, const std::pair<GridIndexType, GlobalPosition> &b) {return (distance(a.second, pos) < distance(b.second, pos));});

                Dune::FieldMatrix<Scalar,6,6> sixCrossSixMatrix;
                Dune::FieldMatrix<Scalar,3,3> threeCrossThreeMatrix;

                int indexToBeReplaced = -1;
                unsigned int dir;

                if (checkFirstThreeAtSameXOrY_(velocityPositions, dir, pos))
                {
                    for (unsigned int i = 0; i<3; ++i)
                    {
                        dofs.push_back(velocityPositions[i].first);

                        const Scalar delta = velocityPositions[i].second[dir]-pos[dir];
                        threeCrossThreeMatrix[i][0] = delta*delta;
                        threeCrossThreeMatrix[i][1] = delta;
                        threeCrossThreeMatrix[i][2] = 1;
                    }

                    threeCrossThreeMatrix.invert();

                    interpFactors.push_back(threeCrossThreeMatrix[2][0]);
                    interpFactors.push_back(threeCrossThreeMatrix[2][1]);
                    interpFactors.push_back(threeCrossThreeMatrix[2][2]);
                }
                else
                {
                    unsigned int positiveDeltaXCount = 0;
                    unsigned int negativeDeltaXCount = 0;
                    unsigned int positiveDeltaYCount = 0;
                    unsigned int negativeDeltaYCount = 0;

                    Scalar eps = 1e-10;
                    unsigned int i = 0;
                    unsigned int matrixIndex = 0;
                    bool filledMatrix = false;

                    std::vector<unsigned int> pushedBackIndices;

                    while (filledMatrix == false)
                    {
                        if (checkIfMoreThanThreeAtSameXOrY_(velocityPositions, indexToBeReplaced, pos) && i == indexToBeReplaced)
                        {
                            ++i;
                            continue;
                        }

                        if (i > velocityPositions.size()-1)
                            DUNE_THROW(Dune::InvalidStateException, "i exceeds velocitypositions");

                        const Scalar deltaX = velocityPositions[i].second[0]-pos[0];
                        const Scalar deltaY = velocityPositions[i].second[1]-pos[1];

                        if (deltaX > eps)
                            ++positiveDeltaXCount;
                        else if (deltaX < -eps)
                            ++negativeDeltaXCount;
                        if (deltaY > eps)
                            ++positiveDeltaYCount;
                        else if (deltaY < -eps)
                            ++negativeDeltaYCount;

                        sixCrossSixMatrix[matrixIndex][0] = deltaX*deltaX;
                        sixCrossSixMatrix[matrixIndex][1] = deltaY*deltaY;
                        sixCrossSixMatrix[matrixIndex][2] = deltaX*deltaY;
                        sixCrossSixMatrix[matrixIndex][3] = deltaX;
                        sixCrossSixMatrix[matrixIndex][4] = deltaY;
                        sixCrossSixMatrix[matrixIndex][5] = 1;

                        if (matrixIndex == 3)
                        {
                            if (checkThatAtLeastNumXAndY_(velocityPositions, pushedBackIndices, indexToBeReplaced, matrixIndex, i))
                            {
                                pushedBackIndices.push_back(i);
                                dofs.push_back(velocityPositions[i].first);
                                ++matrixIndex;
                            }
                        }
                        else if (matrixIndex == 4)
                        {
                            if (checkThatAtLeastNumXAndY_(velocityPositions, pushedBackIndices, indexToBeReplaced, matrixIndex, i))
                            {
                                pushedBackIndices.push_back(i);
                                dofs.push_back(velocityPositions[i].first);
                                ++matrixIndex;
                            }
                        }
                        else if (matrixIndex >= 5)
                        {
                            if (((positiveDeltaXCount == 0 && negativeDeltaXCount == 0) || (positiveDeltaXCount > 0 && negativeDeltaXCount > 0)) &&
                                ((positiveDeltaYCount == 0 && negativeDeltaYCount == 0) || (positiveDeltaYCount > 0 && negativeDeltaYCount > 0)) &&
                                checkThatAtLeastNumXAndY_(velocityPositions, pushedBackIndices, indexToBeReplaced, matrixIndex, i)&&
                                checkNotThreePairs_(velocityPositions, pushedBackIndices, i))
                            {
                                pushedBackIndices.push_back(i);
                                dofs.push_back(velocityPositions[i].first);
                                filledMatrix=true;
                            }
                        }
                        else
                        {
                            pushedBackIndices.push_back(i);
                            dofs.push_back(velocityPositions[i].first);
                            ++matrixIndex;
                        }

                        ++i;
                    }

                    sixCrossSixMatrix.invert();

                    //last line contains interpolation factors
                    interpFactors.push_back(sixCrossSixMatrix[5][0]);
                    interpFactors.push_back(sixCrossSixMatrix[5][1]);
                    interpFactors.push_back(sixCrossSixMatrix[5][2]);
                    interpFactors.push_back(sixCrossSixMatrix[5][3]);
                    interpFactors.push_back(sixCrossSixMatrix[5][4]);
                    interpFactors.push_back(sixCrossSixMatrix[5][5]);
                }
            }
        }
    }

    bool checkFirstThreeAtSameXOrY_(const std::vector<std::pair<GridIndexType, GlobalPosition>>& velPos, unsigned int& dir, const GlobalPosition& pos)
    {
        for (unsigned int trialOtherDir = 0; trialOtherDir < 2; ++trialOtherDir)
        {
            dir = trialOtherDir==0?1:0;

            bool retVal = velPos[0].second[trialOtherDir] == pos[trialOtherDir] &&
                    velPos[1].second[trialOtherDir] == pos[trialOtherDir] &&
                    velPos[2].second[trialOtherDir] == pos[trialOtherDir];

            if (retVal == true)
                return true;
        }
        return false;
    }

    bool checkIfMoreThanThreeAtSameXOrY_(const std::vector<std::pair<GridIndexType, GlobalPosition>>& velocityPositions, int& indexToBeReplaced, const GlobalPosition& pos)
    {
        int trueCount = 0;
        for (unsigned int dir = 0; dir < 2; ++dir)//x,y
        {
            for (unsigned int i = 0; i < 6; ++i)
            {
                for (unsigned int j = i+1; j<6; ++j)
                {
                    if (velocityPositions[i].second[dir]==velocityPositions[j].second[dir] /*&& velocityPositions[i].second[dir]==pos[dir]*/)
                    {
                        trueCount++;

                        if (trueCount == 3)
                        {
                            indexToBeReplaced = j;
                        }
                    }
                }
            }
        }
        return (trueCount > 2);
    }

    //make sure we have three different x as well as three different y involved
    bool checkThatAtLeastNumXAndY_(const std::vector<std::pair<GridIndexType, GlobalPosition>>& velocityPositions,  const std::vector<unsigned int> pushedBackIndices, unsigned int indexToBeReplaced, unsigned int matrixIndex, unsigned int candidateIndex)
    {
        auto pushedBackIndicesVar = pushedBackIndices;
        pushedBackIndicesVar.push_back(candidateIndex);

        bool directionBool = true;

        for (unsigned int dir = 0; dir < 2; ++dir)//x,y
        {
            int differentValuesCountedUpToThree = 1;
            Scalar value1;
            Scalar value2;
            bool value2Set = false;

            value1 = velocityPositions[0].second[dir];

            for (const auto jIt : pushedBackIndicesVar)
            {
                if (velocityPositions[jIt].second[dir]==value1)
                {}
                else if (value2Set == true && velocityPositions[jIt].second[dir]==value2)
                {}
                else if (value2Set == false)
                {
                    differentValuesCountedUpToThree++;
                    value2 = velocityPositions[jIt].second[dir];
                    value2Set = true;
                }
                else
                {
                    differentValuesCountedUpToThree++;
                }
            }

            unsigned int num;
            if (matrixIndex == 5)
                num=3;
            else
                num=2;

            if (num==3 && differentValuesCountedUpToThree <= 2)
            {
                directionBool = false;
            }
            else if (num==2 && differentValuesCountedUpToThree <= 1)
            {
                directionBool = false;
            }
        }

        return directionBool;
    }

    //with this I forbid both three same x and three same y values
    bool checkNotThreePairs_(const std::vector<std::pair<GridIndexType, GlobalPosition>>& velocityPositions, const std::vector<unsigned int> pushedBackIndices, unsigned int candidateIndex)
    {
        auto pushedBackIndicesVar = pushedBackIndices;
        pushedBackIndicesVar.push_back(candidateIndex);

        bool directionBool = true;

        for (unsigned int dir = 0; dir < 2; ++dir)//x,y
        {
            int trueCount1 = 0;
            int trueCount2 = 0;
            int trueCount3 = 0;
            Scalar value1 = velocityPositions[0].second[dir];
            Scalar value2 = 0.;
            Scalar value3 = 0.;
            bool value2Set = false;
            bool value3Set = false;

            for (const auto& iIt : pushedBackIndicesVar)
            {
                for (const auto& jIt : pushedBackIndicesVar)
                {
                    if (velocityPositions[iIt].second[dir]==velocityPositions[jIt].second[dir])
                    {
                        if (velocityPositions[jIt].second[dir]==value1)
                        {
                            trueCount1++;
                        }
                        else if (value2Set && velocityPositions[jIt].second[dir]==value2)
                        {
                            trueCount2++;
                        }
                        else if (value3Set && velocityPositions[jIt].second[dir]==value3)
                        {
                            trueCount3++;
                        }
                    }
                    else if (value2Set == false)
                    {
                        value2 = velocityPositions[jIt].second[dir];
                        value2Set = true;
                    }
                    else if (value3Set == false)
                    {
                        if (velocityPositions[jIt].second[dir] !=value2 && velocityPositions[jIt].second[dir] != value1)
                        {
                            value3 = velocityPositions[jIt].second[dir];
                            value3Set = true;
                        }
                    }
                }
            }

            if (trueCount1==1 && trueCount2==1 && trueCount3==1)
            {
                directionBool = false;
            }
        }

        return directionBool;
    }

};
}  // namespace Dumux
#endif
