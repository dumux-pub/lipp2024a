// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \brief something
 */
#ifndef DUMUX_STAGGERED_DATA_HH
#define DUMUX_STAGGERED_DATA_HH

namespace Dumux {

/*!
 * \ingroup StaggeredDiscretization
 * \brief Parallel Data stored per sub face
 *
 * ------------
 * |          |
 * |          |
 * |          |
 * -----------------------
 * | yyyyyyyy s          |
 * | yyyyyyyy s          |
 * | yyyyyyyy s          |
 * -----------------------
 * In this corner geometry, hasParallelNeighbor will return true for subcontrolvolumeface s belonging to the
 * element filled by 'y's, but hasParallelNeighbor will return false for the subcontrolvolumeface that has the
 * same dofIndex. We name this situation hasHalfParallelNeighbor.
 *
 * ------------
 * | yyyyyyyy s
 * | yyyyyyyy s
 * | yyyyyyyy s
 * -----------------------
 * |          |          |
 * |          |          |
 * |          |          |
 * -----------------------
 * In this corner geometry, hasParallelNeighbor will return true for subcontrolvolumeface s belonging to the
 * element filled by 'y's. However, as there also might be a boundary velocity value known at the corner, which
 * can be used instead of the standard parallel velocity in some cases, we want to identify this situation. We
 * name it cornerParallelNeighbor.
 */
template <class Scalar, class GlobalPosition>
struct MyPairData {
    bool hasHalfParallelNeighbor = false;
    bool hasCornerParallelNeighbor = false;
    std::vector<std::vector<int>> parallelDofs;
    std::vector<std::vector<Scalar>> parallelDofsInterpolationFactors;
    std::vector<Scalar> parallelDistances;
    //     --------------
    //     | o parallel2|
    //     | o         -->
    //     | o          |
    //     --------------
    //     | * parallel1|
    //     | *         -->
    //     | *          |
    //     --------------
    //     | +     self||
    //     | +         -->
    //     | +         ||
    //     --------------
    //
    //     +: parallelDistances[0]
    //     *: parallelDistances[1]
    //     o: parallelDistances[2]
    std::pair<std::vector<int>, std::vector<int>> normalPair;
    std::pair<std::vector<Scalar>, std::vector<Scalar>> normalPairInterpolationFactors;
    int localNormalFluxCorrectionIndex;  //local index of the normal intersection touching the intersection_
    Scalar normalDistance;               //distance between outer normal dof position (second) and inner normal dof position (first) of a certain pair
    Scalar innerNormalDistance;
    Scalar outerNormalDistance;
    GlobalPosition virtualFirstParallelFaceDofPos;
    int cornerGeometryEIdx = -1;
    int cornerGeometryLocalFaceIdx = -1;
};

/*!
 * \ingroup StaggeredDiscretization
 * \brief In Axis Data stored per sub face
 */
template <class Scalar>
struct MyAxisData {
    int selfDof;
    std::vector<int> oppositeDofs;
    std::vector<Scalar> oppositeDofsInterpolationFactors;
    std::vector<int> inAxisForwardDofs;
    std::vector<int> inAxisBackwardDofs;
    Scalar selfToOppositeDistance;
    std::vector<Scalar> inAxisForwardDistances;
    std::vector<Scalar> inAxisBackwardDistances;
};

struct ScvfBuildingData {
    int eIdx;
    int localScvfIdx;
};

/*!
 * \ingroup StaggeredDiscretization
 * \brief Volume variables data stored per sub face
 */
template <class Scalar, int numPairs>
struct MyVolVarsData {
    //     -------
    //     |     |
    //     |  *  |
    //     |     |
    // -----------
    //    ||     |
    //  x ||  o  |
    //    ||     |
    // -----------
    //     |     |
    //     |  *  |
    //     |     |
    //     -------
    //
    // ||: intersection, *: normalVolVars, o: innerVolVars, x: outerVolVars

    //inner is the reference staggered element, outer are neighbors which touch in a normalFace
    std::vector<int> innerVolVarsDofs;
    std::vector<Scalar> innerVolVarsDofsInterpolationFactors;
    std::vector<ScvfBuildingData> innerVolVarsScvfBuildingData;
    std::vector<int> outerVolVarsDofs;
    std::vector<Scalar> outerVolVarsDofsInterpolationFactors;
    std::vector<ScvfBuildingData> outerVolVarsScvfBuildingData;
    std::array<std::vector<int>, numPairs> normalVolVarsDofs;
    std::array<std::vector<Scalar>, numPairs> normalVolVarsDofsInterpolationFactors;
    std::array<std::vector<ScvfBuildingData>, numPairs> normalVolVarsScvfBuildingData;
};

} // namespace Dumux
#endif
