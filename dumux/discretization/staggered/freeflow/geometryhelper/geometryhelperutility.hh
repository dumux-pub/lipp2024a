// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::GeometryHelperUtility
 */
#ifndef DUMUX_GH_UTILITY_HH
#define DUMUX_GH_UTILITY_HH

#include <algorithm>
#include <type_traits>

#include <dumux/discretization/staggered/freeflow/geometryhelper/geometryhelpergenericmethods.hh>

namespace Dumux {
template <class GridView>
class GeometryHelperUtility {
    using Scalar = typename GridView::ctype;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    using ScvGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim, dimWorld>;
    using ScvfGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim - 1, dimWorld>;

    using GlobalPosition = typename ScvGeometry::GlobalCoordinate;

    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;

    using ReferenceElements = typename Dune::ReferenceElements<Scalar, dim>;
    using GridIndexType = typename GridView::IndexSet::IndexType;

   private:
    const GridView gridView_;
    Intersection intersection_;
    std::vector<std::vector<int>> elementFacetIsNormalMatrix_;
    std::vector<std::vector<int>> outsideElementFacetIsNormalMatrix_;

   public:
    GeometryHelperUtility(const GridView& gridView, const Intersection& intersection)
    : gridView_(gridView),
    intersection_(intersection)
    {
        std::vector<int> dummyVec;
        dummyVec.resize(8,-1);
        elementFacetIsNormalMatrix_.resize(8,dummyVec);
        outsideElementFacetIsNormalMatrix_.resize(8,dummyVec);

        auto func = [&](std::vector<std::vector<int>>& matrix, const Element& element)
        {
            std::vector<std::vector<int>> localOppositeIndices;
            std::vector<std::vector<int>> localSelfIndices;

            for (const auto& selfIs : intersections(gridView_, element))
            {
                localOppositeIndices.push_back(localOppositeIndices_(selfIs, element));
                localSelfIndices.push_back(localSelfIndices_(selfIs, element));
            }

            for (unsigned int selfIdx = 0; selfIdx < localSelfIndices.size(); ++selfIdx)
            {
                for (unsigned int otherIdx = 0; otherIdx < localSelfIndices.size(); ++otherIdx)
                {
                    matrix[selfIdx][otherIdx] = !(
                    std::find(localSelfIndices[selfIdx].begin(), localSelfIndices[selfIdx].end(), otherIdx) != localSelfIndices[selfIdx].end()
                    ||
                    std::find(localOppositeIndices[selfIdx].begin(), localOppositeIndices[selfIdx].end(), otherIdx) != localOppositeIndices[selfIdx].end()
                    );
                }
            }
        };

        func(elementFacetIsNormalMatrix_, intersection_.inside());
        if (intersection_.neighbor())
            func(outsideElementFacetIsNormalMatrix_, intersection_.outside());
    }

    void setIntersection(const Intersection& intersection){
        intersection_ = intersection;
    }


    /*!
     * \brief Returns the local opposing intersection index
     *
     * \param idx The local index of the intersection itself
     */
    std::vector<int> localOppositeIndices_(const Intersection& myIs, const Element& element) const
    {
        std::vector<int> retVec;

        int localIsIdx = 0;
        for (const auto& is : intersections(this->gridView_, element))
        {
            bool isNotParallelSelf = false;
            bool isNotNormal = false;
            //TODO think about dim vs dimworld
            for (unsigned int direction = 0; direction < dim; ++direction)
            {
                //direction is normal to myIs
                if (direction == Dumux::directionIndex(std::move(myIs.centerUnitOuterNormal())))
                {
                    if(!scalarCmp(is.geometry().center()[direction], myIs.geometry().center()[direction]))
                    {
                        isNotParallelSelf = true;
                    }
                }
                //direction is parallel to myIs, in 3D this is two directions
                else
                {
                    int noCornerIndicator = 0;
                    for (unsigned int i=0; i < element.geometry().corners(); ++i){
                        if(!scalarCmp(is.geometry().center()[direction], element.geometry().corner(i)[direction]))
                        {
                            ++noCornerIndicator;
                        }
                    }
                    if (noCornerIndicator == element.geometry().corners())
                    {
                        isNotNormal = true;
                    }
                }
            }
            if (isNotParallelSelf == true && isNotNormal == true)
            {
                retVec.push_back(localIsIdx);
            }
            ++localIsIdx;
        }

        return retVec;
    }

    //returns the index of self and of parallelSelf
    std::vector<int> localSelfIndices_(const Intersection& myIs, const Element& element) const
    {
        std::vector<int> retVec;

        int localIsIdx = 0;
        for (const auto& is : intersections(this->gridView_, element))
        {
            //TODO think about dim vs dimworld
            for (unsigned int direction = 0; direction < dim; ++direction)
            {
                //direction is normal
                if (direction == Dumux::directionIndex(std::move(myIs.centerUnitOuterNormal())))
                {
                    if(scalarCmp(is.geometry().center()[direction], myIs.geometry().center()[direction]))
                    {
                        retVec.push_back(localIsIdx);
                    }
                }
            }
            ++localIsIdx;
        }

        return retVec;
    }

    Intersection getFacet_(const int localFacetIdx, const Element& element) const
    {
        int localMyIsIdx = 0;
        for (const auto& is : intersections(this->gridView_, element)){
            if (localMyIsIdx == localFacetIdx)
            {
                return is;
            }
            ++localMyIsIdx;
        }
        std::cout << "should not be after for loop in getFacet_ in staggeredgeometryhelper!" << std::endl;
        return intersection_;//just to avoid compiler warning
    };

    /*!
     * \brief Returns the local opposing intersection index
     *
     * \param idx The local index of the intersection itself
     */
    std::vector<int> localOppositeIndices_(const int idx, const Element& element) const
    {
        std::vector<int> retVec;
        const auto myIs = getFacet_(idx, element);

        return localOppositeIndices_(myIs, element);
    }

    //returns the index of self and of parallelSelf
    std::vector<int> localSelfIndices_(const int idx, const Element& element) const
    {
        std::vector<int> retVec;
        const auto myIs = getFacet_(idx, element);

        return localSelfIndices_(myIs, element);
    }

    /*!
     * \brief Returns true if the intersection lies normal to another given intersection
     *
     * \param selfIdx The local index of the intersection itself
     * \param otherIdx The local index of the other intersection
     */
    bool elementFacetIsNormal_(const int selfIdx, const int otherIdx) const
    {
        if (elementFacetIsNormalMatrix_[selfIdx][otherIdx] == -1)
            DUNE_THROW(Dune::InvalidStateException, "");

        return elementFacetIsNormalMatrix_[selfIdx][otherIdx];
    }

    /*!
     * \brief Returns true if the intersection lies normal to another given intersection
     *
     * \param selfIdx The local index of the intersection itself
     * \param otherIdx The local index of the other intersection
     */
    bool outsideElementFacetIsNormal_(const int selfIdx, const int otherIdx) const
    {
        if (outsideElementFacetIsNormalMatrix_[selfIdx][otherIdx] == -1)
            DUNE_THROW(Dune::InvalidStateException, "");

        return outsideElementFacetIsNormalMatrix_[selfIdx][otherIdx];
    }

    // non-directional: not in the direction of the normal vector of intersection_
    // true if the non-directional coordinate of one of the corners of intersection_ matches the
    // non-directional coordinate of the center of isWithCenter
    // is typically used in a case intersection_.inside().level() < intersection_.outside().level()
    //                     -------------
    //                    |            |
    //                    |            |warning:
    //                    |            |this opposite intersection also returns true
    //                    ||           |if there is a coarse neighbor -> needs to be
    //                    ||           |combined with utility_.elementFacetIsNormal_ or utility_.outsideElementFacetIsNormal_ !!!
    //                    ||           |
    //                     ======-******
    //                    |      |     |
    //                    |      |     |
    //                    |      |     |
    //                     -------------
    // ||:myintersection, === and ***: isWithCenters which return true
    bool haveNonDirectionCornerCenterMatch_(const Intersection& isWithCenter)
    {
        //TODO figure out in 3D what I want

        bool retVal = false;

        const auto nonDirIndices = nonDirectionIndices();

        if (nonDirIndices.size() != 1)
        {
            DUNE_THROW(Dune::InvalidStateException, "Not yet prepared for 3D.");
        }

        for (const auto nonDirectionIdx : nonDirIndices)
        {
            for (unsigned int i = 0; i < intersection_.geometry().corners(); ++i)
            {
                const auto cor = intersection_.geometry().corner(i);

                if (scalarCmp(cor[nonDirectionIdx], isWithCenter.geometry().center()[nonDirectionIdx]))
                {
                    retVal = true;
                }
            }
        }

        return retVal;
    }

    bool haveNonDirectionCenterCornerMatch_(const Intersection& isWithCorners)
    {
        bool retVal = false;

        for (unsigned int i = 0; i < isWithCorners.geometry().corners(); ++i)
        {
            const auto cor = isWithCorners.geometry().corner(i);

            if (scalarCmp(cor[directionIndex()], intersection_.geometry().center()[directionIndex()]))
            {
                retVal = true;
            }
        }

        return retVal;
    }

    Scalar quadraticInterpolationFactor_(Scalar ownDistance, Scalar otherDistance1, Scalar otherDistance2)
    {
        return otherDistance1 * otherDistance2 / ((otherDistance1 - ownDistance) * (otherDistance2 - ownDistance));
    }

    std::array<int, dim-1> nonDirectionIndices() const
    {
        std::array<int, dim-1> retArray; //initialize just to avoid a "may be used uninitialized" warning

        int vectorPos = 0;
        for (int i = 0; i < dim; ++i)
        {
            if (vectorPos <= dim-1)
            {
                if (i != directionIndex())
                {
                    retArray[vectorPos] = i;
                    ++vectorPos;
                }
            }
            else
            {
                DUNE_THROW(Dune::InvalidStateException, "Should not happen.");
            }
        }

        if (retArray.size() != 1)
        {
            DUNE_THROW(Dune::InvalidStateException, "Not yet prepared for 3D.");
        }

        return retArray;
    }

    /*!
     * \brief Returns the dirction index of the primary facet (0 = x, 1 = y, 2 = z)
     */
    int directionIndex() const
    {
        return Dumux::directionIndex(std::move(intersection_.centerUnitOuterNormal()));
    }

    template<class ScvfBuildingData>
    void fillIsTwoLevelDiagonal_(std::vector<int>& dofs,  std::vector<Scalar>& interpFacts, const Element& closestElement, const Element& smallerElement, const Element& sameSizedElement, const Element& largerElement, Scalar borderToLargerLength, Scalar borderToSmallerLength, Scalar smallerLonelyLength, Scalar largerLonelyLength, std::vector<ScvfBuildingData>& scvfBuildingData)
    {
        ScvfBuildingData emptyBuildingData;
        emptyBuildingData.eIdx = -1;
        emptyBuildingData.localScvfIdx = -1;

        dofs.push_back(gridView_.indexSet().index(closestElement));
        interpFacts.push_back((0.5*(1.0 - (0.5*borderToLargerLength)/(borderToLargerLength + 0.5*smallerLonelyLength))/*C, eq. (4.8)*/) *
                            (1.0 - (0.5*borderToSmallerLength)/(borderToSmallerLength + largerLonelyLength)/*A, eq. (4.9)*/));
        scvfBuildingData.push_back(emptyBuildingData);

        dofs.push_back(gridView_.indexSet().index(smallerElement));
        interpFacts.push_back(1.0 - (0.5*borderToLargerLength + 0.5*smallerLonelyLength)/(borderToLargerLength + 0.5*smallerLonelyLength)/*D, eq. (4.9)*/);
        scvfBuildingData.push_back(emptyBuildingData);

        dofs.push_back(gridView_.indexSet().index(largerElement));
        interpFacts.push_back((1.0 - (0.5*borderToLargerLength)/(borderToLargerLength + 0.5*smallerLonelyLength)/*C, eq. (4.8)*/) *
                            (1.0 - (largerLonelyLength + 0.5*borderToSmallerLength)/(borderToSmallerLength + largerLonelyLength)/*B, eq. (4.9)*/));
        scvfBuildingData.push_back(emptyBuildingData);

        dofs.push_back(gridView_.indexSet().index(sameSizedElement));
        interpFacts.push_back(0.5*(1.0 - (0.5*borderToLargerLength)/(borderToLargerLength + 0.5*smallerLonelyLength)/*C, eq. (4.8)*/) *
                            (1.0 - (0.5*borderToSmallerLength)/(borderToSmallerLength + largerLonelyLength)/*A, eq. (4.9)*/));
        scvfBuildingData.push_back(emptyBuildingData);
    }

};
}  // namespace Dumux
#endif
