// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::StaggeredAxisDataHandler
 */
#ifndef DUMUX_STAGGERED_AXIS_DATA_HANDLER_HH
#define DUMUX_STAGGERED_AXIS_DATA_HANDLER_HH

#include <dumux/discretization/staggered/freeflow/geometryhelper/geometryhelperutility.hh>
#include <dumux/discretization/staggered/freeflow/mystaggeredgeometryhelper.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/geometryhelpergenericmethods.hh>

namespace Dumux {

/*!
 * \ingroup StaggeredDiscretization
 * \brief Helper class constructing the dual grid finite volume geometries
 *        for the free flow staggered discretization method.
 */
template <class GridView, class IntersectionMapper, class AD, class GP>
class StaggeredAxisDataHandler {
    using Scalar = typename GridView::ctype;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    using ScvGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim, dimWorld>;
    using ScvfGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim - 1, dimWorld>;

    using GlobalPosition = typename ScvGeometry::GlobalCoordinate;

    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;

    using ReferenceElements = typename Dune::ReferenceElements<Scalar, dim>;
    using GridIndexType = typename GridView::IndexSet::IndexType;

   private:
    const GridView gridView_;
    std::shared_ptr<const IntersectionMapper> intersectionMapper_;
    const Element element_;
    Intersection intersection_;
    GeometryHelperUtility<GridView> utility_;
    AD axisData_;
    GP oppositePosition_;

   public:
    StaggeredAxisDataHandler(const GeometryHelperUtility<GridView>& utility,
                                const GridView& gridView,
      std::shared_ptr<const IntersectionMapper> intersectionMapper,
      const Element& element,
      Intersection& intersection,
      AD& axisData,
      GP& oppositePosition)
        : gridView_(gridView),
          intersectionMapper_(intersectionMapper),
          element_(element),
          intersection_(intersection),
          utility_(utility),
          axisData_(axisData),
          oppositePosition_(oppositePosition) {}

    AD getAxisData(){
        return axisData_;
    }

    GP getOppositePosition(){
        return oppositePosition_;
    }

    /*!
     * \brief Fills all entries of the in axis data
     */
    void fillAxisData_() {
        // Clear the containers before filling them
        axisData_.inAxisForwardDofs.clear();
        axisData_.inAxisBackwardDofs.clear();
        axisData_.inAxisForwardDistances.clear();
        axisData_.inAxisBackwardDistances.clear();

        // Set the self Dof
        const auto inIdx = intersectionMapper_->isIndexInInside(intersection_);
        axisData_.selfDof = intersectionMapper_->globalIntersectionIndex(element_, inIdx);

        // Set the opposite Dof
        fillOppositeDofs_();
    }

        void fillOppositeDofs_(){
        const auto localOppoIndices = utility_.localOppositeIndices_(intersectionMapper_->isIndexInInside(intersection_), element_);

        if (!intersection_.neighbor() || (element_.level() >= intersection_.outside().level()))
        {
//             !intersection_.neighbor()
//           ------------/      ------------/
//             |       ||/        |       ||/
//             |  in   ||/      --|  in   ||/
//             |       ||/        |       ||/
//           ------------/      ------------/
//
//             /: boundary
//             ||: intersection_

//             inLevel > outLevel
//             -----------------        -----------------
//             |   |in||       |        |-|-|in||       |
//             |--------  out  |        |--------  out  |
//             |   |   |       |        |   |   |       |
//             -----------------        -----------------
//
//             ||: intersection_

//             inLevel = outLevel
//           -------------------      -------------------
//             |      ||       |        |      ||       |
//             |  in  ||  out  |      --|  in  ||  out  |
//             |      ||       |        |      ||       |
//           -------------------      -------------------
//
//             ||: intersection_

            treatOppoInNotCoarserOut_(localOppoIndices);
        }
        else
        {
            if (localOppoIndices.size() == 1)
            {
//            -->*
//             -----------------
//             |      ||out|   |
//            --> in   |--------
//             |       |   |   |
//             -----------------
//             ||: intersection_
//             -->*: a velocity value from above is considered for the interpolation

                treatOppoInCoarserOutOneLocalOppo_(localOppoIndices);
            }
            else if (localOppoIndices.size() == 2)
            {
//             -----------------
//            -->     ||out|   |
//             |  in   |--------
//            -->      |   |   |
//             -----------------
//             ||: intersection_

                treatOppoInCoarserOutTwoLocalOppo_(localOppoIndices);
            }
            else
            {
                std::cout << "localOppositeIndices should be one or two. If there is a problem here please check if you are grading too extensively. E.g. if the smallest cell size is around 1e-10 this might not be a good idea." << std::endl;
            }
        }
    }

    void treatOppoInCoarserOutTwoLocalOppo_(const std::vector<int>& localOppoIndices)
    {
        auto& oppoDofs = axisData_.oppositeDofs;
        auto& oppoDofsInterp = axisData_.oppositeDofsInterpolationFactors;

        oppoDofs.clear();
        oppoDofsInterp.clear();

        for (const auto& localOppoIndex : localOppoIndices)
        {
            const auto& oppoFacet = utility_.getFacet_(localOppoIndex, element_);

            //TODO adapt to 3d
            unsigned int nonDirectionIdx = (utility_.directionIndex() == 0) ? 1 : 0;

            if (scalarCmp(oppoFacet.geometry().center()[nonDirectionIdx], intersection_.geometry().center()[nonDirectionIdx]))
            {
                oppositePosition_ = oppoFacet.geometry().center();
                oppoDofs.push_back(intersectionMapper_->globalIntersectionIndex(element_, localOppoIndex));
                oppoDofsInterp.push_back(1.);
            }
        }
    }

    void treatOppoInNotCoarserOut_(const std::vector<int>& localOppoIndices)
    {
        auto& oppoDofs = axisData_.oppositeDofs;
        auto& oppoDofsInterp = axisData_.oppositeDofsInterpolationFactors;

        oppoDofs.resize(localOppoIndices.size());
        for (unsigned int i = 0; i < localOppoIndices.size(); ++i)
        {
            const auto& oppoFacet = utility_.getFacet_(localOppoIndices[i], element_);
            oppoDofs[i] = intersectionMapper_->globalIntersectionIndex(element_, localOppoIndices[i]);
            oppositePosition_[utility_.directionIndex()] = oppoFacet.geometry().center()[utility_.directionIndex()];
            for (const auto nonDirectionIdx : utility_.nonDirectionIndices())
            {
                oppositePosition_[nonDirectionIdx] = intersection_.geometry().center()[nonDirectionIdx];
            }
        }

        if (oppoDofs.size() == 1)
        {
            oppoDofsInterp.resize(oppoDofs.size());
            oppoDofsInterp[0] = 1.0;
        }
        else if (oppoDofs.size() == 2)
        {
            oppoDofsInterp.resize(oppoDofs.size());
            oppoDofsInterp[0] = 0.5;
            oppoDofsInterp[1] = 0.5;
        }
        else
        {
            DUNE_THROW(Dune::InvalidStateException, "Expected oppoDofs.size to either be 1 or 2 but is not the case. If there is a problem here please check if you are grading too extensively. E.g. if the smallest cell size is around 1e-10 this might not be a good idea.");
        }
    }

    void treatOppoInCoarserOutOneLocalOppo_(const std::vector<int>& localOppoIndices)
    {
        auto& oppoDofs = axisData_.oppositeDofs;
        auto& oppoDofsInterp = axisData_.oppositeDofsInterpolationFactors;

        oppoDofs.clear();
        oppoDofsInterp.clear();

        const auto& oppoFacet = utility_.getFacet_(localOppoIndices[0], element_);
        oppositePosition_[utility_.directionIndex()] = oppoFacet.geometry().center()[utility_.directionIndex()];
        for (const auto nonDirectionIdx : utility_.nonDirectionIndices())
        {
            oppositePosition_[nonDirectionIdx] = intersection_.geometry().center()[nonDirectionIdx];
        }

        oppoDofs.push_back(intersectionMapper_->globalIntersectionIndex(element_, localOppoIndices[0]));

        for (const auto& elementIs : intersections(gridView_, element_))
        {
            if (utility_.haveNonDirectionCornerCenterMatch_(elementIs) && utility_.elementFacetIsNormal_(intersectionMapper_->isIndexInInside(intersection_), intersectionMapper_->isIndexInInside(elementIs)))
            {
                if (elementIs.neighbor())
                {
                    auto minusNormal = intersection_.centerUnitOuterNormal();
                    minusNormal *= (-1);

                    if (elementIs.outside().level() == element_.level())
                    {
//             above here there can be a coarse cell with one velocity on the left, a coarse cell with two velocities on the left or four fine cells
//             in any of the cases, only the lower left velocity is considered
//             #       |
//             *********--------
//             |      ||out|   |
//             +      ||   |   |
//             |      ||   |   |
//            --> in   |--------
//             |       |   |   |
//             |       |   |   |
//             |       |   |   |
//             -----------------
//             ||: intersection_
//             **: elementIs
//             #: neighborElemIntersection
//             +: interpolate a velocity here

                        for (const auto& neighborElemIntersection : intersections(gridView_, elementIs.outside()))
                        {
                            if (containerCmp(neighborElemIntersection.centerUnitOuterNormal(), minusNormal) && haveCommonNonDirectionalCornerCoordinate_(neighborElemIntersection, intersection_))
                            {
                                oppoDofs.push_back(intersectionMapper_->globalIntersectionIndex(elementIs.outside(), intersectionMapper_->isIndexInInside(neighborElemIntersection)));

                                Scalar distanceSelf = 0.5 * intersection_.geometry().volume(); //positive sign here is just a choice;//this is one quater of the opposite intersection, because the opposite intersection is twice the size of the self intersection in 2D
                                Scalar distanceClose = 0.5 * intersection_.geometry().volume() + 0.5 * neighborElemIntersection.geometry().volume(); //negative sign here is just a choice

                                Scalar interpFactSelf = distanceClose / (distanceSelf + distanceClose);
                                Scalar interpFactClose = distanceSelf / (distanceSelf + distanceClose);

                                oppoDofsInterp.push_back(interpFactSelf);
                                oppoDofsInterp.push_back(interpFactClose);
                            }
                        }
                    }
                    else if (elementIs.outside().level() > element_.level())
                    {
//             |   |   |
//             ****-****--------
//             |      ||out|   |
//            --> in   |--------
//             |       |   |   |
//             -----------------
//             ||: intersection_
//             **: element intersections
                        if (!haveCommonCorner(elementIs, intersection_) )
                        {
//             #   |   |
//             ****-------------
//             |      ||out|   |
//            --> in   |--------
//             |       |   |   |
//             -----------------
//             ||: intersection_oppoDofs
//             **: elementIs
//              #: neighbor element intersection
                            for (const auto& neighborElemIntersection : intersections(gridView_, elementIs.outside()))
                            {
                                if (containerCmp(neighborElemIntersection.centerUnitOuterNormal(), minusNormal))
                                {
                                    oppoDofs.push_back(intersectionMapper_->globalIntersectionIndex(elementIs.outside(), intersectionMapper_->isIndexInInside(neighborElemIntersection)));

                                    Scalar distanceSelf = 0.5 * intersection_.geometry().volume(); //positive sign here is just a choice;
                                    Scalar distanceClose = 0.5 * intersection_.geometry().volume() + 0.5 * neighborElemIntersection.geometry().volume(); //negative sign here is just a choice

                                    Scalar interpFactSelf = distanceClose / (distanceSelf + distanceClose);
                                    Scalar interpFactClose = distanceSelf / (distanceSelf + distanceClose);

                                    oppoDofsInterp.push_back(interpFactSelf);
                                    oppoDofsInterp.push_back(interpFactClose);
                                }
                            }
                        }
                    }
                    else
                    {
                        //          ________________________________________________________________________
                        //         |                 |                 |                                   h
                        //         |                 |                 |                                   h
                        //         |                 |                 |                                   h
                        //         |                 |                 |                                   h
                        //         |                 |                 |                                   h
                        //         |_________________|_________________|                                   h
                        //         |                 |                 n                                   h
                        //         |                 |                 n                                   h
                        //         |                 |                 n                                   h
                        //         |                 |                 n    element.outside                h
                        //         |                 |                 n                                   h
                        //         |_________________|_________________n==================_________________h
                        //         |                 |        |inter-  I                 |                 |
                        //         |                 |        |section.I                 |                 |
                        //         |                 |        |outside I                 |                 |
                        //         |                 |‾‾‾‾‾‾‾‾|‾‾‾‾‾‾‾‾|     element_    |                 |
                        //         |                 |        |        |                 |                 |
                        //         |_________________|________|________|_________________|_________________|
                        //         |                 |                 |                 |                 |
                        //         |                 |                 |                 |                 |
                        //         |                 |                 |                 |                 |
                        //         |                 |                 |                 |                 |
                        //         |                 |                 |                 |                 |
                        //         |_________________|_________________|_________________|_________________|

                        //I: intersection_.geometry().volume()
                        //n: neighborElemIntersection.geometry().volume()
                        //=: elementIs

                        if(intersection_.neighbor())
                        {
                            //check if difference of elementIs.outside().level() and intersection_.outside().level() is >= 2
                            if(element_.level() < intersection_.outside().level())
                            {
                                //placerholder value for interpFactorSelf
                                oppoDofsInterp.push_back(0.0);

                                Scalar intersectionNormalCounter = 0.0; //count the number of oppoDofs/intersections with the same normal vector as intersection_
                                Scalar minusNormalCounter = 0.0;        //count intersections with opposite normal vector as intersection_
                                auto intersectionNormal = intersection_.centerUnitOuterNormal();
                                for (const auto& neighborElemIntersection : intersections(gridView_, elementIs.outside()))
                                    { //in top neighboring elem we need dofs on all parallel intersections (parallel to intersection_) not just the one on the opposite
                                    //allow both directions for parallel normal vector of intersections (minus (opposite) and plus (in direction) of intersection_ normal
                                        if (containerCmp(neighborElemIntersection.centerUnitOuterNormal(), minusNormal)
                                            || containerCmp(neighborElemIntersection.centerUnitOuterNormal(), intersectionNormal))
                                        {
                                            if(containerCmp(neighborElemIntersection.centerUnitOuterNormal(), minusNormal))
                                            {
                                                minusNormalCounter += 1.0; //count intersections with opposite normal vector as intersection_
                                            }

                                            if(containerCmp(neighborElemIntersection.centerUnitOuterNormal(), intersectionNormal))
                                            {
                                                intersectionNormalCounter += 1.0; //count the number of oppoDofs/intersections with the same normal vector as intersection_
                                            }
                                        }
                                    }

                                    Scalar lengthForInterpFactorSelf = 0.0;

                                    for (const auto& neighborElemIntersection : intersections(gridView_, elementIs.outside()))
                                    {
                                        Scalar interpFactor = 0.0;
                                        if (containerCmp(neighborElemIntersection.centerUnitOuterNormal(), minusNormal)
                                            || containerCmp(neighborElemIntersection.centerUnitOuterNormal(), intersectionNormal))
                                        {
                                            //oppoDofSelf was pushed back in the beginning of this whole function, need to assign a interpFactor to it
                                            if (containerCmp(neighborElemIntersection.centerUnitOuterNormal(), intersectionNormal))
                                            {
                                                oppoDofs.push_back(intersectionMapper_->globalIntersectionIndex(elementIs.outside(), intersectionMapper_->isIndexInInside(neighborElemIntersection)));

                                                if(intersectionNormalCounter == 1) //should never occur, not permitted to levels difference for non-diagonals
                                                {
                                                    interpFactor = 0.5*(1.0 - (0.5*intersection_.geometry().volume() + 0.5*neighborElemIntersection.geometry().volume())/(intersection_.geometry().volume() + 0.5*neighborElemIntersection.geometry().volume()));
                                                }
                                                else if(intersectionNormalCounter == 2) //two intersections on the left side of coarse cell
                                                {
                                                    lengthForInterpFactorSelf = neighborElemIntersection.geometry().volume();

                                                    //interpFactor 0.5 from presequent interpolation already integrated into factor 0.25
                                                    interpFactor = 0.25*(1.0 - (0.5*intersection_.geometry().volume() + neighborElemIntersection.geometry().volume())/(intersection_.geometry().volume() + neighborElemIntersection.geometry().volume()));
                                                }

                                                oppoDofsInterp.push_back(interpFactor);
                                            }
                                            else if(containerCmp(neighborElemIntersection.centerUnitOuterNormal(), minusNormal))
                                            {
                                                oppoDofs.push_back(intersectionMapper_->globalIntersectionIndex(elementIs.outside(), intersectionMapper_->isIndexInInside(neighborElemIntersection)));

                                                if(minusNormalCounter == 1) //one intersection on the right side of coarse cell
                                                {
                                                    interpFactor = 0.5*(1.0 - (0.5*intersection_.geometry().volume() + 0.5*neighborElemIntersection.geometry().volume())/(intersection_.geometry().volume() + 0.5*neighborElemIntersection.geometry().volume()));
                                                }
                                                else if(minusNormalCounter == 2) //two intersections on the right side of coarse cell
                                                {
                                                    //interpFactor 0.5 from presequent interpolation already integrated into factor 0.25
                                                    interpFactor = 0.25*(1.0 - (0.5*intersection_.geometry().volume() + neighborElemIntersection.geometry().volume())/(intersection_.geometry().volume() + neighborElemIntersection.geometry().volume()));
                                                }

                                                oppoDofsInterp.push_back(interpFactor);
                                            }
                                        }
                                    }

                                    Scalar interpFactorSelf = 0.0;
                                    interpFactorSelf = 1.0 - (0.5*intersection_.geometry().volume())/(intersection_.geometry().volume() + lengthForInterpFactorSelf);

                                    //insert right value for placeholder at [0] for self
                                    oppoDofsInterp[0] = interpFactorSelf;
                            }
                        }
                    }
                }
                else
                {
//             /////////
//             *********--------
//             |      ||out|   |
//            --> in   |--------
//             |       |   |   |
//             -----------------
//             ||: intersection_
//             **: elementIs
//             //: boundary

                    //get Dirichlet boundary value and weight it by 0.5
                    oppoDofs.push_back(-1);
                    oppoDofsInterp.push_back(0.5);
                    oppoDofsInterp.push_back(0.5);
                }
            }
        }
    }


    bool haveCommonNonDirectionalCornerCoordinate_(const Intersection& isA, const Intersection& isB)
    {
        bool commonCoordinates = false;

        const auto nonDirIndices = utility_.nonDirectionIndices();

        if (nonDirIndices.size() != 1)
        {
            DUNE_THROW(Dune::InvalidStateException, "Not yet prepared for 3D.");
        }

        for (const auto nonDirectionIdx : nonDirIndices)
        {
            std::vector<Scalar> isACorners;
            for (unsigned int i = 0; i < isA.geometry().corners(); ++i)
            {
                isACorners.push_back(isA.geometry().corner(i)[nonDirectionIdx]);
            }

            for (unsigned int i = 0; i < isB.geometry().corners(); ++i)
            {
                const auto isBCorner = isB.geometry().corner(i)[nonDirectionIdx];

                if (scalarFind_(isACorners.begin(), isACorners.end(), isBCorner) != isACorners.end())
                {
                    commonCoordinates = true;
                }
            }
        }

        return commonCoordinates;
    }
};

}  // namespace Dumux
#endif
