// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::CVDFreeFlowStaggeredGeometryHelper
 */
#ifndef DUMUX_STAGGERED_NORMAL_VOLUME_VARS_HANDLER_HH
#define DUMUX_STAGGERED_NORMAL_VOLUME_VARS_HANDLER_HH

#include <dumux/discretization/staggered/freeflow/staggeredgeometryhelper.hh>
#include <dumux/discretization/staggered/freeflow/mystaggeredgeometryhelper.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/geometryhelperutility.hh>

namespace Dumux {

/*!
 * \ingroup StaggeredDiscretization
 * \brief Helper class constructing the dual grid finite volume geometries
 *        for the free flow staggered discretization method.
 */
template <class GridView, class IntersectionMapper, class MyVolVarsData>
class StaggeredNormalVolumeVarsHandler {
    using Scalar = typename GridView::ctype;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    using ScvGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim, dimWorld>;
    using ScvfGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim - 1, dimWorld>;

    using GlobalPosition = typename ScvGeometry::GlobalCoordinate;

    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;

    using ReferenceElements = typename Dune::ReferenceElements<Scalar, dim>;
    using GridIndexType = typename GridView::IndexSet::IndexType;

    //TODO include assert that checks for quad geometry
    static constexpr int numPairs = 2 * (dimWorld - 1);
    static int order_;


    public:
     StaggeredNormalVolumeVarsHandler(const GeometryHelperUtility<GridView>& utility, const GridView& gridView, std::shared_ptr<const IntersectionMapper> intersectionMapper, const Element& element, Intersection& intersection, MyVolVarsData& data, bool linearInterpolation)
         : gridView_(gridView),
           intersectionMapper_(intersectionMapper),
           element_(element),
           intersection_(intersection),
           utility_(utility),
           volVarsData_(data),
           linearInterpolation_(linearInterpolation){
           }

    MyVolVarsData getVolVarsData(){
        return volVarsData_;
    }
    /*!
     * \brief Fills all entries of the volume variables of the reference element
     */
    void fillNormalVolVarsData_()
    {
        auto& dofs = volVarsData_.normalVolVarsDofs;
        auto& interpFacts = volVarsData_.normalVolVarsDofsInterpolationFactors;
        auto& buildingData = volVarsData_.normalVolVarsScvfBuildingData;

        for (auto& dofVect : dofs)
            dofVect.clear();

        for (auto& interpFactsVect : interpFacts)
            interpFactsVect.clear();

       for (auto& buildingDataVect : buildingData)
            buildingDataVect.clear();

        if (!linearInterpolation_)
        {
            DUNE_THROW(Dune::InvalidStateException, "NormalVolVarsData not prepared for quadratic interpolation.");
        }

        ScvfBuildingData emptyBuildingData;
        emptyBuildingData.eIdx = -1;
        emptyBuildingData.localScvfIdx = -1;

        int numPairParallelIdx = 0;

        for(const auto& normalIs : intersections(gridView_, element_))
        {
            if(utility_.elementFacetIsNormal_(intersectionMapper_->isIndexInInside(normalIs), intersectionMapper_->isIndexInInside(intersection_)))
            {
                if (!intersection_.neighbor() || (element_.level() >= intersection_.outside().level()))//scvf is the edge of an element
                {
                    if( normalIs.neighbor())
                    {
                        const auto insideLevel = normalIs.inside().level();
                        const auto outsideLevel = normalIs.outside().level();
                        if (insideLevel == outsideLevel) {
                            for (const auto& parallelIs : intersections(gridView_, normalIs.outside())) {
                                if (containerCmp(parallelIs.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal()) && haveCommonCorner(parallelIs, intersection_)) {
                                    if (!parallelIs.neighbor() || parallelIs.inside().level() >= parallelIs.outside().level()) {
                                        handleCases1and4(numPairParallelIdx, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                                    } else /*parallelIs.inside().level() < parallelIs.outside().level()*/
                                    {
                                        handleCase8(numPairParallelIdx, parallelIs, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                                    }
                                }
                            }
                        } else if (insideLevel < outsideLevel) {
                            handleCase2(numPairParallelIdx, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                        } else  //insideLevel > outsideLevel
                        {
                            if (haveCommonCorner(normalIs.outside(), intersection_)) {
                                for (const auto& parallelIs : intersections(gridView_, normalIs.outside())) {
                                    if (containerCmp(parallelIs.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal()) && haveCommonCorner(parallelIs, intersection_)) {
                                        if (!parallelIs.neighbor()) {
                                            handleCases3aAnd3b(numPairParallelIdx, parallelIs, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                                        } else if (parallelIs.inside().level() == parallelIs.outside().level()) {
                                            handleCase3c(numPairParallelIdx, parallelIs, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                                        } else if (parallelIs.inside().level() > parallelIs.outside().level()) {
                                            handleCase3d(numPairParallelIdx, parallelIs, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                                        } else /*parallelIs.inside().level() < parallelIs.outside().level()*/
                                        {
                                            handleCase7(numPairParallelIdx, parallelIs, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                                        }
                                    }
                                }
                            } else {
                                for (const auto& parallelIs : intersections(gridView_, normalIs.outside())) {
                                    auto minusIntersectionNormal = intersection_.centerUnitOuterNormal();
                                    minusIntersectionNormal *= -1.;

                                    if (containerCmp(parallelIs.centerUnitOuterNormal(), minusIntersectionNormal)) {
                                        if (!parallelIs.neighbor() || parallelIs.inside().level() == parallelIs.outside().level()) {
                                            handleCase9(numPairParallelIdx, parallelIs, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                                        } else if (parallelIs.inside().level() < parallelIs.outside().level()) {
                                            handleCase11(numPairParallelIdx, parallelIs, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                                        } else /*parallelIs.inside().level() > parallelIs.outside().level()*/
                                        {
                                            handleCase10(numPairParallelIdx, parallelIs, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else //!normalIs.neighbor()
                    {
                        //Dirichlet & non-dirichlet cases
                        handleCase5and6(numPairParallelIdx, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                    }
                }
                else if (!utility_.haveNonDirectionCornerCenterMatch_(normalIs)) /*intersection_.neighbor() && (element_.level() < intersection_.outside().level()) && !utility_.haveNonDirectionCornerCenterMatch_(normalIs)*/ //scvf is half edge of an element and localSubFaceIdx determines that normal vol var at end of edge closer to element center
                {
                    if (normalIs.neighbor())
                    {
                        if (normalIs.inside().level() == normalIs.outside().level())
                        {
                            handleCase12(numPairParallelIdx, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                        }
                        else if (normalIs.inside().level() < normalIs.outside().level())
                        {
                            handleCase13(numPairParallelIdx, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                        }
                        else /*normalIs.inside().level() > normalIs.outside().level()*/
                        {
                            handleCase14(numPairParallelIdx, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                        }
                    }
                    else //!normalIs.neighbor()
                    {
                        //Dirichlet and non-Dirichlet cases
                        handleCases15and16(numPairParallelIdx, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                    }
                }
                else /*intersection_.neighbor() && (element_.level() < intersection_.outside().level()) && utility_.haveNonDirectionCornerCenterMatch_(normalIs)*/ //scvf is half edge of an element and localSubFaceIdx determines that normal vol var at end of edge further from element center
                {
                    if (normalIs.neighbor())
                    {
                        if (normalIs.inside().level() == normalIs.outside().level())
                        {
                            for(const auto& parallelIs : intersections(gridView_, normalIs.outside()))
                            {
                                if ( containerCmp(parallelIs.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal()) )
                                {
                                    if (parallelIs.neighbor() && parallelIs.inside().level() < parallelIs.outside().level())
                                    {
                                        handleCase17(numPairParallelIdx, parallelIs, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                                        break;
                                    }
                                    else
                                    {
                                        handleCase18(numPairParallelIdx, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                                    }
                                }
                            }
                        }
                        else if (normalIs.inside().level() < normalIs.outside().level())
                        {
                            bool isTwoLevelDiagonal = false;
                            Intersection parallelIs;

                            for(const auto& normalIsVar : intersections(gridView_, element_))
                            {
                                if(containerCmp(normalIsVar.centerUnitOuterNormal(),normalIs.centerUnitOuterNormal()))
                                {
                                    for(const auto& parallelIsVar : intersections(gridView_, normalIsVar.outside()))
                                    {
                                        if ( containerCmp(parallelIsVar.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal()) && haveCommonCorner(parallelIsVar, intersection_))
                                        {
                                            parallelIs = parallelIsVar;

                                            if (parallelIsVar.neighbor() && parallelIsVar.inside().level() < parallelIsVar.outside().level())
                                            {
                                                isTwoLevelDiagonal = true;
                                            }
                                        }
                                    }
                                }
                            }

                            if (!isTwoLevelDiagonal)
                            {
                                handleCase19(numPairParallelIdx, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                            }
                            else
                            {
                                handleCase20(numPairParallelIdx, parallelIs, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                            }
                        }
                        else /*normalIs.inside().level() > normalIs.outside().level()*/
                        {
                            handleCase21(numPairParallelIdx, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                        }
                    }
                    else //!normalIs.neighbor()
                    {
                        //Dirichlet and non-Dirichlet cases
                        handleCases22and23(numPairParallelIdx, normalIs, dofs, interpFacts, buildingData, emptyBuildingData);
                    }
                }
            }
        }
    }

    private:
    const GridView gridView_;
    std::shared_ptr<const IntersectionMapper> intersectionMapper_;
    const Element element_;
    Intersection intersection_;
    GeometryHelperUtility<GridView> utility_;
    MyVolVarsData volVarsData_;
    bool linearInterpolation_;

    void handleCases1and4(int& numPairParallelIdx,
                          const Intersection& normalIs,
                          std::array<std::vector<int>, 2>& dofs,
                          std::array<std::vector<Scalar>, 2>& interpFacts,
                          std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                          ScvfBuildingData& emptyBuildingData) {
        /*1*/
        // -----------------
        // |       |       |
        // |       |   *1  |
        // |       |       |
        // -----------------
        //         ||      |
        //         ||      |
        //         ||      |
        //         ---------
        //
        // ||: intersection_
        // *: dof (with interpolation factor)

        /*4*/
        // ---------
        // |       |
        // |       |----
        // |       | *1|
        // -------------
        //         ||  |
        //         -----
        //
        // ||: intersection_
        // *: dof (with interpolation factor)
        // +: interpolate location
        // may: here may or may not be a horizontal grid line
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(normalIs.outside()));
        interpFacts[numPairParallelIdx].push_back(1.);
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        ++numPairParallelIdx;
    }

    void handleCase8(int& numPairParallelIdx,
                          const Intersection& parallelIs,
                          const Intersection& normalIs,
                          std::array<std::vector<int>, 2>& dofs,
                          std::array<std::vector<Scalar>, 2>& interpFacts,
                          std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                          ScvfBuildingData& emptyBuildingData) {
        /*8*/
        // -------------
        // |   |       |
        // -----   *a  |
        // |   |   +   |
        // -------------
        //     ||      |
        //     ||  *b  |
        //     ||      |
        //     ---------
        //
        // a=L_d/(L_u+L_d)
        // b=L_u/(L_u+L_d)
        // ||: intersection_
        // *: dof (with interpolation factor)
        // +: interpolate location
        Scalar elementWidthClose = intersection_.geometry().volume();
        Scalar elementWidthClosest = 2 * parallelIs.geometry().volume();

        Scalar distanceClosest = 0.25 * elementWidthClosest;
        Scalar distanceClose = 0.25 * elementWidthClosest + 0.5 * elementWidthClose;

        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(element_));            //close
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(normalIs.outside()));  //closest

        interpFacts[numPairParallelIdx].push_back(distanceClosest / (distanceClosest + distanceClose));  //close
        interpFacts[numPairParallelIdx].push_back(distanceClose / (distanceClosest + distanceClose));    //closest

        buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);

        ++numPairParallelIdx;
    }

    void handleCase2(int& numPairParallelIdx,
                const Intersection& normalIs,
                std::array<std::vector<int>, 2>& dofs,
                std::array<std::vector<Scalar>, 2>& interpFacts,
                std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                ScvfBuildingData& emptyBuildingData) {
        /*2*/
        // -------------
        // |     |     |
        // -------------
        // |  *.5+  *.5|
        // -------------
        // ||          |
        // ||          |
        // ||          |
        // -------------
        //
        // ||: intersection_
        // *: dof (with interpolation factor)
        // +: interpolate location
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(normalIs.outside()));
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        interpFacts[numPairParallelIdx].push_back(.5);

        if (dofs[numPairParallelIdx].size() == 2)
            ++numPairParallelIdx;
    }

    void handleCases3aAnd3b(int& numPairParallelIdx,
                const Intersection& parallelIs,
                const Intersection& normalIs,
                std::array<std::vector<int>, 2>& dofs,
                std::array<std::vector<Scalar>, 2>& interpFacts,
                std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                ScvfBuildingData& emptyBuildingData) {
        /*3a*/  //Dirichlet
                //   |
                //  /|--------
                //  /|       |
                //  /*.5 *.5 |
                //  /|       |
                //  /---------
                //   ||  |
                //   -----
                //
        /*3b*/  //non-Dirichlet
                //   |
                //  /---------
                //  /|       |
                //  /|   *1  |
                //  /|       |
                //  /---------
                //   ||  |
                //   -----
                //
                // ||: intersection_
                // *: dof (with interpolation factor)
                // +: interpolate location
                // may: here may or may not be a horizontal grid line
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(normalIs.outside()));
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        interpFacts[numPairParallelIdx].push_back(.5);

        dofs[numPairParallelIdx].push_back(-1);
        ScvfBuildingData parallelIsBuildingData;
        parallelIsBuildingData.eIdx = gridView_.indexSet().index(parallelIs.inside());
        parallelIsBuildingData.localScvfIdx = intersectionMapper_->isIndexInInside(parallelIs);
        buildingData[numPairParallelIdx].push_back(parallelIsBuildingData);
        interpFacts[numPairParallelIdx].push_back(.5);

        ++numPairParallelIdx;
    }

    void handleCase3c(int& numPairParallelIdx,
                const Intersection& parallelIs,
                const Intersection& normalIs,
                std::array<std::vector<int>, 2>& dofs,
                std::array<std::vector<Scalar>, 2>& interpFacts,
                std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                ScvfBuildingData& emptyBuildingData) {
        /*3c*/
        //   Ll       Lr
        //|-------|-------|
        //
        //        |
        //-----------------
        //|       |       |
        //|   *a  | + *b  |
        //|       |       |
        //-----------------
        //        ||  |
        //        -----
        //
        // ||: intersection_
        // *: dof (with interpolation factor)
        // +: interpolate location
        // may: here may or may not be a horizontal grid line
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(parallelIs.outside()));  //a
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(normalIs.outside()));    //b

        buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);

        auto minusNormalNormal = normalIs.centerUnitOuterNormal();
        minusNormalNormal *= -1.;

        Scalar Ll = 0.;
        for (const auto& testIs : intersections(gridView_, parallelIs.outside()))
            if (containerCmp(minusNormalNormal, testIs.centerUnitOuterNormal()))
                Ll += testIs.geometry().volume();

        Scalar Lr = 2 * normalIs.geometry().volume();

        Scalar dr = 0.25 * Lr;
        Scalar dl = 0.25 * Lr + 0.5 * Ll;

        interpFacts[numPairParallelIdx].push_back(dr / (dl + dr));  //a
        interpFacts[numPairParallelIdx].push_back(dl / (dl + dr));  //b

        ++numPairParallelIdx;
    }

    void handleCase3d(int& numPairParallelIdx,
                const Intersection& parallelIs,
                const Intersection& normalIs,
                std::array<std::vector<int>, 2>& dofs,
                std::array<std::vector<Scalar>, 2>& interpFacts,
                std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                ScvfBuildingData& emptyBuildingData) {
        /*3d*/
        //       L_l         L_r
        //|---------------|-------|
        //
        //|               |   *b  |   |
        //|               |       |   |
        //|       *c      |--------   |
        //|               |       |   |
        //|               | + *b  |   | L_u
        //|               |       |   |
        //-------------------------   -
        //                ||*a|       | L_d
        //                -----       -
        //
        //
        // ||: intersection_
        // *: dof (with interpolation factor)
        // +: interpolate location
        // may: here may or may not be a horizontal grid line
        Intersection farAwayParallelIs;
        for (const auto& farAwayNormalIs : intersections(gridView_, normalIs.outside()))
            if (containerCmp(normalIs.centerUnitOuterNormal(), farAwayNormalIs.centerUnitOuterNormal()))
                for (const auto& testIs : intersections(gridView_, farAwayNormalIs.outside()))
                    if (containerCmp(testIs.centerUnitOuterNormal(), parallelIs.centerUnitOuterNormal()))
                        farAwayParallelIs = testIs;

        auto minusNormalNormal = normalIs.centerUnitOuterNormal();
        minusNormalNormal *= -1.;

        Scalar Ll = 0.;
        for (const auto& testIs : intersections(gridView_, parallelIs.outside()))
            if (containerCmp(minusNormalNormal, testIs.centerUnitOuterNormal()))
                Ll += testIs.geometry().volume();

        Scalar Lr = 2 * normalIs.geometry().volume();
        Scalar Lu = 2 * parallelIs.geometry().volume();
        Scalar Ld = intersection_.geometry().volume();

        Scalar du = 0.25 * Lu;
        Scalar dd = 0.25 * Lu + 0.5 * Ld;
        Scalar dr = 0.25 * Lr;
        Scalar dl = 0.25 * Lr + 0.5 * Ll;

        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(element_));                    //a
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(parallelIs.inside()));         //b
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(farAwayParallelIs.inside()));  //b
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(parallelIs.outside()));        //c

        buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);

        interpFacts[numPairParallelIdx].push_back(du / (du + dd));                        //a
        interpFacts[numPairParallelIdx].push_back(.5 * dd / (du + dd) * dl / (dl + dr));  //b
        interpFacts[numPairParallelIdx].push_back(.5 * dd / (du + dd) * dl / (dl + dr));  //b
        interpFacts[numPairParallelIdx].push_back(dd / (du + dd) * dr / (dl + dr));       //c

        ++numPairParallelIdx;
    }

    void handleCase7(int& numPairParallelIdx,
                     const Intersection& parallelIs,
                     const Intersection& normalIs,
                     std::array<std::vector<int>, 2>& dofs,
                     std::array<std::vector<Scalar>, 2>& interpFacts,
                     std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                     ScvfBuildingData& emptyBuildingData) {
        /*7*/
        //  L_l    L_r
        // |---|-------|
        // -------------   -
        // | *b|       |   |
        // tesIs   *a  |   | L_u
        // | *b| +     |   |
        // -------------   -
        //     ||*c|   |   | L_d
        //     ---------   -
        //     |   |   |
        //     ---------
        // ||: intersection_
        // *: dof (with interpolation factor)
        // +: interpolate location
        Scalar Ld = intersection_.geometry().volume();
        Scalar Lu = 2 * parallelIs.geometry().volume();
        Scalar Lr = 2 * normalIs.geometry().volume();

        Scalar Ll = 0.;
        Intersection parallelIs2;
        for (const auto& testIs : intersections(gridView_, parallelIs.outside()))
            if (containerCmp(testIs.centerUnitOuterNormal(), normalIs.centerUnitOuterNormal()))
                Ll = testIs.geometry().volume();

        for (const auto& otherIs : intersections(gridView_, parallelIs.inside()))
            if (containerCmp(otherIs.centerUnitOuterNormal(), parallelIs.centerUnitOuterNormal()) && !containerCmp(otherIs.geometry().center(), parallelIs.geometry().center()))
                parallelIs2 = otherIs;

        Scalar du = 0.25 * Lu;
        Scalar dd = 0.25 * Lu + 0.5 * Ld;
        Scalar dr = 0.25 * Lr;
        Scalar dl = 0.25 * Lr + 0.5 * Ll;

        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(normalIs.outside()));     //a
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(parallelIs.outside()));   //b
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(parallelIs2.outside()));  //b
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(element_));               //c

        buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);

        interpFacts[numPairParallelIdx].push_back(dl / (dl + dr) * dd / (du + dd));        //a
        interpFacts[numPairParallelIdx].push_back(dr / (dl + dr) * dd / (du + dd) * 0.5);  //b
        interpFacts[numPairParallelIdx].push_back(dr / (dl + dr) * dd / (du + dd) * 0.5);  //b
        interpFacts[numPairParallelIdx].push_back(du / (du + dd));                         //c

        ++numPairParallelIdx;
    }

    void handleCase9(int& numPairParallelIdx,
                     const Intersection& parallelIs,
                     const Intersection& normalIs,
                     std::array<std::vector<int>, 2>& dofs,
                     std::array<std::vector<Scalar>, 2>& interpFacts,
                     std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                     ScvfBuildingData& emptyBuildingData) {
        /*9*/
        // -----------------
        // |       |       |
        // |   *a+ |   *b  |
        // |       |       |
        // -----------------
        // |   ||  |
        // ---------
        // |   |   |
        // ---------
        //
        // a=L_r/(L_l+L_r)
        // b=L_l/(L_l+L_r)
        // ||: intersection_
        // *: dof (with interpolation factor)
        // +: interpolate location
        Scalar elementWidthClose = 0.;

        if (parallelIs.neighbor()) {
            for (const auto& parallelToNormalIs : intersections(gridView_, parallelIs.outside())) {
                auto minusNormalIntersectionNormal = normalIs.centerUnitOuterNormal();
                minusNormalIntersectionNormal *= -1.;

                if (containerCmp(parallelToNormalIs.centerUnitOuterNormal(), minusNormalIntersectionNormal))
                    elementWidthClose += parallelToNormalIs.geometry().volume();
            }
        }

        Scalar elementWidthClosest = 2. * normalIs.geometry().volume();

        Scalar distanceClosest = 0.25 * elementWidthClosest;
        Scalar distanceClose = 0.25 * elementWidthClosest + 0.5 * elementWidthClose;

        if (!parallelIs.neighbor()) {
            dofs[numPairParallelIdx].push_back(-1);  //close

            ScvfBuildingData parallelIsBuildingData;
            parallelIsBuildingData.eIdx = gridView_.indexSet().index(parallelIs.inside());
            parallelIsBuildingData.localScvfIdx = intersectionMapper_->isIndexInInside(parallelIs);

            buildingData[numPairParallelIdx].push_back(parallelIsBuildingData);
        } else {
            dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(parallelIs.outside()));  //close
            buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        }
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(normalIs.outside()));  //closest
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);

        interpFacts[numPairParallelIdx].push_back(distanceClosest / ((distanceClosest + distanceClose)));  //close
        interpFacts[numPairParallelIdx].push_back(distanceClose / (distanceClosest + distanceClose));      //closest

        ++numPairParallelIdx;
    }

    void handleCase11(int& numPairParallelIdx,
                      const Intersection& parallelIs,
                      const Intersection& normalIs,
                      std::array<std::vector<int>, 2>& dofs,
                      std::array<std::vector<Scalar>, 2>& interpFacts,
                      std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                      ScvfBuildingData& emptyBuildingData) {
        /*11*/
        // -------------
        // |       | *b|
        // |   *a+ -----
        // |       | *b|
        // -------------
        // |   ||  |
        // ---------
        //
        // a=L_r/(L_l+L_r)
        // b=L_l/(2*(L_l+L_r))
        // ||: intersection_
        // *: dof (with interpolation factor)
        // +: interpolate location
        //getting here twice fill closest as well as interpolation factors only the first time, increment numPairParallelIdx only the second time
        if (dofs[numPairParallelIdx].size() == 0) {
            Scalar elementWidthClose = 0.;

            for (const auto& parallelToNormalIs : intersections(gridView_, parallelIs.outside())) {
                auto minusNormalIntersectionNormal = normalIs.centerUnitOuterNormal();
                minusNormalIntersectionNormal *= -1.;

                if (containerCmp(parallelToNormalIs.centerUnitOuterNormal(), minusNormalIntersectionNormal))
                    elementWidthClose += parallelToNormalIs.geometry().volume();
            }

            Scalar elementWidthClosest = 2. * normalIs.geometry().volume();

            Scalar distanceClosest = 0.25 * elementWidthClosest;
            Scalar distanceClose = 0.25 * elementWidthClosest + 0.5 * elementWidthClose;

            dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(normalIs.outside()));    //closest
            dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(parallelIs.outside()));  //close
            buildingData[numPairParallelIdx].push_back(emptyBuildingData);
            buildingData[numPairParallelIdx].push_back(emptyBuildingData);

            interpFacts[numPairParallelIdx].push_back(distanceClose / (distanceClosest + distanceClose));          //closest
            interpFacts[numPairParallelIdx].push_back(distanceClosest / ((distanceClosest + distanceClose) * 2));  //close
            interpFacts[numPairParallelIdx].push_back(distanceClosest / ((distanceClosest + distanceClose) * 2));  //close
        } else {
            dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(parallelIs.outside()));  //close
            buildingData[numPairParallelIdx].push_back(emptyBuildingData);
            ++numPairParallelIdx;
        }
    }

    void handleCase10(int& numPairParallelIdx,
                      const Intersection& parallelIs,
                      const Intersection& normalIs,
                      std::array<std::vector<int>, 2>& dofs,
                      std::array<std::vector<Scalar>, 2>& interpFacts,
                      std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                      ScvfBuildingData& emptyBuildingData) {
        /*10*/
        //         ________________________________________________________________________
        //         |                 |                 |                                   |
        //         |                 |                 |                                   |
        //         |                 |        *        |                                   |
        //         |                 |sameSizedElement |                                   |
        //         |_________________|_________________|                                   |
        //         |                 |                 h                 *                 |
        //         |                 |     closest     h            largerElement          |
        //         |                 |        *    +   h                                   |
        //         |                 |     element     h                                   |
        //         |                 |                 h                                   |
        //         |_________________|___======nnnnnnnn_wwwwwwwwwwwwwwwwww_________________|
        //         |                 |   =    |smal=lers                 |                 |
        //         |                 |   =    |    *   |                 |                 |
        //         |                 |   =    |elem=ents                 |                 |
        //         |                 |___===========___s                 |                 |
        //         |                 |        |        |                 |                 |
        //         |                 |        |        |                 |                 |
        //         |_________________|________|________|_________________|_________________|
        //         |                 |                 |                 |                 |
        //         |                 |                 |                 |                 |
        //         |                 |                 |                 |                 |
        //         |                 |                 |                 |                 |
        //         |                 |                 |                 |                 |
        //         |_________________|_________________|_________________|_________________|

        // n: borderToSmallerLength, Int_{Self} in report
        // h: borderToLargerLength, Int_{Self,N} in report
        // s: smallerLonelyLength, 2*Int_{neighb,N} in report
        // w: largerLonelyLength, Int_{neighb} in report
        // closestElement, Phi_2 in report
        // smallerElement, Phi_1 in report
        // sameSizedElement, Phi_3 in report
        // largerElement, Phi_4, report
        // =: control volume, n replaces = at times
        // *: dof
        // +: interpolate location
        Scalar smallerLonelyLength = intersection_.geometry().volume();
        Scalar borderToSmallerLength = normalIs.geometry().volume();
        Scalar borderToLargerLength = parallelIs.geometry().volume();

        Scalar largerLonelyLength = 0.;
        for (const auto& parallelToNormalIs : intersections(gridView_, parallelIs.outside())) {
            auto minusNormalIntersectionNormal = normalIs.centerUnitOuterNormal();
            minusNormalIntersectionNormal *= -1.;

            if (containerCmp(parallelToNormalIs.centerUnitOuterNormal(), minusNormalIntersectionNormal)) {
                largerLonelyLength = parallelToNormalIs.geometry().volume();
                break;
            }
        }

        Element smallerElement = element_;
        Element closestElement = normalIs.outside();
        Element largerElement = parallelIs.outside();

        Element sameSizedElement;
        for (const auto& oppositeToNormalIs : intersections(gridView_, normalIs.outside())) {
            if (containerCmp(oppositeToNormalIs.centerUnitOuterNormal(), normalIs.centerUnitOuterNormal())) {
                sameSizedElement = oppositeToNormalIs.outside();
            }
        }

        utility_.fillIsTwoLevelDiagonal_(dofs[numPairParallelIdx], interpFacts[numPairParallelIdx], closestElement, smallerElement, sameSizedElement, largerElement, borderToLargerLength, borderToSmallerLength, smallerLonelyLength, largerLonelyLength, buildingData[numPairParallelIdx]);

        ++numPairParallelIdx;
    }

    void handleCase5and6(int& numPairParallelIdx,
                      const Intersection& normalIs,
                      std::array<std::vector<int>, 2>& dofs,
                      std::array<std::vector<Scalar>, 2>& interpFacts,
                      std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                      ScvfBuildingData& emptyBuildingData) {
        /*5*/
        // Dirichlet
        // /////////
        // ----*----
        // ||  1   |
        // ||      |
        // ||      |
        // ---------
        //
        // ||: intersection_
        // *: position of taken value (with interpolation factor)

        /*6*/
        // non-Dirichlet
        // /////////
        // ---------
        // ||      |
        // ||  *1  |
        // ||      |
        // ---------
        //
        // ||: intersection_
        // *: dof (with interpolation factor)
        dofs[numPairParallelIdx].push_back(-1);
        interpFacts[numPairParallelIdx].push_back(1.);

        ScvfBuildingData normalIsBuildingData;
        normalIsBuildingData.eIdx = gridView_.indexSet().index(normalIs.inside());
        normalIsBuildingData.localScvfIdx = intersectionMapper_->isIndexInInside(normalIs);

        buildingData[numPairParallelIdx].push_back(normalIsBuildingData);

        ++numPairParallelIdx;
    }

    void handleCase12(int& numPairParallelIdx,
                      const Intersection& normalIs,
                      std::array<std::vector<int>, 2>& dofs,
                      std::array<std::vector<Scalar>, 2>& interpFacts,
                      std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                      ScvfBuildingData& emptyBuildingData) {
        /*12*/
        // -----------------
        // |   |   ||      |
        // ---------   *a  |
        // |   |   |   +   |
        // -----------------
        //         |       |
        //         |   *b  |
        //         |       |
        //         ---------
        //
        // a=L_d/(L_u+L_d)
        // b=L_u/(L_u+L_d)
        // ||: intersection_
        // *: dof (with interpolation factor)
        // +: interpolate location
        Scalar elementWidthClosest = 2 * intersection_.geometry().volume();

        Scalar elementWidthClose = 0.;
        for (const auto& parallelIs : intersections(gridView_, normalIs.outside())) {
            if (containerCmp(parallelIs.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal())) {
                elementWidthClose += parallelIs.geometry().volume();
            }
        }

        Scalar distanceClosest = 0.25 * elementWidthClosest;
        Scalar distanceClose = 0.25 * elementWidthClosest + 0.5 * elementWidthClose;

        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(normalIs.outside()));  //close
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(element_));            //closest

        buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);

        interpFacts[numPairParallelIdx].push_back(distanceClosest / (distanceClosest + distanceClose));  //close
        interpFacts[numPairParallelIdx].push_back(distanceClose / (distanceClosest + distanceClose));    //closest

        ++numPairParallelIdx;
    }

    void handleCase13(int& numPairParallelIdx,
                      const Intersection& normalIs,
                      std::array<std::vector<int>, 2>& dofs,
                      std::array<std::vector<Scalar>, 2>& interpFacts,
                      std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                      ScvfBuildingData& emptyBuildingData) {
        /*13*/
        // -----------------
        // |   |   ||      |
        // ---------   *a  |
        // |   |   |   +   |
        // -----------------
        //         | *b| *b|
        //         ---------
        //         |   |   |
        //         ---------
        //
        // a=L_d/(L_u+L_d)
        // b=L_u/(2(L_u+L_d))
        // ||: intersection_
        // *: dof (with interpolation factor)
        // +: interpolate location
        if (dofs[numPairParallelIdx].size() == 0) {
            Scalar elementWidthClosest = 2 * intersection_.geometry().volume();

            Scalar elementWidthClose = 0.;
            for (const auto& parallelIs : intersections(gridView_, normalIs.outside())) {
                if (containerCmp(parallelIs.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal())) {
                    elementWidthClose += parallelIs.geometry().volume();
                }
            }

            Scalar distanceClosest = 0.25 * elementWidthClosest;
            Scalar distanceClose = 0.25 * elementWidthClosest + 0.5 * elementWidthClose;

            dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(element_));            //closest
            dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(normalIs.outside()));  //close

            buildingData[numPairParallelIdx].push_back(emptyBuildingData);
            buildingData[numPairParallelIdx].push_back(emptyBuildingData);

            interpFacts[numPairParallelIdx].push_back(distanceClose / (distanceClosest + distanceClose));          //closest
            interpFacts[numPairParallelIdx].push_back(distanceClosest / ((distanceClosest + distanceClose) * 2));  //close
            interpFacts[numPairParallelIdx].push_back(distanceClosest / ((distanceClosest + distanceClose) * 2));  //close
        } else {
            dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(normalIs.outside()));  //close
            buildingData[numPairParallelIdx].push_back(emptyBuildingData);

            ++numPairParallelIdx;
        }
    }

    void handleCase14(int& numPairParallelIdx,
                      const Intersection& normalIs,
                      std::array<std::vector<int>, 2>& dofs,
                      std::array<std::vector<Scalar>, 2>& interpFacts,
                      std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                      ScvfBuildingData& emptyBuildingData) {
        /*14*/
        //         ________________________________________________________________________
        //         |                 |                 |                                   |
        //         |                 |                 |                                   |
        //         |                 |                 |                                   |
        //         |                 |sameSize*dElement|                                   |
        //         |                 |                 |                                   |
        //         |_________________|_________________|                                   |
        //         |                 |                 h          larger*Element           |
        //         |                 |                 h                                   |
        //         |                 |         closest h                                   |
        //         |                 =========*elem+enth                                   |
        //         |                 =        =        h                                   |
        //         |_________________=________=nnnnnnnn_wwwwwwwwwwwwwwwwww_________________|
        //         |                 =        =smaller s                 |                 |
        //         |                 ==========elem*ents                 |                 |
        //         |                 |________|________s                 |                 |
        //         |                 |        |        |                 |                 |
        //         |                 |        |        |                 |                 |
        //         |_________________|________|________|_________________|_________________|
        //         |                 |                 |                 |                 |
        //         |                 |                 |                 |                 |
        //         |                 |                 |                 |                 |
        //         |                 |                 |                 |                 |
        //         |                 |                 |                 |                 |
        //         |_________________|_________________|_________________|_________________|

        // n: borderToSmallerLength, Int_{Self} in report
        // h: borderToLargerLength, Int_{Self,N} in report
        // s: smallerLonelyLength, 2*Int_{neighb,N} in report
        // w: largerLonelyLength, Int_{neighb} in report
        // closestElement, Phi_2 in report
        // smallerElement, Phi_1 in report
        // sameSizedElement, Phi_3 in report
        // largerElement, Phi_4, report
        // =: control volume, n replaces = at times
        Scalar borderToSmallerLength = intersection_.geometry().volume();
        Scalar borderToLargerLength = normalIs.geometry().volume();

        Scalar largerLonelyLength = 0.;
        for (const auto& parallelIs : intersections(gridView_, normalIs.outside())) {
            if (containerCmp(parallelIs.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal())) {
                largerLonelyLength = parallelIs.geometry().volume();
                break;
            }
        }

        Scalar smallerLonelyLength = 0.;
        Element smallerElement = {};

        for (const auto& outerNormalIs : intersections(gridView_, intersection_.outside())) {
            if (containerCmp(outerNormalIs.centerUnitOuterNormal(), normalIs.centerUnitOuterNormal())) {
                smallerLonelyLength = outerNormalIs.geometry().volume();
                smallerElement = outerNormalIs.outside();
            }
        }
        //
        Element closestElement = element_;
        Element largerElement = normalIs.outside();

        Element sameSizedElement = {};
        for (const auto& oppositeIs : intersections(gridView_, element_)) {
            auto minusNormal = intersection_.centerUnitOuterNormal();
            minusNormal *= -1.;

            if (containerCmp(oppositeIs.centerUnitOuterNormal(), minusNormal)) {
                sameSizedElement = oppositeIs.outside();
            }
        }

        utility_.fillIsTwoLevelDiagonal_(dofs[numPairParallelIdx], interpFacts[numPairParallelIdx], closestElement, smallerElement, sameSizedElement, largerElement, borderToLargerLength, borderToSmallerLength, smallerLonelyLength, largerLonelyLength, buildingData[numPairParallelIdx]);

        ++numPairParallelIdx;
    }

    void handleCases15and16(int& numPairParallelIdx,
                            const Intersection& normalIs,
                            std::array<std::vector<int>, 2>& dofs,
                            std::array<std::vector<Scalar>, 2>& interpFacts,
                            std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                            ScvfBuildingData& emptyBuildingData) {
        /*15*/
        // Dirichlet
        // /////////
        // ----*.5------------
        // |   +   |    |    |
        // |   *.5 -----------
        // |      ||    |    |
        // -------------------
        //
        // ||: intersection_
        // *: position of taken value (with interpolation factor)

        /*16*/
        // non Dirichlet
        // /////////
        // -------------------
        // |   +   |    |    |
        // |   *1  -----------
        // |      ||    |    |
        // -------------------
        //
        // ||: intersection_
        // *: position of taken value (with interpolation factor)
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(element_));
        interpFacts[numPairParallelIdx].push_back(.5);
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);

        dofs[numPairParallelIdx].push_back(-1);
        interpFacts[numPairParallelIdx].push_back(.5);

        ScvfBuildingData normalIsBuildingData;
        normalIsBuildingData.eIdx = gridView_.indexSet().index(normalIs.inside());
        normalIsBuildingData.localScvfIdx = intersectionMapper_->isIndexInInside(normalIs);

        buildingData[numPairParallelIdx].push_back(normalIsBuildingData);

        ++numPairParallelIdx;
    }

    void handleCase17(int& numPairParallelIdx,
                      const Intersection& parallelIs,
                      const Intersection& normalIs,
                      std::array<std::vector<int>, 2>& dofs,
                      std::array<std::vector<Scalar>, 2>& interpFacts,
                      std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                      ScvfBuildingData& emptyBuildingData) {
        /*17*/
        // -----------------
        // |   |   |       |
        // ---------   *a  |
        // |   |   ||      |
        // -----------------
        // |   |   |   +   |
        // ---------   *b  |
        // |   |   |       |
        // -----------------
        //
        // a=L_d/(L_u+L_d)
        // b=L_u/(L_u+L_d)
        // ||: intersection_
        // *: dof (with interpolation factor)
        // +: interpolate location
        Scalar elementWidthClose = 2 * intersection_.geometry().volume();
        Scalar elementWidthClosest = 2 * parallelIs.geometry().volume();

        Scalar distanceClosest = 0.25 * elementWidthClosest;
        Scalar distanceClose = 0.25 * elementWidthClosest + 0.5 * elementWidthClose;

        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(element_));            //close
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(normalIs.outside()));  //closest

        interpFacts[numPairParallelIdx].push_back(distanceClosest / (distanceClosest + distanceClose));  //close
        interpFacts[numPairParallelIdx].push_back(distanceClose / (distanceClosest + distanceClose));    //closest

        buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);

        ++numPairParallelIdx;
    }

    void handleCase18(int& numPairParallelIdx,
                      const Intersection& normalIs,
                      std::array<std::vector<int>, 2>& dofs,
                      std::array<std::vector<Scalar>, 2>& interpFacts,
                      std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                      ScvfBuildingData& emptyBuildingData) {
        /*18*/
        // -----------------
        // |   |   |       |
        // ---------       |
        // |   |   ||      |
        // -----------------
        // |       |       |
        // |       |   *1  |
        // |       |       |
        // -----------------
        //
        // ||: intersection_
        // *: dof (with interpolation factor)
        // +: interpolate location
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(normalIs.outside()));
        interpFacts[numPairParallelIdx].push_back(1.);
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        ++numPairParallelIdx;
    }

    void handleCase19(int& numPairParallelIdx,
                      const Intersection& normalIs,
                      std::array<std::vector<int>, 2>& dofs,
                      std::array<std::vector<Scalar>, 2>& interpFacts,
                      std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                      ScvfBuildingData& emptyBuildingData) {
        /*19*/
        // -----------------
        // |   |   |       |
        // ---------       |
        // |   |   ||      |
        // -----------------
        //         : *a+ *a|
        //         ---------
        //         |   |   |
        //         ---------
        //
        // : parallelIs
        // a = .5
        // || intersection_
        // * dof (with interpolation factor)
        // + interpolate location
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(normalIs.outside()));
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        interpFacts[numPairParallelIdx].push_back(.5);

        if (dofs[numPairParallelIdx].size() == 2)
            ++numPairParallelIdx;
    }

    void handleCase20(int& numPairParallelIdx,
                      const Intersection& parallelIs,
                      const Intersection& normalIs,
                      std::array<std::vector<int>, 2>& dofs,
                      std::array<std::vector<Scalar>, 2>& interpFacts,
                      std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                      ScvfBuildingData& emptyBuildingData) {
        /*20*/
        //         ________________________________________________________________________
        //         |                 |                 |                                   |
        //         |                 |                 |                                   |
        //         |                 |                 |                                   |
        //         |                 |                 |                                   |
        //         |                 |                 |                                   |
        //         |_________________|_________________|                                   |
        //         |                 |                 ||               * a                |
        //         |                 |                 ||                                  |
        //         |                 |                 ||                                  |
        //         |                 |                 ||                                  |
        //         |                 |                 ||                                  |
        //         |_________________|_________________||__________________________________|
        //         |                 |        |        |                 |                 |
        //         |                 |        |        |                 +                 |
        //         |                 |________|________|         b       |         b       |
        //         |                 |        |        |        * mean   |        * mean   |
        //         |                 |        |        |mid point by this|mid point by this|
        //         |_________________|________|________|_________________|_________________|
        //         |                 |                 |                 |                 |
        //         |                 |                 |                 |                 |
        //         |                 |                 |                 |                 |
        //         |                 |                 |                 |                 |
        //         |                 |                 |                 |                 |
        //         |_________________|_________________|_________________|_________________|
        // a=L_d/(L_u+L_d)
        // b=L_u/(2(L_u+L_d))
        // || intersection_
        // * dof (with interpolation factor)
        // + interpolate location
        if (dofs[numPairParallelIdx].size() == 0) {
            Scalar elementWidthClosest = 2 * parallelIs.geometry().volume();
            Scalar elementWidthClose = 2 * intersection_.geometry().volume();

            Scalar distanceClosest = 0.25 * elementWidthClosest;
            Scalar distanceClose = 0.25 * elementWidthClosest + 0.5 * elementWidthClose;

            dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(element_));            //close
            dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(normalIs.outside()));  //closest

            interpFacts[numPairParallelIdx].push_back(distanceClosest / (distanceClosest + distanceClose));      //close
            interpFacts[numPairParallelIdx].push_back(distanceClose / ((distanceClosest + distanceClose) * 2));  //closest
            interpFacts[numPairParallelIdx].push_back(distanceClose / ((distanceClosest + distanceClose) * 2));  //closest

            buildingData[numPairParallelIdx].push_back(emptyBuildingData);
            buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        } else {
            dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(normalIs.outside()));  //closest
            buildingData[numPairParallelIdx].push_back(emptyBuildingData);
            ++numPairParallelIdx;
        }
    }

    void handleCase21(int& numPairParallelIdx,
                      const Intersection& normalIs,
                      std::array<std::vector<int>, 2>& dofs,
                      std::array<std::vector<Scalar>, 2>& interpFacts,
                      std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                      ScvfBuildingData& emptyBuildingData) {
        /*21*/
        //                                  L_l                         L_r
        //                           |-----------------|-----------------------------------|
        //         ________________________________________________________________________   _ _
        //         |                 |                 |                                   |   |
        //         |                 |                 |                                   |   |
        //         |                 |                 |                                   |   |
        //         |                 |                 |                                   |   |
        //         |                 |                 |                                   |   |
        //         |_________________|_________________|                                   |   | L_u
        //         |                 |                 |                * a                |   |
        //         |                 |                 |                                   |   |
        //         |                 |                 |                                   |   |
        //         |                 |        *c       |        +                          |   |
        //         |                 |                 |                                   |   |
        //         |_________________|_________________|___________________________________|  _|_
        //         |                 |        |        ||                |                 |   |
        //         |                 |        |        ||                |                 |   |
        //         |                 |________|________||        b       |         b       |   | L_d
        //         |                 |        |        |        *        |        *        |   |
        //         |                 |        |        |                 |                 |   |
        //         |_________________|________|________|_________________|_________________|  _|_
        //         |                 |                 |                 |                 |
        //         |                 |                 |                 |                 |
        //         |                 |                 |                 |                 |
        //         |                 |                 |                 |                 |
        //         |                 |                 |                 |                 |
        //         |_________________|_________________|_________________|_________________|
        // a=d_d/(d_u+d_d)*d_l/(d_l+d_r)
        // b=d_u/(2(d_u+d_d))*d_l/(d_l+d_r)
        // c=d_r/(d_l+d_r)
        // || intersection_
        // * dof (with interpolation factor)
        // + interpolate location
        Scalar Ld = 2 * intersection_.geometry().volume();
        Scalar Lr = 2 * normalIs.geometry().volume();

        Scalar Lu = 0.;
        Element elementC = {};
        for (const auto& parallelIs : intersections(gridView_, normalIs.outside())) {
            if (containerCmp(parallelIs.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal())) {
                Lu += parallelIs.geometry().volume();

                if (haveCommonCorner(parallelIs, intersection_)) {
                    elementC = parallelIs.outside();
                }
            }
        }

        Scalar Ll = 0.;
        for (const auto& otherNormalIs : intersections(gridView_, intersection_.outside())) {
            if (containerCmp(otherNormalIs.centerUnitOuterNormal(), normalIs.centerUnitOuterNormal())) {
                Ll = 2 * otherNormalIs.geometry().volume();
            }
        }

        Element oppositeElement = {};
        for (const auto& oppoIs : intersections(gridView_, element_)) {
            auto minusNormal = intersection_.centerUnitOuterNormal();
            minusNormal *= -1.;

            if (containerCmp(oppoIs.centerUnitOuterNormal(), minusNormal)) {
                oppositeElement = oppoIs.outside();
            }
        }

        Scalar du = 0.25 * Lu;
        Scalar dd = 0.25 * Lu + 0.5 * Ld;
        Scalar dr = 0.25 * Lr;
        Scalar dl = 0.25 * Lr + 0.5 * Ll;

        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(element_));            //b
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(oppositeElement));     //b
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(normalIs.outside()));  //a
        dofs[numPairParallelIdx].push_back(gridView_.indexSet().index(elementC));            //c

        interpFacts[numPairParallelIdx].push_back(du / ((du + dd) * 2) * dl / (dl + dr));  //b
        interpFacts[numPairParallelIdx].push_back(du / ((du + dd) * 2) * dl / (dl + dr));  //b
        interpFacts[numPairParallelIdx].push_back(dd / (du + dd) * dl / (dl + dr));        //a
        interpFacts[numPairParallelIdx].push_back(dr / (dl + dr));                         //c

        buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);
        buildingData[numPairParallelIdx].push_back(emptyBuildingData);

        ++numPairParallelIdx;
    }

    void handleCases22and23(int& numPairParallelIdx,
                      const Intersection& normalIs,
                      std::array<std::vector<int>, 2>& dofs,
                      std::array<std::vector<Scalar>, 2>& interpFacts,
                      std::array<std::vector<ScvfBuildingData>, 2>& buildingData,
                      ScvfBuildingData& emptyBuildingData) {
        /*22*/
        // Dirichlet
        // /////////
        // ----*1-------------
        // |      ||    |    |
        // |       -----------
        // |       |    |    |
        // -------------------
        //
        // ||: intersection_
        // *: position of taken value (with interpolation factor)

        /*23*/
        // non Dirichlet
        // /////////
        // ----+--------------
        // |      ||    |    |
        // |   *1  -----------
        // |       |    |    |
        // -------------------
        //
        // ||: intersection_
        // *: position of taken value (with interpolation factor)
        dofs[numPairParallelIdx].push_back(-1);
        interpFacts[numPairParallelIdx].push_back(1.);

        ScvfBuildingData normalIsBuildingData;
        normalIsBuildingData.eIdx = gridView_.indexSet().index(normalIs.inside());
        normalIsBuildingData.localScvfIdx = intersectionMapper_->isIndexInInside(normalIs);

        buildingData[numPairParallelIdx].push_back(normalIsBuildingData);

        ++numPairParallelIdx;
    }
};

}  // namespace Dumux
#endif
