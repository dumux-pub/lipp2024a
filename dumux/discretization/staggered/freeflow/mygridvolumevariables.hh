// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::MyStaggeredGridVolumeVariables
 */
#ifndef DUMUX_DISCRETIZATION_STAGGERED_GRID_MYVOLUMEVARIABLES_HH
#define DUMUX_DISCRETIZATION_STAGGERED_GRID_MYVOLUMEVARIABLES_HH

#include <dune/common/exceptions.hh>

//! make the local view function available whenever we use this class
#include <dumux/discretization/localview.hh>
#include <dumux/discretization/staggered/elementsolution.hh>
#include <dumux/discretization/staggered/freeflow/elementvolumevariables1.hh>
#include <dumux/discretization/staggered/freeflow/gridandelemvolvarsfunctions.hh>

namespace Dumux {
template<class P, class PVV, class DVV, class VV, class GV, class SCV, class SCVF>
struct MyStaggeredGridDefaultGridVolumeVariablesTraits
{
    using Problem = P;
    using VolumeVariables = VV;
    using PrimaryVariables = typename VV::PrimaryVariables;
    using GridView = GV;
    using SubControlVolume = SCV;
    using SubControlVolumeFace = SCVF;
    using DualCVsVolVars = DVV;
    using PrimalCVsVolVars = PVV;

    template<class GridVolumeVariables, bool cachingEnabled>
    using LocalView = MyStaggeredElementVolumeVariables<GridVolumeVariables, cachingEnabled>;
};

/*!
 * \ingroup StaggeredDiscretization
 * \brief Grid volume variables class for staggered models
 */
template<class Traits, bool cachingEnabled>
class MyStaggeredGridVolumeVariables;

/*!
 * \ingroup StaggeredDiscretization
 * \brief Grid volume variables class for staggered models.
          Specialization in case of storing the volume variables
 */
template<class Traits>
class MyStaggeredGridVolumeVariables<Traits, /*cachingEnabled*/true>
{
    using ThisType = MyStaggeredGridVolumeVariables<Traits, true>;
    using Problem = typename Traits::Problem;
    using PrimaryVariables = typename Traits::VolumeVariables::PrimaryVariables;
    using ElementSolution = StaggeredElementSolution<PrimaryVariables>;
    using GridView = typename Traits::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = typename GridView::ctype;

    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr int numPairs = 2 * (dimWorld - 1);

public:
    using SubControlVolume = typename Traits::SubControlVolume;
    using SubControlVolumeFace = typename Traits::SubControlVolumeFace;

    //! export the type of the indices
    using Indices = typename Traits::VolumeVariables::Indices;

    //! export the type of the VolumeVariables
    using VolumeVariables = typename Traits::VolumeVariables;
    using PrimalCVsVolVars = typename Traits::PrimalCVsVolVars;
    using DualCVsVolVars = typename Traits::DualCVsVolVars;

    //! make it possible to query if caching is enabled
    static constexpr bool cachingEnabled = true;

    //! export the type of the local view
    using LocalView = typename Traits::template LocalView<ThisType, cachingEnabled>;

    MyStaggeredGridVolumeVariables(const Problem& problem) : problemPtr_(&problem) {}

    //! Update all volume variables
    template<class GridGeometry, class SolutionVector>
    void update(const GridGeometry& gridGeometry, const SolutionVector& sol)
    {
        auto numScv = gridGeometry.numScv();
        auto numScvf = gridGeometry.numScvf();
        auto numBoundaryScvf = gridGeometry.numBoundaryScvf();

        cellCenterVolumeVariables_.resize(numScv + numBoundaryScvf);
        dualCVsVolVars_.resize(numScvf + numBoundaryScvf);
        primalCVsFluxesVolVars_.resize(numScv);

        for (const auto& element : elements(gridGeometry.gridView()))
        {
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                // construct a privars object from the cell center solution vector
                const auto& cellCenterPriVars = sol[scv.dofIndex()];
                PrimaryVariables priVars = makePriVarsFromCellCenterPriVars<PrimaryVariables>(cellCenterPriVars);

                auto elemSol = elementSolution<typename GridGeometry::LocalView>(std::move(priVars));

                std::vector<ElementSolution> elemSols;
                elemSols.push_back(elemSol);
                std::vector<Element> elements;
                elements.push_back(element);
                std::vector<SubControlVolume> scvs;
                scvs.push_back(scv);
                std::vector<Scalar> interpolationFactors;
                interpolationFactors.push_back(1.);

                cellCenterVolumeVariables_[scv.dofIndex()].adaptiveUpdate(elemSols, problem(), elements, scvs, interpolationFactors);
            }

            // handle the boundary volume variables
            for (auto&& scvf : scvfs(fvGeometry))
            {
                // if we are not on a boundary, skip the rest
                if (!scvf.boundary())
                    continue;

                const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
                auto boundaryPriVars = getBoundaryPriVars(problem(), sol[scvf.insideScvIdx()], scvf, cellCenterVolumeVariables_[0]/*only workaround to get a type*/);
                const auto elemSol = elementSolution<typename GridGeometry::LocalView>(std::move(boundaryPriVars));

                std::vector<ElementSolution> elemSols;
                elemSols.push_back(elemSol);
                std::vector<Element> elements;
                elements.push_back(element);
                std::vector<SubControlVolume> insideScvs;
                insideScvs.push_back(insideScv);
                std::vector<Scalar> interpolationFactors;
                interpolationFactors.push_back(1.);

                cellCenterVolumeVariables_[scvf.outsideScvIdx()].adaptiveUpdate(elemSols, problem(), elements, insideScvs, interpolationFactors);
            }

            for (auto&& scv : scvs(fvGeometry))
            {
                for (auto&& scvf : scvfs(fvGeometry))
                {
                    using CellCenterPrimaryVariables = typename SolutionVector::value_type;

                    std::vector<CellCenterPrimaryVariables> insideScvSols;
                    for (unsigned int i = 0; i < scvf.volVarsData().innerVolVarsDofs.size(); ++i)
                    {
                        const auto scvDof = scvf.volVarsData().innerVolVarsDofs[i];

                        if (scvDof < 0)
                        {
                            const auto& scvfBuildingData = scvf.volVarsData().innerVolVarsScvfBuildingData[i];
                            SubControlVolumeFace boundaryFace = fvGeometry.gridGeometry().scvf(scvfBuildingData.eIdx, scvfBuildingData.localScvfIdx);
                            insideScvSols.push_back(sol[boundaryFace.insideScvIdx()]);
                        }
                        else
                        {
                            insideScvSols.push_back(sol[scvDof]);
                        }
                    }

                    std::vector<CellCenterPrimaryVariables> outsideScvSols;
                    for (unsigned int i = 0; i < scvf.volVarsData().outerVolVarsDofs.size(); ++i)
                    {
                        const auto scvDof = scvf.volVarsData().outerVolVarsDofs[i];

                        if (scvDof < 0)
                        {
                            const auto& scvfBuildingData = scvf.volVarsData().outerVolVarsScvfBuildingData[i];
                            SubControlVolumeFace boundaryFace = fvGeometry.gridGeometry().scvf(scvfBuildingData.eIdx, scvfBuildingData.localScvfIdx);
                            outsideScvSols.push_back(sol[boundaryFace.insideScvIdx()]);
                        }
                        else
                        {
                            outsideScvSols.push_back(sol[scvDof]);
                        }
                    }

                    std::array<std::vector<CellCenterPrimaryVariables>, SubControlVolumeFace::numPairs> normalFaceOutsideSols;
                    for (unsigned int localSubFaceIdx = 0; localSubFaceIdx < SubControlVolumeFace::numPairs; ++localSubFaceIdx)
                    {
                        for (unsigned int i = 0; i < scvf.volVarsData().normalVolVarsDofs[localSubFaceIdx].size(); ++i)
                        {
                            const auto scvDof = scvf.volVarsData().normalVolVarsDofs[localSubFaceIdx][i];

                            if (scvDof < 0)
                            {
                                const auto& scvfBuildingData = scvf.volVarsData().normalVolVarsScvfBuildingData[localSubFaceIdx][i];
                                SubControlVolumeFace boundaryFace = fvGeometry.gridGeometry().scvf(scvfBuildingData.eIdx, scvfBuildingData.localScvfIdx);
                                normalFaceOutsideSols[localSubFaceIdx].push_back(sol[boundaryFace.insideScvIdx()]);
                            }
                            else
                            {
                                normalFaceOutsideSols[localSubFaceIdx].push_back(sol[scvDof]);
                            }
                        }
                    }

                    const Problem& problem = *problemPtr_;

                    updateDual(dualCVsVolVars_[scvf.index()].inside, dualCVsVolVars_[scvf.index()].virtualOppo, dualCVsVolVars_[scvf.index()].normal, gridGeometry,  insideScvSols, normalFaceOutsideSols, scvf, problem);
                    updatePrimal(primalCVsFluxesVolVars_[scv.dofIndex()][scvf.localFaceIdx()], gridGeometry, insideScvSols, outsideScvSols, scvf, problem);
                }
            }
        }
    }

//illustration of, why there need to exist volVars that depend not only on the scv but on the scvf and that differ inner and outer(localSubFaceIdx):
// -------------------------
// |     |     |     |     |
// |     |     |     |     |
// |     |     |     |     |
// ---------*******---------
// |     |  *  |  *  |     |
// |     |  *  |  *  |     |
// |     |  *  |  *  |     |
// ---------*******---------
// |     |     ||          |
// |     |     || x        |
// |     |     ||          |
// ------------||          |
// |     |     |           |
// |     |     |           |
// |     |     |           |
// -------------------------
// but
// -------------------------
// |     |     |     |     |
// |     |     |     |     |
// |     |     |     |     |
// -------------------------
// |     |     |     |     |
// |     |     |     |     |
// |     |     |     |     |
// ---------**********------
// |     |  *  ||    *     |
// |     |  *  ||    x     |
// |     |  *  ||    *     |
// ---------**********     |
// |     |     |           |
// |     |     |           |
// |     |     |           |
// -------------------------
//
//  ||: intersection
//  *: control volume
//  x: The position at which the volume variables are taken in the lower right cell, both times x corresponds to the lower right cell and to the same intersection, and the positions are still different depending if the the value is inner or outer.

    const DualCVsVolVars& volVars(const SubControlVolumeFace& scvf) const
    { return dualCVsVolVars_[scvf.index()]; }

    DualCVsVolVars& volVars(const SubControlVolumeFace& scvf)
    { return dualCVsVolVars_[scvf.index()]; }

    const PrimalCVsVolVars& volVars(const std::size_t scvIndex) const
    { return primalCVsFluxesVolVars_[scvIndex]; }

    PrimalCVsVolVars& volVars(const std::size_t scvIndex)
    { return primalCVsFluxesVolVars_[scvIndex]; }

    const VolumeVariables& volVars(const SubControlVolume& scv) const
    { return cellCenterVolumeVariables_[scv.dofIndex()]; }

    VolumeVariables& volVars(const SubControlVolume& scv)
    { return cellCenterVolumeVariables_[scv.dofIndex()]; }

    const Problem& problem() const
    { return *problemPtr_; }

private:
    const Problem* problemPtr_;
    std::vector<VolumeVariables> cellCenterVolumeVariables_;
    std::vector<PrimalCVsVolVars> primalCVsFluxesVolVars_;
    std::vector<DualCVsVolVars> dualCVsVolVars_;
};


/*!
 * \ingroup StaggeredDiscretization
 * \brief Grid volume variables class for staggered models.
          Specialization in case of not storing the volume variables
 */
template<class Traits>
class MyStaggeredGridVolumeVariables<Traits, /*cachingEnabled*/false>
{
    using ThisType = MyStaggeredGridVolumeVariables<Traits, false>;
    using Problem = typename Traits::Problem;

public:
    //! export the type of the VolumeVariables
    using VolumeVariables = typename Traits::VolumeVariables;

    //! make it possible to query if caching is enabled
    static constexpr bool cachingEnabled = false;

    //! export the type of the local view
    using LocalView = typename Traits::template LocalView<ThisType, cachingEnabled>;

    MyStaggeredGridVolumeVariables(const Problem& problem) : problemPtr_(&problem) {}

    template<class GridGeometry, class SolutionVector>
    void update(const GridGeometry& gridGeometry, const SolutionVector& sol) {}

    const Problem& problem() const
    { return *problemPtr_;}

private:

    const Problem* problemPtr_;
};

} // end namespace Dumux

#endif
