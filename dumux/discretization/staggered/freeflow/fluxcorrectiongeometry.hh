// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::StaggeredGridGeometry
 */
#ifndef DUMUX_DISCRETIZATION_STAGGERED_MY_FLUX_CORRECTION_GEOMETRY
#define DUMUX_DISCRETIZATION_STAGGERED_MY_FLUX_CORRECTION_GEOMETRY

namespace Dumux {

/*!
 * \ingroup StaggeredDiscretization
 * \brief Base class for cell center of face specific auxiliary FvGridGeometry classes.
 *        Provides a common interface and a pointer to the actual gridGeometry.
 */
template<class GridGeometry>
class MyStaggeredFreeFlowFluxCorrectionGeometry
{
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;

    using Element = typename GridView::template Codim<0>::Entity;
    using IndexType = std::size_t;

public:
    void update(const GridGeometry& gridGeometry)
    {
        firstOpposingFace_.resize(gridGeometry.numScvf());
        secondOpposingFace_.resize(gridGeometry.numScvf());

        firstNeighborFace_.resize(gridGeometry.numScvf());
        secondNeighborFace_.resize(gridGeometry.numScvf());
        thirdNeighborFace_.resize(gridGeometry.numScvf());
        forthNeighborFace_.resize(gridGeometry.numScvf());

        firstNeighElement_.resize(gridGeometry.numScvf());
        secondNeighElement_.resize(gridGeometry.numScvf());
        thirdNeighElement_.resize(gridGeometry.numScvf());
        forthNeighElement_.resize(gridGeometry.numScvf());

        for(auto&& element: elements(gridGeometry.gridView()))
        {
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bindElement(element);

            for(auto&& scvf : scvfs(fvGeometry))
            {
                //faces for corrections
                const auto eIdx = scvf.insideScvIdx();
                const auto sampleOpposingFace = fvGeometry.scvf(eIdx, scvf.localIndicesOpposingFace()[0]);//any one of the possibly two opposing faces is fine -> sample face
                const bool frontalCorrectionRequired = sampleOpposingFace.neighbor() && (scvf.inside().level() < sampleOpposingFace.outside().level()) && (!scvf.neighbor() || scvf.inside().level() == scvf.outside().level());

                if (frontalCorrectionRequired)
                {
                    firstOpposingFace_[scvf.index()] = sampleOpposingFace;
                    secondOpposingFace_[scvf.index()] = fvGeometry.scvf(eIdx, scvf.localIndicesOpposingFace()[1]);
                }
                else
                {
                    firstOpposingFace_[scvf.index()] = scvf;
                    secondOpposingFace_[scvf.index()] = scvf;
                }

                for (unsigned int localSubFaceIdx = 0; localSubFaceIdx < SubControlVolumeFace::numPairs; localSubFaceIdx++)
                {
                    SubControlVolumeFace sampleNormalFace = gridGeometry.scvf(eIdx, scvf.pairData(localSubFaceIdx).localNormalFluxCorrectionIndex);

                    bool lateralCorrectionRequired = false;

                    if (sampleNormalFace.neighbor())
                    {
                        lateralCorrectionRequired = scvf.inside().level() < sampleNormalFace.outside().level();
                    }

                    if (lateralCorrectionRequired)
                    {
                        //refined neighbor, refinement is always a dividing into two in one direction
                        SubControlVolumeFace normalFace = gridGeometry.scvf(eIdx, scvf.pairData(localSubFaceIdx).localNormalFluxCorrectionIndex);
                        FVElementGeometry fvNormalGeometry(gridGeometry);
                        fvNormalGeometry.bind(normalFace.outside());

                        for (const auto& outerScvf : scvfs(fvNormalGeometry))
                        {
                            const auto& scvfNormal = scvf.unitOuterNormal();
                            auto minusScvfNormal = scvfNormal;
                            minusScvfNormal *= -1.;

                            if (containerCmp(outerScvf.unitOuterNormal(), scvfNormal))
                            {
                                if (localSubFaceIdx == 0)
                                {
                                    firstNeighborFace_[scvf.index()] = outerScvf;
                                    firstNeighElement_[scvf.index()] = normalFace.outside();
                                }
                                else if (localSubFaceIdx == 1)
                                {
                                    thirdNeighborFace_[scvf.index()] = outerScvf;
                                    thirdNeighElement_[scvf.index()] = normalFace.outside();
                                }
                                else
                                {
                                    DUNE_THROW(Dune::InvalidStateException, "not yet prepared for dim neq 2.");
                                }
                            }
                            else if (containerCmp(outerScvf.unitOuterNormal(), minusScvfNormal))
                            {
                                if (localSubFaceIdx == 0)
                                {
                                    secondNeighborFace_[scvf.index()] = outerScvf;
                                    secondNeighElement_[scvf.index()] = normalFace.outside();
                                }
                                else if (localSubFaceIdx == 1)
                                {
                                    forthNeighborFace_[scvf.index()] = outerScvf;
                                    forthNeighElement_[scvf.index()] = normalFace.outside();
                                }
                                else
                                {
                                    DUNE_THROW(Dune::InvalidStateException, "not yet prepared for dim neq 2.");
                                }
                            }
                        }
                    }
                    else
                    {
                        if (localSubFaceIdx == 0)
                        {
                            firstNeighborFace_[scvf.index()] = scvf;
                            secondNeighborFace_[scvf.index()] = scvf;

                            firstNeighElement_[scvf.index()] = element;
                            secondNeighElement_[scvf.index()] = element;
                        }
                        else if (localSubFaceIdx == 1)
                        {
                            thirdNeighborFace_[scvf.index()] = scvf;
                            forthNeighborFace_[scvf.index()] = scvf;

                            thirdNeighElement_[scvf.index()] = element;
                            forthNeighElement_[scvf.index()] = element;
                        }
                        else
                        {
                            DUNE_THROW(Dune::InvalidStateException, "not yet prepared for dim neq 2.");
                        }
                    }
                }
            }
        }
    }

            SubControlVolumeFace firstOpposingFace(const IndexType globalI) const
            {
                return firstOpposingFace_[globalI];
            }

            SubControlVolumeFace secondOpposingFace(const IndexType globalI) const
            {
                return secondOpposingFace_[globalI];
            }

            SubControlVolumeFace firstNeighborFace(const IndexType globalI) const
            {
                return firstNeighborFace_[globalI];
            }
            SubControlVolumeFace secondNeighborFace(const IndexType globalI) const
            {
                return secondNeighborFace_[globalI];
            }
            SubControlVolumeFace thirdNeighborFace(const IndexType globalI) const
            {
                return thirdNeighborFace_[globalI];
            }
            SubControlVolumeFace forthNeighborFace(const IndexType globalI) const
            {
                return forthNeighborFace_[globalI];
            }

            Element firstNeighElement(const IndexType globalI) const
            {
                return firstNeighElement_[globalI];
            }
            Element secondNeighElement(const IndexType globalI) const
            {
                return secondNeighElement_[globalI];
            }

            Element thirdNeighElement(const IndexType globalI) const
            {
                return thirdNeighElement_[globalI];
            }
            Element forthNeighElement(const IndexType globalI) const
            {
                return forthNeighElement_[globalI];
            }

private:
            std::vector<SubControlVolumeFace> firstOpposingFace_;
            std::vector<SubControlVolumeFace> secondOpposingFace_;

            std::vector<SubControlVolumeFace> firstNeighborFace_;
            std::vector<SubControlVolumeFace> secondNeighborFace_;
            std::vector<SubControlVolumeFace> thirdNeighborFace_;
            std::vector<SubControlVolumeFace> forthNeighborFace_;

            std::vector<Element> firstNeighElement_;
            std::vector<Element> secondNeighElement_;
            std::vector<Element> thirdNeighElement_;
            std::vector<Element> forthNeighElement_;
};

} // end namespace

#endif
