// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::MyStaggeredFreeFlowConnectivityMap
 */
#ifndef DUMUX_STAGGERED_FREEFLOW_MY_CONNECTIVITY_MAP_HH
#define DUMUX_STAGGERED_FREEFLOW_MY_CONNECTIVITY_MAP_HH

#include <vector>

namespace Dumux {

/*!
 * \ingroup StaggeredDiscretization
 * \brief Stores the dof indices corresponding to the neighboring cell centers and faces
 *        that contribute to the derivative calculation. Specialization for the staggered free flow model.
 */
template<class GridGeometry>
class MyStaggeredFreeFlowConnectivityMap
{
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;

    using Element = typename GridView::template Codim<0>::Entity;
    using GridIndexType = typename IndexTraits<GridView>::GridIndex;

    using CellCenterIdxType = typename GridGeometry::DofTypeIndices::CellCenterIdx;
    using FaceIdxType = typename GridGeometry::DofTypeIndices::FaceIdx;

    using CellCenterToCellCenterMap = std::vector<std::vector<GridIndexType>>;
    using CellCenterToFaceMap = std::vector<std::vector<GridIndexType>>;
    using FaceToCellCenterMap = std::vector<std::vector<GridIndexType>>;
    using FaceToFaceMap = std::vector<std::vector<GridIndexType>>;

    using Stencil = std::vector<GridIndexType>;

public:

    //! Update the map and prepare the stencils
    void update(const GridGeometry& gridGeometry)
    {
        const auto numDofsCC = gridGeometry.gridView().size(0);
        const auto numDofsFace = gridGeometry.numIntersections();
        const auto numBoundaryFacets = gridGeometry.numBoundaryScvf();

        cellCenterToCellCenterMap_.clear();
        cellCenterToFaceMap_.clear();
        faceToCellCenterMap_.clear();
        faceToFaceMap_.clear();

        cellCenterToCellCenterMap_.resize(numDofsCC);
        cellCenterToFaceMap_.resize(numDofsCC);
        faceToCellCenterMap_.resize(2*numDofsFace - numBoundaryFacets);
        faceToFaceMap_.resize(2*numDofsFace - numBoundaryFacets);

        for(auto&& element: elements(gridGeometry.gridView()))
        {
            // restrict the FvGeometry locally and bind to the element
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bindElement(element);

            // loop over sub control faces
            for (auto&& scvf : scvfs(fvGeometry))
            {
                const auto dofIdxCellCenter = gridGeometry.elementMapper().index(element);
                computeCellCenterToCellCenterStencil_(cellCenterToCellCenterMap_[dofIdxCellCenter], element, fvGeometry, scvf);
                computeCellCenterToFaceStencil_(cellCenterToFaceMap_[dofIdxCellCenter], element, fvGeometry, scvf);

                const auto scvfIdx = scvf.index();
                computeFaceToCellCenterStencil_(faceToCellCenterMap_[scvfIdx], fvGeometry, scvf);
                computeFaceToFaceStencil_(faceToFaceMap_[scvfIdx], fvGeometry, scvf);
            }
        }

        for(auto&& element: elements(gridGeometry.gridView()))
        {
            // restrict the FvGeometry locally and bind to the element
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bindElement(element);

            // loop over sub control faces
            for (auto&& scvf : scvfs(fvGeometry))
            {
                // change the flux correction connectivity maps

                //facetoface
                for (const auto& val : faceToFaceMap_[gridGeometry.fluxCorrectionGeometry().firstOpposingFace(scvf.index()).index()])
                {
                    if (std::find(faceToFaceMap_[scvf.index()].begin(), faceToFaceMap_[scvf.index()].end(), val) == faceToFaceMap_[scvf.index()].end())
                        faceToFaceMap_[scvf.index()].push_back(val);
                }
                for (const auto& val : faceToFaceMap_[gridGeometry.fluxCorrectionGeometry().secondOpposingFace(scvf.index()).index()])
                {
                    if (std::find(faceToFaceMap_[scvf.index()].begin(), faceToFaceMap_[scvf.index()].end(), val) == faceToFaceMap_[scvf.index()].end())
                        faceToFaceMap_[scvf.index()].push_back(val);
                }
                for (const auto& val : faceToFaceMap_[gridGeometry.fluxCorrectionGeometry().firstNeighborFace(scvf.index()).index()])
                {
                    if (std::find(faceToFaceMap_[scvf.index()].begin(), faceToFaceMap_[scvf.index()].end(), val) == faceToFaceMap_[scvf.index()].end())
                        faceToFaceMap_[scvf.index()].push_back(val);
                }
                for (const auto& val : faceToFaceMap_[gridGeometry.fluxCorrectionGeometry().secondNeighborFace(scvf.index()).index()])
                {
                    if (std::find(faceToFaceMap_[scvf.index()].begin(), faceToFaceMap_[scvf.index()].end(), val) == faceToFaceMap_[scvf.index()].end())
                        faceToFaceMap_[scvf.index()].push_back(val);
                }
                for (const auto& val : faceToFaceMap_[gridGeometry.fluxCorrectionGeometry().thirdNeighborFace(scvf.index()).index()])
                {
                    if (std::find(faceToFaceMap_[scvf.index()].begin(), faceToFaceMap_[scvf.index()].end(), val) == faceToFaceMap_[scvf.index()].end())
                        faceToFaceMap_[scvf.index()].push_back(val);
                }
                for (const auto& val : faceToFaceMap_[gridGeometry.fluxCorrectionGeometry().forthNeighborFace(scvf.index()).index()])
                {
                    if (std::find(faceToFaceMap_[scvf.index()].begin(), faceToFaceMap_[scvf.index()].end(), val) == faceToFaceMap_[scvf.index()].end())
                        faceToFaceMap_[scvf.index()].push_back(val);
                }

                //FaceToCC
                for (const auto& val : faceToCellCenterMap_[gridGeometry.fluxCorrectionGeometry().firstOpposingFace(scvf.index()).index()])
                {
                    if (std::find(faceToCellCenterMap_[scvf.index()].begin(), faceToCellCenterMap_[scvf.index()].end(), val) == faceToCellCenterMap_[scvf.index()].end())
                        faceToCellCenterMap_[scvf.index()].push_back(val);
                }
                for (const auto& val : faceToCellCenterMap_[gridGeometry.fluxCorrectionGeometry().secondOpposingFace(scvf.index()).index()])
                {
                    if (std::find(faceToCellCenterMap_[scvf.index()].begin(), faceToCellCenterMap_[scvf.index()].end(), val) == faceToCellCenterMap_[scvf.index()].end())
                        faceToCellCenterMap_[scvf.index()].push_back(val);
                }
                for (const auto& val : faceToCellCenterMap_[gridGeometry.fluxCorrectionGeometry().firstNeighborFace(scvf.index()).index()])
                {
                    if (std::find(faceToCellCenterMap_[scvf.index()].begin(), faceToCellCenterMap_[scvf.index()].end(), val) == faceToCellCenterMap_[scvf.index()].end())
                        faceToCellCenterMap_[scvf.index()].push_back(val);
                }
                for (const auto& val : faceToCellCenterMap_[gridGeometry.fluxCorrectionGeometry().secondNeighborFace(scvf.index()).index()])
                {
                    if (std::find(faceToCellCenterMap_[scvf.index()].begin(), faceToCellCenterMap_[scvf.index()].end(), val) == faceToCellCenterMap_[scvf.index()].end())
                        faceToCellCenterMap_[scvf.index()].push_back(val);
                }
                for (const auto& val : faceToCellCenterMap_[gridGeometry.fluxCorrectionGeometry().thirdNeighborFace(scvf.index()).index()])
                {
                    if (std::find(faceToCellCenterMap_[scvf.index()].begin(), faceToCellCenterMap_[scvf.index()].end(), val) == faceToCellCenterMap_[scvf.index()].end())
                        faceToCellCenterMap_[scvf.index()].push_back(val);
                }
                for (const auto& val : faceToCellCenterMap_[gridGeometry.fluxCorrectionGeometry().forthNeighborFace(scvf.index()).index()])
                {
                    if (std::find(faceToCellCenterMap_[scvf.index()].begin(), faceToCellCenterMap_[scvf.index()].end(), val) == faceToCellCenterMap_[scvf.index()].end())
                        faceToCellCenterMap_[scvf.index()].push_back(val);
                }
            }
        }
    }

    //! Returns the stencil of a cell center dof w.r.t. other cell center dofs
    const std::vector<GridIndexType>& operator() (CellCenterIdxType, CellCenterIdxType, const GridIndexType globalI) const
    {
        return cellCenterToCellCenterMap_[globalI];
    }

    //! Returns the stencil of a cell center dof w.r.t. face dofs
    const std::vector<GridIndexType>& operator() (CellCenterIdxType, FaceIdxType, const GridIndexType globalI) const
    {
        return cellCenterToFaceMap_[globalI];
    }

    //! Returns the stencil of a face dof w.r.t. cell center dofs
    const std::vector<GridIndexType>& operator() (FaceIdxType, CellCenterIdxType, const GridIndexType globalI) const
    {
        return faceToCellCenterMap_[globalI];
    }

    //! Returns the stencil of a face dof w.r.t. other face dofs
    const std::vector<GridIndexType>& operator() (FaceIdxType, FaceIdxType, const GridIndexType globalI) const
    {
        return faceToFaceMap_[globalI];
    }

private:

    /*
     * \brief Computes the stencil for cell center dofs w.r.t to other cell center dofs.
     *        Basically, these are the dof indices of the neighboring elements plus the dof index of the element itself.
     */
    void computeCellCenterToCellCenterStencil_(Stencil& stencil,
                                               const Element& element,
                                               const FVElementGeometry& fvGeometry,
                                               const SubControlVolumeFace& scvf)
    {
        // the first entry is always the cc dofIdx itself
        for (unsigned int i = 0; i < scvf.volVarsData().innerVolVarsDofs.size(); ++i)
        {
            const auto scvIdx = scvf.volVarsData().innerVolVarsDofs[i];

            if (scvIdx != -1)
            {
                if (std::find(stencil.begin(), stencil.end(), scvIdx) == stencil.end())
                {
                    stencil.push_back(scvIdx);
                }
            }
        }

        for (unsigned int i = 0; i < scvf.volVarsData().outerVolVarsDofs.size(); ++i)
        {
            const auto scvIdx = scvf.volVarsData().outerVolVarsDofs[i];

            if (scvIdx != -1)
            {
                if (std::find(stencil.begin(), stencil.end(), scvIdx) == stencil.end())
                {
                    stencil.push_back(scvIdx);
                }
            }
        }
    }

    /*
     * \brief Computes the stencil for cell center dofs w.r.t to face dofs.
     *        Basically, these are the dof indices of the element's faces.
     */
    void computeCellCenterToFaceStencil_(Stencil& stencil,
                                         const Element& element,
                                         const FVElementGeometry& fvGeometry,
                                         const SubControlVolumeFace& scvf)
    {
        if (std::find(stencil.begin(), stencil.end(), scvf.axisData().selfDof) == stencil.end())
        {
            stencil.push_back(scvf.axisData().selfDof);
        }
    }

    /*
     * \brief Computes the stencil for face dofs w.r.t to cell center dofs.
     *        Basically, these are the dof indices of the elements adjacent to the face and those of
     *        the elements adjacent to the faces parallel to the own face.
     */
    void computeFaceToCellCenterStencil_(Stencil& stencil,
                                         const FVElementGeometry& fvGeometry,
                                         const SubControlVolumeFace& scvf)
    {
        for (unsigned int i = 0; i < scvf.volVarsData().innerVolVarsDofs.size(); ++i)
        {
            const auto scvIdx = scvf.volVarsData().innerVolVarsDofs[i];

            if (scvIdx != -1 && (std::find(stencil.begin(), stencil.end(), scvIdx) == stencil.end()))
            {
                stencil.push_back(scvIdx);
            }
        }

        for (unsigned int pairIdx = 0; pairIdx < SubControlVolumeFace::numPairs; ++pairIdx)
        {
            for (unsigned int i = 0; i < scvf.volVarsData().normalVolVarsDofs[pairIdx].size(); ++i)
            {
                const auto scvIdx = scvf.volVarsData().normalVolVarsDofs[pairIdx][i];

                if (scvIdx != -1 && (std::find(stencil.begin(), stencil.end(), scvIdx) == stencil.end()))
                {
                    stencil.push_back(scvIdx);
                }
            }
        }
    }

    /*
     * \brief Computes the stencil for face dofs w.r.t to face dofs.
     *        For a full description of the stencil, please see the document under dumux/doc/docextra/staggered
     */
    void computeFaceToFaceStencil_(Stencil& stencil,
                                   const FVElementGeometry& fvGeometry,
                                   const SubControlVolumeFace& scvf)
    {
        if(stencil.empty())
        {
            for(int i = 0; i < scvf.axisData().inAxisBackwardDofs.size(); i++)
            {
                if(scvf.hasBackwardNeighbor(i) && (std::find(stencil.begin(), stencil.end(), scvf.axisData().inAxisBackwardDofs[i]) == stencil.end()))
                {
                    stencil.push_back(scvf.axisData().inAxisBackwardDofs[i]);
                }
            }

            if (std::find(stencil.begin(), stencil.end(), scvf.axisData().selfDof) == stencil.end())
                stencil.push_back(scvf.axisData().selfDof);

            const auto& oppoDofs = scvf.axisData().oppositeDofs;
            for (unsigned int i = 0; i < oppoDofs.size(); ++i)
            {
                if (oppoDofs[i] != -1 && std::find(stencil.begin(), stencil.end(), oppoDofs[i]) == stencil.end())
                    stencil.push_back(oppoDofs[i]);
            }

            for(int i = 0; i < scvf.axisData().inAxisForwardDofs.size(); i++)
            {
                if(scvf.hasForwardNeighbor(i) && std::find(stencil.begin(), stencil.end(), scvf.axisData().inAxisForwardDofs[i]) == stencil.end())
                {
                    stencil.push_back(scvf.axisData().inAxisForwardDofs[i]);
                }
            }
        }

        for(const auto& data : scvf.pairData())
        {
            // add normal dofs
            for (unsigned int i = 0; i < data.normalPair.first.size(); ++i)
            {
                if (data.normalPair.first[i] != -1 && std::find(stencil.begin(), stencil.end(), data.normalPair.first[i]) == stencil.end())
                {
                    stencil.push_back(data.normalPair.first[i]);
                }
            }
            if(!scvf.boundary())
            {
                for (unsigned int i = 0; i < data.normalPair.second.size(); ++i)
                {
                    if (data.normalPair.second[i] != -1 && std::find(stencil.begin(), stencil.end(), data.normalPair.second[i]) == stencil.end())
                    {
                        stencil.push_back(data.normalPair.second[i]);
                    }
                }
            }

            // add parallel dofs
            for (int i = 0; i < data.parallelDofs.size(); i++)
            {
                for (unsigned int j = 0; j < data.parallelDofs[i].size(); ++j)
                {
                    if(!(data.parallelDofs[i][j] < 0) && std::find(stencil.begin(), stencil.end(), data.parallelDofs[i][j]) == stencil.end())
                    {
                        stencil.push_back(data.parallelDofs[i][j]);
                    }
                }
            }
        }
    }

    CellCenterToCellCenterMap cellCenterToCellCenterMap_;
    CellCenterToFaceMap cellCenterToFaceMap_;
    FaceToCellCenterMap faceToCellCenterMap_;
    FaceToFaceMap faceToFaceMap_;
};

} // end namespace Dumux

#endif // DUMUX_STAGGERED_FREEFLOW_MY_CONNECTIVITY_MAP_HH
