// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Implementation of the single-phase transmissibility laws for throats
 */
#ifndef DUMUX_PNM_THROAT_TRANSMISSIBILITY_1P_HH
#define DUMUX_PNM_THROAT_TRANSMISSIBILITY_1P_HH

#include <dumux/porenetworkflow/common/throatproperties.hh>

namespace Dumux {

template<class Scalar>
class TransmissibilityBruus
{
public:

     //! Returns the conductivity of a throat when only one phase is present.
    template<class Element, class FVElementGeometry, class FluxVariablesCache>
    static Scalar singlePhaseTransmissibility(const Element& element,
                                              const FVElementGeometry& fvGeometry,
                                              const typename FVElementGeometry::SubControlVolumeFace& scvf,
                                              const FluxVariablesCache& fluxVarsCache)
    {
        const auto eIdx = fvGeometry.gridGeometry().elementMapper().index(element);
        const auto shape = fvGeometry.gridGeometry().throatCrossSectionShape(eIdx);
        const Scalar inscribedRadius = fluxVarsCache.throatRadius();
        const Scalar throatLength = fluxVarsCache.throatLength();
        const Scalar aspectRatio = fluxVarsCache.throatAspectRatio();
        return 1.0 / rHydThroat_(shape, inscribedRadius, throatLength, aspectRatio);
    }

protected:

    static Scalar rHydThroat_(const Throat::Shape shape,
                              const Scalar radius,
                              const Scalar length,
                              const Scalar aspectRatio)
    {
        switch(shape)
        {
            case Throat::Shape::square:
            {
                const Scalar sideLength = 2.0*radius;
                return 28.4 * length * 1.0/(sideLength*sideLength*sideLength*sideLength);
            }
            case Throat::Shape::circle:
            {
                return 8.0/M_PI * length * 1.0/(radius*radius*radius*radius);
            }
            case Throat::Shape::equilateralTriangle:
            {
                static constexpr Scalar sqrt3 = std::sqrt(3.0);
                const Scalar sideLength = 6.0/sqrt3 * radius;
                return 320.0/sqrt3 * length * 1.0/(sideLength*sideLength*sideLength*sideLength);
            }
            case Throat::Shape::twoPlates:
            {
                // the distance between the two parallel plates
                const Scalar width = 2*radius;
                return 12.0/(width*width*width) * length;
            }
            case Throat::Shape::rectangle:
            {
                // w is the width of the rectangle, h is the height
                // w > h
                auto result = [&](const Scalar w, const Scalar h)
                {
                    assert(w >= h);
                    return 12.0*length / (1.0 - 0.63*(h/w)) * 1.0/(h*h*h*w);
                };

                // get the correct dimensions, considering w > h
                if (aspectRatio < 1.0)
                {
                    const Scalar w = 2.0*radius;
                    const Scalar h = aspectRatio * w;
                    return result(w,h);
                }
                else
                {
                    const Scalar h = 2.0*radius;
                    const Scalar w = aspectRatio * h;
                    return result(w,h);
                }
            }
            default: DUNE_THROW(Dune::InvalidStateException, "Throat geometry not supported");
        }
    }
};

template<class Scalar, bool interpolateK = false>
class TransmissibilityPatzekSilin
{
    static_assert(!interpolateK,  "Interpolation of k not implemented");
public:

     //! Returns the conductivity of a throat when only one phase is present. See Patzek & Silin (2001)
    template<class Element, class FVElementGeometry, class FluxVariablesCache>
    static Scalar singlePhaseTransmissibility(const Element& element,
                                              const FVElementGeometry& fvGeometry,
                                              const typename FVElementGeometry::SubControlVolumeFace& scvf,
                                              const FluxVariablesCache& fluxVarsCache)
    {
        const auto eIdx = fvGeometry.gridGeometry().elementMapper().index(element);
        const auto shapeFactor = fvGeometry.gridGeometry().shapeFactor(eIdx);
        const Scalar area = fluxVarsCache.throatCrossSectionalArea();
        const Scalar throatLength = fluxVarsCache.throatLength();

        return k_(shapeFactor) * area*area * shapeFactor / throatLength;
    }

private:
    static Scalar k_(const Scalar shapeFactor)
    {
        if (shapeFactor <= Throat::shapeFactorEquilateralTriangle<Scalar>())
            return 0.6; // == 3/5
        else if (shapeFactor <= Throat::shapeFactorSquare<Scalar>())
            return 0.5623;
        else // circle
            return 0.5;
    }

    // TODO interpolation
};


//! Used by Joeakar-Niasar, probably wrong, TODO: check and maybe remove
template<class Scalar>
class TransmissibilityAzzamDullien
{
public:

    //! Returns the conductivity of a throat when only one phase is present.
    template<class Element, class FVElementGeometry,  class FluxVariablesCache>
    static Scalar singlePhaseTransmissibility(const Element& element,
                                              const FVElementGeometry& fvGeometry,
                                              const typename FVElementGeometry::SubControlVolumeFace& scvf,
                                              const FluxVariablesCache& fluxVarsCache)
    {
        const Scalar throatRadius = fluxVarsCache.throatRadius();
        const Scalar throatLength = fluxVarsCache.throatLength();

        const Scalar rEff= std::sqrt(4.0/M_PI)*throatRadius ;
        return M_PI/(8.0*throatLength) *rEff*rEff*rEff*rEff ;
    }
};

}

#endif // DUMUX_PNM_THROAT_TRANSMISSIBILITY_1P_HH
