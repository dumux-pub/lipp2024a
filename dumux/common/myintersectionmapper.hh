// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Common
 * \brief defines intersection mappers.
 */
#ifndef DUMUX_MYINTERSECTIONITERATOR_HH
#define DUMUX_MYINTERSECTIONITERATOR_HH

#include <vector>
#include <unordered_map>

#include <dune/grid/common/mcmgmapper.hh>
#include <dune/grid/common/rangegenerators.hh>
#include <dumux/common/intersectionmapper.hh>


namespace Dumux {

/*!
 * \ingroup Common
 * \brief defines an intersection mapper for mapping of global DOFs assigned
 *        to faces which also works for non-conforming grids and corner-point grids.
 */
template<class GridView>
class MyNonConformingGridIntersectionMapper
{
    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;
    using GridIndexType = typename GridView::IndexSet::IndexType;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
    using Scalar = typename GridView::ctype;
    using ScvGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim, dimWorld>;
    using GlobalPosition = typename ScvGeometry::GlobalCoordinate;

public:
    MyNonConformingGridIntersectionMapper(const GridView& gridview)
    : gridView_(gridview),
      indexSet_(&gridView_.indexSet()),
      numIntersections_(gridView_.size(1)),
      intersectionMapGlobal_(gridView_.size(0))
    {

    }

    //! intersection.indexInInside does not give an intersection index, but a facetIndex. E.g. in 2D the rectangular elements have 0,1,2,3 as indexInInside. This function is intended to, in the case of hanging nodes, also have e.g. 4 as possible indexInInside - considering intersections instead of facets.
    GridIndexType isIndexInInside(const Intersection& myIs) const
    {
        int localMyIsIdx = 0;
        for (const auto& is : intersections(this->gridView_, myIs.inside())){
            if (myIs.geometry().center() == is.geometry().center())
            {
                return localMyIsIdx;
            }
            ++localMyIsIdx;
        }
        std::cout << "should not be after for loop in isIndexInInside in intersectionmapper!" << std::endl;
        return 0;//just to avoid compiler warning
    }

    GridIndexType isIndexInOutside(const Intersection& myIs) const
    {
        int localMyIsIdx = 0;
        for (const auto& is : intersections(this->gridView_, myIs.outside())){
            if (containerCmp(myIs.geometry().center(), is.geometry().center()))
            {
                return localMyIsIdx;
            }
            ++localMyIsIdx;
        }
        std::cout << "should not be after for loop in isIndexInOutside in intersectionmapper!" << std::endl;
        return 0;//just to avoid compiler warning
    }

    //! The total number of intersections
    std::size_t numIntersections() const
    {
        return numIntersections_;
    }

    GridIndexType globalIntersectionIndex(const Element& element, const std::size_t localFaceIdx) const
    {
        return (intersectionMapGlobal_[index(element)].find(localFaceIdx))->second; //use find() for const function!
    }

    std::size_t numFaces(const Element& element)
    {
        return intersectionMapGlobal_[index(element)].size();
    }

    const auto velocityXPositions() const
    {
        return velocityXPositions_;
    }

    const auto velocityYPositions() const
    {
        return velocityYPositions_;
    }

    void update (const GridView& gridView)
    {
        gridView_ = gridView;
        indexSet_ = &gridView_.indexSet();
        update_();
    }

    void update (GridView&& gridView)
    {
        gridView_ = std::move(gridView);
        indexSet_ = &gridView_.indexSet();
        update_();
    }

private:
    void update_()
    {
        intersectionMapGlobal_.clear();
        intersectionMapGlobal_.resize(gridView_.size(0));

        velocityXPositions_.clear();
        velocityYPositions_.clear();

        GridIndexType globalIntersectionIdx = 0;
        for (const auto& element : elements(gridView_))
        {
            int eIdx = index(element);
            int fIdx = 0;

            // run through all intersections with neighbors
            for (const auto& intersection : intersections(gridView_, element))
            {
                if (intersection.neighbor())
                {
                    auto neighbor = intersection.outside();
                    int eIdxN = index(neighbor);

                    if (element.level() > neighbor.level() || (element.level() == neighbor.level() && eIdx < eIdxN))
                    {
                        int fIdxN = 0;
                        for (const auto& intersectionNeighbor : intersections(gridView_, neighbor))
                        {
                            if (intersectionNeighbor.neighbor())
                            {
                                if (intersectionNeighbor.outside() == element)
                                {
                                    break;
                                }
                            }
                            fIdxN++;
                        }
                        intersectionMapGlobal_[eIdx][fIdx] = globalIntersectionIdx;
                        intersectionMapGlobal_[eIdxN][fIdxN] = globalIntersectionIdx;

                        unsigned int directionIdx = Dumux::directionIndex(std::move(intersection.centerUnitOuterNormal()));
                        if(directionIdx == 0)
                        {
                            velocityXPositions_.push_back(std::make_pair(globalIntersectionIdx, intersection.geometry().center()));
                        }
                        else if (directionIdx == 1)
                        {
                            velocityYPositions_.push_back(std::make_pair(globalIntersectionIdx, intersection.geometry().center()));
                        }
                        else
                        {
                            DUNE_THROW(Dune::InvalidStateException, "Adaptive not prepated for three dimensions.");
                        }

                        globalIntersectionIdx++;
                    }
                }
                else
                {
                    unsigned int directionIdx = Dumux::directionIndex(std::move(intersection.centerUnitOuterNormal()));
                    if(directionIdx == 0)
                    {
                        velocityXPositions_.push_back(std::make_pair(globalIntersectionIdx, intersection.geometry().center()));
                    }
                    else if (directionIdx == 1)
                    {
                        velocityYPositions_.push_back(std::make_pair(globalIntersectionIdx, intersection.geometry().center()));
                    }
                    else
                    {
                        DUNE_THROW(Dune::InvalidStateException, "Adaptive not prepated for three dimensions.");
                    }

                    intersectionMapGlobal_[eIdx][fIdx] = globalIntersectionIdx;
                    globalIntersectionIdx++;
                }

                fIdx++;
            }
        }
        numIntersections_ = globalIntersectionIdx;
    }

    GridIndexType index(const Element& element) const
    {
        return indexSet_->index(element);
    }

    GridView gridView_;
    const typename GridView::IndexSet* indexSet_;
    unsigned int numIntersections_;
    std::vector<std::unordered_map<int, int> > intersectionMapGlobal_;
    std::vector<std::pair<GridIndexType, GlobalPosition>> velocityXPositions_;
    std::vector<std::pair<GridIndexType, GlobalPosition>> velocityYPositions_;
};

} // end namespace Dumux

#endif
