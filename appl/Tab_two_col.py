import sys
import pandas as pd
import numpy as np
import math

pd.set_option("display.precision", 11)

df = pd.read_csv(sys.argv[1])

def latex_float(f):
    float_str = f
    if "e" in float_str:
        base, exponent = float_str.split("e")
        return r"{0}$\cdot 10^{{{1}}}$".format(base, int(exponent))
    else:
        return float_str

def rate(myFile,index,labelDelta,labelValues):
       return format((math.log(abs(myFile[labelValues][index]))-math.log(abs(myFile[labelValues][index-1])))/(math.log(myFile[labelDelta][index])-math.log(myFile[labelDelta][index-1])), ".2f")

file1 = open(sys.argv[4], "w")
file1.write("1&"+latex_float(format(df["delta"][0], ".2e"))+"&"+latex_float(format(df[sys.argv[2]][0], ".2e"))+"& "+"&"+latex_float(format(df[sys.argv[3]][0], ".2e"))+"& \\\\")
for i in range (2,9):
       file1.write('\n' + str(i)+"&"+latex_float('\n' + format(df["delta"][i-1], ".2e"))+"&"+latex_float(format(df[sys.argv[2]][i-1], ".2e"))+"&"+rate(df,i-1,"delta",sys.argv[2])+"&"+latex_float(format(df[sys.argv[3]][i-1], ".2e"))+"&"+rate(df,i-1,"delta",sys.argv[3])+"\\\\")
file1.write("\\hline \n \\end{tabular}")
file1.close()
