mkdir -p texfiles

#Fig78
python3 increase_decrease_localTruncErr.py datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv.csv datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD.csv "texfiles/Fig78aL1norm.tex" "texfiles/Fig78bL1norm.tex"
python3 increase_decrease_localTruncErr.py datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_0307.csv datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_0307.csv "texfiles/Fig78cL1norm.tex" "texfiles/Fig78dL1norm.tex"
python3 increase_decrease_localTruncErr.py datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_rightFine.csv datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_rightFine.csv "texfiles/Fig78eL1norm.tex" "texfiles/Fig78fL1norm.tex"
python3 increase_decrease_localTruncErr.py datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_100x100.csv datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_100x100.csv "texfiles/Fig78gL1norm.tex" "texfiles/Fig78hL1norm.tex"

#Fig79
python3 increase_decrease_localTruncErr.py datafiles/localTruncErrExplanation_Conti.csv datafiles/localTruncErrExplanation_Conti_proj.csv "texfiles/Fig79aL1norm.tex" "texfiles/Fig79bL1norm.tex"
python3 increase_decrease_localTruncErr.py datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_gauss.csv datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_gauss.csv "texfiles/Fig79cL1norm.tex" "texfiles/Fig79dL1norm.tex"
python3 increase_decrease_localTruncErr.py datafiles/localTruncErrExplanation_MomY_projCoarseOnCVOptionD_WithConserv_gauss.csv datafiles/localTruncErrExplanation_MomY_projectionCoarseOnCVOptionD_gauss.csv "texfiles/Fig79eL1norm.tex" "texfiles/Fig79fL1norm.tex"

#Fig80
python3 increase_decrease_localTruncErr.py datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD.csv datafiles/localTruncErrExplanation_MomX_projectionCVOptionDOnCoarse.csv "texfiles/unused.tex" "texfiles/Fig80aL1norm.tex"
python3 increase_decrease_localTruncErr_sixth_col_version.py datafiles/localTruncErrExplanation_MomX_CV_D_StandStenc_NoInterp.csv datafiles/localTruncErrExplanation_MomX_WithConserv.csv "texfiles/Fig80bL1norm.tex"

#Fig83
python3 increase_decrease_solErr.py datafiles/solErr_donea_U_10x10_projCVOptDOnCoarse_0406refined.csv datafiles/solErr_donea_U_10x10_projCoarseOnCVOptD_0406unrefined.csv datafiles/solErr_donea_Uexact_10x10_projCVOptDOnCoarse_0406refined.csv datafiles/solErr_donea_Uexact_10x10_projCoarseOnCVOptD_0406unrefined.csv "texfiles/Fig83aL1norm.tex" "texfiles/Fig83bL1norm.tex"
python3 increase_decrease_solErr.py datafiles/solErr_donea_U_10x10_projCVOptDOnCoarse_0307refined.csv datafiles/solErr_donea_U_10x10_projCoarseOnCVOptD_0307unrefined.csv datafiles/solErr_donea_Uexact_10x10_projCVOptDOnCoarse_0307refined.csv datafiles/solErr_donea_Uexact_10x10_projCoarseOnCVOptD_0307unrefined.csv "texfiles/Fig83cL1norm.tex" "texfiles/Fig83dL1norm.tex"
python3 increase_decrease_solErr.py datafiles/solErr_donea_U_10x10_projCVOptDOnCoarse_rightRefined.csv datafiles/solErr_donea_U_10x10_projCoarseOnCVOptD_rightFineUnrefined.csv datafiles/solErr_donea_Uexact_10x10_projCVOptDOnCoarse_rightRefined.csv datafiles/solErr_donea_Uexact_10x10_projCoarseOnCVOptD_rightFineUnrefined.csv "texfiles/Fig83eL1norm.tex" "texfiles/Fig83fL1norm.tex"
python3 increase_decrease_solErr.py datafiles/solErr_donea_U_10x10_projCVOptDOnCoarse_100x100refined.csv datafiles/solErr_donea_U_10x10_projCoarseOnCVOptD_100x100unrefined.csv datafiles/solErr_donea_Uexact_10x10_projCVOptDOnCoarse_100x100refined.csv datafiles/solErr_donea_Uexact_10x10_projCoarseOnCVOptD_100x100unrefined.csv "texfiles/Fig83gL1norm.tex" "texfiles/Fig83hL1norm.tex"

#Fig84
python3 increase_decrease_solErr.py datafiles/solErr_gauss_P_projOnCoarse_refined.csv datafiles/solErr_gauss_P_unrefined.csv datafiles/solErr_gauss_Pexact_projOnCoarse_refined.csv datafiles/solErr_gauss_Pexact_unrefined.csv "texfiles/Fig84aL1norm.tex" "texfiles/Fig84bL1norm.tex"
python3 increase_decrease_solErr.py datafiles/solErr_gauss_U_projCVOptDOnCoarse_refined.csv datafiles/solErr_gauss_U_projCoarseOnCVOptD_unrefined.csv datafiles/solErr_gauss_Uexact_projCVOptDOnCoarse_refined.csv datafiles/solErr_gauss_Uexact_projCoarseOnCVOptD_unrefined.csv "texfiles/Fig84cL1norm.tex" "texfiles/Fig84dL1norm.tex"
python3 increase_decrease_solErr.py datafiles/solErr_gauss_V_projCVOptDOnCoarse_refined.csv datafiles/solErr_gauss_V_projCoarseOnCVOptD_unrefined.csv datafiles/solErr_gauss_Vexact_projCVOptDOnCoarse_refined.csv datafiles/solErr_gauss_Vexact_projCoarseOnCVOptD_unrefined.csv "texfiles/Fig84eL1norm.tex" "texfiles/Fig84fL1norm.tex"

#Tab. 15
python3 Tab_one_col.py datafiles/orderNumerics_div.csv " cell1" "texfiles/Tab15a.tex"
python3 Tab_one_col.py datafiles/orderNumerics_divLequals3.csv " cell1" "texfiles/Tab15b.tex"

#Tab. 16
python3 Tab_three_col.py datafiles/orderNumerics_q.csv " cell1" " cell7" " cell5"  "texfiles/Tab16.tex" #q,mo,x

#Tab. 17
python3 Tab_one_col.py datafiles/orderNumerics_gradient.csv " cell1" "texfiles/Tab17.tex"

#Tab. 18
python3 Tab_three_col.py datafiles/orderNumerics_normalDiffFrontal.csv " cell1" " cell7" " cell8"  "texfiles/Tab18a.tex"
python3 Tab_two_col.py datafiles/orderNumerics_normalDiffFrontal.csv " cell5" " cell9" "texfiles/Tab18b.tex"

#Tab. 19
python3 Tab_three_col.py datafiles/orderNumerics_nonMixed.csv " cell1" " cell7" " cell8" "texfiles/Tab19a.tex"
python3 Tab_two_col.py datafiles/orderNumerics_nonMixed.csv " cell5" " cell9" "texfiles/Tab19b.tex"

#Tab. 20
python3 Tab_four_col.py datafiles/orderNumerics_normalDiffLateralZerothOrder.csv " cell1" " cell7"  " cell2" " cell5" "texfiles/Tab20a.tex"
python3 Tab_two_col.py datafiles/orderNumerics_normalDiffLateralZerothOrder.csv " cell3" " cell6" "texfiles/Tab20b.tex"

#Tab. 21
python3 Tab_three_col.py datafiles/orderNumerics_normalDiffLateralFirstOrder.csv " cell1" " cell7" " cell5" "texfiles/Tab21a.tex"
python3 Tab_three_col.py datafiles/orderNumerics_normalDiffLateralFirstOrder.csv " cell2" " cell3" " cell6" "texfiles/Tab21b.tex"

#Tab. 22
python3 Tab_three_col.py datafiles/orderNumerics_mixed.csv " cell1" " cell5" " cell7" "texfiles/Tab22a.tex"
python3 Tab_two_col.py datafiles/orderNumerics_mixed.csv " cell2" " cell6" "texfiles/Tab22b.tex"

#Tab. 23
python3 Tab_one_col.py datafiles/orderNumerics_tangential.csv " cell10" "texfiles/Tab23.tex"

#Tab 24
python3 Tab_one_col.py datafiles/orderNumerics_normalDiffLateralZerothOrder.csv " cell11" "texfiles/Tab24a.tex"
python3 Tab_one_col.py datafiles/orderNumerics_normalDiffLateralFirstOrder.csv " cell11" "texfiles/Tab24b.tex"
python3 Tab_one_col.py datafiles/orderNumerics_mixed.csv " cell11" "texfiles/Tab24c.tex"

#Tab. 25
python3 Tab_one_col.py datafiles/orderNumerics_div_sum.csv " sum" "texfiles/Tab25a.tex"
python3 Tab_one_col.py datafiles/orderNumerics_q_sum.csv " sum" "texfiles/Tab25b.tex"
python3 Tab_one_col.py datafiles/orderNumerics_gradient_sum.csv " sum" "texfiles/Tab25c.tex"
python3 Tab_one_col.py datafiles/orderNumerics_normalDiffFrontal_sum.csv " sum" "texfiles/Tab25d.tex"
python3 Tab_one_col.py datafiles/orderNumerics_nonMixed_sum.csv " sum" "texfiles/Tab25e.tex"
python3 Tab_one_col.py datafiles/orderNumerics_normalDiffLateralZerothOrder_sum.csv " sum" "texfiles/Tab25f.tex"
python3 Tab_one_col.py datafiles/orderNumerics_normalDiffLateralFirstOrder_sum.csv " sum" "texfiles/Tab25g.tex"
python3 Tab_one_col.py datafiles/orderNumerics_mixed_sum.csv " sum" "texfiles/Tab25h.tex"

#Tab. 26
python3 Tab_26_and_27.py ' L1LocalTruncErr(p)' ' L1LocalTruncErr(u)' ' L1LocalTruncErr(v)' '\localTruncationErrorSymbol_{\text{ma}}' '\localTruncationErrorSymbol_{\text{mo},x}' '\localTruncationErrorSymbol_{\text{mo},y}' "texfiles/Tab26.tex"

#Tab. 27
python3 Tab_26_and_27.py ' L1Abs(p)' ' L1Abs(u)' ' L1Abs(v)' '\localSolutionErrorSymbol_{p}' '\localSolutionErrorSymbol_{u}' '\localSolutionErrorSymbol_{v}' "texfiles/Tab27.tex"

#Tab. 28
python3 Tab_28.py "texfiles/Tab28.tex"

#Sec 5.3.2 Improved approximation of quantities of interest
python3 reynoldscheck.py
