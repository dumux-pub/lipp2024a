import sys
import pandas as pd
import numpy as np
import math

pd.set_option("display.precision", 11)

file1 = open("./datafiles/pnm_pore_grid.csv", "w")
file1.write("xMin,yMin,xMax,yMax"+ '\n')
file1.write("-0.005,-0.0025,0,-0.00125"+ '\n')
file1.write("-0.005,-0.00125,0,0"+ '\n')
file1.write("0,-0.0025,0.005,-0.00125"+ '\n')
file1.write("0,-0.00125,0.005,0"+ '\n')
file1.close()
