import sys
import pandas as pd

pd.set_option("display.precision", 10)

df1 = pd.read_csv(sys.argv[1])
df1.columns = ['Col1', 'Col2', 'Col3', 'Col4', 'Col5', 'Col6']

df2 = pd.read_csv(sys.argv[2])
df2.columns = ['Col1', 'Col2', 'Col3', 'Col4', 'Col5']

eps=1e-10

def latex_float(f):
    float_str = f
    if "e" in float_str:
        base, exponent = float_str.split("e")
        return r"{0}$\cdot 10^{{{1}}}$".format(base, int(exponent))
    else:
        return float_str

#calcs
myDf = df1
myDf['Col7'] = df2.Col5
myDf['Diff'] = myDf.Col6.abs() - myDf.Col7.abs()
myDf['PositiveOnes'] = myDf.Diff.where(myDf.Diff>eps,other=0)
myDf['NegativeOnes'] = -myDf.Diff.where(myDf.Diff<-eps,other=0)
myDf['Area']= (myDf.Col3-myDf.Col1) * (myDf.Col4-myDf.Col2)
myDf['AreaWeightedPositive'] = myDf.PositiveOnes * myDf.Area
myDf['AreaWeightedNegative'] = myDf.NegativeOnes * myDf.Area

areaSum = myDf['Area'].sum()
weightedPositiveSum = myDf['AreaWeightedPositive'].sum()
weightedNegativeSum = myDf['AreaWeightedNegative'].sum()

file = open(sys.argv[3], "w")
file.write(latex_float('{0:1.2e}'.format(weightedNegativeSum/areaSum)))
file.close()
