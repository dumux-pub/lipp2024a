// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief A test problem for the coupled FreeFlow/Darcy problem (1p).
 */

bool doFirstOrderLocalTruncErrorGlobalRefinement = false;

#include <config.h>

#include <ctime>
#include <iostream>

#include <dumux/assembly/staggeredrefinedfvassembler.hh>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/partial.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/mystaggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>

#include <dumux/multidomain/staggeredtraits.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>

#include <dumux/adaptive/adapt.hh>
#include <dumux/adaptive/markelements.hh>
#include <dumux/freeflow/gridadaptindicator.hh>
#include <dumux/freeflow/navierstokes/staggered/griddatatransfer.hh>
#include <dumux/freeflow/navierstokes/errors.hh>

#include <dumux/multidomain/boundary/stokesdarcy/mycouplingmanager.hh>

#include <dumux/discretization/staggered/myfvgridgeometry.hh>
#include <dumux/discretization/staggered/freeflow/fvgridgeometrytraits1.hh>

#include "testcase.hh"
#include "problem_darcy.hh"
#include "problem_stokes.hh"

namespace Dumux {
namespace Properties {

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::FreeFlowOneP>
{
    using Traits = StaggeredMultiDomainTraits<TypeTag, TypeTag, Properties::TTag::DarcyOneP>;
    using type = Dumux::MyStokesDarcyCouplingManager<Traits>;
};

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::DarcyOneP>
{
    using Traits = StaggeredMultiDomainTraits<Properties::TTag::FreeFlowOneP, Properties::TTag::FreeFlowOneP, TypeTag>;
    using type = Dumux::MyStokesDarcyCouplingManager<Traits>;
};

} // end namespace Properties
} // end namespace Dumux

/*!
* \brief Creates analytical solution.
* Returns a tuple of the analytical solution for the pressure, the velocity and the velocity at the faces
* \param problem the problem for which to evaluate the analytical solution
*/
template<class Scalar, class Problem>
auto createFreeFlowAnalyticalSolution(const Problem& problem)
{
    const auto& gridGeometry = problem.gridGeometry();
    using GridView = typename std::decay_t<decltype(gridGeometry)>::GridView;

    static constexpr auto dim = GridView::dimension;
    static constexpr auto dimWorld = GridView::dimensionworld;

    using VelocityVector = Dune::FieldVector<Scalar, dimWorld>;

    std::vector<Scalar> analyticalPressure;
    std::vector<VelocityVector> analyticalVelocity;
    std::vector<Scalar> analyticalVelocityOnFace;

    analyticalPressure.resize(gridGeometry.numCellCenterDofs());
    analyticalVelocity.resize(gridGeometry.numCellCenterDofs());
    analyticalVelocityOnFace.resize(gridGeometry.numFaceDofs());

    using Indices = typename Problem::Indices;
    for (const auto& element : elements(gridGeometry.gridView()))
    {
        auto fvGeometry = localView(gridGeometry);
        fvGeometry.bindElement(element);
        for (auto&& scv : scvs(fvGeometry))
        {
            auto ccDofIdx = scv.dofIndex();
            auto ccDofPosition = scv.dofPosition();
            auto analyticalSolutionAtCc = problem.analyticalSolutionAtPos(ccDofPosition);

            // velocities on faces
            for (auto&& scvf : scvfs(fvGeometry))
            {
                const auto faceDofIdx = scvf.dofIndex();
                const auto faceDofPosition = scvf.center();
                const auto dirIdx = scvf.directionIndex();
                const auto analyticalSolutionAtFace = problem.analyticalSolutionAtPos(faceDofPosition);
                analyticalVelocityOnFace[faceDofIdx] = analyticalSolutionAtFace[Indices::velocity(dirIdx)];
            }

            analyticalPressure[ccDofIdx] = analyticalSolutionAtCc[Indices::pressureIdx];

            for (int dirIdx = 0; dirIdx < dim; ++dirIdx)
                analyticalVelocity[ccDofIdx][dirIdx] = analyticalSolutionAtCc[Indices::velocity(dirIdx)];
        }
    }

    return std::make_tuple(analyticalPressure, analyticalVelocity, analyticalVelocityOnFace);
}

/*!
* \brief Creates analytical solution.
* Returns a tuple of the analytical solution for the pressure, the velocity and the velocity at the faces
* \param problem the problem for which to evaluate the analytical solution
*/
template<class Scalar, class Problem>
auto createDarcyAnalyticalSolution(const Problem& problem)
{
    const auto& gridGeometry = problem.gridGeometry();
    using GridView = typename std::decay_t<decltype(gridGeometry)>::GridView;

    static constexpr auto dim = GridView::dimension;
    static constexpr auto dimWorld = GridView::dimensionworld;

    using VelocityVector = Dune::FieldVector<Scalar, dimWorld>;

    std::vector<Scalar> analyticalPressure;
    std::vector<VelocityVector> analyticalVelocity;

    analyticalPressure.resize(gridGeometry.numDofs());
    analyticalVelocity.resize(gridGeometry.numDofs());

    for (const auto& element : elements(gridGeometry.gridView()))
    {
        auto fvGeometry = localView(gridGeometry);
        fvGeometry.bindElement(element);
        for (auto&& scv : scvs(fvGeometry))
        {
            const auto ccDofIdx = scv.dofIndex();
            const auto ccDofPosition = scv.dofPosition();
            const auto analyticalSolutionAtCc = problem.analyticalSolution(ccDofPosition);
            analyticalPressure[ccDofIdx] = analyticalSolutionAtCc[dim];
/*
            if (isinf(analyticalSolutionAtCc[dim]))
                std::cout << "pressure  " << std::endl;*/

            for (int dirIdx = 0; dirIdx < dim; ++dirIdx)
            {
                analyticalVelocity[ccDofIdx][dirIdx] = analyticalSolutionAtCc[dirIdx];

//                 if (isinf (analyticalSolutionAtCc[dirIdx]))
//                 std::cout << "velocity " << std::endl;
            }
        }
    }

    return std::make_tuple(analyticalPressure, analyticalVelocity);
}

template<class Problem, class SolutionVector>
void printDarcyL2Error(const Problem& problem, const SolutionVector& x)
{
    using namespace Dumux;
    using Scalar = double;

    Scalar l2error = 0.0;

    for (const auto& element : elements(problem.gridGeometry().gridView()))
    {
        auto fvGeometry = localView(problem.gridGeometry());
        fvGeometry.bindElement(element);

        for (auto&& scv : scvs(fvGeometry))
        {
            const auto dofIdx = scv.dofIndex();
            const Scalar delta = x[dofIdx] - problem.analyticalSolution(scv.center())[2/*pressureIdx*/];
            l2error += scv.volume()*(delta*delta);
        }
    }
    using std::sqrt;
    l2error = sqrt(l2error);

    const auto numDofs = problem.gridGeometry().numDofs();
    std::ostream tmp(std::cout.rdbuf());
    tmp << std::setprecision(8) << "** L2 error (abs) for "
            << std::setw(6) << numDofs << " cc dofs "
            << std::scientific
            << "L2 error = " << l2error
            << std::endl;

    // write the norm into a log file
    std::ofstream logFile;
    logFile.open(problem.name() + ".log", std::ios::app);
    logFile << "[ConvergenceTest] L2(p) = " << l2error << std::endl;
    logFile.close();
}

int main(int argc, char** argv)
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // Define the sub problem type tags
    using FreeFlowTypeTag = Properties::TTag::FreeFlowOneP;
    using DarcyTypeTag = Properties::TTag::DarcyOneP;

    using FreeFlowHelpingGridManager = Dumux::GridManager<GetPropType<FreeFlowTypeTag, Properties::HelpingGrid>>;
    FreeFlowHelpingGridManager freeFlowHelpingGridManager;
    freeFlowHelpingGridManager.init("FreeFlowHelper");
    const auto& freeFlowHelpingGridView = freeFlowHelpingGridManager.grid().leafGridView();
    Dune::DGFWriter dgfWriter(freeFlowHelpingGridView);
    std::string inputFileName = getParam<std::string>("FreeFlow.Grid.File");
    dgfWriter.write(inputFileName);

    // try to create a grid (from the given grid file or the input file)
    // for both sub-domains
    using DarcyGridManager = Dumux::GridManager<GetPropType<DarcyTypeTag, Properties::Grid>>;
    DarcyGridManager darcyGridManager;
    darcyGridManager.init("Darcy");

    using FreeFlowGridManager = Dumux::GridManager<GetPropType<FreeFlowTypeTag, Properties::Grid>>;
    FreeFlowGridManager freeFlowGridManager;
    freeFlowGridManager.init("FreeFlow");

    // we compute on the leaf grid view
    const auto& darcyGridView = darcyGridManager.grid().leafGridView();
    const auto& freeFlowGridView = freeFlowGridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using FreeFlowGridGeometry = GetPropType<FreeFlowTypeTag, Properties::GridGeometry>;
    auto freeFlowGridGeometry = std::make_shared<FreeFlowGridGeometry>(freeFlowGridView);
    using DarcyGridGeometry = GetPropType<DarcyTypeTag, Properties::GridGeometry>;
    auto darcyGridGeometry = std::make_shared<DarcyGridGeometry>(darcyGridView);

    using Traits = StaggeredMultiDomainTraits<FreeFlowTypeTag, FreeFlowTypeTag, DarcyTypeTag>;

    // the coupling manager
    using CouplingManager = MyStokesDarcyCouplingManager<Traits>;
    auto couplingManager = std::make_shared<CouplingManager>(freeFlowGridGeometry, darcyGridGeometry);

    // the indices
    constexpr auto freeFlowCellCenterIdx = CouplingManager::stokesCellCenterIdx;
    constexpr auto freeFlowFaceIdx = CouplingManager::stokesFaceIdx;
    constexpr auto darcyIdx = CouplingManager::darcyIdx;

    // the problem (initial and boundary conditions)
    const auto testCaseName = getParam<std::string>("Problem.TestCase");
    const auto testCase = [&testCaseName]()
    {
        if (testCaseName == "ShiueExampleOne")
            return TestCase::ShiueExampleOne;
        else if (testCaseName == "ShiueExampleTwo")
            return TestCase::ShiueExampleTwo;
        else if (testCaseName == "Rybak")
            return TestCase::Rybak;
        else if (testCaseName == "Schneider")
            return TestCase::Schneider;
        else
            DUNE_THROW(Dune::InvalidStateException, testCaseName + " is not a valid test case");
    }();

    using FreeFlowProblem = GetPropType<FreeFlowTypeTag, Properties::Problem>;
    auto freeFlowProblem = std::make_shared<FreeFlowProblem>(freeFlowGridGeometry, couplingManager, testCase, testCaseName);
    using DarcyProblem = GetPropType<DarcyTypeTag, Properties::Problem>;
    auto spatialParams = std::make_shared<typename DarcyProblem::SpatialParams>(darcyGridGeometry, testCase);
    auto darcyProblem = std::make_shared<DarcyProblem>(darcyGridGeometry, couplingManager, spatialParams, testCase, testCaseName);

    const auto freeFlowNumCellCenterDofs = freeFlowGridView.size(0);
    const auto freeFlowNumFaceDofs = (*freeFlowGridGeometry).numIntersections();
    // the solution vector
    Traits::SolutionVector sol;
    sol[freeFlowCellCenterIdx].resize(freeFlowNumCellCenterDofs);
    sol[freeFlowFaceIdx].resize(freeFlowNumFaceDofs);
    sol[darcyIdx].resize(darcyGridGeometry->numDofs());

    // get a solution vector storing references to the two FreeFlow solution vectors
    auto freeFlowSol = partial(sol, freeFlowFaceIdx, freeFlowCellCenterIdx);
    using FreeFlowSolutionVector = std::decay_t<decltype(freeFlowSol)>;

    couplingManager->init(freeFlowProblem, darcyProblem, sol);

    // the grid variables
    using FreeFlowGridVariables = GetPropType<FreeFlowTypeTag, Properties::GridVariables>;
    auto freeFlowGridVariables = std::make_shared<FreeFlowGridVariables>(freeFlowProblem, freeFlowGridGeometry);
    freeFlowGridVariables->init(freeFlowSol);
    using DarcyGridVariables = GetPropType<DarcyTypeTag, Properties::GridVariables>;
    auto darcyGridVariables = std::make_shared<DarcyGridVariables>(darcyProblem, darcyGridGeometry);
    darcyGridVariables->init(sol[darcyIdx]);

    // intialize the vtk output module
    using Scalar = typename Traits::Scalar;
    MyStaggeredVtkOutputModule<FreeFlowGridVariables, decltype(freeFlowSol)> freeFlowVtkWriter(*freeFlowGridVariables, freeFlowSol, freeFlowProblem->name());
    GetPropType<FreeFlowTypeTag, Properties::IOFields>::initOutputModule(freeFlowVtkWriter);
    auto freeFlowAnalyticalSolution = createFreeFlowAnalyticalSolution<Scalar>(*freeFlowProblem);
    freeFlowVtkWriter.addField(std::get<0>(freeFlowAnalyticalSolution), "pressureExact");
    freeFlowVtkWriter.addField(std::get<1>(freeFlowAnalyticalSolution), "velocityExact");
    freeFlowVtkWriter.addFaceField(std::get<2>(freeFlowAnalyticalSolution), "faceVelocityExact");

    VtkOutputModule<DarcyGridVariables, GetPropType<DarcyTypeTag, Properties::SolutionVector>> darcyVtkWriter(*darcyGridVariables, sol[darcyIdx],  darcyProblem->name());
    using DarcyVelocityOutput = GetPropType<DarcyTypeTag, Properties::VelocityOutput>;
    darcyVtkWriter.addVelocityOutput(std::make_shared<DarcyVelocityOutput>(*darcyGridVariables));
    GetPropType<DarcyTypeTag, Properties::IOFields>::initOutputModule(darcyVtkWriter);
    auto darcyAnalyticalSolution = createDarcyAnalyticalSolution<Scalar>(*darcyProblem);
    darcyVtkWriter.addField(std::get<0>(darcyAnalyticalSolution), "pressureExact");
    darcyVtkWriter.addField(std::get<1>(darcyAnalyticalSolution), "velocityExact");

    // the assembler for a stationary problem
    using Assembler = RefinedMultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(freeFlowProblem, freeFlowProblem, darcyProblem),
                                                 std::make_tuple(freeFlowGridGeometry->faceGridGeometryPtr(),
                                                                 freeFlowGridGeometry->cellCenterGridGeometryPtr(),
                                                                 darcyGridGeometry),
                                                 std::make_tuple(freeFlowGridVariables->faceGridVariablesPtr(),
                                                                 freeFlowGridVariables->cellCenterGridVariablesPtr(),
                                                                 darcyGridVariables),
                                                 couplingManager);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);

    printDarcyL2Error(*darcyProblem, sol[darcyIdx]);

    const Scalar refineTol = getParam<Scalar>("Adaptive.RefineTolerance");
    const Scalar coarsenTol = getParam<Scalar>("Adaptive.CoarsenTolerance");
    MyFreeflowGridAdaptIndicator<FreeFlowTypeTag> indicator(freeFlowProblem);
    const unsigned int numRefinementSteps = getParam<Scalar>("Adaptive.MaxLevel");

    FreeFlowGridDataTransfer<FreeFlowTypeTag, FreeFlowSolutionVector> freeflowDataTransfer(freeFlowProblem, freeFlowGridGeometry, freeFlowGridVariables, freeFlowSol);

    NavierStokesRefinedErrors freeFlowErrors(freeFlowProblem, freeFlowSol, 0);
    NavierStokesRefinedErrorCSVWriter freeFlowErrorCSVWriter(freeFlowProblem);

    for (unsigned int i=0; i < numRefinementSteps; ++i)
    {
        if (i>0)
        {
            // compute refinement indicator
            indicator.calculateVelocityError(freeFlowSol, std::get<1>(freeFlowAnalyticalSolution), refineTol, coarsenTol);

            // mark elements and maybe adapt grid
            bool wasAdapted = false;
            if (markElements(freeFlowGridManager.grid(), indicator))
                wasAdapted = adapt(freeFlowGridManager.grid(), freeflowDataTransfer);

            if (wasAdapted)
            {
                darcyGridManager.grid().globalRefine(1);

                freeFlowGridGeometry->update(freeFlowGridView);
                darcyGridGeometry->update(darcyGridView);

                freeFlowSol[FreeFlowGridGeometry::cellCenterIdx()] = 0.;
                freeFlowSol[FreeFlowGridGeometry::cellCenterIdx()].resize(freeFlowGridView.size(0));
                freeFlowSol[FreeFlowGridGeometry::faceIdx()] = 0.;
                freeFlowSol[FreeFlowGridGeometry::faceIdx()].resize((*freeFlowGridGeometry).numIntersections());
                sol[darcyIdx]= 0.;
                sol[darcyIdx].resize(darcyGridGeometry->numDofs());

                freeFlowGridVariables->updateAfterGridAdaption(freeFlowSol); //!< Initialize the secondary variables to the new (and "new old") solution
                darcyGridVariables->updateAfterGridAdaption(sol[darcyIdx]);

                couplingManager->update(sol);

                assembler->reSetJacobianSize(); // has to be after nonlinear solver
                assembler->reSetJacobianPattern(); //!< Tell the assembler to resize the matrix and set pattern
                assembler->reSetResidualSize(); //!< Tell the assembler to resize the residual
            }
        }

        nonLinearSolver.solve(sol);

        // write vtk output
        freeFlowAnalyticalSolution = createFreeFlowAnalyticalSolution<Scalar>(*freeFlowProblem);
        darcyAnalyticalSolution = createDarcyAnalyticalSolution<Scalar>(*darcyProblem);
        freeFlowVtkWriter.write(i+1.0);
        darcyVtkWriter.write(i+1.0);

        freeFlowErrors.update(freeFlowSol, i);
        freeFlowErrorCSVWriter.printErrors(freeFlowErrors);

        printDarcyL2Error(*darcyProblem, sol[darcyIdx]);
    }

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
