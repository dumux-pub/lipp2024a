import sys
import pandas as pd

pd.set_option("display.precision", 11)

df_abc_underlying_coarse = pd.read_csv('datafiles/errors_donea_10x10.csv')
df_d_underlying_coarse = pd.read_csv('datafiles/errors_donea_100x100.csv')
df_e_underlying_coarse_and_locally_refined = pd.read_csv('datafiles/errors_gauss_refined.csv')
df_a_reference = pd.read_csv('datafiles/errors_donea_11x11.csv')
df_a_locally_refined = pd.read_csv('datafiles/errors_donea_0406refined.csv')
df_b_reference = pd.read_csv('datafiles/errors_donea_13x13.csv')
df_b_locally_refined = pd.read_csv('datafiles/errors_donea_0307refined.csv')
df_c_reference = pd.read_csv('datafiles/errors_donea_16x16.csv')
df_c_locally_refined = pd.read_csv('datafiles/errors_donea_RightRefined.csv')
df_d_reference = pd.read_csv('datafiles/errors_donea_106x106.csv')
df_d_locally_refined = pd.read_csv('datafiles/errors_donea_100x100refined.csv')
df_e_reference = pd.read_csv('datafiles/errors_gauss_131x131.csv')

def latex_float(f):
    float_str = f
    if "e" in float_str:
        base, exponent = float_str.split("e")
        return r"{0}$\cdot 10^{{{1}}}$".format(base, int(exponent))
    else:
        return float_str

def myformat(f):
       if abs(f)<1e-15:
              return '{0:1.2e}'.format(0)
       else:
              return '{0:1.2e}'.format(f)

file1 = open(sys.argv[7], "w")
file1.write("\\begin{tabular}[c]{@{}l@{}}Change in L1\\\\norm of  $\\text{abs}\left(" + sys.argv[4] + "\\right)$\\end{tabular}& \\begin{tabular}[c]{@{}l@{}}Underlying $\\rightarrow$\\\\ Reference\\end{tabular} & ")
file1.write(latex_float(myformat(df_a_reference[sys.argv[1]][0]-df_abc_underlying_coarse[sys.argv[1]][0]))+
       ' & '+ latex_float(myformat(df_b_reference[sys.argv[1]][0]-df_abc_underlying_coarse[sys.argv[1]][0]))+
       ' & '+ latex_float(myformat(df_c_reference[sys.argv[1]][0]-df_abc_underlying_coarse[sys.argv[1]][0]))+
       ' & '+ latex_float(myformat(df_d_reference[sys.argv[1]][0]-df_d_underlying_coarse[sys.argv[1]][0]))+
       ' & '+ latex_float(myformat(df_e_reference[sys.argv[1]][0]-df_e_underlying_coarse_and_locally_refined[sys.argv[1]][0]))+
       '\\\\' + '\n')
file1.write("& \\begin{tabular}[c]{@{}l@{}}Underlying $\\rightarrow$\\\\ Locally refined\\end{tabular} &")
file1.write(latex_float(myformat(df_a_locally_refined[sys.argv[1]][0]-df_abc_underlying_coarse[sys.argv[1]][0]))+
       ' & '+ latex_float(myformat(df_b_locally_refined[sys.argv[1]][0]-df_abc_underlying_coarse[sys.argv[1]][0]))+
       ' & '+ latex_float(myformat(df_c_locally_refined[sys.argv[1]][0]-df_abc_underlying_coarse[sys.argv[1]][0]))+
       ' & '+ latex_float(myformat(df_d_locally_refined[sys.argv[1]][0]-df_d_underlying_coarse[sys.argv[1]][0]))+
       ' & '+ latex_float(myformat(df_e_underlying_coarse_and_locally_refined[sys.argv[1]][1]-df_e_underlying_coarse_and_locally_refined[sys.argv[1]][0]))+
       '\\\\\\hline')
file1.write('\n')

file1.write("\\begin{tabular}[c]{@{}l@{}}Change in L1\\\\norm of $\\text{abs}\\left(" + sys.argv[5] + "\\right)$\\end{tabular} & \\begin{tabular}[c]{@{}l@{}}Underlying $\\rightarrow$\\\\ Reference\\end{tabular} &")
file1.write(latex_float(myformat(df_a_reference[sys.argv[2]][0]-df_abc_underlying_coarse[sys.argv[2]][0]))+
       ' & '+ latex_float(myformat(df_b_reference[sys.argv[2]][0]-df_abc_underlying_coarse[sys.argv[2]][0]))+
       ' & '+ latex_float(myformat(df_c_reference[sys.argv[2]][0]-df_abc_underlying_coarse[sys.argv[2]][0]))+
       ' & '+ latex_float(myformat(df_d_reference[sys.argv[2]][0]-df_d_underlying_coarse[sys.argv[2]][0]))+
       ' & '+ latex_float(myformat(df_e_reference[sys.argv[2]][0]-df_e_underlying_coarse_and_locally_refined[sys.argv[2]][0]))+
       '\\\\' + '\n')
file1.write("& \\begin{tabular}[c]{@{}l@{}}Underlying $\\rightarrow$\\\\ Locally refined\\end{tabular} &")
file1.write(latex_float(myformat(df_a_locally_refined[sys.argv[2]][0]-df_abc_underlying_coarse[sys.argv[2]][0]))+
       ' & '+ latex_float(myformat(df_b_locally_refined[sys.argv[2]][0]-df_abc_underlying_coarse[sys.argv[2]][0]))+
       ' & '+ latex_float(myformat(df_c_locally_refined[sys.argv[2]][0]-df_abc_underlying_coarse[sys.argv[2]][0]))+
       ' & '+ latex_float(myformat(df_d_locally_refined[sys.argv[2]][0]-df_d_underlying_coarse[sys.argv[2]][0]))+
       ' & '+ latex_float(myformat(df_e_underlying_coarse_and_locally_refined[sys.argv[2]][1]-df_e_underlying_coarse_and_locally_refined[sys.argv[2]][0]))+
       '\\\\\\hline')
file1.write('\n')

file1.write("\\begin{tabular}[c]{@{}l@{}}Change in L1\\\\norm of $\\text{abs}\\left(" + sys.argv[6] + "\\right)$\\end{tabular} & \\begin{tabular}[c]{@{}l@{}}Underlying $\\rightarrow$\\\\ Reference\\end{tabular} &")
file1.write(latex_float(myformat(df_a_reference[sys.argv[3]][0]-df_abc_underlying_coarse[sys.argv[3]][0]))+
       ' & '+ latex_float(myformat(df_b_reference[sys.argv[3]][0]-df_abc_underlying_coarse[sys.argv[3]][0]))+
       ' & '+ latex_float(myformat(df_c_reference[sys.argv[3]][0]-df_abc_underlying_coarse[sys.argv[3]][0]))+
       ' & '+ latex_float(myformat(df_d_reference[sys.argv[3]][0]-df_d_underlying_coarse[sys.argv[3]][0]))+
       ' & '+ latex_float(myformat(df_e_reference[sys.argv[3]][0]-df_e_underlying_coarse_and_locally_refined[sys.argv[3]][0]))+
       '\\\\' + '\n')
file1.write("& \\begin{tabular}[c]{@{}l@{}}Underlying $\\rightarrow$\\\\ Locally refined\\end{tabular} &")
file1.write(latex_float(myformat(df_a_locally_refined[sys.argv[3]][0]-df_abc_underlying_coarse[sys.argv[3]][0]))+
       ' & '+ latex_float(myformat(df_b_locally_refined[sys.argv[3]][0]-df_abc_underlying_coarse[sys.argv[3]][0]))+
       ' & '+ latex_float(myformat(df_c_locally_refined[sys.argv[3]][0]-df_abc_underlying_coarse[sys.argv[3]][0]))+
       ' & '+ latex_float(myformat(df_d_locally_refined[sys.argv[3]][0]-df_d_underlying_coarse[sys.argv[3]][0]))+
       ' & '+ latex_float(myformat(df_e_underlying_coarse_and_locally_refined[sys.argv[3]][1]-df_e_underlying_coarse_and_locally_refined[sys.argv[3]][0]))+
       '\\\\')
file1.write("\\hline\n\\end{tabular}")

file1.close()



