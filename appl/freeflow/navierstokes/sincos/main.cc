// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \brief Test for the instationary staggered grid Navier-Stokes model
 *        with analytical solution.
 */

bool doFirstOrderLocalTruncErrorGlobalRefinement = false;

#include <config.h>

#include <ctime>
#include <iostream>
#include <type_traits>
#include <tuple>

#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>

namespace Dumux {
namespace Properties {
template<class TypeTag, class MyTypeTag>
struct FaceVolumeVariables { using type = UndefinedProperty; };                     //!< The secondary variables within a sub-control volume
}
}

#include <dumux/assembly/staggeredrefinedfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/grid/controlvolumegrids.hh>
#include <dumux/io/mystaggeredvtkoutputmodule.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/adaptive/adapt.hh>
#include <dumux/adaptive/markelements.hh>

#include <dumux/freeflow/gridadaptindicator.hh>
#include <dumux/freeflow/navierstokes/staggered/griddatatransfer.hh>

#include <dumux/discretization/staggered/myfvgridgeometry.hh>
#include <dumux/discretization/staggered/freeflow/fvgridgeometrytraits1.hh>

#include <dumux/freeflow/navierstokes/staggered/residualCalc.hh>
#include <dumux/freeflow/navierstokes/errors.hh>

#include <dumux/io/outputFacility.hh>

#include "problem.hh"

template<class Problem>
auto createSource(const Problem& problem)
{
    using Scalar = double;
    using Indices = typename Problem::Indices;

    const auto& gridGeometry = problem.gridGeometry();
    std::array<std::vector<Scalar>, Problem::ModelTraits::numEq()> source;

    for (auto& component : source)
    {
        component.resize(gridGeometry.numCellCenterDofs());
    }

    for (const auto& element : elements(gridGeometry.gridView()))
    {
        auto fvGeometry = localView(gridGeometry);
        fvGeometry.bindElement(element);
        for (auto&& scv : scvs(fvGeometry))
        {
            auto ccDofIdx = scv.dofIndex();
            auto ccDofPosition = scv.dofPosition();

            auto sourceAtPosVal = problem.sourceAtPos(ccDofPosition);

            source[Indices::momentumXBalanceIdx][ccDofIdx] = sourceAtPosVal[Indices::momentumXBalanceIdx];
            source[Indices::momentumYBalanceIdx][ccDofIdx] = sourceAtPosVal[Indices::momentumYBalanceIdx];
        }
    }

    return source;
}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::SincosTest;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    bool adapt = getParam<bool>("Adaptivity.Adapt", false);
    bool doFirstOrderLocalTruncError = getParam<bool>("Numerics.DoFirstOrderLocalTruncError", false);
    //when adapt is true refinement is local, not only global
    doFirstOrderLocalTruncErrorGlobalRefinement = !adapt && doFirstOrderLocalTruncError;

    using HelpingGridManager = Dumux::GridManager<GetPropType<TypeTag, Properties::HelpingGrid>>;
    HelpingGridManager helpingGridManager;
    helpingGridManager.init("Helper");
    const auto& helpingGridView = helpingGridManager.grid().leafGridView();
    Dune::DGFWriter dgfWriter(helpingGridView);
    std::string inputFileName = getParam<std::string>("Grid.File");
    dgfWriter.write(inputFileName);

    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager;
    gridManager.init();

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition =  typename Element::Geometry::GlobalCoordinate;

    if (adapt)
    {
        using Scalar = GetPropType<TypeTag, Properties::Scalar>;

        Scalar leftX = getParam<Scalar>("Adaptivity.LeftX");
        Scalar rightX = getParam<Scalar>("Adaptivity.RightX");
        Scalar lowerY = getParam<Scalar>("Adaptivity.LowerY");
        Scalar upperY = getParam<Scalar>("Adaptivity.UpperY");

        gridManager.grid().preAdapt();

        for (const auto& element : elements(leafGridView))
        {
            GlobalPosition pos = element.geometry().center();

            auto x = pos[0];
            auto y = pos[1];

            if((leftX < x) && (x < rightX) && (y > lowerY) && (y < upperY))
            {
                gridManager.grid().mark( 1,  element);
            }
        }
        gridManager.grid().adapt();
        gridManager.grid().postAdapt();
    }

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);

    // get some time loop parameters
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // instantiate time loop
    auto timeLoop = std::make_shared<TimeLoop<Scalar>>(0.0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);
    problem->updateTimeStepSize(timeLoop->timeStepSize());

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x;
    x[GridGeometry::cellCenterIdx()].resize(gridGeometry->numCellCenterDofs());
    x[GridGeometry::faceIdx()].resize(gridGeometry->numFaceDofs());
    problem->applyInitialSolution(x);
    auto xOld = x;

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    // initialize the vtk output module
    MyStaggeredVtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    IOFields::initOutputModule(vtkWriter); // Add model specific output fields

    auto source = createSource(*problem);
    auto sourceX = source[Problem::Indices::momentumXBalanceIdx];
    auto sourceY = source[Problem::Indices::momentumYBalanceIdx];
    vtkWriter.addField(sourceX, "sourceX");
    vtkWriter.addField(sourceY, "sourceY");

    problem->createOutputFields(timeLoop->timeStepSize(), x);

    vtkWriter.addField(problem->getAnalyticalPressureSolution(), "analyticalP");



    vtkWriter.addField(problem->getAnalyticalDxU(), "analyticalDxU");
    vtkWriter.addField(problem->getAnalyticalDyV(), "analyticalDyV");
    vtkWriter.addField(problem->getAnalyticalDivU(), "analyticalDivVectorU");

    vtkWriter.addField(problem->getNumericalDxU(), "numericalDxU");
    vtkWriter.addField(problem->getNumericalDyV(), "numericalDyV");
    vtkWriter.addField(problem->getNumericalDivU(), "numericalDivVectorU");



    using CellCenterSolutionVector = GetPropType<TypeTag, Properties::CellCenterSolutionVector>;
    CellCenterSolutionVector numericalCCResidualWithAnalytical;
    const auto numDofsCellCenter = leafGridView.size(0);
    numericalCCResidualWithAnalytical.resize(numDofsCellCenter);
    vtkWriter.addField(numericalCCResidualWithAnalytical, "localTruncErrorConti");

    const bool isStationary = getParam<bool>("Problem.IsStationary");

    // the assembler with time loop for instationary problem
    using Assembler = StaggeredRefinedFVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = isStationary ? std::make_shared<Assembler>(problem, gridGeometry, gridVariables)
                                  : std::make_shared<Assembler>(problem, gridGeometry, gridVariables, timeLoop, xOld);

    // the linear solver
    using LinearSolver = Dumux::UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    using HostGrid = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >;
    using HostGridManager = GridManager<HostGrid>;

    std::array<std::string, 4> paramGroups = {"finexCVs", "fineyCVs", "coarsexCVs", "coarseyCVs"}; //xfine...
    std::array<HostGridManager, 4> cvGridManagers = {};
    std::array<std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>>,4> cvCenterScvfIndicesMaps;
    CVOutputFacility<TypeTag> cvOutputFacility(problem);
    cvOutputFacility.iniCVGridManagers(cvGridManagers, cvCenterScvfIndicesMaps, paramGroups);
    std::vector<std::pair<unsigned int, Scalar>> pvdFileInfos;

    gridVariables->update(x);

    const bool shouldPrintErrors = getParam<bool>("Problem.PrintErrors", true);

    auto falseLambda = [](unsigned int dirIdx, GlobalPosition pos, Scalar min, Scalar max){return false;};
    auto primalFalseLambda = [](GlobalPosition pos, Scalar min, Scalar max){return false;};

    if (isStationary)
    {
        // linearize & solve
        Dune::Timer timer;
        nonLinearSolver.solve(x);

        //output
        problem->createOutputFields(x);

        NavierStokesRefinedErrors<Problem> errors(problem);
        NavierStokesRefinedErrorCSVWriter errorCSVWriter(problem);

        using FaceSolutionVector = GetPropType<TypeTag, Properties::FaceSolutionVector>;
        FaceSolutionVector numericalFaceResidualWithAnalytical;

        std::optional<std::array<std::vector<Scalar>, 4>> optionalPerfectInterpolationFaceResiduals;
        std::array<std::vector<Scalar>, 4> perfectInterpolationFaceResiduals;
        optionalPerfectInterpolationFaceResiduals.emplace(perfectInterpolationFaceResiduals);

        Scalar leftX = getParam<Scalar>("Adaptivity.LeftX");
        Scalar rightX = getParam<Scalar>("Adaptivity.RightX");
        Scalar lowerY = getParam<Scalar>("Adaptivity.LowerY");
        Scalar upperY = getParam<Scalar>("Adaptivity.UpperY");

        auto inRefined = [&](Scalar myX, Scalar myY){return myX > leftX && myX < rightX && myY > lowerY && myY < upperY;};

        auto coarseToFineInside = [&](unsigned int dirIdx, GlobalPosition pos, Scalar xMin, Scalar xMax){ return inRefined(pos[0],pos[1]); };
        auto coarseToFineLeftOrDown = [&](unsigned int dirIdx, GlobalPosition pos, Scalar xMin, Scalar xMax){
            if (dirIdx == 0)
                return scalarCmp(pos[0],leftX) && pos[1] > lowerY && pos[1] < upperY;
            else
                return scalarCmp(pos[1],lowerY) && pos[0] > leftX && pos[0] < rightX;
        };
        auto coarseToFineRightOrUp = [&](unsigned int dirIdx, GlobalPosition pos, Scalar xMin, Scalar xMax){
            if (dirIdx == 0)
                return scalarCmp(pos[0],rightX) && pos[1] > lowerY && pos[1] < upperY;
            else
                return scalarCmp(pos[1],upperY) && pos[0] > leftX && pos[0] < rightX;
        };
        auto coarseToFineRightOrUpEdgeDualGrid = [&](unsigned int dirIdx, GlobalPosition pos, Scalar xMin, Scalar xMax){
            if (dirIdx == 0)
                return scalarCmp(rightX,1.) && scalarCmp(pos[0],0.9) && pos[1] > lowerY && pos[1] < upperY ;
            else
                return false;
        };
        auto fineToCoarseInside = coarseToFineInside;

        ResidualCalc<TypeTag> residualCalc(problem);
        residualCalc.calcResidualsStationary(gridGeometry, cvGridManagers, cvCenterScvfIndicesMaps, numericalCCResidualWithAnalytical, numericalFaceResidualWithAnalytical, optionalPerfectInterpolationFaceResiduals, fineToCoarseInside,
            coarseToFineInside,
            coarseToFineLeftOrDown,
            coarseToFineRightOrUp,
            coarseToFineRightOrUpEdgeDualGrid);

        bool readDofBasedValues = getParam<bool>("Problem.ReadDofBasedValues", false);
        if (readDofBasedValues)
        {
            CellCenterSolutionVector readInPres;
            readInPres.resize(gridGeometry->numCellCenterDofs());
            ReadDofBasedResult<TypeTag> reader(problem);
            reader.readDofBasedResult(readInPres, "pressure");
            vtkWriter.addField(readInPres, "readInPres");
        }

        vtkWriter.write([&](GlobalPosition pos, Scalar xMin, Scalar xMax){return inRefined(pos[0],pos[1]);},0.);

        cvOutputFacility.onCVsOutputResidualsAndPrimVars(
            gridGeometry,
            cvGridManagers,
            cvCenterScvfIndicesMaps,
            paramGroups,
            x,
            0,
            numericalFaceResidualWithAnalytical,
            optionalPerfectInterpolationFaceResiduals,
            readDofBasedValues,
            fineToCoarseInside,
            coarseToFineInside,
            coarseToFineLeftOrDown,
            coarseToFineRightOrUp,
            coarseToFineRightOrUpEdgeDualGrid);

        if (getParam<bool>("Problem.PrintErrors", true))
        {
            errors.calc(x, numericalCCResidualWithAnalytical, numericalFaceResidualWithAnalytical, 0);
            errorCSVWriter.printErrors(errors);
        }

        pvdFileInfos.push_back(std::make_pair(0, 0));
        cvOutputFacility.printPvdFiles(paramGroups, pvdFileInfos);

        timer.stop();

        const auto& comm = Dune::MPIHelper::getCollectiveCommunication();
        std::cout << "Simulation took " << timer.elapsed() << " seconds on "
                << comm.size() << " processes.\n"
                << "The cumulative CPU time was " << timer.elapsed()*comm.size() << " seconds.\n";
    }
    else
    {
        NavierStokesRefinedErrors<Problem> errors(problem);
        NavierStokesRefinedErrorCSVWriter errorCSVWriter(problem);

        unsigned int timeIndex = 0;

        // time loop
        timeLoop->start(); do
        {
            Scalar actualCurrentTime = timeLoop->time()+timeLoop->timeStepSize();

            // set previous solution for storage evaluations
            assembler->setPreviousSolution(xOld);

            // solve the non-linear system with time step control
            nonLinearSolver.solve(x, *timeLoop);

            using FaceSolutionVector = GetPropType<TypeTag, Properties::FaceSolutionVector>;
            FaceSolutionVector numericalFaceResidualWithAnalytical;

            //output
            if (shouldPrintErrors)
            {
                errors.calc(x, numericalCCResidualWithAnalytical, numericalFaceResidualWithAnalytical, actualCurrentTime, timeIndex);
                errorCSVWriter.printErrors(errors);
            }

            std::optional<std::array<std::vector<Scalar>, 4>> optionalPerfectInterpolationFaceResiduals;
            std::array<std::vector<Scalar>, 4> perfectInterpolationFaceResiduals;
            optionalPerfectInterpolationFaceResiduals.emplace(perfectInterpolationFaceResiduals);

            ResidualCalc<TypeTag> residualCalc(problem);
            residualCalc.calcResidualsInstationary(timeIndex, timeLoop, gridGeometry, cvGridManagers, cvCenterScvfIndicesMaps, numericalCCResidualWithAnalytical, numericalFaceResidualWithAnalytical, optionalPerfectInterpolationFaceResiduals, falseLambda, falseLambda, falseLambda, falseLambda, falseLambda);

            bool readDofBasedValues = getParam<bool>("Problem.ReadDofBasedValues", false) && scalarCmp(getParam<Scalar>("Problem.TimeDofBasedValues"), actualCurrentTime);
            if (readDofBasedValues)
            {
                CellCenterSolutionVector IBAMRpres;
                IBAMRpres.resize(gridGeometry->numCellCenterDofs());
                ReadDofBasedResult<TypeTag> reader(problem);
                reader.readDofBasedResult(IBAMRpres, "pressure");
                vtkWriter.addField(IBAMRpres, "IBAMRpres");
            }

            problem->createOutputFields(actualCurrentTime, x);
            vtkWriter.write(primalFalseLambda, actualCurrentTime);

            if (readDofBasedValues)
                vtkWriter.removeField();

            cvOutputFacility.onCVsOutputResidualsAndPrimVars(
                gridGeometry,
                cvGridManagers,
                cvCenterScvfIndicesMaps,
                paramGroups,
                x,
                timeIndex,/*0 for stationary*/
                numericalFaceResidualWithAnalytical,
                optionalPerfectInterpolationFaceResiduals,
                readDofBasedValues,
                falseLambda, falseLambda, falseLambda, falseLambda, falseLambda);

            pvdFileInfos.push_back(std::make_pair(timeIndex, actualCurrentTime));

            // prepare next time step
            xOld = x;
            gridVariables->advanceTimeStep();
            timeLoop->advanceTimeStep();
            problem->updateTime(timeLoop->time());
            timeLoop->reportTimeStep();
            timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));
            problem->updateTimeStepSize(timeLoop->timeStepSize());

            ++timeIndex;
        } while (!timeLoop->finished());

        cvOutputFacility.printPvdFiles(paramGroups, pvdFileInfos);

        timeLoop->finalize(leafGridView.comm());
    }

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
