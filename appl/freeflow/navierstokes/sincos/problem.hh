// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \brief Test for the staggered grid Navier-Stokes model
 *        with analytical solution.
 */
#ifndef DUMUX_SINCOS_TEST_PROBLEM_HH
#define DUMUX_SINCOS_TEST_PROBLEM_HH

#include <dune/alugrid/grid.hh>
#include <dune/grid/yaspgrid.hh>

#include <dumux/assembly/staggeredrefinedlocalresidual.hh>

#include <dumux/common/numeqvector.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>

#include <dumux/freeflow/navierstokes/myproblem.hh>
#include <dumux/freeflow/navierstokes/model.hh>

#include <dumux/freeflow/navierstokes/boundarytypes.hh>
#include <dumux/freeflow/navierstokes/mylocalresidual.hh>
#include <dumux/freeflow/navierstokes/myfluxvariables.hh>
#include <dumux/freeflow/navierstokes/myvolumevariables.hh>
#include <dumux/freeflow/navierstokes/myiofields.hh>

#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/discretization/staggered/freeflow/myfacevariables.hh>
#include <dumux/discretization/staggered/freeflow/myvelocityoutput.hh>
#include <dumux/discretization/staggered/freeflow/mygridvolumevariables.hh>
#include <dumux/discretization/staggered/freeflow/elementvolumevariables1.hh>
#include <dumux/discretization/staggered/mygridfluxvariablescache.hh>
#include <dumux/discretization/staggered/myfacesolution.hh>
#include <dumux/discretization/staggered/mygridvariables.hh>
#include <dumux/discretization/staggered/myfvgridgeometry.hh>
#include <dumux/discretization/staggered/freeflow/fvgridgeometrytraits1.hh>

namespace Dumux {
template <class TypeTag>
class SincosTestProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct SincosTest { using InheritsFrom = std::tuple<StaggeredFreeFlowModel, NavierStokes>; };
} // end namespace TTag

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::SincosTest>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar> >;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::SincosTest> { using type = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming>; };

template<class TypeTag, class MyTypeTag>
struct HelpingGrid { using type = UndefinedProperty; };

// Set the grid type
template<class TypeTag>
struct HelpingGrid<TypeTag, TTag::SincosTest>
{
    using type = Dune::YaspGrid<2,Dune::TensorProductCoordinates<double, 2>>;
};

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::SincosTest> { using type = Dumux::SincosTestProblem<TypeTag> ; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::SincosTest> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::SincosTest> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::SincosTest> { static constexpr bool value = true; };

//! The variables living on the faces
template<class TypeTag>
struct FaceVariables<TypeTag, TTag::SincosTest>
{
private:
    using FacePrimaryVariables = GetPropType<TypeTag, Properties::FacePrimaryVariables>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
public:
    using type = MyStaggeredFaceVariables<ModelTraits, FacePrimaryVariables, GridView::dimension, upwindSchemeOrder>;
};

//! The velocity output
template<class TypeTag>
struct VelocityOutput<TypeTag, TTag::SincosTest>
{
    using type = MyStaggeredFreeFlowVelocityOutput<GetPropType<TypeTag, Properties::GridVariables>,
                                                 GetPropType<TypeTag, Properties::SolutionVector>>;
};

//! Set the volume variables property
template<class TypeTag>
struct DualCVsVolVars<TypeTag, TTag::SincosTest>
{
private:
    using VV = GetPropType<TypeTag, Properties::VolumeVariables>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    static constexpr auto dimWorld = GridView::dimensionworld;
    static constexpr int numPairs = 2 * (dimWorld - 1);

public:
    using type = NavierStokesDualCVsVolVars<VV, numPairs>;
};

//! Set the volume variables property
template<class TypeTag>
struct PrimalCVsVolVars<TypeTag, TTag::SincosTest>
{
private:
    using VV = GetPropType<TypeTag, Properties::VolumeVariables>;
public:
    using type = std::array<NaviesStokesPrimalScvfPair<VV>,8>;//TODO make work for dim != 2
};

//! Set the default global volume variables cache vector class
template<class TypeTag>
struct GridVolumeVariables<TypeTag, TTag::SincosTest>
{
private:
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using DualCVsVolVars = GetPropType<TypeTag, Properties::DualCVsVolVars>;
    using PrimalCVsVolVars = GetPropType<TypeTag, Properties::PrimalCVsVolVars>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using SubControlVolume = typename GetPropType<TypeTag, Properties::GridGeometry>::SubControlVolume;
    using SubControlVolumeFace = typename GetPropType<TypeTag, Properties::GridGeometry>::SubControlVolumeFace;
    static constexpr auto enableCache = getPropValue<TypeTag, Properties::EnableGridVolumeVariablesCache>();
    using Traits = MyStaggeredGridDefaultGridVolumeVariablesTraits<Problem, PrimalCVsVolVars, DualCVsVolVars, VolumeVariables, GridView, SubControlVolume, SubControlVolumeFace>;
public:
    using type = MyStaggeredGridVolumeVariables<Traits, enableCache>;
};

//! Set the BaseLocalResidual to StaggeredLocalResidual
template<class TypeTag>
struct BaseLocalResidual<TypeTag, TTag::SincosTest> { using type = StaggeredRefinedLocalResidual<TypeTag>; };

//! Set the face solution type
template<class TypeTag>
struct StaggeredFaceSolution<TypeTag, TTag::SincosTest>
{
private:
    using FaceSolutionVector = GetPropType<TypeTag, Properties::FaceSolutionVector>;
public:
    using type = Dumux::MyStaggeredFaceSolution<FaceSolutionVector>;
};

//! Set the grid variables (volume, flux and face variables)
template<class TypeTag>
struct GridVariables<TypeTag, TTag::SincosTest>
{
private:
    using GG = GetPropType<TypeTag, Properties::GridGeometry>;
    using GVV = GetPropType<TypeTag, Properties::GridVolumeVariables>;
    using GFVC = GetPropType<TypeTag, Properties::GridFluxVariablesCache>;
    using GFV = GetPropType<TypeTag, Properties::GridFaceVariables>;
public:
    using type = MyStaggeredGridVariables<GG, GVV, GFVC, GFV>;
};

//! The default fv grid geometry
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::SincosTest>
{
private:
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = typename GetPropType<TypeTag, Properties::Grid>::LeafGridView;
    using Traits = MyStaggeredFreeFlowDefaultGridGeometryTraits<GridView, upwindSchemeOrder>;
public:
    using type = MyStaggeredGridGeometry<GridView, enableCache, Traits>;
};

//! Set the volume variables property
template<class TypeTag>
struct VolumeVariables<TypeTag, TTag::SincosTest>
{
private:
    using PV = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using FSY = GetPropType<TypeTag, Properties::FluidSystem>;
    using FST = GetPropType<TypeTag, Properties::FluidState>;
    using MT = GetPropType<TypeTag, Properties::ModelTraits>;

    static_assert(FSY::numPhases == MT::numFluidPhases(), "Number of phases mismatch between model and fluid system");
    static_assert(FST::numPhases == MT::numFluidPhases(), "Number of phases mismatch between model and fluid state");
    static_assert(!FSY::isMiscible(), "The Navier-Stokes model only works with immiscible fluid systems.");

    using Traits = NavierStokesVolumeVariablesTraits<PV, FSY, FST, MT>;
public:
    using type = MyNavierStokesVolumeVariables<Traits>;
};

//! The local residual
template<class TypeTag>
struct LocalResidual<TypeTag, TTag::SincosTest> { using type = RefinedNavierStokesResidual<TypeTag>; };

//! The flux variables
template<class TypeTag>
struct FluxVariables<TypeTag, TTag::SincosTest> { using type = RefinedNavierStokesFluxVariables<TypeTag>; };

//! The specific I/O fields
template<class TypeTag>
struct IOFields<TypeTag, TTag::SincosTest> { using type = MyNavierStokesIOFields; };

//! Set the global flux variables cache vector class
template<class TypeTag>
struct GridFluxVariablesCache<TypeTag, TTag::SincosTest>
{
private:
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using FluxVariablesCache = GetPropType<TypeTag, Properties::FluxVariablesCache>;
    using FluxVariablesCacheFiller =  GetPropType<TypeTag, Properties::FluxVariablesCacheFiller>;
    static constexpr auto enableCache = getPropValue<TypeTag, Properties::EnableGridFluxVariablesCache>();
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
public:
    using type = MyStaggeredGridFluxVariablesCache<Problem, FluxVariablesCache, FluxVariablesCacheFiller, enableCache, upwindSchemeOrder>;
};

template<class TypeTag>
struct CVGridGeometry<TypeTag, TTag::SincosTest>
{
private:
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = typename Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >::LeafGridView;
    using Traits = MyStaggeredFreeFlowDefaultGridGeometryTraits<GridView, upwindSchemeOrder>;
public:
    using type = MyStaggeredGridGeometry<GridView, enableCache, Traits>;
};
} // end namespace Properties

/*!
 * \ingroup NavierStokesTests
 * \brief  Test problem for the staggered grid.
 *
 * The 2D, incompressible Navier-Stokes equations for zero gravity and a Newtonian
 * flow is solved and compared to an analytical solution (sums/products of trigonometric functions).
 * For the instationary case, the velocities and pressures are periodical in time. The Dirichlet boundary conditions are
 * consistent with the analytical solution and in the instationary case time-dependent.
 */
template <class TypeTag>
class SincosTestProblem : public MyNavierStokesProblem<TypeTag>
{
    using ParentType = MyNavierStokesProblem<TypeTag>;

    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using NumEqVector = Dumux::NumEqVector<GetPropType<TypeTag, Properties::PrimaryVariables>>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using TimeLoopPtr = std::shared_ptr<TimeLoop<Scalar>>;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using FVElementGeometry = typename GridGeometry::LocalView;

    static constexpr auto dimWorld = GetPropType<TypeTag, Properties::GridGeometry>::GridView::dimensionworld;
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using VelocityVector = Dune::FieldVector<Scalar, dimWorld>;

public:
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    SincosTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry), time_(0.0), timeStepSize_(0.0)
    {
        isStationary_ = getParam<bool>("Problem.IsStationary");
        vanDerPlasVersion_ = getParam<bool>("Problem.VanDerPlasVersion", false);

        if (vanDerPlasVersion_)
            isStationary_=true;

        enableInertiaTerms_ = getParam<bool>("Problem.EnableInertiaTerms");
        rho_ = getParam<Scalar>("Component.LiquidDensity");
        //kinematic
        Scalar nu = getParam<Scalar>("Component.LiquidKinematicViscosity", 1.0);
        //dynamic
        mu_ = rho_*nu;
    }

   /*!
     * \brief Returns the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 298.0; }

   /*!
     * \brief Return the sources within the domain.
     *
     * \param globalPos The global position
     */
    NumEqVector instationarySourceAtPos(const GlobalPosition &globalPos, Scalar t) const
    {
        NumEqVector source(0.0);
        const Scalar x = globalPos[0];
        const Scalar y = globalPos[1];

        source[Indices::momentumXBalanceIdx] = qx_(x,y);
        source[Indices::momentumYBalanceIdx] = qy_(x,y);

        return source;
    }


    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    {
         const Scalar t = time_ + timeStepSize_;
         return instationarySourceAtPos(globalPos,t);
    }

    // \}
   /*!
     * \name Boundary conditions
     */
    // \{

   /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        // set Dirichlet values for the velocity everywhere
        values.setDirichlet(Indices::velocityXIdx);
        values.setDirichlet(Indices::velocityYIdx);

        return values;
    }

    /*!
     * \brief Returns whether a fixed Dirichlet value shall be used at a given cell.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param scv The sub control volume
     * \param pvIdx The primary variable index in the solution vector
     */
    bool isDirichletCell(const Element& element,
                         const FVElementGeometry& fvGeometry,
                         const SubControlVolume& scv,
                         int pvIdx) const
    {
        // set fixed pressure in one cell
        return (scv.dofIndex() == 0) && pvIdx == Indices::pressureIdx;
    }

   /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume face (velocities)
     *
     * \param element The finite element
     * \param scvf the sub control volume face
     *
     * Concerning the usage of averaged velocity, see the explanation of the initial function. The average of the nondirection velocity along an scvf is required in the context of dirichlet boundary conditions for the functions getParallelVelocityFromBoundary_ and getParallelVelocityFromOtherBoundary_ within myfluxvariables.hh. There dirichlet is called for ghost faces built from the normalFace but the value then is taken in the the direction scvf.directionIndex, which is along that normalFace.
     */
    PrimaryVariables dirichlet(const Element& element, const SubControlVolumeFace& scvf) const
    {
        Scalar time = time_+timeStepSize_;

        PrimaryVariables priVars(0.0);

        unsigned int dirIdx = scvf.directionIndex();
        priVars[Indices::velocity(dirIdx)] = this->instationaryAnalyticalVelocitySolutionAtPos(scvf, time);
        unsigned int nonDirIdx = (dirIdx == 0)?1:0;
        priVars[Indices::velocity(nonDirIdx)] = this->instationaryAnalyticalNonDirVelocitySolutionAtPos(scvf, time);

        return priVars;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume face (pressure)
     *
     * \param element The finite element
     * \param scv the sub control volume
     */
    PrimaryVariables dirichlet(const Element& element, const SubControlVolume& scv) const
    {
        Scalar time = time_+timeStepSize_;

        PrimaryVariables priVars(0.0);
        priVars[Indices::pressureIdx] = this->instationaryAnalyticalPressureSolutionAtPos(scv, time);
        return priVars;
    }

    /*!
     * \brief Returns the analytical solution of the problem at a given time and position.
     *
     * \param globalPos The global position
     * \param time The current simulation time
     */
    PrimaryVariables instationaryAnalyticalSolutionAtPos(const GlobalPosition& globalPos, const Scalar t) const
    {
        const Scalar x = globalPos[0];
        const Scalar y = globalPos[1];

        PrimaryVariables values = {};

        values[Indices::pressureIdx] = p_(x,y) ;
        values[Indices::velocityXIdx] = u_(x,y);
        values[Indices::velocityYIdx] = v_(x,y);

        return values;
    }

    /*!
     * \brief Returns the analytical solution of the problem at a given position.
     *
     * \param globalPos The global position
     */
    PrimaryVariables analyticalSolutionAtPos(const GlobalPosition& globalPos) const
    {
        if(!isStationary_)
            DUNE_THROW(Dune::InvalidStateException, "analyticalSolutionAtPos was supposed to be used only be the stationary problem.");

        return this->instationaryAnalyticalSolutionAtPos(globalPos, time_+timeStepSize_);
    }

    // \}

   /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the initial value for a sub control volume face (velocities)
     *
     * Simply assigning the value of the analytical solution at the face center
     * gives a discrete solution that is not divergence-free. For small initial
     * time steps, this has a negative impact on the pressure solution
     * after the first time step. The flag UseVelocityAveraging triggers the
     * function averagedVelocity_ which uses a higher order quadrature formula to
     * bring the discrete solution sufficiently close to being divergence-free.
     */
    PrimaryVariables initial(const SubControlVolumeFace& scvf) const
    {
        bool startWithAnalytical = getParam<bool>("Problem.StartWithAnalytical", false);

        if (startWithAnalytical || !isStationary_)
        {
            PrimaryVariables priVars(0.0);
            priVars[Indices::velocity(scvf.directionIndex())] = this->instationaryAnalyticalVelocitySolutionAtPos(scvf, 0.);
            return priVars;
        }
        else
        {
            PrimaryVariables values;
            values[Indices::pressureIdx] = 0.0;
            values[Indices::velocityXIdx] = 0.0;
            values[Indices::velocityYIdx] = 0.0;

            return values;
        }
    }

    /*!
     * \brief Evaluates the initial value for a control volume (pressure)
     */
    PrimaryVariables initial(const SubControlVolume& scv) const
    {
        bool startWithAnalytical = getParam<bool>("Problem.StartWithAnalytical", false);

        if (startWithAnalytical || !isStationary_)
        {
            PrimaryVariables priVars(0.0);
            priVars[Indices::pressureIdx] = this->instationaryAnalyticalPressureSolutionAtPos(scv, 0.);
            return priVars;
        }
        else
        {
            PrimaryVariables values;
            values[Indices::pressureIdx] = 0.0;
            values[Indices::velocityXIdx] = 0.0;
            values[Indices::velocityYIdx] = 0.0;

            return values;
        }
    }

    /*!
     * \brief Updates the time
     */
    void updateTime(const Scalar time)
    {
        time_ = time;
    }

    /*!
     * \brief Updates the time step size
     */
    void updateTimeStepSize(const Scalar timeStepSize)
    {
        timeStepSize_ = timeStepSize;
    }

private:
    double intf2_(double x) const
    { return std::sin(2.*M_PI*x)/(2.*M_PI*std::sqrt(2.*M_PI)); }

    double f2_(double x) const
    { return std::cos(2.*M_PI*x)/std::sqrt(2.*M_PI); }

    double df2_(double x) const
    { return -std::sqrt(2.*M_PI)*std::sin(2.*M_PI*x); }

    double ddf2_(double x) const
    { return -std::sqrt(2.*M_PI)*2.*M_PI*std::cos(2.*M_PI*x); }

    double dddf2_(double x) const
    { return std::sqrt(2.*M_PI)*4.*M_PI*M_PI*std::sin(2.*M_PI*x); }

    double dxP_ (double x, double y) const
    { return std::sqrt(2.*M_PI)*(y*df2_(x) + f2_(y)); }

    double dyP_ (double x, double y) const
    { return std::sqrt(2.*M_PI)*(f2_(x) + x*df2_(y)); }

    double dxU_ (double x, double y) const
    { return df2_(x)*df2_(y); }

    double dyU_ (double x, double y) const
    { return f2_(x)*ddf2_(y); }

    double dxxU_ (double x, double y) const
    { return ddf2_(x)*df2_(y); }

    double dxyU_ (double x, double y) const
    { return df2_(x)*ddf2_(y); }

    double dyyU_ (double x, double y) const
    { return f2_(x)*dddf2_(y); }

    double dxV_ (double x, double y) const
    { return -ddf2_(x)*f2_(y); }

    double dyV_ (double x, double y) const
    { return -df2_(y)*df2_(x); }

    double dyyV_ (double x, double y) const
    { return -ddf2_(y)*df2_(x); }

    double dxyV_ (double x, double y) const
    { return -df2_(y)*ddf2_(x); }

    double dxxV_ (double x, double y) const
    { return -f2_(y)*dddf2_(x); }

    double p_ (double x, double y) const
    { return std::sqrt(2.*M_PI)*(y*f2_(x) + x*f2_(y)); }

    double u_ (double x, double y) const
    { return f2_(x)*df2_(y); }

    double v_ (double x, double y) const
    { return -df2_(x)*f2_(y); }

    double dxUU_ (double x, double y) const
    { return 2.*u_(x,y)*dxU_(x,y); }

    double dyVV_ (double x, double y) const
    { return 2.*v_(x,y)*dyV_(x,y); }

    double dyUV_ (double x, double y) const
    { return u_(x,y)*dyV_(x,y) + v_(x,y)*dyU_(x,y); }

    double dxUV_ (double x, double y) const
    { return u_(x,y)*dxV_(x,y) + v_(x,y)*dxU_(x,y); }

    double qx_ (double x, double y) const
    {
        double retVal = -2.0*mu_*dxxU_(x,y) - mu_*dyyU_(x,y) - mu_*dxyV_(x,y) + dxP_(x,y);

        if (enableInertiaTerms_)
            retVal += rho_*dxUU_(x,y) + rho_*dyUV_(x,y);

        return retVal; }

    double qy_ (double x, double y) const
    {
        double retVal = -2.0*mu_*dyyV_(x,y) - mu_*dxyU_(x,y) - mu_*dxxV_(x,y) + dyP_(x,y);

        if (enableInertiaTerms_)
            retVal += rho_*dxUV_(x,y) + rho_*dyVV_(x,y);

        return retVal;
    }

    Scalar rho_;
    Scalar mu_;

    static constexpr Scalar eps_ = 1e-6;

    Scalar time_;
    Scalar timeStepSize_;

    bool isStationary_;
    bool vanDerPlasVersion_;
    bool enableInertiaTerms_;
};

} // end namespace Dumux

#endif // DUMUX_SINCOS_TEST_PROBLEM_HH
