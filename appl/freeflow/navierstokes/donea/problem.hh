// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \brief Test for the staggered grid (Navier-)Stokes model with analytical solution (Donea et al., 2003)
 */
#ifndef DUMUX_DONEA_TEST_PROBLEM_HH
#define DUMUX_DONEA_TEST_PROBLEM_HH

#include <dune/alugrid/grid.hh>
#include <dune/subgrid/subgrid.hh>
#include <dune/grid/yaspgrid.hh>

#include <dumux/assembly/staggeredrefinedlocalresidual.hh>

#include <dumux/common/numeqvector.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>

#include <dumux/freeflow/navierstokes/boundarytypes.hh>
#include <dumux/freeflow/navierstokes/myproblem.hh>
#include <dumux/freeflow/navierstokes/mylocalresidual.hh>
#include <dumux/freeflow/navierstokes/myfluxvariables.hh>
#include <dumux/freeflow/navierstokes/myvolumevariables.hh>
#include <dumux/freeflow/navierstokes/myiofields.hh>
#include <dumux/freeflow/navierstokes/model.hh>

#include <dumux/discretization/method.hh>
#include <dumux/discretization/staggered/myfacesolution.hh>
#include <dumux/discretization/staggered/mygridvariables.hh>
#include <dumux/discretization/staggered/myfvgridgeometry.hh>
#include <dumux/discretization/staggered/freeflow/fvgridgeometrytraits1.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>

#include <dumux/discretization/staggered/freeflow/myfacevariables.hh>
#include <dumux/discretization/staggered/freeflow/elementvolumevariables1.hh>
#include <dumux/discretization/staggered/freeflow/myvelocityoutput.hh>
#include <dumux/discretization/staggered/freeflow/mygridvolumevariables.hh>
#include <dumux/discretization/staggered/mygridfluxvariablescache.hh>

namespace Dumux
{
template <class TypeTag>
class DoneaTestProblem;

namespace Properties
{
// Create new type tags
namespace TTag {
struct DoneaTestTypeTag { using InheritsFrom = std::tuple<NavierStokes, StaggeredFreeFlowModel>; };
} // end namespace TTag

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::DoneaTestTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar> >;
};

template<class TypeTag, class MyTypeTag>
struct HelpingGrid { using type = UndefinedProperty; };

// Set the grid type
template<class TypeTag>
struct HelpingGrid<TypeTag, TTag::DoneaTestTypeTag>
{
    using type = Dune::YaspGrid<2,Dune::TensorProductCoordinates<double, 2>>;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::DoneaTestTypeTag>
{
private:
    static constexpr auto dim = 2;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using HostGrid = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >;
    using type = Dune::SubGrid<dim, HostGrid>;
};

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::DoneaTestTypeTag> { using type = Dumux::DoneaTestProblem<TypeTag> ; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::DoneaTestTypeTag> { static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::DoneaTestTypeTag> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::DoneaTestTypeTag> { static constexpr bool value = true; };

//! The variables living on the faces
template<class TypeTag>
struct FaceVariables<TypeTag, TTag::DoneaTestTypeTag>
{
private:
    using FacePrimaryVariables = GetPropType<TypeTag, Properties::FacePrimaryVariables>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
public:
    using type = MyStaggeredFaceVariables<ModelTraits, FacePrimaryVariables, GridView::dimension, upwindSchemeOrder>;
};

//! The velocity output
template<class TypeTag>
struct VelocityOutput<TypeTag, TTag::DoneaTestTypeTag>
{
    using type = MyStaggeredFreeFlowVelocityOutput<GetPropType<TypeTag, Properties::GridVariables>,
                                                 GetPropType<TypeTag, Properties::SolutionVector>>;
};

//! Set the volume variables property
template<class TypeTag>
struct DualCVsVolVars<TypeTag, TTag::DoneaTestTypeTag>
{
private:
    using VV = GetPropType<TypeTag, Properties::VolumeVariables>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    static constexpr auto dimWorld = GridView::dimensionworld;
    static constexpr int numPairs = 2 * (dimWorld - 1);

public:
    using type = NavierStokesDualCVsVolVars<VV, numPairs>;
};

//! Set the volume variables property
template<class TypeTag>
struct PrimalCVsVolVars<TypeTag, TTag::DoneaTestTypeTag>
{
public:
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using type = std::array<NaviesStokesPrimalScvfPair<VolumeVariables>,8>;//TODO make work for dim != 2
};

template<class TypeTag>
struct GridVolumeVariables<TypeTag, TTag::DoneaTestTypeTag>
{
private:
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using DualCVsVolVars = GetPropType<TypeTag, Properties::DualCVsVolVars>;
    using PrimalCVsVolVars = GetPropType<TypeTag, Properties::PrimalCVsVolVars>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using SubControlVolume = typename GetPropType<TypeTag, Properties::GridGeometry>::SubControlVolume;
    using SubControlVolumeFace = typename GetPropType<TypeTag, Properties::GridGeometry>::SubControlVolumeFace;
    static constexpr auto enableCache = getPropValue<TypeTag, Properties::EnableGridVolumeVariablesCache>();
    using Traits = MyStaggeredGridDefaultGridVolumeVariablesTraits<Problem, PrimalCVsVolVars, DualCVsVolVars, VolumeVariables, GridView, SubControlVolume, SubControlVolumeFace>;
public:
    using type = MyStaggeredGridVolumeVariables<Traits, enableCache>;
};

//! Set the BaseLocalResidual to StaggeredLocalResidual
template<class TypeTag>
struct BaseLocalResidual<TypeTag, TTag::DoneaTestTypeTag> { using type = StaggeredRefinedLocalResidual<TypeTag>; };

//! Set the face solution type
template<class TypeTag>
struct StaggeredFaceSolution<TypeTag, TTag::DoneaTestTypeTag>
{
private:
    using FaceSolutionVector = GetPropType<TypeTag, Properties::FaceSolutionVector>;
public:
    using type = Dumux::MyStaggeredFaceSolution<FaceSolutionVector>;
};

//! Set the grid variables (volume, flux and face variables)
template<class TypeTag>
struct GridVariables<TypeTag, TTag::DoneaTestTypeTag>
{
private:
    using GG = GetPropType<TypeTag, Properties::GridGeometry>;
    using GVV = GetPropType<TypeTag, Properties::GridVolumeVariables>;
    using GFVC = GetPropType<TypeTag, Properties::GridFluxVariablesCache>;
    using GFV = GetPropType<TypeTag, Properties::GridFaceVariables>;
public:
    using type = MyStaggeredGridVariables<GG, GVV, GFVC, GFV>;
};

//! The default fv grid geometry
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::DoneaTestTypeTag>
{
private:
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = typename GetPropType<TypeTag, Properties::Grid>::LeafGridView;
    using Traits = MyStaggeredFreeFlowDefaultGridGeometryTraits<GridView, upwindSchemeOrder>;
public:
    using type = MyStaggeredGridGeometry<GridView, enableCache, Traits>;
};

//! Set the volume variables property
template<class TypeTag>
struct VolumeVariables<TypeTag, TTag::DoneaTestTypeTag>
{
private:
    using PV = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using FSY = GetPropType<TypeTag, Properties::FluidSystem>;
    using FST = GetPropType<TypeTag, Properties::FluidState>;
    using MT = GetPropType<TypeTag, Properties::ModelTraits>;

    static_assert(FSY::numPhases == MT::numFluidPhases(), "Number of phases mismatch between model and fluid system");
    static_assert(FST::numPhases == MT::numFluidPhases(), "Number of phases mismatch between model and fluid state");
    static_assert(!FSY::isMiscible(), "The Navier-Stokes model only works with immiscible fluid systems.");

    using Traits = NavierStokesVolumeVariablesTraits<PV, FSY, FST, MT>;
public:
    using type = MyNavierStokesVolumeVariables<Traits>;
};

//! The local residual
template<class TypeTag>
struct LocalResidual<TypeTag, TTag::DoneaTestTypeTag> { using type = RefinedNavierStokesResidual<TypeTag>; };

//! The flux variables
template<class TypeTag>
struct FluxVariables<TypeTag, TTag::DoneaTestTypeTag> { using type = RefinedNavierStokesFluxVariables<TypeTag>; };

template<class TypeTag>
struct CVGridGeometry<TypeTag, TTag::DoneaTestTypeTag>
{
private:
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = typename Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >::LeafGridView;
    using Traits = MyStaggeredFreeFlowDefaultGridGeometryTraits<GridView, upwindSchemeOrder>;
public:
    using type = MyStaggeredGridGeometry<GridView, enableCache, Traits>;
};

//! The specific I/O fields
template<class TypeTag>
struct IOFields<TypeTag, TTag::DoneaTestTypeTag> {
#if NONISOTHERMAL
    using type = FreeflowNonIsothermalIOFields<MyNavierStokesIOFields>;
#else
    using type = MyNavierStokesIOFields;
#endif
};

//! Set the global flux variables cache vector class
template<class TypeTag>
struct GridFluxVariablesCache<TypeTag, TTag::DoneaTestTypeTag>
{
private:
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using FluxVariablesCache = GetPropType<TypeTag, Properties::FluxVariablesCache>;
    using FluxVariablesCacheFiller =  GetPropType<TypeTag, Properties::FluxVariablesCacheFiller>;
    static constexpr auto enableCache = getPropValue<TypeTag, Properties::EnableGridFluxVariablesCache>();
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
public:
    using type = MyStaggeredGridFluxVariablesCache<Problem, FluxVariablesCache, FluxVariablesCacheFiller, enableCache, upwindSchemeOrder>;
};
}

/*!
 * \ingroup NavierStokesTests
 * \brief  Test problem for the staggered grid (Donea et al., 2003)
 * \todo doc me!
 */
template <class TypeTag>
class DoneaTestProblem : public MyNavierStokesProblem<TypeTag>
{
    using ParentType = MyNavierStokesProblem<TypeTag>;

    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using NumEqVector = Dumux::NumEqVector<GetPropType<TypeTag, Properties::PrimaryVariables>>;
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using FaceSolutionVector = GetPropType<TypeTag, Properties::FaceSolutionVector>;

    static constexpr auto dimWorld = GetPropType<TypeTag, Properties::GridGeometry>::GridView::dimensionworld;
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using VelocityVector = Dune::FieldVector<Scalar, dimWorld>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GetPropType<TypeTag, Properties::GridGeometry>::SubControlVolumeFace;

public:
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    DoneaTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry), eps_(1e-6)
    {
        mu_ = getParam<Scalar>("Component.LiquidKinematicViscosity", 1.0);
    }

   /*!
     * \name Problem parameters
     */
    // \{

    bool shouldWriteRestartFile() const
    {
        return false;
    }



   /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 298.0; }

   /*!
     * \brief Return the sources within the domain.
     *
     * \param globalPos The global position
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    {
        NumEqVector source(0.0);
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        source[Indices::momentumXBalanceIdx] = -2.0*mu_*dxxU_(x,y) - mu_*dyyU_(x,y) - mu_*dxyV_(x,y) + dxP_(x,y);
        source[Indices::momentumYBalanceIdx] = -2.0*mu_*dyyV_(x,y) - mu_*dxyU_(x,y) - mu_*dxxV_(x,y) + dyP_(x,y);

        return source;
    }
    // \}
   /*!
     * \name Boundary conditions
     */
    // \{

   /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;

        // set Dirichlet values for the velocity and pressure everywhere
        values.setDirichlet(Indices::velocityXIdx);
        values.setDirichlet(Indices::velocityYIdx);
        return values;
    }

    /*!
    * \brief Returns whether a fixed Dirichlet value shall be used at a given cell.
    *
    * \param element The finite element
    * \param fvGeometry The finite-volume geometry
    * \param scv The sub control volume
    * \param pvIdx The primary variable index in the solution vector
    */
    bool isDirichletCell(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const SubControlVolume& scv,
                        int pvIdx) const
    {
        // set fixed pressure in all cells at the left boundary
        auto isBoundary = [&](const FVElementGeometry& fvGeometry)
        {
            if (fvGeometry.hasBoundaryScvf())
            {
                return true;
            }
            return false;
        };
        return (isBoundary(fvGeometry) && pvIdx == Indices::pressureIdx);
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume face (velocities)
     *
     * \param element The finite element
     * \param scvf the sub control volume face
     *
     * Concerning the usage of averaged velocity, see the explanation of the initial function. The average of the nondirection velocity along an scvf is required in the context of dirichlet boundary conditions for the functions getParallelVelocityFromBoundary_ and getParallelVelocityFromOtherBoundary_ within myfluxvariables.hh. There dirichlet is called for ghost faces built from the normalFace but the value then is taken in the the direction scvf.directionIndex, which is along that normalFace.
      */
    PrimaryVariables dirichlet(const Element& element, const SubControlVolumeFace& scvf) const
    {
        PrimaryVariables priVars(0.0);

        unsigned int dirIdx = scvf.directionIndex();
        priVars[Indices::velocity(dirIdx)] = this->analyticalVelocitySolutionAtPos(scvf);
        unsigned int nonDirIdx = (dirIdx == 0)?1:0;
        priVars[Indices::velocity(nonDirIdx)] = this->analyticalNonDirVelocitySolutionAtPos(scvf);

        return priVars;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume face (pressure)
     *
     * \param element The finite element
     * \param scv the sub control volume
     */
    PrimaryVariables dirichlet(const Element& element, const SubControlVolume& scv) const
    {
        PrimaryVariables priVars(0.0);
        priVars[Indices::pressureIdx] = this->analyticalPressureSolutionAtPos(scv);
        return priVars;
    }

    /*!
     * \brief Return the analytical solution of the problem at a given position
     *
     * \param globalPos The global position
     */
    PrimaryVariables analyticalSolutionAtPos(const GlobalPosition& globalPos) const
    {
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        PrimaryVariables values = {};

        values[Indices::pressureIdx] = f1_(x);
        values[Indices::velocityXIdx] = f2_(x)*df2_(y);
        values[Indices::velocityYIdx] = -df2_(x)*f2_(y);

        return values;
    }

    // \}

   /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the initial value for a sub control volume face (velocities)
     *
     * Simply assigning the value of the analytical solution at the face center
     * gives a discrete solution that is not divergence-free. For small initial
     * time steps, this has a negative impact on the pressure solution
     * after the first time step. The flag UseVelocityAveraging triggers the
     * function averagedVelocity_ which uses a higher order quadrature formula to
     * bring the discrete solution sufficiently close to being divergence-free.
     */
    PrimaryVariables initial(const SubControlVolumeFace& scvf) const
    {
        bool startWithAnalytical = getParam<bool>("Problem.StartWithAnalytical", false);

        if (startWithAnalytical)
        {
            PrimaryVariables priVars(0.0);
            priVars[Indices::velocity(scvf.directionIndex())] = this->analyticalVelocitySolutionAtPos(scvf);
            return priVars;
        }
        else
        {
            PrimaryVariables values;
            values[Indices::pressureIdx] = 0.0;
            values[Indices::velocityXIdx] = 0.0;
            values[Indices::velocityYIdx] = 0.0;

            return values;
        }
    }

    /*!
     * \brief Evaluates the initial value for a control volume (pressure)
     */
    PrimaryVariables initial(const SubControlVolume& scv) const
    {
        bool startWithAnalytical = getParam<bool>("Problem.StartWithAnalytical", false);

        if (startWithAnalytical)
        {
            PrimaryVariables priVars(0.0);
            priVars[Indices::pressureIdx] = this->analyticalPressureSolutionAtPos(scv);
            return priVars;
        }
        else
        {
            PrimaryVariables values;
            values[Indices::pressureIdx] = 0.0;
            values[Indices::velocityXIdx] = 0.0;
            values[Indices::velocityYIdx] = 0.0;

            return values;
        }
    }

private:
    Scalar f1_(Scalar x) const
    { return x*(1.0-x); /*x - x^2*/ }

    Scalar df1_(Scalar x) const
    { return 1.0 - 2.0*x; }

    Scalar f2_(Scalar x) const
    { return f1_(x)*f1_(x); /*=x^2*(1-2x+x^2)=x^2-2x^3+x^4*/ }

    Scalar df2_(Scalar x) const
    { return 2.0*x - 6.0*x*x + 4.0*x*x*x; }

    Scalar ddf2_(Scalar x) const
    { return 2.0 - 12.0*x + 12.0*x*x; }

    Scalar dddf2_(Scalar x) const
    { return - 12.0 + 24.0*x; }

    Scalar dxP_ (Scalar x, Scalar y) const
    { return df1_(x); }

    Scalar dyP_ (Scalar x, Scalar y) const
    { return 0.0; }

    Scalar dxU_ (Scalar x, Scalar y) const
    { return df2_(x)*df2_(y); }

    Scalar dxxU_ (Scalar x, Scalar y) const
    { return ddf2_(x)*df2_(y); }

    Scalar dxyU_ (Scalar x, Scalar y) const
    { return df2_(x)*ddf2_(y); }

    Scalar dyyU_ (Scalar x, Scalar y) const
    { return f2_(x)*dddf2_(y); }

    Scalar dyV_ (Scalar x, Scalar y) const
    { return -df2_(y)*df2_(x); }

    Scalar dyyV_ (Scalar x, Scalar y) const
    { return -ddf2_(y)*df2_(x); }

    Scalar dxyV_ (Scalar x, Scalar y) const
    { return -df2_(y)*ddf2_(x); }

    Scalar dxxV_ (Scalar x, Scalar y) const
    { return -f2_(y)*dddf2_(x); }

    Scalar eps_;
    Scalar mu_;
};
} //end namespace

#endif
