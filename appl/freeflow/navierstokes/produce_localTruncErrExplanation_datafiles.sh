#!/bin/bash

#dumux commit 2ec8f9159a
#dune releases/2.9
#Example of successful use: dumux1/dumux-adaptivestaggered/build-cmake/appl/freeflow/navierstokes$ bash ./produce_localTruncErrExplanation_datafiles.sh

######\subsection{Understanding local-refinement-specific aspects of local truncation errors}
#non-refined regular grids
cd donea
make test_ff_stokes_donea

./test_ff_stokes_donea params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true -Adaptivity.Adapt false -Helper.Grid.Cells0 13 -Helper.Grid.Positions0 "0.0125 0.9875"
cp localTruncErrorMom_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_13x10Grid.csv

./test_ff_stokes_donea params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true -Adaptivity.Adapt false -Helper.Grid.Cells0 13 -Helper.Grid.Positions0 "0.0125 0.9875" -Helper.Grid.Cells1 20
cp localTruncErrorMom_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_13x20Grid.csv

./test_ff_stokes_donea params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true -Adaptivity.Adapt false
cp localTruncErrorMom_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_10x10Grid.csv
cp projCoarseOnCVOptD_localTruncErrorMom_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD.csv
cp projCoarseOnCVOptD_numericalVelocity_x-00000.csv ../../../datafiles/solErr_donea_U_10x10_projCoarseOnCVOptD_0406unrefined.csv
cp projCoarseOnCVOptD_analyticalVelocity_x-00000.csv ../../../datafiles/solErr_donea_Uexact_10x10_projCoarseOnCVOptD_0406unrefined.csv
cp test_donea_error.csv ../../../datafiles/errors_donea_10x10.csv

./test_ff_stokes_donea params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true -Adaptivity.Adapt false -Helper.Grid.Cells0 20 -Helper.Grid.Cells1 20
cp localTruncErrorMom_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_20x20Grid.csv

#mymethod locally refined, with conservation
./test_ff_stokes_donea params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true
cp localTruncErrorMom_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_WithConserv.csv
cp numericalVelocity_x-00000.csv ../../../datafiles/solErr_u.csv
cp analyticalVelocity_x-00000.csv ../../../datafiles/solErr_uExact.csv
cp projCVOptDOnCoarse_localTruncErrorMom_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv.csv
cp projCVOptDOnCoarse_numericalVelocity_x-00000.csv ../../../datafiles/solErr_donea_U_10x10_projCVOptDOnCoarse_0406refined.csv
cp projCVOptDOnCoarse_analyticalVelocity_x-00000.csv ../../../datafiles/solErr_donea_Uexact_10x10_projCVOptDOnCoarse_0406refined.csv
cp test_donea_error.csv ../../../datafiles/errors_donea_0406refined.csv

./test_ff_stokes_donea params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true -Helper.Grid.Cells0 11 -Helper.Grid.Cells1 11 -Adaptivity.Adapt false
cp test_donea_error.csv ../../../datafiles/errors_donea_11x11.csv

#mymethod locally refined, without conservation
./test_ff_stokes_donea params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true -Adaptivity.Conservation false
cp localTruncErrorMom_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_WithoutConserv.csv
cp CVOptionA_StandardStencilPositions_localTruncErrorMomWithoutInterpol_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_CV_A_StandStenc_NoInterp.csv
cp CVOptionB_StandardStencilPositions_localTruncErrorMomWithoutInterpol_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_CV_B_StandStenc_NoInterp.csv
cp CVOptionC_StandardStencilPositions_localTruncErrorMomWithoutInterpol_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_CV_C_StandStenc_NoInterp.csv
cp CVOptionD_StandardStencilPositions_localTruncErrorMomWithoutInterpol_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_CV_D_StandStenc_NoInterp.csv
cp CVOptionD_myStencilPositions_localTruncErrorMomWithoutInterpol_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_CV_D_MyStenc_NoInterp.csv
cp projectionCVOptionDOnCoarse_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_projectionCVOptionDOnCoarse.csv

######\subsection{Upon dividing some grid cells into four while introducing new refinement interfaces} \subsubsection{Square refinement interface example }
###### refinement region enlarged
./test_ff_stokes_donea params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true -Adaptivity.LowerY 0.3 -Adaptivity.UpperY 0.7 -Adaptivity.LeftX 0.3 -Adaptivity.RightX 0.7
cp projCVOptDOnCoarse_localTruncErrorMom_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_0307.csv
cp projCVOptDOnCoarse_numericalVelocity_x-00000.csv ../../../datafiles/solErr_donea_U_10x10_projCVOptDOnCoarse_0307refined.csv
cp projCVOptDOnCoarse_analyticalVelocity_x-00000.csv ../../../datafiles/solErr_donea_Uexact_10x10_projCVOptDOnCoarse_0307refined.csv
cp test_donea_error.csv ../../../datafiles/errors_donea_0307refined.csv

./test_ff_stokes_donea params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true  -Adaptivity.LowerY 0.3 -Adaptivity.UpperY 0.7 -Adaptivity.LeftX 0.3 -Adaptivity.RightX 0.7 -Adaptivity.Adapt false
cp projCoarseOnCVOptD_localTruncErrorMom_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_0307.csv
cp projCoarseOnCVOptD_numericalVelocity_x-00000.csv ../../../datafiles/solErr_donea_U_10x10_projCoarseOnCVOptD_0307unrefined.csv
cp projCoarseOnCVOptD_analyticalVelocity_x-00000.csv ../../../datafiles/solErr_donea_Uexact_10x10_projCoarseOnCVOptD_0307unrefined.csv

./test_ff_stokes_donea params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true -Helper.Grid.Cells0 13 -Helper.Grid.Cells1 13 -Adaptivity.Adapt false
cp test_donea_error.csv ../../../datafiles/errors_donea_13x13.csv

###### refinement region right half
./test_ff_stokes_donea params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true -Adaptivity.LowerY 0 -Adaptivity.UpperY 1 -Adaptivity.LeftX 0.5 -Adaptivity.RightX 1
cp projCVOptDOnCoarse_localTruncErrorMom_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_rightFine.csv
cp projCVOptDOnCoarse_numericalVelocity_x-00000.csv ../../../datafiles/solErr_donea_U_10x10_projCVOptDOnCoarse_rightRefined.csv
cp projCVOptDOnCoarse_analyticalVelocity_x-00000.csv ../../../datafiles/solErr_donea_Uexact_10x10_projCVOptDOnCoarse_rightRefined.csv
cp test_donea_error.csv ../../../datafiles/errors_donea_RightRefined.csv

./test_ff_stokes_donea params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true  -Adaptivity.LowerY 0 -Adaptivity.UpperY 1 -Adaptivity.LeftX 0.5 -Adaptivity.RightX 1 -Adaptivity.Adapt false
cp projCoarseOnCVOptD_localTruncErrorMom_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_rightFine.csv
cp projCoarseOnCVOptD_numericalVelocity_x-00000.csv ../../../datafiles/solErr_donea_U_10x10_projCoarseOnCVOptD_rightFineUnrefined.csv
cp projCoarseOnCVOptD_analyticalVelocity_x-00000.csv ../../../datafiles/solErr_donea_Uexact_10x10_projCoarseOnCVOptD_rightFineUnrefined.csv

./test_ff_stokes_donea params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true -Helper.Grid.Cells0 16 -Helper.Grid.Cells1 16 -Adaptivity.Adapt false
cp test_donea_error.csv ../../../datafiles/errors_donea_16x16.csv

###### 100x100
./test_ff_stokes_donea params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true -Helper.Grid.Cells0 100 -Helper.Grid.Cells1 100 -Adaptivity.Adapt false
cp projCoarseOnCVOptD_localTruncErrorMom_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_100x100.csv
cp projCoarseOnCVOptD_numericalVelocity_x-00000.csv ../../../datafiles/solErr_donea_U_10x10_projCoarseOnCVOptD_100x100unrefined.csv
cp projCoarseOnCVOptD_analyticalVelocity_x-00000.csv ../../../datafiles/solErr_donea_Uexact_10x10_projCoarseOnCVOptD_100x100unrefined.csv
cp test_donea_error.csv ../../../datafiles/errors_donea_100x100.csv

./test_ff_stokes_donea params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true -Helper.Grid.Cells0 100 -Helper.Grid.Cells1 100
cp projCVOptDOnCoarse_localTruncErrorMom_x-00000.csv ../../../datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_100x100.csv
cp projCVOptDOnCoarse_numericalVelocity_x-00000.csv ../../../datafiles/solErr_donea_U_10x10_projCVOptDOnCoarse_100x100refined.csv
cp projCVOptDOnCoarse_analyticalVelocity_x-00000.csv ../../../datafiles/solErr_donea_Uexact_10x10_projCVOptDOnCoarse_100x100refined.csv
cp test_donea_error.csv ../../../datafiles/errors_donea_100x100refined.csv

./test_ff_stokes_donea params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true -Helper.Grid.Cells0 106 -Helper.Grid.Cells1 106 -Adaptivity.Adapt false
cp test_donea_error.csv ../../../datafiles/errors_donea_106x106.csv

###### supergauss
cd ../gaussLocal
make test_ff_gauss_local
./test_ff_gauss_local params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient false -Adaptivity.DeltaX 0.1 -Adaptive.MaxLevel 2 -Helper.Grid.Cells0 100 -Helper.Grid.Cells1 100
# local truncation errors
cp projCoarseOnCVOptD_localTruncErrorMom_x-00001.csv ../../../datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_gauss.csv
cp projCVOptDOnCoarse_localTruncErrorMom_x-00002.csv ../../../datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_gauss.csv
cp projCoarseOnCVOptD_localTruncErrorMom_y-00001.csv ../../../datafiles/localTruncErrExplanation_MomY_projectionCoarseOnCVOptionD_gauss.csv
cp projCVOptDOnCoarse_localTruncErrorMom_y-00002.csv ../../../datafiles/localTruncErrExplanation_MomY_projCoarseOnCVOptionD_WithConserv_gauss.csv
cp test_gauss_local_localTruncErrorConti-00002.csv ../../../datafiles/localTruncErrExplanation_Conti.csv
cp test_gauss_local_localTruncErrorConti_projOnFine-00001.csv ../../../datafiles/localTruncErrExplanation_Conti_proj.csv
# errors
cp test_gauss_local_error.csv ../../../datafiles/errors_gauss_refined.csv
# pressure
cp test_gauss_local_numericalP_projOnFine-00001.csv ../../../datafiles/solErr_gauss_P_unrefined.csv
cp test_gauss_local_numericalP-00002.csv ../../../datafiles/solErr_gauss_P_projOnCoarse_refined.csv
cp test_gauss_local_analyticalP_projOnFine-00001.csv ../../../datafiles/solErr_gauss_Pexact_unrefined.csv
cp test_gauss_local_analyticalP-00002.csv ../../../datafiles/solErr_gauss_Pexact_projOnCoarse_refined.csv
# velocity x
cp projCoarseOnCVOptD_numericalVelocity_x-00001.csv ../../../datafiles/solErr_gauss_U_projCoarseOnCVOptD_unrefined.csv
cp projCVOptDOnCoarse_numericalVelocity_x-00002.csv ../../../datafiles/solErr_gauss_U_projCVOptDOnCoarse_refined.csv
cp projCoarseOnCVOptD_analyticalVelocity_x-00001.csv ../../../datafiles/solErr_gauss_Uexact_projCoarseOnCVOptD_unrefined.csv
cp projCVOptDOnCoarse_analyticalVelocity_x-00002.csv ../../../datafiles/solErr_gauss_Uexact_projCVOptDOnCoarse_refined.csv
# velocity y
cp projCoarseOnCVOptD_numericalVelocity_y-00001.csv ../../../datafiles/solErr_gauss_V_projCoarseOnCVOptD_unrefined.csv
cp projCVOptDOnCoarse_numericalVelocity_y-00002.csv ../../../datafiles/solErr_gauss_V_projCVOptDOnCoarse_refined.csv
cp projCoarseOnCVOptD_analyticalVelocity_y-00001.csv ../../../datafiles/solErr_gauss_Vexact_projCoarseOnCVOptD_unrefined.csv
cp projCVOptDOnCoarse_analyticalVelocity_y-00002.csv ../../../datafiles/solErr_gauss_Vexact_projCVOptDOnCoarse_refined.csv

./test_ff_gauss_local params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient false -Adaptivity.DeltaX 0.1 -Adaptive.MaxLevel 1 -Helper.Grid.Cells0 131 -Helper.Grid.Cells1 131
cp test_gauss_local_error.csv ../../../datafiles/errors_gauss_131x131.csv

##### for local truncation error orders
cd ../orderNumerics
make test_ff_orderNumerics
./test_ff_orderNumerics params.input -Problem.UseScvfLineAveraging false -FreeFlow.EnableUnsymmetrizedVelocityGradient false
cp q.csv ../../../datafiles/orderNumerics_q.csv
cp q_sum.csv ../../../datafiles/orderNumerics_q_sum.csv
cp gradient.csv ../../../datafiles/orderNumerics_gradient.csv
cp gradient_sum.csv ../../../datafiles/orderNumerics_gradient_sum.csv
cp normalDiffFrontal.csv ../../../datafiles/orderNumerics_normalDiffFrontal.csv
cp normalDiffFrontal_sum.csv ../../../datafiles/orderNumerics_normalDiffFrontal_sum.csv
cp normalDiffLateralZerothOrder.csv ../../../datafiles/orderNumerics_normalDiffLateralZerothOrder.csv
cp normalDiffLateralZerothOrder_sum.csv ../../../datafiles/orderNumerics_normalDiffLateralZerothOrder_sum.csv
cp normalDiffLateralFirstOrder.csv ../../../datafiles/orderNumerics_normalDiffLateralFirstOrder.csv
cp normalDiffLateralFirstOrder_sum.csv ../../../datafiles/orderNumerics_normalDiffLateralFirstOrder_sum.csv
cp tangential.csv ../../../datafiles/orderNumerics_tangential.csv
cp tangential_sum.csv ../../../datafiles/orderNumerics_tangential_sum.csv
cp nonMixed.csv ../../../datafiles/orderNumerics_nonMixed.csv
cp nonMixed_sum.csv ../../../datafiles/orderNumerics_nonMixed_sum.csv
cp mixed.csv ../../../datafiles/orderNumerics_mixed.csv
cp mixed_sum.csv ../../../datafiles/orderNumerics_mixed_sum.csv
cp div.csv ../../../datafiles/orderNumerics_div.csv
cp div_sum.csv ../../../datafiles/orderNumerics_div_sum.csv
cp divLequals3.csv ../../../datafiles/orderNumerics_divLequals3.csv
cp divLequals3_sum.csv ../../../datafiles/orderNumerics_divLequals3_sum.csv
cp Laplacian.csv ../../../datafiles/orderNumerics_Laplacian.csv
cp LaplacianLequals3.csv ../../../datafiles/orderNumerics_LaplacianLequals3.csv

cd ..
