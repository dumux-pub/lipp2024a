// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Test for the staggered grid Navier-Stokes model (Kovasznay 1947)
 */

bool doFirstOrderLocalTruncErrorGlobalRefinement = false;

#include <config.h>

#include <ctime>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/staggeredrefinedfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>

#include <dumux/io/mystaggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>

#include <dumux/adaptive/adapt.hh>
#include <dumux/adaptive/markelements.hh>
#include <dumux/freeflow/gridadaptindicator.hh>
#include <dumux/freeflow/navierstokes/staggered/griddatatransfer.hh>
#include <dumux/freeflow/navierstokes/errors.hh>

#include "problem.hh"

/*!
 * \brief Provides an interface for customizing error messages associated with
 *        reading in parameters.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
                    errorMessageOut += progName;
                    errorMessageOut += " [options]\n";
                    errorMessageOut += errorMsg;
                    errorMessageOut += "\n\nThe list of mandatory arguments for this program is:\n"
                                        "\t-TimeManager.TEnd               End of the simulation [s] \n"
                                        "\t-TimeManager.DtInitial          Initial timestep size [s] \n"
                                        "\t-Grid.File                      Name of the file containing the grid \n"
                                        "\t                                definition in DGF format\n"
                                        "\t-SpatialParams.LensLowerLeftX   x-coordinate of the lower left corner of the lens [m] \n"
                                        "\t-SpatialParams.LensLowerLeftY   y-coordinate of the lower left corner of the lens [m] \n"
                                        "\t-SpatialParams.LensUpperRightX  x-coordinate of the upper right corner of the lens [m] \n"
                                        "\t-SpatialParams.LensUpperRightY  y-coordinate of the upper right corner of the lens [m] \n"
                                        "\t-SpatialParams.Permeability     Permeability of the domain [m^2] \n"
                                        "\t-SpatialParams.PermeabilityLens Permeability of the lens [m^2] \n";

        std::cout << errorMessageOut
                  << "\n";
    }
}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::GaussGlobalTestTypeTag;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv, usage);

    using HelpingGridManager = Dumux::GridManager<GetPropType<TypeTag, Properties::HelpingGrid>>;
    HelpingGridManager helpingGridManager;
    helpingGridManager.init("Helper");
    const auto& helpingGridView = helpingGridManager.grid().leafGridView();
    Dune::DGFWriter dgfWriter(helpingGridView);
    std::string inputFileName = getParam<std::string>("Grid.File");
    dgfWriter.write(inputFileName);

    // try to create a grid (from the given grid file or the input file)
    using GridManager = Dumux::GridManager<GetPropType<TypeTag, Properties::Grid>>;
    GridManager gridManager;
    gridManager.init();

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

//     gridManager.grid().preAdapt();
//     for (const auto& element : elements(leafGridView))
//     {
//         using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
//         using GridView = typename GridGeometry::GridView;
//         using Element = typename GridView::template Codim<0>::Entity;
//         using GlobalPosition =  typename Element::Geometry::GlobalCoordinate;
//
//         GlobalPosition pos = element.geometry().center();
//
//         auto x = pos[0];
//         auto y = pos[1];
//
//         if(0.4 < x && x < 0.6 || y < 0.03 || y > 0.97)
//         {
//             gridManager.grid().mark( 1,  element);
//         }
//     }
//     gridManager.grid().adapt();
//     gridManager.grid().postAdapt();

//         Dune::VTKWriter<GridView> vtkwriter(this->gridView());
//         vtkwriter.write("latestgrid", Dune::VTK::appenddraw);

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);

    // the problem (boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    const auto numDofsCellCenter = leafGridView.size(0);
    const auto numDofsFace = (*gridGeometry).numIntersections();
    SolutionVector x;
    x[GridGeometry::cellCenterIdx()].resize(numDofsCellCenter);
    x[GridGeometry::faceIdx()].resize(numDofsFace);
    problem->applyInitialSolution(x);
    problem->createOutputFields(x);

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    // intialize the vtk output module
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    MyStaggeredVtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    IOFields::initOutputModule(vtkWriter); //!< Add model specific output fields
    vtkWriter.addField(problem->getAnalyticalPressureSolution(), "analyticalP");
    
    

    vtkWriter.addField(problem->getAnalyticalDxU(), "analyticalDxU");
    vtkWriter.addField(problem->getAnalyticalDyV(), "analyticalDyV");
    vtkWriter.addField(problem->getAnalyticalDivU(), "analyticalDivVectorU");

    vtkWriter.addField(problem->getNumericalDxU(), "numericalDxU");
    vtkWriter.addField(problem->getNumericalDyV(), "numericalDyV");
    vtkWriter.addField(problem->getNumericalDivU(), "numericalDivVectorU");

    

    // use the staggered FV assembler
    using Assembler = StaggeredRefinedFVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables);

    // the linear solver
    using LinearSolver = Dumux::UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    const double refineTol = getParam<double>("Adaptive.RefineTolerance");
    const double coarsenTol = getParam<double>("Adaptive.CoarsenTolerance");
    MyFreeflowGridAdaptIndicator<TypeTag> indicator(problem);
    const unsigned int numRefinementSteps = getParam<double>("Adaptive.MaxLevel");

    FreeFlowGridDataTransfer<TypeTag, SolutionVector> freeflowDataTransfer(problem, gridGeometry, gridVariables, x);
    Dune::Timer timer;

    NavierStokesRefinedErrors errors(problem, x, 0);
    NavierStokesRefinedErrorCSVWriter errorCSVWriter(problem);

    for (unsigned int i=0; i < numRefinementSteps; ++i)
    {
        if (i>0)
        {
            // compute refinement indicator
            indicator.calculateVelocityError(x, problem->getAnalyticalVelocitySolution(), refineTol, coarsenTol);

            // mark elements and maybe adapt grid
            bool wasAdapted = false;
            if (markElements(gridManager.grid(), indicator))
                wasAdapted = adapt(gridManager.grid(), freeflowDataTransfer);

            if (wasAdapted)
            {
                gridGeometry->update(leafGridView);

                x[GridGeometry::cellCenterIdx()] = 0.;
                x[GridGeometry::cellCenterIdx()].resize(leafGridView.size(0));
                x[GridGeometry::faceIdx()] = 0.;
                x[GridGeometry::faceIdx()].resize((*gridGeometry).numIntersections());

                gridVariables->updateAfterGridAdaption(x);

                assembler->reSetJacobianSize(); // has to be after nonlinear solver
                assembler->reSetJacobianPattern(); //!< Tell the assembler to resize the matrix and set pattern
                assembler->reSetResidualSize(); //!< Tell the assembler to resize the residual
            }
        }


        // linearize & solve
        nonLinearSolver.solve(x);

        // write vtk output
        problem->createOutputFields(x);
        if (getParam<bool>("Problem.PrintErrors", true))
        {
            errors.update(x, i);
            errorCSVWriter.printErrors(errors);
        }

        vtkWriter.write(i+1.0);

        timer.stop();
    }

    const auto& comm = Dune::MPIHelper::getCollectiveCommunication();
    std::cout << "Simulation took " << timer.elapsed() << " seconds on "
              << comm.size() << " processes.\n"
              << "The cumulative CPU time was " << timer.elapsed()*comm.size() << " seconds.\n";

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
