// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Channel flow test for the staggered grid (Navier-)Stokes model
 */

bool doFirstOrderLocalTruncErrorGlobalRefinement = false;

#include <config.h>

#include <ctime>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/staggeredrefinedfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>

#include <dumux/io/mystaggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>

#include <dumux/freeflow/navierstokes/staggered/myfluxoversurface.hh>

#include <dumux/adaptive/adapt.hh>
#include <dumux/adaptive/markelements.hh>
#include <dumux/freeflow/gridadaptindicator.hh>
#include <dumux/freeflow/navierstokes/staggered/griddatatransfer.hh>
#include <dumux/freeflow/navierstokes/staggered/residualCalc.hh>
#include <dumux/freeflow/navierstokes/errors.hh>

#include <dumux/io/outputFacility.hh>

#include <dumux/discretization/staggered/myfvgridgeometry.hh>
#include <dumux/discretization/staggered/freeflow/fvgridgeometrytraits1.hh>

#include "problem.hh"

/*!
 * \brief Provides an interface for customizing error messages associated with
 *        reading in parameters.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
                    errorMessageOut += progName;
                    errorMessageOut += " [options]\n";
                    errorMessageOut += errorMsg;
                    errorMessageOut += "\n\nThe list of mandatory arguments for this program is:\n"
                                        "\t-Grid.File                      Name of the file containing the grid \n"
                                        "\t                                definition in DGF format\n"
                                        "\t-SpatialParams.LensLowerLeftX   x-coordinate of the lower left corner of the lens [m] \n"
                                        "\t-SpatialParams.LensLowerLeftY   y-coordinate of the lower left corner of the lens [m] \n"
                                        "\t-SpatialParams.LensUpperRightX  x-coordinate of the upper right corner of the lens [m] \n"
                                        "\t-SpatialParams.LensUpperRightY  y-coordinate of the upper right corner of the lens [m] \n"
                                        "\t-SpatialParams.Permeability     Permeability of the domain [m^2] \n"
                                        "\t-SpatialParams.PermeabilityLens Permeability of the lens [m^2] \n";

        std::cout << errorMessageOut
                  << "\n";
    }
}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::ChannelTestTypeTag;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv, usage);

    using HelpingGridManager = Dumux::GridManager<GetPropType<TypeTag, Properties::HelpingGrid>>;
    HelpingGridManager helpingGridManager;
    helpingGridManager.init("Helper");
    const auto& helpingGridView = helpingGridManager.grid().leafGridView();
    Dune::DGFWriter dgfWriter(helpingGridView);
    std::string inputFileName = getParam<std::string>("Grid.File");
    dgfWriter.write(inputFileName);

    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager;
    gridManager.init();

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    bool adapt = getParam<bool>("Adaptivity.Adapt", false);
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    bool FVCAgrid = getParam<bool>("Grid.FVCAGrid", false);
    if (FVCAgrid)
    {
        adapt = false;

        gridManager.grid().preAdapt();
        for (const auto& element : elements(leafGridView))
        {
            using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
            using GridView = typename GridGeometry::GridView;
            using Element = typename GridView::template Codim<0>::Entity;
            using GlobalPosition =  typename Element::Geometry::GlobalCoordinate;

            GlobalPosition pos = element.geometry().center();

            auto x = pos[0];
            auto y = pos[1];

            if(x > .5 && y < .5)
            {
                gridManager.grid().mark( 1,  element);
            }
        }
        gridManager.grid().adapt();
        gridManager.grid().postAdapt();

        gridManager.grid().preAdapt();
        for (const auto& element : elements(leafGridView))
        {
            using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
            using GridView = typename GridGeometry::GridView;
            using Element = typename GridView::template Codim<0>::Entity;
            using GlobalPosition =  typename Element::Geometry::GlobalCoordinate;

            GlobalPosition pos = element.geometry().center();

            auto x = pos[0];
            auto y = pos[1];

            if(x > .75 && y < .25)
            {
                gridManager.grid().mark( 1,  element);
            }
        }
        gridManager.grid().adapt();
        gridManager.grid().postAdapt();
    }

    Scalar leftX = getParam<Scalar>("Adaptivity.LeftX");
    Scalar rightX = getParam<Scalar>("Adaptivity.RightX");
    Scalar lowerY = getParam<Scalar>("Adaptivity.LowerY");
    Scalar upperY = getParam<Scalar>("Adaptivity.UpperY");

    if (adapt)
    {
        gridManager.grid().preAdapt();
        for (const auto& element : elements(leafGridView))
        {
            using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
            using GridView = typename GridGeometry::GridView;
            using Element = typename GridView::template Codim<0>::Entity;
            using GlobalPosition =  typename Element::Geometry::GlobalCoordinate;
            GlobalPosition pos = element.geometry().center();

            auto x = pos[0];
            auto y = pos[1];

            if((leftX < x) && (x < rightX) && (y > lowerY) && (y < upperY))
            {
                gridManager.grid().mark( 1,  element);
            }
        }
        gridManager.grid().adapt();
        gridManager.grid().postAdapt();
    }

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    const auto numDofsCellCenter = leafGridView.size(0);
    const auto numDofsFace = (*gridGeometry).numIntersections();
    SolutionVector x;
    x[GridGeometry::cellCenterIdx()].resize(numDofsCellCenter);
    x[GridGeometry::faceIdx()].resize(numDofsFace);
    problem->applyInitialSolution(x);
    auto xOld = x;

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    problem->createOutputFields(x);

    // initialize the vtk output module
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    MyStaggeredVtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    IOFields::initOutputModule(vtkWriter); //!< Add model specific output fields

    vtkWriter.addField(problem->getAnalyticalPressureSolution(), "analyticalP");
    
    

    vtkWriter.addField(problem->getAnalyticalDxU(), "analyticalDxU");
    vtkWriter.addField(problem->getAnalyticalDyV(), "analyticalDyV");
    vtkWriter.addField(problem->getAnalyticalDivU(), "analyticalDivVectorU");

    vtkWriter.addField(problem->getNumericalDxU(), "numericalDxU");
    vtkWriter.addField(problem->getNumericalDyV(), "numericalDyV");
    vtkWriter.addField(problem->getNumericalDivU(), "numericalDivVectorU");

    

    using CellCenterSolutionVector = GetPropType<TypeTag, Properties::CellCenterSolutionVector>;
    CellCenterSolutionVector numericalCCResidualWithAnalytical;
    numericalCCResidualWithAnalytical.resize(numDofsCellCenter);
    vtkWriter.addField(numericalCCResidualWithAnalytical, "localTruncErrorConti");

    // the assembler with time loop for instationary problem
    using Assembler = StaggeredRefinedFVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables);

    // the linear solver
    using LinearSolver = Dumux::UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    // set up two surfaces over which fluxes are calculated
    MyFluxOverSurface<GridVariables,
                    SolutionVector,
                    GetPropType<TypeTag, Properties::ModelTraits>,
                    GetPropType<TypeTag, Properties::LocalResidual>> flux(*gridVariables, x);
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;

    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const Scalar xMin = gridGeometry->bBoxMin()[0];
    const Scalar xMax = gridGeometry->bBoxMax()[0];
    const Scalar yMin = gridGeometry->bBoxMin()[1];
    const Scalar yMax = gridGeometry->bBoxMax()[1];

    // The first surface shall be placed at the middle of the channel.
    // If we have an odd number of cells in x-direction, there would not be any cell faces
    // at the position of the surface (which is required for the flux calculation).
    // In this case, we add half a cell-width to the x-position in order to make sure that
    // the cell faces lie on the surface. This assumes a regular cartesian grid.
    const Scalar planePosMiddleX = xMin + 0.5*(xMax - xMin);

    using CellVector = std::vector<unsigned int>;
    const auto numCellsXVec = getParam<CellVector>("Helper.Grid.Cells0");
    const unsigned int numCellsX = std::accumulate(numCellsXVec.begin(), numCellsXVec.end(), 0);

    const Scalar offsetX = (numCellsX % 2 == 0) ? 0.0 : 0.5*((xMax - xMin) / numCellsX);

    const auto p0middle = GlobalPosition{planePosMiddleX + offsetX, yMin};
    const auto p1middle = GlobalPosition{planePosMiddleX + offsetX, yMax};
    flux.addSurface("middle", p0middle, p1middle);

    // The second surface is placed at the outlet of the channel.
    const auto p0outlet = GlobalPosition{xMax, yMin};
    const auto p1outlet = GlobalPosition{xMax, yMax};
    flux.addSurface("outlet", p0outlet, p1outlet);

    using HostGrid = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >;
    using HostGridManager = GridManager<HostGrid>;

    std::array<std::string, 4> paramGroups = {"finexCVs", "fineyCVs", "coarsexCVs", "coarseyCVs"}; //xfine...
    std::array<HostGridManager, 4> cvGridManagers = {};
    std::array<std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>>,4> cvCenterScvfIndicesMaps;
    CVOutputFacility<TypeTag> cvOutputFacility(problem);
    cvOutputFacility.iniCVGridManagers(cvGridManagers, cvCenterScvfIndicesMaps, paramGroups);
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    std::vector<std::pair<unsigned int, Scalar>> pvdFileInfos;

    // linearize & solve
    Dune::Timer timer;
    nonLinearSolver.solve(x);

    //output
    problem->createOutputFields(x);

    if (getParam<bool>("Problem.PrintErrors", true))
    {
        NavierStokesRefinedErrors errors(problem, x);
        NavierStokesRefinedErrorCSVWriter(problem).printErrors(errors);
    }

    using FaceSolutionVector = GetPropType<TypeTag, Properties::FaceSolutionVector>;
    FaceSolutionVector numericalFaceResidualWithAnalytical;

    std::optional<std::array<std::vector<Scalar>, 4>> optionalPerfectInterpolationFaceResiduals;
    bool isOutflow = getParam<bool>("Problem.IsOutflow",true);
    if (!isOutflow)
    {
        std::array<std::vector<Scalar>, 4> perfectInterpolationFaceResiduals;
        optionalPerfectInterpolationFaceResiduals.emplace(perfectInterpolationFaceResiduals);
    }

    ResidualCalc<TypeTag> residualCalc(problem);
    residualCalc.calcResidualsStationary(gridGeometry, cvGridManagers, cvCenterScvfIndicesMaps, numericalCCResidualWithAnalytical, numericalFaceResidualWithAnalytical, optionalPerfectInterpolationFaceResiduals);

    bool readDofBasedValues = getParam<bool>("Problem.ReadDofBasedValues", false);
    if (readDofBasedValues)
    {
        CellCenterSolutionVector readInPres;
        readInPres.resize(gridGeometry->numCellCenterDofs());
        ReadDofBasedResult<TypeTag> reader(problem);
        reader.readDofBasedResult(readInPres, "pressure");
        vtkWriter.addField(readInPres, "readInPres");
    }

    vtkWriter.write(0);

    cvOutputFacility.onCVsOutputResidualsAndPrimVars(
        gridGeometry,
        cvGridManagers,
        cvCenterScvfIndicesMaps,
        paramGroups,
        x,
        0,
        numericalFaceResidualWithAnalytical,
        optionalPerfectInterpolationFaceResiduals,
        readDofBasedValues);

    pvdFileInfos.push_back(std::make_pair(0, 0));
    cvOutputFacility.printPvdFiles(paramGroups, pvdFileInfos);

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
