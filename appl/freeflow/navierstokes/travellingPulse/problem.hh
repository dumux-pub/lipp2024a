// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \brief Test for the staggered grid (Navier-)Stokes model with analytical solution
 */
#ifndef DUMUX_TRAVELLING_PULSE_TEST_PROBLEM_HH
#define DUMUX_TRAVELLING_PULSE_TEST_PROBLEM_HH

#include <dune/alugrid/grid.hh>
#include <dune/grid/yaspgrid.hh>

#include <dumux/assembly/staggeredrefinedlocalresidual.hh>

#include <dumux/common/numeqvector.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>

#include <dumux/freeflow/navierstokes/boundarytypes.hh>
#include <dumux/freeflow/navierstokes/myproblem.hh>
#include <dumux/freeflow/navierstokes/mylocalresidual.hh>
#include <dumux/freeflow/navierstokes/myfluxvariables.hh>
#include <dumux/freeflow/navierstokes/myvolumevariables.hh>
#include <dumux/freeflow/navierstokes/myiofields.hh>

#include <dumux/discretization/method.hh>
#include <dumux/discretization/staggered/myfacesolution.hh>
#include <dumux/discretization/staggered/mygridvariables.hh>
#include <dumux/discretization/staggered/myfvgridgeometry.hh>
#include <dumux/discretization/staggered/freeflow/fvgridgeometrytraits1.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/navierstokes/model.hh>

#include <dumux/discretization/staggered/freeflow/myfacevariables.hh>
#include <dumux/discretization/staggered/freeflow/elementvolumevariables1.hh>
#include <dumux/discretization/staggered/freeflow/myvelocityoutput.hh>
#include <dumux/discretization/staggered/freeflow/mygridvolumevariables.hh>
#include <dumux/discretization/staggered/mygridfluxvariablescache.hh>

namespace Dumux
{
template <class TypeTag>
class TravellingPulseTestProblem;

namespace Properties
{
// Create new type tags
namespace TTag {
struct TravellingPulseTestTypeTag { using InheritsFrom = std::tuple<NavierStokes, StaggeredFreeFlowModel>; };
} // end namespace TTag

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TravellingPulseTestTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar> >;
};

template<class TypeTag, class MyTypeTag>
struct HelpingGrid { using type = UndefinedProperty; };

// Set the grid type
template<class TypeTag>
struct HelpingGrid<TypeTag, TTag::TravellingPulseTestTypeTag>
{
    using type = Dune::YaspGrid<2,Dune::TensorProductCoordinates<double, 2>>;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TravellingPulseTestTypeTag> { using type = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::TravellingPulseTestTypeTag> { using type = Dumux::TravellingPulseTestProblem<TypeTag> ; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::TravellingPulseTestTypeTag> { static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::TravellingPulseTestTypeTag> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::TravellingPulseTestTypeTag> { static constexpr bool value = true; };

//! The variables living on the faces
template<class TypeTag>
struct FaceVariables<TypeTag, TTag::TravellingPulseTestTypeTag>
{
private:
    using FacePrimaryVariables = GetPropType<TypeTag, Properties::FacePrimaryVariables>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
public:
    using type = MyStaggeredFaceVariables<ModelTraits, FacePrimaryVariables, GridView::dimension, upwindSchemeOrder>;
};

//! The velocity output
template<class TypeTag>
struct VelocityOutput<TypeTag, TTag::TravellingPulseTestTypeTag>
{
    using type = MyStaggeredFreeFlowVelocityOutput<GetPropType<TypeTag, Properties::GridVariables>,
                                                 GetPropType<TypeTag, Properties::SolutionVector>>;
};

//! Set the volume variables property
template<class TypeTag>
struct DualCVsVolVars<TypeTag, TTag::TravellingPulseTestTypeTag>
{
private:
    using VV = GetPropType<TypeTag, Properties::VolumeVariables>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    static constexpr auto dimWorld = GridView::dimensionworld;
    static constexpr int numPairs = 2 * (dimWorld - 1);

public:
    using type = NavierStokesDualCVsVolVars<VV, numPairs>;
};

//! Set the volume variables property
template<class TypeTag>
struct PrimalCVsVolVars<TypeTag, TTag::TravellingPulseTestTypeTag>
{
public:
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using type = std::array<NaviesStokesPrimalScvfPair<VolumeVariables>,8>;//TODO make work for dim != 2
};

template<class TypeTag>
struct GridVolumeVariables<TypeTag, TTag::TravellingPulseTestTypeTag>
{
private:
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using DualCVsVolVars = GetPropType<TypeTag, Properties::DualCVsVolVars>;
    using PrimalCVsVolVars = GetPropType<TypeTag, Properties::PrimalCVsVolVars>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using SubControlVolume = typename GetPropType<TypeTag, Properties::GridGeometry>::SubControlVolume;
    using SubControlVolumeFace = typename GetPropType<TypeTag, Properties::GridGeometry>::SubControlVolumeFace;
    static constexpr auto enableCache = getPropValue<TypeTag, Properties::EnableGridVolumeVariablesCache>();
    using Traits = MyStaggeredGridDefaultGridVolumeVariablesTraits<Problem, PrimalCVsVolVars, DualCVsVolVars, VolumeVariables, GridView, SubControlVolume, SubControlVolumeFace>;
public:
    using type = MyStaggeredGridVolumeVariables<Traits, enableCache>;
};

//! Set the BaseLocalResidual to StaggeredLocalResidual
template<class TypeTag>
struct BaseLocalResidual<TypeTag, TTag::TravellingPulseTestTypeTag> { using type = StaggeredRefinedLocalResidual<TypeTag>; };

//! Set the face solution type
template<class TypeTag>
struct StaggeredFaceSolution<TypeTag, TTag::TravellingPulseTestTypeTag>
{
private:
    using FaceSolutionVector = GetPropType<TypeTag, Properties::FaceSolutionVector>;
public:
    using type = Dumux::MyStaggeredFaceSolution<FaceSolutionVector>;
};

//! Set the grid variables (volume, flux and face variables)
template<class TypeTag>
struct GridVariables<TypeTag, TTag::TravellingPulseTestTypeTag>
{
private:
    using GG = GetPropType<TypeTag, Properties::GridGeometry>;
    using GVV = GetPropType<TypeTag, Properties::GridVolumeVariables>;
    using GFVC = GetPropType<TypeTag, Properties::GridFluxVariablesCache>;
    using GFV = GetPropType<TypeTag, Properties::GridFaceVariables>;
public:
    using type = MyStaggeredGridVariables<GG, GVV, GFVC, GFV>;
};

//! The default fv grid geometry
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::TravellingPulseTestTypeTag>
{
private:
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = typename GetPropType<TypeTag, Properties::Grid>::LeafGridView;
    using Traits = MyStaggeredFreeFlowDefaultGridGeometryTraits<GridView, upwindSchemeOrder>;
public:
    using type = MyStaggeredGridGeometry<GridView, enableCache, Traits>;
};

//! Set the volume variables property
template<class TypeTag>
struct VolumeVariables<TypeTag, TTag::TravellingPulseTestTypeTag>
{
private:
    using PV = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using FSY = GetPropType<TypeTag, Properties::FluidSystem>;
    using FST = GetPropType<TypeTag, Properties::FluidState>;
    using MT = GetPropType<TypeTag, Properties::ModelTraits>;

    static_assert(FSY::numPhases == MT::numFluidPhases(), "Number of phases mismatch between model and fluid system");
    static_assert(FST::numPhases == MT::numFluidPhases(), "Number of phases mismatch between model and fluid state");
    static_assert(!FSY::isMiscible(), "The Navier-Stokes model only works with immiscible fluid systems.");

    using Traits = NavierStokesVolumeVariablesTraits<PV, FSY, FST, MT>;
public:
    using type = MyNavierStokesVolumeVariables<Traits>;
};

//! The local residual
template<class TypeTag>
struct LocalResidual<TypeTag, TTag::TravellingPulseTestTypeTag> { using type = RefinedNavierStokesResidual<TypeTag>; };

//! The flux variables
template<class TypeTag>
struct FluxVariables<TypeTag, TTag::TravellingPulseTestTypeTag> { using type = RefinedNavierStokesFluxVariables<TypeTag>; };

//! The specific I/O fields
template<class TypeTag>
struct IOFields<TypeTag, TTag::TravellingPulseTestTypeTag> {
#if NONISOTHERMAL
    using type = FreeflowNonIsothermalIOFields<MyNavierStokesIOFields>;
#else
    using type = MyNavierStokesIOFields;
#endif
};

//! Set the global flux variables cache vector class
template<class TypeTag>
struct GridFluxVariablesCache<TypeTag, TTag::TravellingPulseTestTypeTag>
{
private:
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using FluxVariablesCache = GetPropType<TypeTag, Properties::FluxVariablesCache>;
    using FluxVariablesCacheFiller =  GetPropType<TypeTag, Properties::FluxVariablesCacheFiller>;
    static constexpr auto enableCache = getPropValue<TypeTag, Properties::EnableGridFluxVariablesCache>();
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
public:
    using type = MyStaggeredGridFluxVariablesCache<Problem, FluxVariablesCache, FluxVariablesCacheFiller, enableCache, upwindSchemeOrder>;
};

template<class TypeTag>
struct CVGridGeometry<TypeTag, TTag::TravellingPulseTestTypeTag>
{
private:
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = typename Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >::LeafGridView;
    using Traits = MyStaggeredFreeFlowDefaultGridGeometryTraits<GridView, upwindSchemeOrder>;
public:
    using type = MyStaggeredGridGeometry<GridView, enableCache, Traits>;
};
}

/*!
 * \ingroup NavierStokesTests
 * \brief  Test problem for the staggered grid (Bla et al., 2003)
 * \todo doc me!
 */
template <class TypeTag>
class TravellingPulseTestProblem : public MyNavierStokesProblem<TypeTag>
{
    using ParentType = MyNavierStokesProblem<TypeTag>;

    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using NumEqVector = Dumux::NumEqVector<GetPropType<TypeTag, Properties::PrimaryVariables>>;
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;

    static constexpr auto dimWorld = GetPropType<TypeTag, Properties::GridGeometry>::GridView::dimensionworld;
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using VelocityVector = Dune::FieldVector<Scalar, dimWorld>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;

public:
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    TravellingPulseTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry), eps_(1e-6), time_(0.0), timeStepSize_(0.0)
    {
        isStationary_ = getParam<bool>("Problem.IsStationary");
    }

   /*!
     * \name Problem parameters
     */
    // \{

    bool shouldWriteRestartFile() const
    {
        return false;
    }

   /*!
     * \brief Returns the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10°C

    // see Schneider et al., 2019: "Coupling staggered-grid and MPFA finite volume methods for
    // free flow/porous-medium flow problems"
    NumEqVector instationarySourceAtPos(const GlobalPosition& globalPos, Scalar t) const
    {
        NumEqVector source(0.0);

        const Scalar x = globalPos[0];
        const Scalar y = globalPos[1];

        Scalar a = getParam<Scalar>("Problem.GaussWidthFactor");
        Scalar b = getParam<Scalar>("Problem.GaussPowerFactor");
        int chooser = getParam<int>("Problem.Chooser");

        //super Gaussian function (https://en.wikipedia.org/wiki/Gaussian_function)

        if (chooser == 0)
        {
            auto dxp = [&](Scalar x, Scalar y)
            { return -a*std::exp(a*x-t)/((1+std::exp(a*x-t))*(1+std::exp(a*x-t))); };
            auto dyp = [&](Scalar x, Scalar y)
            { return 0; };

            source[Indices::conti0EqIdx] = 0.;
            source[Indices::momentumXBalanceIdx] = dxp(x,y);
            source[Indices::momentumYBalanceIdx] = dyp(x,y);
        }
        else if (chooser == 1)
        {
            auto dtu = [&](Scalar x, Scalar y, Scalar t)
            { return df_(t) * std::exp(-std::pow((x*x+y*y)/a,b)); };
            auto dtv = [&](Scalar x, Scalar y, Scalar t)
            { return df_(t) * std::exp(-std::pow((x*x+y*y)/a,b)); };
            auto dxu = [&](Scalar x, Scalar y, Scalar t)
            { return - f_(t) * b * std::pow(((x*x+y*y)/a),b-1)*2./a* std::exp(-std::pow(((x*x+y*y)/a),b))*x; };
            auto dyu = [&](Scalar x, Scalar y, Scalar t)
            { return - f_(t) * b * std::pow(((x*x+y*y)/a),b-1)*2./a* std::exp(-std::pow(((x*x+y*y)/a),b))*y; };
            auto dxuu = [&](Scalar x, Scalar y, Scalar t)
            { return - f_(t) * f_(t) * 2.*b * std::pow(((x*x+y*y)/a),b-1)*2./a* std::exp(-2.*std::pow(((x*x+y*y)/a),b))*x; };
            auto dyuu = [&](Scalar x, Scalar y, Scalar t)
            { return - f_(t) * f_(t) * 2.*b * std::pow(((x*x+y*y)/a),b-1)*2./a* std::exp(-2.*std::pow(((x*x+y*y)/a),b))*y; };
            auto dxxu = [&](Scalar x, Scalar y, Scalar t)
            { return - f_(t) * 2*b/a * std::exp(-std::pow(((x*x+y*y)/a),b))*(std::pow(((x*x+y*y)/a),b-1)+2./a*(b-1.)*x*x*std::pow(((x*x+y*y)/a),b-2) - 2.*b/a*x*x*std::pow(((x*x+y*y)/a),2*(b-1))); };
            auto dxyu = [&](Scalar x, Scalar y, Scalar t)
            { return - f_(t) * 2*b/a * std::exp(-std::pow(((x*x+y*y)/a),b))*(2./a*(b-1.)*x*y*std::pow(((x*x+y*y)/a),b-2) - 2.*b/a*x*y*std::pow(((x*x+y*y)/a),2*(b-1))); };
            auto dyyu = [&](Scalar x, Scalar y, Scalar t)
            { return - f_(t) * 2*b/a * std::exp(-std::pow(((x*x+y*y)/a),b))*(std::pow(((x*x+y*y)/a),b-1)+2./a*(b-1.)*y*y*std::pow(((x*x+y*y)/a),b-2) - 2.*b/a*y*y*std::pow(((x*x+y*y)/a),2*(b-1))); };


            source[Indices::conti0EqIdx] = dxu(x,y,t) + dyu(x,y,t);
            source[Indices::momentumXBalanceIdx] = dtu(x,y,t) + dxuu(x,y,t) + dyuu(x,y,t) - 2.*dxxu(x,y,t) -dyyu(x,y,t) - dxyu(x,y,t);
            source[Indices::momentumYBalanceIdx] = dtv(x,y,t) + dxuu(x,y,t) + dyuu(x,y,t) - 2.*dyyu(x,y,t) - dxxu(x,y,t) - dxyu(x,y,t);
        }
        else
        {
            DUNE_THROW(Dune::InvalidStateException, "not meaningful");
        }

        return source;
    }

    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    {
         const Scalar t = time_ + timeStepSize_;
         return instationarySourceAtPos(globalPos,t);
    }

    // \}
   /*!
     * \name Boundary conditions
     */
    // \{

   /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;

        // set Dirichlet values for the velocity and pressure everywhere
        values.setDirichlet(Indices::velocityXIdx);
        values.setDirichlet(Indices::velocityYIdx);
        return values;
    }

    /*!
    * \brief Returns whether a fixed Dirichlet value shall be used at a given cell.
    *
    * \param element The finite element
    * \param fvGeometry The finite-volume geometry
    * \param scv The sub control volume
    * \param pvIdx The primary variable index in the solution vector
    */
    bool isDirichletCell(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const SubControlVolume& scv,
                        int pvIdx) const
    {
        // set fixed pressure in all cells at the left boundary
        auto isBoundary = [&](const FVElementGeometry& fvGeometry)
        {
            if (fvGeometry.hasBoundaryScvf())
            {
                return true;
            }
            return false;
        };
        return (isBoundary(fvGeometry) && pvIdx == Indices::pressureIdx);
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume face (velocities)
     *
     * \param element The finite element
     * \param scvf the sub control volume face
     *
     * Concerning the usage of averaged velocity, see the explanation of the initial function. The average of the nondirection velocity along an scvf is required in the context of dirichlet boundary conditions for the functions getParallelVelocityFromBoundary_ and getParallelVelocityFromOtherBoundary_ within myfluxvariables.hh. There dirichlet is called for ghost faces built from the normalFace but the value then is taken in the the direction scvf.directionIndex, which is along that normalFace.
      */
    PrimaryVariables dirichlet(const Element& element, const SubControlVolumeFace& scvf) const
    {
        Scalar time = time_+timeStepSize_;

        PrimaryVariables priVars(0.0);

        unsigned int dirIdx = scvf.directionIndex();
        priVars[Indices::velocity(dirIdx)] = this->instationaryAnalyticalVelocitySolutionAtPos(scvf, time);
        unsigned int nonDirIdx = (dirIdx == 0)?1:0;
        priVars[Indices::velocity(nonDirIdx)] = this->instationaryAnalyticalNonDirVelocitySolutionAtPos(scvf, time);

        return priVars;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume face (pressure)
     *
     * \param element The finite element
     * \param scv the sub control volume
     */
    PrimaryVariables dirichlet(const Element& element, const SubControlVolume& scv) const
    {
        Scalar time = time_+timeStepSize_;

        PrimaryVariables priVars(0.0);
        priVars[Indices::pressureIdx] = this->instationaryAnalyticalPressureSolutionAtPos(scv, time);
        return priVars;
    }

    // see Schneider et al., 2019: "Coupling staggered-grid and MPFA finite volume methods for
    // free flow/porous-medium flow problems"
    PrimaryVariables instationaryAnalyticalSolutionAtPos(const GlobalPosition& globalPos, Scalar t) const
    {
        PrimaryVariables sol(0.0);
        const Scalar x = globalPos[0];
        const Scalar y = globalPos[1];

        Scalar a = getParam<Scalar>("Problem.GaussWidthFactor");
        Scalar b = getParam<Scalar>("Problem.GaussPowerFactor");
        int chooser = getParam<int>("Problem.Chooser");

        // the ITLR group also suggested me at some point to get a travelling pulse by "adding a convection term to the Gauss pulse"

        if (chooser == 0)
        {
            sol[Indices::velocityXIdx] = 0.;
            sol[Indices::velocityYIdx] = 0.;
            //logistic function
            sol[Indices::pressureIdx] = 1-std::exp(a*x-t)/(1+std::exp(a*x-t));
        }
        else if (chooser == 1)
        {
            sol[Indices::velocityXIdx] = f_(t) * std::exp(-std::pow((x*x+y*y)/a,b));
            sol[Indices::velocityYIdx] = f_(t) * std::exp(-std::pow((x*x+y*y)/a,b));
            sol[Indices::pressureIdx] = 0.;
        }
        else
        {
            DUNE_THROW(Dune::InvalidStateException, "not meaningful");
        }

        return sol;
    }

    /*!
     * \brief Returns the analytical solution of the problem at a given position.
     *
     * \param globalPos The global position
     */
    PrimaryVariables analyticalSolutionAtPos(const GlobalPosition& globalPos) const
    {
        if(!isStationary_)
            DUNE_THROW(Dune::InvalidStateException, "analyticalSolutionAtPos was supposed to be used only be the stationary problem.");

        return this->instationaryAnalyticalSolutionAtPos(globalPos, time_+timeStepSize_);
    }

    // \}

   /*!
     * \name Volume terms
     */
    // \{

/*!
     * \brief Evaluates the initial value for a sub control volume face (velocities)
     *
     * Simply assigning the value of the analytical solution at the face center
     * gives a discrete solution that is not divergence-free. For small initial
     * time steps, this has a negative impact on the pressure solution
     * after the first time step. The flag UseVelocityAveraging triggers the
     * function averagedVelocity_ which uses a higher order quadrature formula to
     * bring the discrete solution sufficiently close to being divergence-free.
     */
    PrimaryVariables initial(const SubControlVolumeFace& scvf) const
    {
        bool startWithAnalytical = getParam<bool>("Problem.StartWithAnalytical", false);

        if (startWithAnalytical || !isStationary_)
        {
            PrimaryVariables priVars(0.0);
            priVars[Indices::velocity(scvf.directionIndex())] = this->instationaryAnalyticalVelocitySolutionAtPos(scvf, 0.);
            return priVars;
        }
        else
        {
            PrimaryVariables values;
            values[Indices::pressureIdx] = 0.0;
            values[Indices::velocityXIdx] = 0.0;
            values[Indices::velocityYIdx] = 0.0;

            return values;
        }
    }

    /*!
     * \brief Evaluates the initial value for a control volume (pressure)
     */
    PrimaryVariables initial(const SubControlVolume& scv) const
    {
        bool startWithAnalytical = getParam<bool>("Problem.StartWithAnalytical", false);

        if (startWithAnalytical || !isStationary_)
        {
            PrimaryVariables priVars(0.0);
            priVars[Indices::pressureIdx] = this->instationaryAnalyticalPressureSolutionAtPos(scv, 0.);
            return priVars;
        }
        else
        {
            PrimaryVariables values;
            values[Indices::pressureIdx] = 0.0;
            values[Indices::velocityXIdx] = 0.0;
            values[Indices::velocityYIdx] = 0.0;

            return values;
        }
    }

    /*!
     * \brief Updates the time
     */
    void updateTime(const Scalar time)
    {
        time_ = time;
    }

    /*!
     * \brief Updates the time step size
     */
    void updateTimeStepSize(const Scalar timeStepSize)
    {
        timeStepSize_ = timeStepSize;
    }

private:
    Scalar f_(Scalar t) const
    {
        if (isStationary_)
            return 1.0;
        else
            return std::exp(-t);
    }

    Scalar df_(Scalar t) const
    {
        if (isStationary_)
            return 0.0;
        else
            return -f_(t);
    }

    Scalar eps_;

    Scalar time_;
    Scalar timeStepSize_;

    bool isStationary_;
};
} //end namespace

#endif
