// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \brief calcs for order numerics, a variant of sincos
 */
#ifndef DUMUX_TEST_PROBLEM_FORORDERNUMERICS_HH
#define DUMUX_TEST_PROBLEM_FORORDERNUMERICS_HH

#include <dune/alugrid/grid.hh>
#include <dune/subgrid/subgrid.hh>
#include <dune/grid/yaspgrid.hh>

#include <dumux/assembly/staggeredrefinedlocalresidual.hh>

#include <dumux/common/numeqvector.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>

#include <dumux/freeflow/navierstokes/boundarytypes.hh>
#include <dumux/freeflow/navierstokes/myproblem.hh>
#include <dumux/freeflow/navierstokes/mylocalresidual.hh>
#include <dumux/freeflow/navierstokes/myfluxvariables.hh>
#include <dumux/freeflow/navierstokes/myvolumevariables.hh>
#include <dumux/freeflow/navierstokes/myiofields.hh>
#include <dumux/freeflow/navierstokes/model.hh>

#include <dumux/discretization/method.hh>
#include <dumux/discretization/staggered/myfacesolution.hh>
#include <dumux/discretization/staggered/mygridvariables.hh>
#include <dumux/discretization/staggered/myfvgridgeometry.hh>
#include <dumux/discretization/staggered/freeflow/fvgridgeometrytraits1.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>

#include <dumux/discretization/staggered/freeflow/myfacevariables.hh>
#include <dumux/discretization/staggered/freeflow/elementvolumevariables1.hh>
#include <dumux/discretization/staggered/freeflow/myvelocityoutput.hh>
#include <dumux/discretization/staggered/freeflow/mygridvolumevariables.hh>
#include <dumux/discretization/staggered/mygridfluxvariablescache.hh>


namespace Dumux
{
namespace Properties
{
// Create new type tags
namespace TTag {
struct OrderNumericsTestTypeTag { using InheritsFrom = std::tuple<NavierStokes, StaggeredFreeFlowModel>; };
} // end namespace TTag
}

class TestProblemForOrderNumerics
{
public:
    TestProblemForOrderNumerics()
    : eps_(1e-6)
    {
        mu_ = getParam<double>("Component.LiquidKinematicViscosity", 1.0);
        rho_ = getParam<double>("Component.LiquidDensity", 1.0);
        nu_ = mu_/rho_;
    }

    //approximate
    double div_lequal1_PrimalCV_Approx_(double xL, double xR, double yD, double yU) const
    {
        return (u_(xR,.5*(yD+yU))-u_(xL,.5*(yD+yU)))/(xR-xL);
    }

    double div_lequal3_PrimalCV_Approx_(double xL, double xR, double yD, double yU) const
    {
        return (v_(.5*(xL+xR),yU)-v_(.5*(xL+xR),yD))/(yU-yD);
    }

    double laplacianPrimalCVApprox_(double xL, double xR, double yD, double yU, double xLC, double xRC, double xLL, double xRR, unsigned int i) const
    {
        return -((p_(xRR,.5*(yD+yU))-p_(xRC,.5*(yD+yU)))/(xRR-xRC)-(p_(xLC,.5*(yD+yU))-p_(xLL,.5*(yD+yU)))/(xLC-xLL))/(xR-xL);
    }

    double laplacianPrimalCVApprox_lequals3_(double xL, double xR, double yD, double yU, double yDC, double yUC, double yDD, double yUU, unsigned int i) const
    {
        return -((p_(.5*(xL+xR),yUU)-p_(.5*(xL+xR),yUC))/(yUU-yUC)-(p_(.5*(xL+xR),yDC)-p_(.5*(xL+xR),yDD))/(yDC-yDD))/(yU-yD);
    }

    double qXApprox_(double xL, double xR, double yD, double yU, double xC, unsigned int i) const
    {
        return qx_(xC,0.5*(yD+yU));
    }

    double gradient_xCV_Approx_(double xL, double xR, double yD, double yU, unsigned int i) const
    { return (p_(xR,.5*(yD+yU)) - p_(xL,.5*(yD+yU)))/(rho_*(xR-xL)); }

    double nonMixedConti_xCV_Approx_(double xL, double xR, double yD, double yU, double xC, unsigned int i) const
    {
        double xLL = xL - (xC-xL);
        double xRR = xR + (xR-xC);

        double uRR;
        if (i == 8 || i == 9)
        {
            uRR=.5*(u_(xRR,yU-.25*(yU-yD))+u_(xRR,yD+.25*(yU-yD)));
        }
        else
        {
            uRR=u_(xRR,.5*(yD+yU));
        }

        double leftTransportingVelocity = .5*(u_(xC,.5*(yD+yU))+u_(xLL,.5*(yD+yU)));

        double leftUpwindVelocity;
        if (leftTransportingVelocity > eps_)
        {
            leftUpwindVelocity = u_(xLL,.5*(yD+yU));
        }
        else if (leftTransportingVelocity < -eps_)
        {
            leftUpwindVelocity = u_(xC,.5*(yD+yU));
        }
        else
        {
            std::cout << "undecided at " << xC << "," << .5*(yD+yU) << std::endl;
            leftUpwindVelocity = leftTransportingVelocity;
        }

        double rightTransportingVelocity = .5*(u_(xC,.5*(yD+yU))+uRR);
        double rightUpwindVelocity;
        if (rightTransportingVelocity > eps_)
        {
            rightUpwindVelocity = u_(xC,.5*(yD+yU));
        }
        else if (rightTransportingVelocity < -eps_)
        {
            rightUpwindVelocity = uRR;
        }
        else
        {
            std::cout << "undecided at " << xC << "," << .5*(yD+yU) << std::endl;
            rightUpwindVelocity = rightTransportingVelocity;
        }

        return rightUpwindVelocity*rightTransportingVelocity/(xR-xL) - leftUpwindVelocity*leftTransportingVelocity/(xR-xL);
    }

    double mixedConti_xCV_Approx_(double xL, double xR, double yDD, double yD, double yU, double yUU, double xC, unsigned int i) const
    {
        double res = 0.;

        if (i==10)
        {
            return -1.;//negative should not appear in the log plot; I do not want to deal with this
        }
        else
        {
            double upwindVelocity;

            //x:upright, y: rightup
            if (scalarCmp(yUU-yU,0.))
                upwindVelocity = u_(xC,yUU);
            else
                upwindVelocity = (Dumux::sign(v_(xR,yU))==+1) ? u_(xC,.5*(yD+yU)) : u_(xC,yUU);

            res += (xR-xC) * upwindVelocity * v_(xR,yU);

            //x: downright, y: leftup
            if (scalarCmp(yD-yDD,0.))
                upwindVelocity = u_(xC,yDD);
            else
                upwindVelocity = (Dumux::sign(v_(xR,yD))==+1) ? u_(xC,yDD) : u_(xC,.5*(yD+yU));

            res -= (xR-xC) * upwindVelocity * v_(xR,yD);

            //x:upleft, y: rightdown
            if (scalarCmp(yUU-yU,0.))
                upwindVelocity = u_(xC,yUU);
            else
                upwindVelocity = (Dumux::sign(v_(xL,yU))==+1) ? u_(xC,.5*(yD+yU)) : u_(xC,yUU);

            res += (xC-xL)*  upwindVelocity * v_(xL,yU);

            //x:downleft, y: leftdown
            if (scalarCmp(yD-yDD,0.))
                upwindVelocity = u_(xC,yDD);
            else
                upwindVelocity = (Dumux::sign(v_(xL,yD))==+1) ? u_(xC,yDD) : u_(xC,.5*(yD+yU));

            res -= (xC-xL) * upwindVelocity * v_(xL,yD);

            return res/((xR-xL)*(yU-yD));
        }
    }

    double normalDiffFrontal_Approx_(double xL, double xR, double yD, double yU, double xC, unsigned int i) const
    {
        double xLL = xL - (xC-xL);
        double xRR = xR + (xR-xC);

        double uRR;
        if (i == 8 || i == 9)
        {
            uRR=.5*(u_(xRR,yU-.25*(yU-yD))+u_(xRR,yD+.25*(yU-yD)));
        }
        else
        {
            uRR=u_(xRR,.5*(yD+yU));
        }

        return -nu_*(uRR-u_(xC,.5*(yD+yU)))/((xRR-xC)*(xR-xL)) + nu_ * (u_(xC,.5*(yD+yU))-u_(xLL,.5*(yD+yU)))/((xC-xLL)*(xR-xL));
    }

    double normalDiffLateralDumuxVersion_Approx_(double xC, double yDD, double yD, double yU, double yUU, unsigned int i) const
    {
        return -nu_*(u_(xC,yUU)-u_(xC,.5*(yD+yU)))/(yUU-.5*(yD+yU))/(yU-yD) + nu_*(u_(xC,.5*(yD+yU))-u_(xC,yDD))/(.5*(yD+yU)-yDD)/(yU-yD);
    }

    double tangentialFrontal_Approx_(double xL, double xC, double xR, double yD, double yU, unsigned int i) const //tangential is actually lateral%TODO rename
    {
        if (i==10)
        {
            double xLL = xL - (xC-xL);
            double xRR = xR + (xR-xC);

            return -nu_ *(
                (.5*(v_(.5*(xL+xC),yU)-v_(.5*(xLL+xL),yU))+
                (v_(.5*(xR+xC),yU)-v_(.5*(xL+xC),yU))+
                .5*(v_(.5*(xR+xRR),yU)-v_(.5*(xR+xC),yU)))
            -v_(xR,yD)+v_(xL,yD))/((xR-xL)*(yU-yD));
        }
        else
        {
            return -nu_ *(v_(xR,yU)-v_(xL,yU)-v_(xR,yD)+v_(xL,yD))/((xR-xL)*(yU-yD));
        }
    }

    double momentumX_Approx_(double xL, double xC, double xR, double yDD, double yD, double yU, double yUU, unsigned int i) const
    { return -qXApprox_(xL,xR,yD,yU,xC,i)/rho_ + 2.*normalDiffFrontal_Approx_(xL,xR,yD,yU,xC,i) + gradient_xCV_Approx_(xL,xR,yD,yU,i) + normalDiffLateralDumuxVersion_Approx_(xC, yDD, yD, yU, yUU,i) + tangentialFrontal_Approx_(xL,xC,xR,yD,yU,i) + mixedConti_xCV_Approx_(xL, xR, yDD, yD, yU, yUU, xC, i) + nonMixedConti_xCV_Approx_(xL, xR, yD, yU, xC, i); }

    //continuous
    double div_lequal1_PrimalCV_Conti_(double xL, double xR, double yD, double yU) const
    {
        return (f2_(xR)-f2_(xL))/(xR-xL) * (f2_(yU)-f2_(yD))/(yU-yD);
    }

    double div_lequal3_PrimalCV_Conti_(double xL, double xR, double yD, double yU) const
    {
        return -div_lequal1_PrimalCV_Conti_(xL,xR,yD,yU);
    }

    double laplacianPrimalCVConti_(double xL, double xR, double yD, double yU) const
    {
        return -std::sqrt(2.*M_PI)*(df2_(xR)-df2_(xL))/(xR-xL)*.5*(yD+yU);
    }

    double laplacianPrimalCVConti_lequals3_(double xL, double xR, double yD, double yU) const
    {
        return -std::sqrt(2.*M_PI)*(df2_(yU)-df2_(yD))/(yU-yD)*.5*(xL+xR);
    }

    double gradient_xCV_Conti_ (double xL, double xR, double yD, double yU) const
    { return std::sqrt(2.*M_PI)/rho_*.5*(yD+yU)*(f2_(xR)-f2_(xL))/(xR - xL) + std::sqrt(2.*M_PI)/rho_*(intf2_(yU)-intf2_(yD))/(yU-yD); }

    double nonMixedConti_xCV_Conti_(double xL, double xR, double yD, double yU) const
    { return M_PI*(f2_(xR)*f2_(xR)-f2_(xL)*f2_(xL))/(xR-xL)-mixedConti_xCV_Conti_(xL, xR, yD, yU); }

    double mixedConti_xCV_Conti_(double xL, double xR, double yD, double yU) const
    { return -(f2_(xR)*f2_(xR)-f2_(xL)*f2_(xL))/(2.*(xR-xL))*(df2_(yU)*f2_(yU)-df2_(yD)*f2_(yD))/(yU-yD); }

    double tangentialFrontal_Conti_ (double xL, double xR, double yD, double yU) const //tangential is actually lateral%TODO rename
    { return -nu_ *(v_(xR,yU)-v_(xL,yU)-v_(xR,yD)+v_(xL,yD))/((xR-xL)*(yU-yD)); }

    double normalDiffFrontal_Conti_ (double xL, double xR, double yD, double yU) const
    { return -nu_*(df2_(xR)-df2_(xL))*(f2_(yU)-f2_(yD))/((xR-xL)*(yU-yD)); }

    double normalDiffLateralDumuxVersion_Conti_ (double xL, double xR, double yD, double yU) const
    { return -nu_*(intf2_(xR)-intf2_(xL))*(ddf2_(yU)-ddf2_(yD))/((xR-xL)*(yU-yD)); }

    double qX_Conti_ (double xL, double xR, double yD, double yU) const
    { return rho_*(2.*normalDiffFrontal_Conti_(xL,xR,yD,yU) + normalDiffLateralDumuxVersion_Conti_(xL,xR,yD,yU) + tangentialFrontal_Conti_(xL,xR,yD,yU)+ gradient_xCV_Conti_(xL,xR,yD,yU) + nonMixedConti_xCV_Conti_(xL,xR,yD,yU) + mixedConti_xCV_Conti_(xL,xR,yD,yU)); }

    double momentumX_Conti_(double xL, double xR, double yD, double yU) const
    { return -qX_Conti_(xL,xR,yD,yU)/rho_ + 2.*normalDiffFrontal_Conti_(xL,xR,yD,yU) + gradient_xCV_Conti_(xL,xR,yD,yU) + normalDiffLateralDumuxVersion_Conti_(xL,xR,yD,yU) + tangentialFrontal_Conti_(xL,xR,yD,yU)  + nonMixedConti_xCV_Conti_(xL,xR,yD,yU) + mixedConti_xCV_Conti_(xL,xR,yD,yU);}

    double intf2_(double x) const
    { return std::sin(2.*M_PI*x)/(2.*M_PI*std::sqrt(2.*M_PI)); }

    double f2_(double x) const
    { return std::cos(2.*M_PI*x)/std::sqrt(2.*M_PI); }

    double df2_(double x) const
    { return -std::sqrt(2.*M_PI)*std::sin(2.*M_PI*x); }

    double ddf2_(double x) const
    { return -std::sqrt(2.*M_PI)*2.*M_PI*std::cos(2.*M_PI*x); }

    double dddf2_(double x) const
    { return std::sqrt(2.*M_PI)*4.*M_PI*M_PI*std::sin(2.*M_PI*x); }

    double dxP_ (double x, double y) const
    { return std::sqrt(2.*M_PI)*(y*df2_(x) + f2_(y)); }

    double dyP_ (double x, double y) const
    { return std::sqrt(2.*M_PI)*(f2_(x) + x*df2_(y)); }

    double dxU_ (double x, double y) const
    { return df2_(x)*df2_(y); }

    double dyU_ (double x, double y) const
    { return f2_(x)*ddf2_(y); }

    double dxxU_ (double x, double y) const
    { return ddf2_(x)*df2_(y); }

    double dxyU_ (double x, double y) const
    { return df2_(x)*ddf2_(y); }

    double dyyU_ (double x, double y) const
    { return f2_(x)*dddf2_(y); }

    double dxV_ (double x, double y) const
    { return -ddf2_(x)*f2_(y); }

    double dyV_ (double x, double y) const
    { return -df2_(y)*df2_(x); }

    double dyyV_ (double x, double y) const
    { return -ddf2_(y)*df2_(x); }

    double dxyV_ (double x, double y) const
    { return -df2_(y)*ddf2_(x); }

    double dxxV_ (double x, double y) const
    { return -f2_(y)*dddf2_(x); }

    double p_ (double x, double y) const
    { return std::sqrt(2.*M_PI)*(y*f2_(x) + x*f2_(y)); }

    double u_ (double x, double y) const
    { return f2_(x)*df2_(y); }

    double v_ (double x, double y) const
    { return -df2_(x)*f2_(y); }

    double dxUU_ (double x, double y) const
    { return 2.*u_(x,y)*dxU_(x,y); }

    double dyVV_ (double x, double y) const
    { return 2.*v_(x,y)*dyV_(x,y); }

    double dyUV_ (double x, double y) const
    { return u_(x,y)*dyV_(x,y) + v_(x,y)*dyU_(x,y); }

    double dxUV_ (double x, double y) const
    { return u_(x,y)*dxV_(x,y) + v_(x,y)*dxU_(x,y); }

    double qx_ (double x, double y) const
    { return -2.0*mu_*dxxU_(x,y) - mu_*dyyU_(x,y) - mu_*dxyV_(x,y) + dxP_(x,y) + rho_*dxUU_(x,y) + rho_*dyUV_(x,y); }

    double qy_ (double x, double y) const
    { return -2.0*mu_*dyyV_(x,y) - mu_*dxyU_(x,y) - mu_*dxxV_(x,y) + dyP_(x,y) + rho_*dxUV_(x,y) + rho_*dyVV_(x,y); }

    double eps_;
    double mu_;
    double rho_;
    double nu_;
};
} //end namespace

#endif
