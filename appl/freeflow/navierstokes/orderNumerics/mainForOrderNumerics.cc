// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Test for the staggered grid Navier-Stokes model (Kovasznay 1947)
 */

bool doFirstOrderLocalTruncErrorGlobalRefinement = false;

#include <config.h>

#include <ctime>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/staggeredrefinedfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>

#include <dumux/io/mystaggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/grid/controlvolumegrids.hh>

#include <dumux/freeflow/navierstokes/staggered/residualCalc.hh>
#include <dumux/freeflow/navierstokes/errors.hh>
#include <dumux/io/outputFacility.hh>

#include "problemForOrderNumerics.hh"

/*!
 * \brief Provides an interface for customizing error messages associated with
 *        reading in parameters.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
                    errorMessageOut += progName;
                    errorMessageOut += " [options]\n";
                    errorMessageOut += errorMsg;
                    errorMessageOut += "\n\nThe list of mandatory arguments for this program is:\n"
                                        "\t-TimeManager.TEnd               End of the simulation [s] \n"
                                        "\t-TimeManager.DtInitial          Initial timestep size [s] \n"
                                        "\t-Grid.File                      Name of the file containing the grid \n"
                                        "\t                                definition in DGF format\n"
                                        "\t-SpatialParams.LensLowerLeftX   x-coordinate of the lower left corner of the lens [m] \n"
                                        "\t-SpatialParams.LensLowerLeftY   y-coordinate of the lower left corner of the lens [m] \n"
                                        "\t-SpatialParams.LensUpperRightX  x-coordinate of the upper right corner of the lens [m] \n"
                                        "\t-SpatialParams.LensUpperRightY  y-coordinate of the upper right corner of the lens [m] \n"
                                        "\t-SpatialParams.Permeability     Permeability of the domain [m^2] \n"
                                        "\t-SpatialParams.PermeabilityLens Permeability of the lens [m^2] \n";

        std::cout << errorMessageOut
                  << "\n";
    }
}

struct DualCellProperties {
    double deltaXL;
    double deltaXR;
    double deltaY_C;
    double deltaY_D;
    double deltaY_U;
    double xCenter;
    double yCenter;
};

struct PrimalCellPropertiesForDiv {
    double xCenter;
    double yCenter;

    double deltaX;
    double deltaY;
};

struct PrimalCellPropertiesForLaplacian {
    double xCenter;
    double yCenter;

    double deltaXL;
    double deltaXCL;
    double deltaX;
    double deltaXCR;
    double deltaXR;

    double deltaYD;
    double deltaYCD;
    double deltaY;
    double deltaYCU;
    double deltaYU;
};

struct MomentumCell {
    double xL;
    double xR;
    double yD;
    double yU;
    double xC;
    double yUU;
    double yDD;
};

struct DivCell {
    double xL;
    double xR;
    double yD;
    double yU;
};

struct MassCell {
    double xLL;
    double xL;
    double xLC;
    double xRC;
    double xR;
    double xRR;

    double yDD;
    double yD;
    double yDC;
    double yUC;
    double yU;
    double yUU;
};

DivCell makeDivCV (const PrimalCellPropertiesForDiv& prop)
{
    DivCell tmpCell;

    tmpCell.xL = prop.xCenter - .5*prop.deltaX;
    tmpCell.xR = prop.xCenter + .5*prop.deltaX;
    tmpCell.yD = prop.yCenter - .5*prop.deltaY;
    tmpCell.yU = prop.yCenter + .5*prop.deltaY;

    return tmpCell;
}

MassCell makePrimalCV (const PrimalCellPropertiesForLaplacian& prop)
{
    MassCell tmpCell;

    tmpCell.xL = prop.xCenter - .5*prop.deltaX;
    tmpCell.xR = prop.xCenter + .5*prop.deltaX;

    tmpCell.xLL = tmpCell.xL - .5*prop.deltaXL;
    tmpCell.xLC = tmpCell.xL + .5*prop.deltaXCL;
    tmpCell.xRC = tmpCell.xR - .5*prop.deltaXCR;
    tmpCell.xRR = tmpCell.xR + .5*prop.deltaXR;

    tmpCell.yD = prop.yCenter - .5*prop.deltaY;
    tmpCell.yU = prop.yCenter + .5*prop.deltaY;

    tmpCell.yDD = tmpCell.yD - .5*prop.deltaYD;
    tmpCell.yDC = tmpCell.yD + .5*prop.deltaYCD;
    tmpCell.yUC = tmpCell.yU - .5*prop.deltaYCU;
    tmpCell.yUU = tmpCell.yU + .5*prop.deltaYU;

    return tmpCell;
}

PrimalCellPropertiesForLaplacian setPrimalCellPropertiesForLaplacian (unsigned int i, double delta)
{
    PrimalCellPropertiesForLaplacian prop;
    prop.deltaY = delta;
    prop.xCenter = .3;
    prop.yCenter = .3;

    if (i == 1)//a
    {
        prop.deltaXL = 1.5*delta;
        prop.deltaXCL = 1.5*delta;
        prop.deltaX = 1.5*delta;
        prop.deltaXCR = 1.5*delta;
        prop.deltaXR = 1.5*delta;

        prop.deltaYD = 1.*delta;
        prop.deltaYCD = 1.*delta;
        prop.deltaYCU = 1.*delta;
        prop.deltaYU = 1.*delta;
    }
    else if (i == 2)//b
    {
        prop.deltaXL = 1.5*delta;
        prop.deltaXCL = 1.5*delta;
        prop.deltaX = 1.5*delta;
        prop.deltaXCR = 1.5*delta;
        prop.deltaXR = 1.5*delta;

        prop.deltaYD = 1.5*delta;
        prop.deltaYCD = 1.5*delta;
        prop.deltaYCU = .5*delta;
        prop.deltaYU = .5*delta;
    }
    else if (i == 3)//c
    {
        prop.deltaXL = 1.5*delta;
        prop.deltaXCL = 1.5*delta;
        prop.deltaX = 1.5*delta;
        prop.deltaXCR = 1.5*delta;
        prop.deltaXR = 1.5*delta;

        prop.deltaYD = 1.5*delta;
        prop.deltaYCD = 1.*delta;
        prop.deltaYCU = 1.*delta;
        prop.deltaYU = .5*delta;
    }
    else if (i == 4)//d
    {
        prop.deltaXL = 1.5*delta;
        prop.deltaXCL = 1.5*delta;
        prop.deltaX = 1.5*delta;
        prop.deltaXCR = 1.5*delta;
        prop.deltaXR = 1.5*delta;

        prop.deltaYD = 2.*delta;
        prop.deltaYCD = 1.*delta;
        prop.deltaYCU = 1.*delta;
        prop.deltaYU = .5*delta;
    }
    else
    {
        DUNE_THROW(Dune::InvalidStateException, "");
    }

    return prop;
}

PrimalCellPropertiesForDiv setPrimalCellPropertiesForDiv (double delta)
{
    PrimalCellPropertiesForDiv prop;

    prop.deltaY = delta;
    prop.deltaX = 1.5*delta;
    prop.xCenter = .3;
    prop.yCenter = .3;

    return prop;
}

MomentumCell makeDualCV (const DualCellProperties& prop)
{
    MomentumCell tmpCell;

    tmpCell.xC = prop.xCenter + .25*(prop.deltaXR-prop.deltaXL);
    tmpCell.xL = tmpCell.xC - .5*prop.deltaXL;
    tmpCell.xR = tmpCell.xC + .5*prop.deltaXR;

    tmpCell.yD = prop.yCenter - .5*prop.deltaY_C;
    tmpCell.yU = prop.yCenter + .5*prop.deltaY_C;

    tmpCell.yDD = tmpCell.yD - .5*prop.deltaY_D;
    tmpCell.yUU = tmpCell.yU + .5*prop.deltaY_U;

    return tmpCell;
}

DualCellProperties setDualCellProperties(unsigned int i, double delta)
{
    DualCellProperties prop;

    prop.deltaY_C = 1.*delta;
    prop.xCenter = .3;
    prop.yCenter = .3;

    if (i == 1)//Fig. 92a
    {
        prop.deltaXL = 1.5*delta;
        prop.deltaXR = 1.5*delta;
        prop.deltaY_D = 1.*delta;
        prop.deltaY_U = 1.*delta;
    }
    else if (i == 2)//Fig. 92c
    {
        prop.deltaXL = 1.5*delta;
        prop.deltaXR = 1.5*delta;
        prop.deltaY_D = 1.5*delta;
        prop.deltaY_U = .5*delta;
    }
    else if (i == 3)//Fig. 92d
    {
        prop.deltaXL = 1.5*delta;
        prop.deltaXR = 1.5*delta;
        prop.deltaY_D = 2.*delta;
        prop.deltaY_U = .5*delta;
    }
    else if (i == 4)//Fig. 92e
    {
        prop.deltaXL = 2.25*delta;
        prop.deltaXR = 2.25*delta;
        prop.deltaY_D = 1.*delta;
        prop.deltaY_U = 1.*delta;
    }
    else if (i == 5)//Fig. 92f
    {
        prop.deltaXL = 1.5*delta;
        prop.deltaXR = 3.*delta;
        prop.deltaY_D = 1.*delta;
        prop.deltaY_U = 1.*delta;
    }
    else if (i == 6)//Fig. 92h
    {
        prop.deltaXL = 1.5*delta;
        prop.deltaXR = 3.*delta;
        prop.deltaY_D = 2.*delta;
        prop.deltaY_U = .5*delta;
    }
    else if (i == 7)//Fig. 92i
    {
        prop.deltaXL = -1.+std::sqrt(1.+3.*delta);
        prop.deltaXR = 1.+3.*delta-std::sqrt(1.+3.*delta);
        prop.deltaY_D = delta+delta*delta;
        prop.deltaY_U = -.5+std::sqrt(.25+delta);
    }
    else if (i == 8)//Fig. 93a
    {
        prop.deltaXL = 1.5*delta;
        prop.deltaXR = 1.5*delta;
        prop.deltaY_D = 2.*delta;
        prop.deltaY_U = .5*delta;
    }
    else if (i == 9)//Fig. 93b
    {
        prop.deltaXL = 1.5*delta;
        prop.deltaXR = 3.*delta;
        prop.deltaY_D = 2.*delta;
        prop.deltaY_U = .5*delta;
    }
    else if (i == 10)//Fig. 94
    {
        prop.deltaXL = 1.5*delta;
        prop.deltaXR = 3.*delta;
        prop.deltaY_D = 2.*delta;
        prop.deltaY_U = .5*delta;
    }
    else if (i == 11)//Fig. 95
    {
        prop.deltaXL = 1.5*delta;
        prop.deltaXR = 1.5*delta;
        prop.deltaY_D = 1.*delta;
        prop.deltaY_U = 0.;
    }
    else
    {
        DUNE_THROW(Dune::InvalidStateException, "");
    }

    return prop;
}

double dualIndividualTermIndividualCellLocalTruncationError (const MomentumCell& momentumCell, const Dumux::TestProblemForOrderNumerics& problem, unsigned int i, std::string string)
{
    double error;

    if (string == "q")
    {
        error = problem.qX_Conti_(momentumCell.xL, momentumCell.xR, momentumCell.yD, momentumCell.yU) - problem.qXApprox_(momentumCell.xL, momentumCell.xR, momentumCell.yD, momentumCell.yU, momentumCell.xC,i);
    }
    else if (string == "gradient")
    {
        error = problem.gradient_xCV_Conti_(momentumCell.xL, momentumCell.xR, momentumCell.yD, momentumCell.yU) - problem.gradient_xCV_Approx_(momentumCell.xL, momentumCell.xR, momentumCell.yD, momentumCell.yU,i);
    }
    else if (string == "normalDiffFrontal")
    {
        error = problem.normalDiffFrontal_Conti_(momentumCell.xL, momentumCell.xR, momentumCell.yD, momentumCell.yU) - problem.normalDiffFrontal_Approx_(momentumCell.xL, momentumCell.xR, momentumCell.yD, momentumCell.yU, momentumCell.xC,i);
    }
    else if (string == "normalDiffLateralZerothOrder")
    {
        error = problem.normalDiffLateralDumuxVersion_Conti_(momentumCell.xL, momentumCell.xR, momentumCell.yD, momentumCell.yU) - problem.normalDiffLateralDumuxVersion_Approx_(momentumCell.xC, momentumCell.yDD, momentumCell.yD, momentumCell.yU, momentumCell.yUU,i);
    }
    else if (string == "normalDiffLateralFirstOrder")
    {
        error = problem.normalDiffLateralDumuxVersion_Conti_(momentumCell.xL, momentumCell.xR, momentumCell.yD, momentumCell.yU) - 2.*(momentumCell.yU-momentumCell.yD)/(momentumCell.yUU-momentumCell.yDD)*problem.normalDiffLateralDumuxVersion_Approx_(momentumCell.xC, momentumCell.yDD, momentumCell.yD, momentumCell.yU, momentumCell.yUU,i);
    }
    else if (string == "tangential")
    {
        error = problem.tangentialFrontal_Conti_(momentumCell.xL, momentumCell.xR, momentumCell.yD, momentumCell.yU) - problem.tangentialFrontal_Approx_(momentumCell.xL, momentumCell.xC, momentumCell.xR, momentumCell.yD, momentumCell.yU,i);
    }
    else if (string == "momentumX")
    {
        error = problem.momentumX_Conti_(momentumCell.xL, momentumCell.xR, momentumCell.yD, momentumCell.yU) -  problem.momentumX_Approx_(momentumCell.xL, momentumCell.xC, momentumCell.xR, momentumCell.yDD, momentumCell.yD, momentumCell.yU, momentumCell.yUU,i);
    }
    else if (string == "nonMixed")
    {
        error = problem.nonMixedConti_xCV_Conti_(momentumCell.xL, momentumCell.xR, momentumCell.yD, momentumCell.yU) -  problem.nonMixedConti_xCV_Approx_(momentumCell.xL, momentumCell.xR, momentumCell.yD, momentumCell.yU, momentumCell.xC, i);
    }
    else if (string == "mixed")
    {
        error = problem.mixedConti_xCV_Conti_(momentumCell.xL, momentumCell.xR, momentumCell.yD, momentumCell.yU) -  problem.mixedConti_xCV_Approx_(momentumCell.xL, momentumCell.xR, momentumCell.yDD, momentumCell.yD, momentumCell.yU, momentumCell.yUU, momentumCell.xC, i);
    }
    else
    {
        DUNE_THROW(Dune::InvalidStateException, "");
    }

    return error;
}

double primalIndividualTermIndividualCellLocalTruncationError (const MassCell& massCell, const Dumux::TestProblemForOrderNumerics& problem, unsigned int i, std::string string)
{
    double error;

    if (string == "Laplacian")
    {
        error = problem.laplacianPrimalCVConti_(massCell.xL, massCell.xR, massCell.yD, massCell.yU) - problem.laplacianPrimalCVApprox_(massCell.xL, massCell.xR, massCell.yD, massCell.yU, massCell.xLC, massCell.xRC, massCell.xLL, massCell.xRR ,i);
    }
    else if (string == "LaplacianLequals3")
    {
        error = problem.laplacianPrimalCVConti_lequals3_(massCell.xL, massCell.xR, massCell.yD, massCell.yU) - problem.laplacianPrimalCVApprox_lequals3_(massCell.xL, massCell.xR, massCell.yD, massCell.yU, massCell.yDC, massCell.yUC, massCell.yDD, massCell.yUU ,i);
    }
    else
    {
        DUNE_THROW(Dune::InvalidStateException, "");
    }

    return error;
}


double primalDivTermIndividualCellLocalTruncationError (const DivCell& divCell, const Dumux::TestProblemForOrderNumerics& problem, std::string string)
{
    double error;

    if (string == "div")
    {
        error = problem.div_lequal1_PrimalCV_Conti_(divCell.xL, divCell.xR, divCell.yD, divCell.yU) - problem.div_lequal1_PrimalCV_Approx_(divCell.xL, divCell.xR, divCell.yD, divCell.yU);
    }
    else if (string == "divLequals3")
    {
        error = problem.div_lequal3_PrimalCV_Conti_(divCell.xL, divCell.xR, divCell.yD, divCell.yU) - problem.div_lequal3_PrimalCV_Approx_(divCell.xL, divCell.xR, divCell.yD, divCell.yU);
    }
    else
    {
        DUNE_THROW(Dune::InvalidStateException, "");
    }

    return error;
}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::OrderNumericsTestTypeTag;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv, usage);

    TestProblemForOrderNumerics problem;

    unsigned int NMomentum = 8; //after that it looks like numeric limits might be reached
    unsigned int numCellsMomentum = 11;

    unsigned int NMass = 8; //after that it looks like numeric limits might be reached
    unsigned int numCellsMass = 4;

    double deltaIni = .1;

    std::vector<std::string> momentumTerms;
    momentumTerms.push_back("q");
    momentumTerms.push_back("gradient");
    momentumTerms.push_back("normalDiffFrontal");
    momentumTerms.push_back("normalDiffLateralZerothOrder");
    momentumTerms.push_back("normalDiffLateralFirstOrder");
    momentumTerms.push_back("tangential");
    momentumTerms.push_back("nonMixed");
    momentumTerms.push_back("mixed");
    momentumTerms.push_back("momentumX");

    std::vector<std::string> divTerms;
    divTerms.push_back("div");
    divTerms.push_back("divLequals3");

    std::vector<std::string> laplacianTerms;
    laplacianTerms.push_back("Laplacian");
    laplacianTerms.push_back("LaplacianLequals3");

    //individualCV mass
    for (const auto& divTerm : divTerms)
    {
        std::ofstream file;
        file.open(divTerm + ".csv");

        file << "delta";
        for (unsigned int cvNumber=1; cvNumber<numCellsMass+1; ++cvNumber)
        {
            file << ", cell"<< cvNumber;
        }
        file << std::endl;

        for (unsigned int n=1; n<NMass+1; ++n)
        {
            double delta = deltaIni/std::pow(2.,n);
            file << delta ;

            for (unsigned int cvNumber=1; cvNumber<numCellsMass+1; ++cvNumber)
            {
                PrimalCellPropertiesForDiv cellProperties = setPrimalCellPropertiesForDiv(delta);
                DivCell individualCV = makeDivCV(cellProperties);

                file << "," << primalDivTermIndividualCellLocalTruncationError(individualCV, problem, divTerm);
            }
            file << std::endl;
        }
        file.close();
    }

    for (const auto& laplacianTerm : laplacianTerms)
    {
        std::ofstream file;
        file.open(laplacianTerm + ".csv");

        file << "delta";
        for (unsigned int cvNumber=1; cvNumber<numCellsMass+1; ++cvNumber)
        {
            file << ", cell"<< cvNumber;
        }
        file << std::endl;

        for (unsigned int n=1; n<NMass+1; ++n)
        {
            double delta = deltaIni/std::pow(2.,n);
            file << delta ;

            for (unsigned int cvNumber=1; cvNumber<numCellsMass+1; ++cvNumber)
            {
                PrimalCellPropertiesForLaplacian cellProperties = setPrimalCellPropertiesForLaplacian(cvNumber, delta);
                MassCell individualCV = makePrimalCV(cellProperties);

                file << "," << primalIndividualTermIndividualCellLocalTruncationError(individualCV, problem, cvNumber, laplacianTerm);
            }
            file << std::endl;
        }
        file.close();
    }

    //individual cell momentum
    for (const auto& individualTerm : momentumTerms)
    {
        std::ofstream file;
        file.open(individualTerm + ".csv");

        file << "delta";
        for (unsigned int cvNumber=1; cvNumber<numCellsMomentum+1; ++cvNumber)
        {
            file << ", cell"<< cvNumber;
        }
        file << std::endl;

        for (unsigned int n=1; n<NMomentum+1; ++n)
        {
            double delta = deltaIni/std::pow(2.,n);
            file << delta ;

            for (unsigned int cvNumber=1; cvNumber<numCellsMomentum+1; ++cvNumber)
            {
                DualCellProperties cellProperties = setDualCellProperties(cvNumber, delta);
                MomentumCell individualCV = makeDualCV(cellProperties);

                file << "," << dualIndividualTermIndividualCellLocalTruncationError(individualCV, problem, cvNumber, individualTerm);
            }
            file << std::endl;
        }
        file.close();
    }

    //sum cvs mass
    for (const auto& divTerm : divTerms)
    {
        std::ofstream file;
        file.open(divTerm + "_sum.csv");

        file << "delta, sum" << std::endl;

        for (unsigned int n=1; n<NMass+1; ++n)
        {
            double delta = deltaIni/std::pow(2.,n);
            file << delta ;

            double sumDivError = 0.;
            double sumArea = 0.;

            double xMin = 0.;
            double xMax = 1.;
            double yMin = 0.;
            double yMax = 1.;

            double refineLeft = .4;
            double refineRight = .6;
            double refineDown = .4;
            double refineUp = .6;

            double NxCoarse = (xMax - xMin)/delta;
            double NyCoarse = (yMax - yMin)/delta;

            //unrefined region
            for (unsigned int cellIndexX = 0; cellIndexX < NxCoarse; ++cellIndexX)
            {
                double xC = xMin + .5*delta + cellIndexX*delta;
                bool inXRefinementRegion = refineLeft < xC && xC < refineRight;

                for (unsigned int cellIndexY = 0; cellIndexY < NyCoarse; ++cellIndexY)
                {
                    double yC = yMin + .5*delta + cellIndexY*delta;
                    bool inYRefinementRegion = refineDown < yC && yC < refineUp;

                    if (!(inXRefinementRegion && inYRefinementRegion))
                    {
                        PrimalCellPropertiesForDiv prop;
                        prop.deltaX = delta;
                        prop.deltaY = delta;
                        prop.xCenter = xC;
                        prop.yCenter = yC;

                        DivCell individualCV = makeDivCV(prop);
                        double individualArea = prop.deltaX*prop.deltaY;

                        sumDivError += std::abs(primalDivTermIndividualCellLocalTruncationError(individualCV, problem, divTerm))*individualArea;
                        sumArea += individualArea;
                    }
                }
            }

            double NxFine = 2.*(refineRight - refineLeft)/delta;
            double NyFine = 2.*(refineUp - refineDown)/delta;

            //refined region
            for (unsigned int cellIndexX = 0; cellIndexX < NxFine; ++cellIndexX)
            {
                double xC = refineLeft + .25*delta + cellIndexX*.5*delta;

                for (unsigned int cellIndexY = 0; cellIndexY < NyFine; ++cellIndexY)
                {
                    double yC = refineDown + .25*delta + cellIndexY*.5*delta;

                    PrimalCellPropertiesForDiv prop;
                    prop.deltaX = .5*delta;
                    prop.deltaY = .5*delta;
                    prop.xCenter = xC;
                    prop.yCenter = yC;

                    DivCell individualCV = makeDivCV(prop);
                    double individualArea = prop.deltaX*prop.deltaY;

                    sumDivError += std::abs(primalDivTermIndividualCellLocalTruncationError(individualCV, problem, divTerm))*individualArea;
                    sumArea += individualArea;
                }
            }

            file << "," << sumDivError/sumArea;

            file << std::endl;
        }
        file.close();
    }

    //sum cvs momentum
    for (const auto& individualTerm : momentumTerms)
    {
        std::ofstream file;
        file.open(individualTerm + "_sum.csv");

        file << "delta, sum" << std::endl;

        for (unsigned int n=1; n<NMomentum+1; ++n)
        {
            double delta = deltaIni/std::pow(2.,n);
            file << delta ;

            double sumDivError = 0.;
            double sumArea = 0.;

            double xMin = 0.;
            double xMax = 1.;
            double yMin = 0.;
            double yMax = 1.;

            double refineLeft = .4;
            double refineRight = .6;
            double refineDown = .4;
            double refineUp = .6;

            double NxCoarse = (xMax - xMin)/delta-1;
            double NyCoarse = (yMax - yMin)/delta;

            //unrefined region
            for (unsigned int cellIndexX = 0; cellIndexX < NxCoarse; ++cellIndexX)
            {
                double xC = xMin + delta + cellIndexX*delta;
                bool inXRefinementRegion = refineLeft-.5*delta < xC && xC < refineRight+.5*delta;

                for (unsigned int cellIndexY = 0; cellIndexY < NyCoarse; ++cellIndexY)
                {
                    double yC = yMin + .5*delta + cellIndexY*delta;
                    bool inYRefinementRegion = refineDown < yC && yC < refineUp;

                    if (!(inXRefinementRegion && inYRefinementRegion))
                    {
                        unsigned int cellsBelow = (refineDown-yMin)/delta;
                        unsigned int cellsAbove = (yMax-refineUp)/delta;

                        DualCellProperties prop;

                        prop.xCenter = xC;
                        prop.yCenter = yC;

                        prop.deltaY_C = delta;
                        prop.deltaXL = delta;
                        prop.deltaXR = delta;

                        if (cellIndexY == 0)
                        {
                            prop.deltaY_D = 0.;
                        }
                        else if (inXRefinementRegion && scalarCmp(yC-.5*delta,refineUp))
                        {
                            prop.deltaY_D = .5*delta;
                        }
                        else
                            prop.deltaY_D = delta;

                        if (cellIndexY == NyCoarse-1)
                        {
                            prop.deltaY_U = 0.;
                        }
                        else if (inXRefinementRegion & scalarCmp(yC+.5*delta,refineDown))
                        {
                            prop.deltaY_U = .5*delta;
                        }
                        else
                        {
                            prop.deltaY_U = delta;
                        }

                        MomentumCell individualCV = makeDualCV(prop);
                        double individualArea = .5*(prop.deltaXL+prop.deltaXR)*prop.deltaY_C;

                        sumDivError += std::abs(dualIndividualTermIndividualCellLocalTruncationError(individualCV, problem, 0, individualTerm))*individualArea;
                        sumArea += individualArea;
                    }
                }
            }

            //transition region
            double NyFine = 2.*(refineUp - refineDown)/delta;

            for (unsigned int cellIndexX = 0; cellIndexX < 2; ++cellIndexX)
            {
                double xC;
                if (cellIndexX == 0)
                    xC = refineLeft;
                else
                    xC = refineRight;

                for (unsigned int cellIndexY = 0; cellIndexY < NyFine; ++cellIndexY)
                {
                    double yC = refineDown + .25*delta + cellIndexY*.5*delta;

                    DualCellProperties prop;

                    prop.deltaY_C = .5*delta;

                    if (cellIndexX == 0)
                    {
                        prop.deltaXL = delta;
                        prop.deltaXR = .5*delta;
                    }
                    else
                    {
                        prop.deltaXL = .5*delta;
                        prop.deltaXR = delta;
                    }

                    prop.xCenter = xC - + .25*(prop.deltaXR-prop.deltaXL);
                    prop.yCenter = yC;

                    if (scalarCmp(yC-.5*prop.deltaY_C,refineDown))
                    {
                        prop.deltaY_D = delta;
                    }
                    else
                        prop.deltaY_D = .5*delta;

                    if (scalarCmp(yC+.5*prop.deltaY_C,refineUp))
                    {
                        prop.deltaY_U = delta;
                    }
                    else
                    {
                        prop.deltaY_U = .5*delta;
                    }

                    MomentumCell individualCV = makeDualCV(prop);
                    double individualArea = .5*(prop.deltaXL+prop.deltaXR)*prop.deltaY_C;

                    sumDivError += std::abs(dualIndividualTermIndividualCellLocalTruncationError(individualCV, problem, 0, individualTerm))*individualArea;
                    sumArea += individualArea;
                }
            }

            //fine region
            double NxFine = 2.*(refineRight - refineLeft)/delta-1;
            for (unsigned int cellIndexX = 0; cellIndexX < NxFine; ++cellIndexX)
            {
                double xC = refineLeft + .5*delta + cellIndexX*.5*delta;

                for (unsigned int cellIndexY = 0; cellIndexY < NyFine; ++cellIndexY)
                {
                    double yC = refineDown + .25*delta + cellIndexY*.5*delta;

                    DualCellProperties prop;

                    prop.xCenter = xC;
                    prop.yCenter = yC;

                    prop.deltaY_C = .5*delta;
                    prop.deltaXL = .5*delta;
                    prop.deltaXR = .5*delta;

                    if (scalarCmp(yC-.5*prop.deltaY_C,refineDown))
                    {
                        prop.deltaY_D = delta;
                    }
                    else
                        prop.deltaY_D = .5*delta;

                    if (scalarCmp(yC+.5*prop.deltaY_C,refineUp))
                    {
                        prop.deltaY_U = delta;
                    }
                    else
                    {
                        prop.deltaY_U = .5*delta;
                    }

                    MomentumCell individualCV = makeDualCV(prop);
                    double individualArea = .5*(prop.deltaXL+prop.deltaXR)*prop.deltaY_C;

                    sumDivError += std::abs(dualIndividualTermIndividualCellLocalTruncationError(individualCV, problem, 0, individualTerm))*individualArea;
                    sumArea += individualArea;
                }
            }

            file << "," << sumDivError/sumArea;

            file << std::endl;
        }
        file.close();
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
