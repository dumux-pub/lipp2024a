// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Channel flow test for the staggered grid (Navier-)Stokes model
 */

bool doFirstOrderLocalTruncErrorGlobalRefinement = false;

 #include <config.h>

 #include <ctime>
 #include <iostream>

 #include <dune/common/parallel/mpihelper.hh>
 #include <dune/common/timer.hh>
 #include <dune/grid/io/file/dgfparser/dgfwriter.hh>
 #include <dune/grid/io/file/dgfparser/dgfexception.hh>
 #include <dune/grid/io/file/vtk.hh>
 #include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/staggeredrefinedfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>

#include <dumux/io/mystaggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/grid/gridmanager_sub.hh>

#include <dumux/freeflow/navierstokes/staggered/myfluxoversurface.hh>

#include <dumux/discretization/staggered/myfvgridgeometry.hh>
#include <dumux/discretization/staggered/freeflow/fvgridgeometrytraits1.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/geometryhelpergenericmethods.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <dumux/freeflow/navierstokes/errors.hh>
#include <dumux/freeflow/navierstokes/staggered/residualCalc.hh>
#include <dumux/io/outputFacility.hh>

#include "problem.hh"

/*!
 * \brief Provides an interface for customizing error messages associated with
 *        reading in parameters.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
                    errorMessageOut += progName;
                    errorMessageOut += " [options]\n";
                    errorMessageOut += errorMsg;
                    errorMessageOut += "\n\nThe list of mandatory arguments for this program is:\n"
                                        "\t-Grid.File                      Name of the file containing the grid \n"
                                        "\t                                definition in DGF format\n"
                                        "\t-SpatialParams.LensLowerLeftX   x-coordinate of the lower left corner of the lens [m] \n"
                                        "\t-SpatialParams.LensLowerLeftY   y-coordinate of the lower left corner of the lens [m] \n"
                                        "\t-SpatialParams.LensUpperRightX  x-coordinate of the upper right corner of the lens [m] \n"
                                        "\t-SpatialParams.LensUpperRightY  y-coordinate of the upper right corner of the lens [m] \n"
                                        "\t-SpatialParams.Permeability     Permeability of the domain [m^2] \n"
                                        "\t-SpatialParams.PermeabilityLens Permeability of the lens [m^2] \n";

        std::cout << errorMessageOut
                  << "\n";
    }
}

namespace Dumux
{
namespace Properties
{
// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::ChannelTestTypeTag>
{
private:
    static constexpr auto dim = 2;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using HostGrid = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >;
    using type = Dune::SubGrid<dim, HostGrid>;
};
}}

template <class SomeType>
bool copyOfContainerCmp(const SomeType& a, const SomeType& b, double eps) {
    if (a.size() != b.size()) {
        std::cout << "copyOfContainerCmp with different sizes" << std::endl;
        return false;
    } else {
        bool retVal = true;
        for (unsigned int i = 0; i < a.size(); ++i) {
            if (!((a[i] > b[i] - eps) && (a[i] < b[i] + eps))) {
                retVal = false;
                break;
            }
        }

        return retVal;
    }
}

template <class SomeType>
bool copyOfContainerCmp(const SomeType& a, const SomeType& b) {
    return copyOfContainerCmp(a,b,1e-10);
}

template <class InputIt, class T>
constexpr InputIt copyOfContainerFind(InputIt first, InputIt last, const T& value) {
    for (; first != last; ++first) {
        if (copyOfContainerCmp(*first, value)) {
            return first;
        }
    }
    return last;
}

template <class GeometricType1, class GeometricType2>
bool copyOfHaveCommonCorner(const GeometricType1& geometry1, const GeometricType2& geometry2) {
    bool commonCorners = false;

    using GlobalPosition = typename std::decay_t<decltype(geometry1.geometry().corner(0))>;

    std::vector<GlobalPosition> geometry1Corners;
    for (unsigned int i = 0; i < geometry1.geometry().corners(); ++i) {
        geometry1Corners.push_back(geometry1.geometry().corner(i));
    }

    for (unsigned int i = 0; i < geometry2.geometry().corners(); ++i) {
        const auto geometry2Corner = geometry2.geometry().corner(i);

        if (copyOfContainerFind(geometry1Corners.begin(), geometry1Corners.end(), geometry2Corner) != geometry1Corners.end()) {
            commonCorners = true;
        }
    }

    return commonCorners;
}

 /*!
     * \brief Method ensuring the refinement ratio of 2:1
     *
     *  For any given element, a loop over the neighbors checks if the
     *  entities refinement would require that any of the neighbors has
     *  to be refined, too. This is done recursively over all levels of the grid.
     *
     * \param element Element of interest that is to be refined
     * \param level level of the refined element: it is at least 1
     * \return true if everything was successful
     */
    template<class SomeGridView, class Element, class MyVector>
    void checkNeighborsRefine_(const SomeGridView& leafGridViewVar, unsigned int numRefinementLevels, MyVector& toBeAdaptedElements, const Element &element, std::size_t level = 1)
    {
        std::vector<Element> surroundingElements;

        using GlobalPosition =  typename Element::Geometry::GlobalCoordinate;
        std::vector<GlobalPosition> surroundingElementCenters;

        for (const auto& intersection : intersections(leafGridViewVar, element))
        {
            if(!intersection.neighbor())
                continue;

            //direct neighbor
            surroundingElements.push_back(intersection.outside());

            //diagonal neighbor
            for (const auto& outsideIntersection : intersections(leafGridViewVar, intersection.outside()))
            {
                if (!outsideIntersection.neighbor())
                    continue;

                if (copyOfHaveCommonCorner(outsideIntersection,element) && !copyOfContainerCmp(outsideIntersection.geometry().center(),intersection.geometry().center()))
                {
                    if(copyOfContainerFind(surroundingElementCenters.begin(), surroundingElementCenters.end(), outsideIntersection.outside().geometry().center()) == surroundingElementCenters.end())
                    {
                        surroundingElements.push_back(outsideIntersection.outside());
                        surroundingElementCenters.push_back(outsideIntersection.outside().geometry().center());
                    }
                }
            }
        }

        for (const auto& surroundingElement : surroundingElements)
        {
            if (surroundingElement.level() < numRefinementLevels && surroundingElement.level() < element.level()/*is not yet refined but will be*/)
            {
                // ensure refinement for surroundingElement element
                toBeAdaptedElements.push_back(surroundingElement);

                if(level < numRefinementLevels)
                    checkNeighborsRefine_(leafGridViewVar, numRefinementLevels, toBeAdaptedElements, surroundingElement, ++level);
            }
        }
    }

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::ChannelTestTypeTag;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv, usage);

    using HelpingGridManager = Dumux::GridManager<GetPropType<TypeTag, Properties::HelpingGrid>>;
    HelpingGridManager helpingGridManager;
    helpingGridManager.init("Helper");
    const auto& helpingGridView = helpingGridManager.grid().leafGridView();
    Dune::DGFWriter dgfWriter(helpingGridView);
    std::string inputFileName = getParam<std::string>("Grid.File");
    dgfWriter.write(inputFileName);

    // The hostgrid
    using HostGrid = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >;/*typename GetPropType<TypeTag, Properties::Grid>::HostGrid;*/
    using HostGridManager = GridManager<HostGrid>;
    HostGridManager hostGridManager;
    hostGridManager.init();
    auto& hostGrid = hostGridManager.grid();

    using CellVector = std::vector<unsigned int>;
    const auto numCellsXVec = getParam<CellVector>("Helper.Grid.Cells0");
    const unsigned int numCellsX = std::accumulate(numCellsXVec.begin(), numCellsXVec.end(), 0);
    const auto numCellsYVec = getParam<CellVector>("Helper.Grid.Cells1");
    const unsigned int numCellsY = std::accumulate(numCellsYVec.begin(), numCellsYVec.end(), 0);
    CellVector numCells;
    numCells.push_back(numCellsX);
    numCells.push_back(numCellsY);

    using GeomSizeVector = std::vector<double>;
    const auto geomSizeX = getParam<GeomSizeVector>("Helper.Grid.Positions0");
    double geomSizeXRight = geomSizeX[1];
    const auto geomSizeY = getParam<GeomSizeVector>("Helper.Grid.Positions1");
    const double bBoxMinY = geomSizeY[0];
    double geomSizeYTop = geomSizeY[1];
    GeomSizeVector geomSize;
    geomSize.push_back(geomSizeXRight);
    geomSize.push_back(geomSizeYTop);

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const Scalar cellSizeX = geomSize[0] / numCells[0];

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridViewVar = hostGrid.leafGridView();

    unsigned int numRefinementLevels = getParam<unsigned int>("Adaptivity.NumRefinementLevels");
    Scalar upRefinementRegion = getParam<Scalar>("Adaptivity.UpRefinementRegion");
    Scalar leftRefinementRegion = getParam<Scalar>("Adaptivity.LeftRefinementRegion");
    Scalar rightRefinementRegion = getParam<Scalar>("Adaptivity.RightRefinementRegion");
    Scalar obstacleHeight = getParam<Scalar>("Obstacles.Height");

    bool adapt = getParam<bool>("Adaptivity.Adapt");
    if (adapt)
    {
        for (unsigned int i = 0; i < numRefinementLevels; ++i)
        {
            using HostGridView = typename HostGrid::LeafGridView;
            using HostGridElement = typename HostGridView::template Codim<0>::Entity;

            std::vector<HostGridElement> toBeAdaptedElements;

            for (const auto& element : elements(leafGridViewVar))
            {
                using GlobalPosition =  typename HostGridElement::Geometry::GlobalCoordinate;

                GlobalPosition currentCenter = element.geometry().center();

                std::vector<GlobalPosition> futureCenters;
                futureCenters.push_back(currentCenter);

                unsigned int elementLevel = element.level();

                for (unsigned int j = 1; j <= numRefinementLevels-elementLevel; ++j)
                {
                    double deltaX = .2/std::pow(2.,elementLevel);
                    double deltaY = .2/std::pow(2.,elementLevel);//element.geometry().corners();

                    for (unsigned int k = 0; k < std::pow(2.,j); ++k)
                    {
                        for (unsigned int l = 0; l < std::pow(2.,j); ++l)
                        {
                            GlobalPosition futureCenter;
                            futureCenter[0] = currentCenter[0] - .5*deltaX + (deltaX*(1.+2.*k))/std::pow(2.,j+1);
                            futureCenter[1] = currentCenter[1] - .5*deltaY + (deltaY*(1.+2.*l))/std::pow(2.,j+1);

                            futureCenters.push_back(futureCenter);
                        }
                    }
                }

                bool xyCondition = false;

                for (const auto& pos : futureCenters)
                {
                    auto x = pos[0];
                    auto y = pos[1];

                    if ((y < 0 && i == 0) || (y < upRefinementRegion && leftRefinementRegion < x && x < rightRefinementRegion))
                    {
                        xyCondition = true;
                    }
                }

                if(element.level() < numRefinementLevels && xyCondition)
                {
                    checkNeighborsRefine_(leafGridViewVar, numRefinementLevels, toBeAdaptedElements, element);
                    toBeAdaptedElements.push_back(element);
                }
            }

            for (const auto& element : toBeAdaptedElements)
            {
                hostGrid.preAdapt();
                hostGrid.mark( 1,  element);
                hostGrid.adapt();
                hostGrid.postAdapt();
            }
        }


    }

    // The subgrid
    auto selector = [&](const auto& element)
    {
        bool checker = true;

        Scalar x = element.geometry().center()[0];
        Scalar y = element.geometry().center()[1];

        Scalar actualLowerBoundary = getParam<Scalar>("Obstacles.ActualLowerBoundary");
        Scalar obstacleWidth = getParam<Scalar>("Obstacles.Width");
        Scalar obstacleDistance = getParam<Scalar>("Obstacles.Distance");
        Scalar firstObstacleX = getParam<Scalar>("Obstacles.FirstObstacleX");
        unsigned int numObstacles = getParam<unsigned int>("Obstacles.Num");

        for (unsigned int i = 0; i < numObstacles; ++i)
        {
            if (y<actualLowerBoundary || (y < 0. && firstObstacleX+i*(obstacleDistance+obstacleWidth) < x && x < firstObstacleX+i*(obstacleDistance+obstacleWidth)+obstacleWidth))
                checker = false;
        }

        return checker;
    };

//      */


    Dumux::GridManager<Dune::SubGrid<2, HostGrid>> subGridManager;
    subGridManager.init(hostGrid, selector);

    // we compute on the leaf grid view
    const auto& leafGridView = subGridManager.grid().leafGridView();

    Dune::VTKWriter vtkWriterVar(leafGridView);
    vtkWriterVar.write("bla", Dune::VTK::ascii);

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    const auto numDofsCellCenter = leafGridView.size(0);
    const auto numDofsFace = (*gridGeometry).numIntersections();
    SolutionVector x;
    x[GridGeometry::cellCenterIdx()].resize(numDofsCellCenter);
    x[GridGeometry::faceIdx()].resize(numDofsFace);
    problem->applyInitialSolution(x);
    problem->createOutputFields(x);
    auto xOld = x;

    std::cout << "numDofsCellCenter = " << numDofsCellCenter << std::endl;

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    // initialize the vtk output module
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    MyStaggeredVtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    IOFields::initOutputModule(vtkWriter); //!< Add model specific output fields


    // the assembler with time loop for instationary problem
    using Assembler = StaggeredRefinedFVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables);

    // the linear solver
    using LinearSolver = Dumux::UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    // solve the non-linear system with time step control
    nonLinearSolver.solve(x);

    // write vtk output
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition =  typename Element::Geometry::GlobalCoordinate;
    vtkWriter.write([&](GlobalPosition pos, Scalar min, Scalar max){return false;}, 1.);

     std::array<std::string, 4> paramGroups = {"finexCVs", "fineyCVs", "coarsexCVs", "coarseyCVs"}; //xfine...
    CVOutputFacility<TypeTag> cvOutputFacility(problem);
    std::array<HostGridManager, 4> cvGridManagers = {};
    std::array<std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>>,4> cvCenterScvfIndicesMaps;
    cvOutputFacility.iniCVGridManagers(cvGridManagers, cvCenterScvfIndicesMaps, paramGroups);

    auto falseLambda = [](unsigned int dirIdx, GlobalPosition pos, Scalar min, Scalar max){return false;};
    bool readDofBasedValues = getParam<bool>("Problem.ReadDofBasedValues", false);

    using FaceSolutionVector = GetPropType<TypeTag, Properties::FaceSolutionVector>;
    FaceSolutionVector numericalFaceResidualWithAnalytical = {};
    numericalFaceResidualWithAnalytical.resize(problem->gridGeometry().numFaceDofs());

    std::optional<std::array<std::vector<Scalar>, 4>> optionalPerfectInterpolationFaceResiduals;

    cvOutputFacility.onCVsOutputResidualsAndPrimVars(
                gridGeometry,
                cvGridManagers,
                cvCenterScvfIndicesMaps,
                paramGroups,
                x,
                0,
                numericalFaceResidualWithAnalytical,
                optionalPerfectInterpolationFaceResiduals,
                readDofBasedValues,
                falseLambda,
                falseLambda,
                falseLambda,
                falseLambda,
                falseLambda);

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
