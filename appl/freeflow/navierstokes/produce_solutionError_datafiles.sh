#!/bin/bash

#dumux commit 2ec8f9159a
#dune releases/2.9
#Example of successful use: dumux1/dumux-adaptivestaggered/build-cmake/appl/freeflow/navierstokes$ bash ./produce_solutionError_datafiles.sh

###### obstacles
cd obstacles
make test_ff_obstacles
./test_ff_obstacles params_refined.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient false
cp test_obstacles_refined_numericalP-00000.csv ../../../datafiles/obstacles_refined_p.csv
cp test_obstacles_refined_numericalP_cavityOnly-00000.csv ../../../datafiles/obstacles_refined_p_cavityOnly.csv
cp numericalVelocity_x-00000.csv ../../../datafiles/obstacles_refined_u.csv
cp numericalVelocity_x-00000_cavityOnly.csv ../../../datafiles/obstacles_refined_u_cavityOnly.csv
cp numericalVelocity_y-00000.csv ../../../datafiles/obstacles_refined_v.csv
cp numericalVelocity_y-00000_cavityOnly.csv ../../../datafiles/obstacles_refined_v_cavityOnly.csv
cp test_obstacles_refined_midcellvelocity-00000.csv ../../../datafiles/obstacles_refined_midcellvelocities.csv
cp test_obstacles_refined_midcellvelocity_cavityOnly-00000.csv ../../../datafiles/obstacles_refined_midcellvelocities_cavityOnly.csv

./test_ff_obstacles params_unrefined.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient false
cp test_obstacles_unrefined_numericalP-00000.csv ../../../datafiles/obstacles_unrefined_p.csv
cp test_obstacles_unrefined_numericalP_cavityOnly-00000.csv ../../../datafiles/obstacles_unrefined_p_cavityOnly.csv
cp numericalVelocity_x-00000.csv ../../../datafiles/obstacles_unrefined_u.csv
cp numericalVelocity_x-00000_cavityOnly.csv ../../../datafiles/obstacles_unrefined_u_cavityOnly.csv
cp numericalVelocity_y-00000.csv ../../../datafiles/obstacles_unrefined_v.csv
cp numericalVelocity_y-00000_cavityOnly.csv ../../../datafiles/obstacles_unrefined_v_cavityOnly.csv
cp test_obstacles_unrefined_midcellvelocity-00000.csv ../../../datafiles/obstacles_unrefined_midcellvelocities.csv
cp test_obstacles_unrefined_midcellvelocity_cavityOnly-00000.csv ../../../datafiles/obstacles_unrefined_midcellvelocities_cavityOnly.csv
cd ..
