// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \brief Channel flow test for the staggered grid (Navier-)Stokes model
 */
#ifndef DUMUX_CHANNEL_TEST_UNREFINED_PROBLEM_HH
#define DUMUX_CHANNEL_TEST_UNREFINED_PROBLEM_HH

#include <dumux/common/numeqvector.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/components/constant.hh>

#include <dumux/freeflow/navierstokes/problem.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/navierstokes/model.hh>

namespace Dumux
{
template <class TypeTag>
class UnrefinedPNMChannelTestProblem;

namespace Properties
{
// Create new type tags
namespace TTag {
struct StokesTypeTag { using InheritsFrom = std::tuple<StaggeredFreeFlowModel, NavierStokes>; };
} // end namespace TTag

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::StokesTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar> >;
};


// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::StokesTypeTag> { using type = Dumux::UnrefinedPNMChannelTestProblem<TypeTag> ; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::StokesTypeTag> { static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::StokesTypeTag> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::StokesTypeTag> { static constexpr bool value = true; };
}

/*!
 * \ingroup NavierStokesTests
 * \brief  Test problem for the one-phase (Navier-) Stokes problem in a channel.
 * \todo doc me!
 */
template <class TypeTag>
class UnrefinedPNMChannelTestProblem : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;

    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    // copy some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;

    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<GetPropType<TypeTag, Properties::PrimaryVariables>>;

    using TimeLoopPtr = std::shared_ptr<CheckPointTimeLoop<Scalar>>;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

public:
    UnrefinedPNMChannelTestProblem(std::shared_ptr<const GridGeometry> gridGeometry, std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(gridGeometry, "Stokes"), eps_(1e-6), couplingManager_(couplingManager)
    {
        inletVelocity_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InletVelocity", 0.0);
        outLetOnTop_ = getParamFromGroup<bool>(this->paramGroup(), "Problem.OutletOnTop", false);

        kinematicViscosity_ = getParam<Scalar>("Component.LiquidKinematicViscosity", 1.0);
        outletPressure_ = getParam<Scalar>("Problem.OutletPressure", 0.);

        const Scalar yMin = this->gridGeometry().bBoxMin()[1];
        const Scalar yMax = this->gridGeometry().bBoxMax()[1];

        velocityQuadraticCoefficient_ = - inletVelocity_ / (0.25*(yMax - yMin)*(yMax - yMin));
        velocityLinearCoefficient_ = velocityQuadraticCoefficient_ * (-yMax - yMin);
        velocityConstant_ = velocityQuadraticCoefficient_ * yMin * yMax;
        pressureLinearCoefficient_ = 2. * velocityQuadraticCoefficient_ * kinematicViscosity_;
        pressureConstant_ = -pressureLinearCoefficient_ * this->gridGeometry().bBoxMax()[0]  + outletPressure_;
    }

   /*!
     * \name Problem parameters
     */
    // \{

   /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; }

   /*!
     * \brief Return the sources within the domain.
     *
     * \param globalPos The global position
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    {
        return NumEqVector(0.0);
    }
    // \}
   /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param element The finite element
     * \param scvf The sub control volume face
     */
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        const auto& globalPos = scvf.dofPosition();

        // set Dirichlet values for the velocity everywhere except at the outlet
        if (isOutlet(globalPos))
            values.setDirichlet(Indices::pressureIdx);
        else if (couplingManager().isCoupledEntity(CouplingManager::bulkIdx, scvf))
        {
            values.setCouplingDirichlet(Indices::velocityXIdx);
            values.setCouplingDirichlet(Indices::velocityYIdx);
        }
        else
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
        }
        return values;
    }

    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFaceVariables& elemFaceVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param element The element
     * \param scvf The subcontrolvolume face
     */
    PrimaryVariables dirichlet(const Element& element, const SubControlVolumeFace& scvf) const
    {
        const auto& globalPos = scvf.dofPosition();
        PrimaryVariables values;
        values[Indices::pressureIdx] = 0.0;
        values[Indices::velocityXIdx] = 0.0;
        values[Indices::velocityYIdx] = 0.0;

        if(isInlet(globalPos))
        {
            Scalar y = globalPos[1];
            values[Indices::velocityXIdx] = velocityQuadraticCoefficient_ * y * y + velocityLinearCoefficient_ * y + velocityConstant_;
        }

        if(couplingManager().isCoupledEntity(CouplingManager::bulkFaceIdx, scvf))
        {
            const auto couplingVelocity = couplingManager().couplingData().boundaryVelocity(element, scvf);
            values[Indices::velocityXIdx] = couplingVelocity[0];
            values[Indices::velocityYIdx] = couplingVelocity[1];
        }

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param element The element
     * \param scvf The subcontrolvolume face
     */
    PrimaryVariables dirichlet(const Element& element, const SubControlVolume& scv) const
    {
        return PrimaryVariables(0.0);
    }

    // \}

    template<class FluxOverPlane>
    void setPlanes(FluxOverPlane& flux, const std::vector<Scalar>& auxiliaryPositions)
    {
        const Scalar xMin = this->gridGeometry().bBoxMin()[0];
        const Scalar xMax = this->gridGeometry().bBoxMax()[0];
        const Scalar yMin = this->gridGeometry().bBoxMin()[1];
        const Scalar yMax = this->gridGeometry().bBoxMax()[1];
        //set a plane at the outlet
        if(outLetOnTop_)
        {
            const GlobalPosition pLeft{xMin, yMax};
            const GlobalPosition pRight{xMax, yMax};
            flux.addSurface("outlet", pLeft, pRight);
        }
        else
        {
            const GlobalPosition pBottom{xMax, yMin};
            const GlobalPosition pTop{xMax, yMax};
            flux.addSurface("outlet", pBottom, pTop);

            for(auto i : auxiliaryPositions)
                std::cout << i << std::endl;

            for(int i = 0; i < auxiliaryPositions.size(); i+=2)
            {
                const GlobalPosition pLeft{auxiliaryPositions[i] , yMin};
                const GlobalPosition pRight{auxiliaryPositions[i+1] , yMin};
                flux.addSurface("throatsVerticalFlux", pLeft, pRight);
            }
        }

    }

    template<class FluxOverPlane>
    void printFluxes(const FluxOverPlane& flux) const
    {
        std::cout << "mass flux at outlet is: " << flux.netFlux("outlet") << std::endl;

        if(!outLetOnTop_)
        {
            const auto& values = flux.values("throatsVerticalFlux");

            for(int i = 0; i < values.size(); ++i)
                std::cout << "throat " << i << " : " << values[i] << std::endl;

            std::cout << "\nvertical net flux (should be zero): "  << flux.netFlux("throatsVerticalFlux") << std::endl;
            std::cout << "\n##################################\n" << std::endl;

        }

    }

    //! Set the coupling manager
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

   /*!
     * \name Volume terms
     */
    // \{

   /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        values[Indices::pressureIdx] = 1.1e+5;
        values[Indices::velocityXIdx] = 0.0;
        values[Indices::velocityYIdx] = 0.0;
        return values;
    }

    // \}
    void setTimeLoop(TimeLoopPtr timeLoop)
    {
        timeLoop_ = timeLoop;
        if(inletVelocity_ > eps_)
            timeLoop_->setCheckPoint({200.0, 210.0});
    }

    Scalar time() const
    {
        return timeLoop_->time();
    }

private:

    bool isInlet(const GlobalPosition& globalPos) const
    {
        return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_;
    }

    bool isOutlet(const GlobalPosition& globalPos) const
    {
        if(outLetOnTop_)
            return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_;
        else
            return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_;
    }


    Scalar eps_;
    Scalar inletVelocity_;
    bool outLetOnTop_;
    TimeLoopPtr timeLoop_;

    std::shared_ptr<CouplingManager> couplingManager_;

    Scalar kinematicViscosity_;
    Scalar outletPressure_;
    Scalar velocityQuadraticCoefficient_;
    Scalar velocityLinearCoefficient_;
    Scalar velocityConstant_;
    Scalar pressureLinearCoefficient_;
    Scalar pressureConstant_;
};
} //end namespace

#endif
