// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Test for the 1d-3d embedded mixed-dimension model coupling two
 *        one-phase porous medium flow problems
 */

bool doFirstOrderLocalTruncErrorGlobalRefinement = false;

#include <config.h>

#include <ctime>
#include <iostream>

#include <dune/grid/io/file/gmshwriter.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>
#include <dune/alugrid/grid.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>

#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <dune/istl/io.hh>

#include <dumux/assembly/staggeredrefinedfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/partial.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>
#include <dumux/geometry/diameter.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>
#include <dumux/discretization/staggered/myfvgridgeometry.hh>
#include <dumux/discretization/staggered/freeflow/fvgridgeometrytraits1.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/geometryhelpergenericmethods.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/myvtkoutputmodule.hh>
#include <dumux/io/mystaggeredvtkoutputmodule.hh>

#include <dumux/multidomain/staggeredtraits.hh>
#include <dumux/multidomain/myfvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>

#include <dumux/freeflow/navierstokes/staggered/myfluxoversurface.hh>
#include <dumux/freeflow/navierstokes/staggered/griddatatransfer.hh>
#include <dumux/freeflow/gridadaptindicator.hh>

#include <dumux/freeflow/navierstokes/myiofields.hh>

#include <dumux/adaptive/adapt.hh>
#include <dumux/adaptive/markelements.hh>

#include <dumux/porenetworkflow/common/pnmvtkoutputmodule.hh>

#include <dumux/mixeddimension/boundary/pnmstokes/refinedcouplingmanager.hh>
#include <dumux/io/grid/snappygridcreator.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/grid/porenetworkgridcreator.hh>

#include <dumux/freeflow/navierstokes/errors.hh>
#include <dumux/freeflow/navierstokes/staggered/residualCalc.hh>
#include <dumux/io/outputFacility.hh>

#include "problem_pnm.hh"
#include "problem_stokes.hh"

namespace Dumux {
namespace Properties {

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::StokesTypeTag>
{
    using Traits = StaggeredMultiDomainTraits<TypeTag, TypeTag, Properties::TTag::PNMOnePTypeTag>;
    using type = Dumux::RefinedPNMStokesCouplingManager<Traits, false>;
};

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::PNMOnePTypeTag>
{
    using Traits = StaggeredMultiDomainTraits<Properties::TTag::StokesTypeTag, Properties::TTag::StokesTypeTag, TypeTag>;
    using type = Dumux::RefinedPNMStokesCouplingManager<Traits, false>;
};


// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::StokesTypeTag>
{
private:
    static constexpr auto dim = 2;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = Dune::ALUGrid<dim,dim,Dune::cube,Dune::nonconforming >;
};

template<class TypeTag, class MyTypeTag>
struct HelpingGrid { using type = UndefinedProperty; };

// Set the grid type
template<class TypeTag>
struct HelpingGrid<TypeTag, TTag::StokesTypeTag>
{
    using type = Dune::YaspGrid<2,Dune::TensorProductCoordinates<double, 2>>;
};
} // end namespace Properties
} // end namespace Dumux

template <class SomeType>
bool copyOfContainerCmp(const SomeType& a, const SomeType& b, double eps) {
    if (a.size() != b.size()) {
        std::cout << "copyOfContainerCmp with different sizes" << std::endl;
        return false;
    } else {
        bool retVal = true;
        for (unsigned int i = 0; i < a.size(); ++i) {
            if (!((a[i] > b[i] - eps) && (a[i] < b[i] + eps))) {
                retVal = false;
                break;
            }
        }

        return retVal;
    }
}

template <class SomeType>
bool copyOfContainerCmp(const SomeType& a, const SomeType& b) {
    return copyOfContainerCmp(a,b,1e-10);
}

template <class InputIt, class T>
constexpr InputIt copyOfContainerFind(InputIt first, InputIt last, const T& value) {
    for (; first != last; ++first) {
        if (copyOfContainerCmp(*first, value)) {
            return first;
        }
    }
    return last;
}

template <class GeometricType1, class GeometricType2>
bool copyOfHaveCommonCorner(const GeometricType1& geometry1, const GeometricType2& geometry2) {
    bool commonCorners = false;

    using GlobalPosition = typename std::decay_t<decltype(geometry1.geometry().corner(0))>;

    std::vector<GlobalPosition> geometry1Corners;
    for (unsigned int i = 0; i < geometry1.geometry().corners(); ++i) {
        geometry1Corners.push_back(geometry1.geometry().corner(i));
    }

    for (unsigned int i = 0; i < geometry2.geometry().corners(); ++i) {
        const auto geometry2Corner = geometry2.geometry().corner(i);

        if (copyOfContainerFind(geometry1Corners.begin(), geometry1Corners.end(), geometry2Corner) != geometry1Corners.end()) {
            commonCorners = true;
        }
    }

    return commonCorners;
}

 /*!
     * \brief Method ensuring the refinement ratio of 2:1
     *
     *  For any given element, a loop over the neighbors checks if the
     *  entities refinement would require that any of the neighbors has
     *  to be refined, too. This is done recursively over all levels of the grid.
     *
     * \param element Element of interest that is to be refined
     * \param level level of the refined element: it is at least 1
     * \return true if everything was successful
     */
    template<class SomeGridView, class Element, class MyVector>
    void checkNeighborsRefine_(const SomeGridView& bulkGridView, unsigned int numRefinementLevels, MyVector& toBeAdaptedElements, const Element &element, std::size_t level = 1)
    {
        std::vector<Element> surroundingElements;

        using GlobalPosition =  typename Element::Geometry::GlobalCoordinate;
        std::vector<GlobalPosition> surroundingElementCenters;

        for (const auto& intersection : intersections(bulkGridView, element))
        {
            if(!intersection.neighbor())
                continue;

            //direct neighbor
            surroundingElements.push_back(intersection.outside());

            //diagonal neighbor
            for (const auto& outsideIntersection : intersections(bulkGridView, intersection.outside()))
            {
                if (!outsideIntersection.neighbor())
                    continue;

                if (copyOfHaveCommonCorner(outsideIntersection,element) && !copyOfContainerCmp(outsideIntersection.geometry().center(),intersection.geometry().center()))
                {
                    if(copyOfContainerFind(surroundingElementCenters.begin(), surroundingElementCenters.end(), outsideIntersection.outside().geometry().center()) == surroundingElementCenters.end())
                    {
                        surroundingElements.push_back(outsideIntersection.outside());
                        surroundingElementCenters.push_back(outsideIntersection.outside().geometry().center());
                    }
                }
            }
        }

        for (const auto& surroundingElement : surroundingElements)
        {
            if (surroundingElement.level() < numRefinementLevels && surroundingElement.level() < element.level()/*is not yet refined but will be*/)
            {
                // ensure refinement for surroundingElement element
                toBeAdaptedElements.push_back(surroundingElement);

                if(level < numRefinementLevels)
                    checkNeighborsRefine_(bulkGridView, numRefinementLevels, toBeAdaptedElements, surroundingElement, ++level);
            }
        }
    }

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // Define the sub problem type tags
    using BulkTypeTag = Properties::TTag::StokesTypeTag;
    using LowDimTypeTag = Properties::TTag::PNMOnePTypeTag;

    // try to create a grid (from the given grid file or the input file)
    // for both sub-domains
    using LowDimGridManager = Dumux::PoreNetworkGridCreator<2>;
    LowDimGridManager lowDimGridManager;
    lowDimGridManager.init("PNM"); // pass parameter group

    const bool localRefinement = getParam<bool>("Stokes.Grid.LocalRefinement", false);
    bool snappyGrid = getParam<bool>("Stokes.Grid.SnappyGrid", true);
    if (localRefinement)
        snappyGrid = false;

    if (snappyGrid)
    {
        using FreeFlowHelpingGridManager = Dumux::SnappyGridCreator<2, LowDimGridManager>;
        FreeFlowHelpingGridManager bulkHelpingGridManager;
        auto data = bulkHelpingGridManager.init(lowDimGridManager.grid(), *(lowDimGridManager.getGridData()), "FreeFlowHelper");
        const auto auxiliaryPositions = data.interFacePositions[0/*dimIdx*/].value();
        const auto& bulkHelpingGridView = bulkHelpingGridManager.grid().leafGridView();
        Dune::DGFWriter dgfWriter(bulkHelpingGridView);
        std::string inputFileName = getParam<std::string>("Stokes.Grid.File");
        dgfWriter.write(inputFileName);
    }
    else
    {
        using FreeFlowHelpingGridManager = Dumux::GridManager<GetPropType<BulkTypeTag, Properties::HelpingGrid>>;
        FreeFlowHelpingGridManager bulkHelpingGridManager;
        bulkHelpingGridManager.init("FreeFlowHelper");
        const auto& bulkHelpingGridView = bulkHelpingGridManager.grid().leafGridView();
        Dune::DGFWriter dgfWriter(bulkHelpingGridView);
        std::string inputFileName = getParam<std::string>("Stokes.Grid.File");
        dgfWriter.write(inputFileName);
    }

    using BulkGrid = GetPropType<BulkTypeTag, Properties::Grid>;
    using BulkGridManager = GridManager<BulkGrid>;
    BulkGridManager bulkGridManager;
    bulkGridManager.init("Stokes");
    auto& bulkGrid = bulkGridManager.grid();

    // we compute on the leaf grid view
    const auto& bulkGridView = bulkGrid.leafGridView();

    // instantiate indicator & data transfer, read parameters for indicator
    using Scalar = GetPropType<BulkTypeTag, Properties::Scalar>;
    const unsigned int numRefinementLevels = getParam<Scalar>("Adaptive.MaxLevel");
    Scalar bigRefineRadius = getParam<Scalar>("Adaptive.BigRefineRadius");
    Scalar bigRefineRadiusCenterHeight = getParam<Scalar>("Adaptive.BigRefineRadiusCenterHeight");
    Scalar smallRefineRadius = getParam<Scalar>("Adaptive.SmallRefineRadius");
    Scalar smallRefineRadiusCenterHeight = getParam<Scalar>("Adaptive.SmallRefineRadiusCenterHeight");

    const std::vector<Scalar> xPositions = getParam<std::vector<Scalar>>("FreeFlowHelper.Grid.Positions0");
    const unsigned int numCellsX = getParam<unsigned int>("FreeFlowHelper.Grid.Cells0");
    const Scalar deltaXIni = (xPositions[1] - xPositions[0])/numCellsX;

    const std::vector<Scalar> yPositions = getParam<std::vector<Scalar>>("FreeFlowHelper.Grid.Positions1");
    const unsigned int numCellsY = getParam<unsigned int>("FreeFlowHelper.Grid.Cells1");
    const Scalar deltaYIni = (yPositions[1] - yPositions[0])/numCellsY;

    const std::vector<Scalar> poreLowerLeft = getParam<std::vector<Scalar>>("PNM.Grid.LowerLeft");
    const std::vector<Scalar> poreUpperRight = getParam<std::vector<Scalar>>("PNM.Grid.UpperRight");

    const std::string xyConditionType = getParam<std::string>("Adaptive.ConditionType");

    if (localRefinement)
    {
        for (unsigned int i = 0; i < numRefinementLevels; ++i)
        {
            using BulkGridView = typename BulkGrid::LeafGridView;
            using BulkGridElement = typename BulkGridView::template Codim<0>::Entity;

            std::vector<BulkGridElement> toBeAdaptedElements;

            for (const auto& element : elements(bulkGridView))
            {
                using GlobalPosition =  typename BulkGridElement::Geometry::GlobalCoordinate;

                GlobalPosition currentCenter = element.geometry().center();

                std::vector<GlobalPosition> futureCenters;
                futureCenters.push_back(currentCenter);

                unsigned int elementLevel = element.level();

                for (unsigned int j = 1; j <= numRefinementLevels-elementLevel; ++j)
                {
                    double deltaX = deltaXIni/std::pow(2.,elementLevel);
                    double deltaY = deltaYIni/std::pow(2.,elementLevel);//element.geometry().corners();

                    for (unsigned int k = 0; k < std::pow(2.,j); ++k)
                    {
                        for (unsigned int l = 0; l < std::pow(2.,j); ++l)
                        {
                            GlobalPosition futureCenter;
                            futureCenter[0] = currentCenter[0] - .5*deltaX + (deltaX*(1.+2.*k))/std::pow(2.,j+1);
                            futureCenter[1] = currentCenter[1] - .5*deltaY + (deltaY*(1.+2.*l))/std::pow(2.,j+1);

                            futureCenters.push_back(futureCenter);
                        }
                    }
                }

                bool xyCondition = false;

                for (const auto& pos : futureCenters)
                {
                    auto x = pos[0];
                    auto y = pos[1];

                    GlobalPosition porePosition1, porePosition2, porePosition3, porePosition4;
                    porePosition1[0] = poreLowerLeft[0];
                    porePosition2[0] = poreUpperRight[0];

                    porePosition1[1] = bigRefineRadiusCenterHeight;
                    porePosition2[1] = bigRefineRadiusCenterHeight;

                    porePosition3[0] = -0.00025;
                    porePosition4[0] = 0.00025;

                    porePosition3[1] = smallRefineRadiusCenterHeight;
                    porePosition4[1] = smallRefineRadiusCenterHeight;

                    auto distance = [](GlobalPosition pos1, GlobalPosition pos2)
                    {
                        GlobalPosition diff;
                        diff[0] = pos1[0] - pos2[0];
                        diff[1] = pos1[1] - pos2[1];

                        return std::sqrt(diff[0]*diff[0] + diff[1]*diff[1]);
                    };

                    Scalar minCellSize = 0.001/std::pow(2.,numRefinementLevels);
                    Scalar delta = minCellSize+0.0005;

                    if (xyConditionType == "circles" && distance(pos,porePosition1) < bigRefineRadius ||  distance(pos,porePosition2) < bigRefineRadius || distance(pos,porePosition3) < smallRefineRadius || distance(pos,porePosition4) < smallRefineRadius)
                    {
                        xyCondition = true;
                    }

                    if (xyConditionType == "continuousLine" && ((porePosition1[0]-delta<pos[0] && pos[0] < porePosition1[0]+delta) || (porePosition2[0]-delta<pos[0] && pos[0] < porePosition2[0]+delta) || (porePosition3[0]-delta<pos[0] && pos[0] < porePosition3[0]+delta)) && pos[1] < 0.01+minCellSize)
                    {
                        xyCondition = true;
                    }

                    if (xyConditionType == "intermittantLine" && ((porePosition1[0]-delta<pos[0] && pos[0] < porePosition1[0]+delta) || (porePosition2[0]-delta<pos[0] && pos[0] < porePosition2[0]+delta) || (porePosition3[0]-delta<pos[0] && pos[0] < porePosition3[0]+delta)) && pos[1] < 0.01+minCellSize)
                    {
                        xyCondition = true;
                    }
                }

                if(element.level() < numRefinementLevels && xyCondition)
                {
                    checkNeighborsRefine_(bulkGridView, numRefinementLevels, toBeAdaptedElements, element);
                    toBeAdaptedElements.push_back(element);
                }
            }

            for (const auto& element : toBeAdaptedElements)
            {
                bulkGrid.preAdapt();
                bulkGrid.mark( 1,  element);
                bulkGrid.adapt();
                bulkGrid.postAdapt();
            }
        }
    }

    const auto& lowDimGridView = lowDimGridManager.grid().leafGridView();
    auto lowDimGridData = lowDimGridManager.getGridData();

    Dune::VTKWriter vtkWriterVar(bulkGridView);
    vtkWriterVar.write("bla", Dune::VTK::ascii);
    // create the finite volume grid geometry
    using BulkGridGeometry = GetPropType<BulkTypeTag, Properties::GridGeometry>;
    auto bulkGridGeometry = std::make_shared<BulkGridGeometry>(bulkGridView);
    using LowDimGridGeometry = GetPropType<LowDimTypeTag, Properties::GridGeometry>;
    auto lowDimGridGeometry = std::make_shared<LowDimGridGeometry>(lowDimGridView, *lowDimGridData);

    // the mixed dimension type traits
    using Traits = StaggeredMultiDomainTraits<BulkTypeTag, BulkTypeTag, LowDimTypeTag>;

    // the coupling manager
    using CouplingManager = RefinedPNMStokesCouplingManager<Traits, false>;
    auto couplingManager = std::make_shared<CouplingManager>();

    // the indices
    constexpr auto bulkCellCenterIdx = CouplingManager::bulkCellCenterIdx;
    constexpr auto bulkFaceIdx = CouplingManager::bulkFaceIdx;
    constexpr auto lowDimIdx = CouplingManager::lowDimIdx;

    // the problem (initial and boundary conditions)
    using BulkProblem = GetPropType<BulkTypeTag, Properties::Problem>;
    auto bulkProblem = std::make_shared<BulkProblem>(bulkGridGeometry, couplingManager);

    // the spatial parameters
    using LowDimSpatialParams = GetPropType<LowDimTypeTag, Properties::SpatialParams>;
    auto lowDimspatialParams = std::make_shared<LowDimSpatialParams>(lowDimGridGeometry);

    using LowDimProblem = GetPropType<LowDimTypeTag, Properties::Problem>;
    auto lowDimProblem = std::make_shared<LowDimProblem>(lowDimGridGeometry, lowDimspatialParams, couplingManager);

    // the solution vector
    Traits::SolutionVector sol;
    const auto numDofsCellCenter = bulkGridView.size(0);
    const auto numDofsFace = (*bulkGridGeometry).numIntersections();
    sol[bulkCellCenterIdx].resize(numDofsCellCenter);
    sol[bulkFaceIdx].resize(numDofsFace);
    sol[lowDimIdx].resize(lowDimGridGeometry->numDofs());

    std::cout << "cc " << numDofsCellCenter << ", face " << numDofsFace << std::endl;

    auto bulkSol = partial(sol, bulkFaceIdx, bulkCellCenterIdx);

    couplingManager->init(bulkProblem, lowDimProblem, sol);

    // the grid variables
    using BulkGridVariables = GetPropType<BulkTypeTag, Properties::GridVariables>;
    auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkProblem, bulkGridGeometry);
    bulkGridVariables->init(bulkSol);
    using LowDimGridVariables = GetPropType<LowDimTypeTag, Properties::GridVariables>;
    auto lowDimGridVariables = std::make_shared<LowDimGridVariables>(lowDimProblem, lowDimGridGeometry);
    lowDimGridVariables->init(sol[lowDimIdx]);

    // pass the grid variables to the coupling manager
    couplingManager->setGridVariables(std::make_tuple(bulkGridVariables->faceGridVariablesPtr(),
                                                      bulkGridVariables->cellCenterGridVariablesPtr(),
                                                      lowDimGridVariables));

    // intialize the vtk output module
    const auto bulkName = getParam<std::string>("Vtk.OutputName") + "_" + bulkProblem->name();
    const auto lowDimName = getParam<std::string>("Vtk.OutputName") + "_" + lowDimProblem->name();

    MyStaggeredVtkOutputModule<BulkGridVariables, decltype(bulkSol)> bulkVtkWriter(*bulkGridVariables, bulkSol, bulkName);
    GetPropType<BulkTypeTag, Properties::IOFields>::initOutputModule(bulkVtkWriter);

    PNMVtkOutputModule<LowDimTypeTag, MyVtkOutputModule<LowDimGridVariables, GetPropType<LowDimTypeTag, Properties::SolutionVector>>> lowDimVtkWriter(*lowDimGridVariables, sol[lowDimIdx], lowDimName);
    GetPropType<LowDimTypeTag, Properties::IOFields>::initOutputModule(lowDimVtkWriter);

    // the assembler with time loop for instationary problem
    using Assembler = RefinedMultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(bulkProblem, bulkProblem, lowDimProblem),
                                                 std::make_tuple(bulkGridGeometry->faceGridGeometryPtr(),
                                                                 bulkGridGeometry->cellCenterGridGeometryPtr(),
                                                                 lowDimGridGeometry),
                                                 std::make_tuple(bulkGridVariables->faceGridVariablesPtr(),
                                                                 bulkGridVariables->cellCenterGridVariablesPtr(),
                                                                 lowDimGridVariables),
                                                 couplingManager);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);

    using GridGeometry = GetPropType<BulkTypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition =  typename Element::Geometry::GlobalCoordinate;

    nonLinearSolver.solve(sol);

    // write vtk output
    bulkVtkWriter.write([&](GlobalPosition pos, Scalar min, Scalar max){return false;}, 0.0);
    lowDimVtkWriter.write(0.0);

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
