// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \brief A test problem for the staggered (Navier-) Stokes model.
 */

#ifndef DUMUX_CLOSEDSYSTEM_TEST_PROBLEM_HH
#define DUMUX_CLOSEDSYSTEM_TEST_PROBLEM_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/common/numeqvector.hh>

#include <dumux/discretization/staggered/freeflow/properties.hh>

#include <dumux/freeflow/navierstokes/boundarytypes.hh>
#include <dumux/freeflow/navierstokes/model.hh>
#include <dumux/freeflow/navierstokes/simpleproblem.hh>

#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/assembly/simpleassemblystructs.hh>
#include <dumux/assembly/simplestaggeredlocalresidual.hh>

#include <dumux/discretization/staggered/freeflow/simplefvgridgeometrytraits.hh>
#include <dumux/discretization/staggered/simplefvgridgeometry.hh>
#include <dumux/freeflow/navierstokes/simplelocalresidual.hh>
#include <dumux/freeflow/navierstokes/simplefluxvariables.hh>

namespace Dumux {
template <class TypeTag>
class ClosedSystemTestProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct ClosedSystemTest { using InheritsFrom = std::tuple<NavierStokes, StaggeredFreeFlowModel>; };
} // end namespace TTag

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::ClosedSystemTest>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar> >;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::ClosedSystemTest> { using type = Dune::YaspGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::ClosedSystemTest> { using type = Dumux::ClosedSystemTestProblem<TypeTag> ; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::ClosedSystemTest> { static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::ClosedSystemTest> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::ClosedSystemTest> { static constexpr bool value = true; };

//! Set the SimpleMassBalanceSummands
template<class TypeTag>
struct SimpleMassBalanceSummands<TypeTag, TTag::ClosedSystemTest> { using type = MySimpleMassBalanceSummands<GetPropType<TypeTag, Properties::CellCenterPrimaryVariables>, TypeTag>; };

//! Set the SimpleMomentumBalanceSummands
template<class TypeTag>
struct SimpleMomentumBalanceSummands<TypeTag, TTag::ClosedSystemTest> { using type = MySimpleMomentumBalanceSummands<GetPropType<TypeTag, Properties::FacePrimaryVariables>, TypeTag>; };

template<class TypeTag>
struct SimpleMomentumBalanceSummandsVector<TypeTag, TTag::ClosedSystemTest> { using type = std::vector<MySimpleMomentumBalanceSummands<GetPropType<TypeTag, Properties::FacePrimaryVariables>, TypeTag>>; };

//! The local residual
template<class TypeTag>
struct LocalResidual<TypeTag, TTag::ClosedSystemTest> { using type = SimpleNavierStokesResidual<TypeTag>; };

//! The flux variables
template<class TypeTag>
struct FluxVariables<TypeTag, TTag::ClosedSystemTest> { using type = SimpleNavierStokesFluxVariables<TypeTag>; };

//! Set the BaseLocalResidual to StaggeredLocalResidual
template<class TypeTag>
struct BaseLocalResidual<TypeTag, TTag::ClosedSystemTest> { using type = SimpleStaggeredLocalResidual<TypeTag>; };

//! The default grid geometry
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::ClosedSystemTest>
{
private:
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = typename GetPropType<TypeTag, Properties::Grid>::LeafGridView;
    using Traits = SimpleStaggeredFreeFlowDefaultFVGridGeometryTraits<GridView, upwindSchemeOrder>;
public:
    using type = SimpleStaggeredFVGridGeometry<GridView, enableCache, Traits>;
};
} // end namespace Properties

/*!
 * \ingroup NavierStokesTests
 * \brief Test problem for the one-phase (Navier-) Stokes model.
 *
 * Here, a quadratic two-dimensional domain with closed walls at all sides is considered.
 * If all walls are immobile and gravity is switched on, a hydrostatic pressure
 * gradient will develop. When assigning a fixed velocity to the top wall (without gravity),
 * this test corresponds to a lid-driven cavity problem.
 */
template <class TypeTag>
class ClosedSystemTestProblem : public SimpleNavierStokesProblem<TypeTag>
{
    using ParentType = SimpleNavierStokesProblem<TypeTag>;

    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using NumEqVector = Dumux::NumEqVector<GetPropType<TypeTag, Properties::PrimaryVariables>>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    static constexpr auto dimWorld = GridGeometry::GridView::dimensionworld;
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    ClosedSystemTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        lidVelocity_ = getParam<Scalar>("Problem.LidVelocity");
    }

   /*!
     * \name Problem parameters
     */
    // \{

   /*!
     * \brief Returns the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10C

    // \}
   /*!
     * \name Boundary conditions
     */
    // \{

   /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        // set Dirichlet values for the velocity everywhere
        values.setDirichlet(Indices::velocityXIdx);
        values.setDirichlet(Indices::velocityYIdx);

        return values;
    }

    /*!
     * \brief Returns whether a fixed Dirichlet value shall be used at a given cell.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param scv The sub control volume
     * \param pvIdx The primary variable index in the solution vector
     */
    bool isDirichletCell(const Element& element,
                         const FVElementGeometry& fvGeometry,
                         const SubControlVolume& scv,
                         int pvIdx) const
    {
        auto isLowerLeftCell = [&](const SubControlVolume& scv)
        { return scv.dofIndex() == 0; };

        // set a fixed pressure in one cell
        return (isLowerLeftCell(scv) && pvIdx == Indices::pressureIdx);
    }

   /*!
     * \brief Returns Dirichlet boundary values at a given position.
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        values[Indices::pressureIdx] = 1.1e+5;
        values[Indices::velocityXIdx] = 0.0;
        values[Indices::velocityYIdx] = 0.0;

        if(globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_)
            values[Indices::velocityXIdx] = lidVelocity_;

        return values;
    }

   /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        values[Indices::pressureIdx] = 1.0e+5;
        values[Indices::velocityXIdx] = 0.0;
        values[Indices::velocityYIdx] = 0.0;

        return values;
    }

    // \}

private:

    static constexpr Scalar eps_=1e-6;
    Scalar lidVelocity_;
};
} // end namespace Dumux

#endif
