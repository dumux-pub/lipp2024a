// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \brief Test for the staggered grid Navier-Stokes model with analytical solution (Kovasznay 1948, \cite Kovasznay1948)
 */

#ifndef DUMUX_KOVASZNAY_TEST_PROBLEM_HH
#define DUMUX_KOVASZNAY_TEST_PROBLEM_HH

#ifndef UPWINDSCHEMEORDER
#define UPWINDSCHEMEORDER 0
#endif

#include <dune/grid/yaspgrid.hh>

#include <dumux/common/numeqvector.hh>

#include <dumux/discretization/staggered/freeflow/properties.hh>

#include <dumux/freeflow/navierstokes/boundarytypes.hh>
#include <dumux/freeflow/navierstokes/model.hh>
#include <dumux/freeflow/navierstokes/simpleproblem.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>

#include <dumux/assembly/simpleassemblystructs.hh>
#include <dumux/assembly/simplestaggeredlocalresidual.hh>

#include <dumux/discretization/staggered/freeflow/simplefvgridgeometrytraits.hh>
#include <dumux/discretization/staggered/simplefvgridgeometry.hh>
#include <dumux/freeflow/navierstokes/simplelocalresidual.hh>
#include <dumux/freeflow/navierstokes/simplefluxvariables.hh>

namespace Dumux {
template <class TypeTag>
class KovasznayTestProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct KovasznayTest { using InheritsFrom = std::tuple<NavierStokes, StaggeredFreeFlowModel>; };
} // end namespace TTag

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::KovasznayTest>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar> >;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::KovasznayTest> { using type = Dune::YaspGrid<2, Dune::EquidistantOffsetCoordinates<GetPropType<TypeTag, Properties::Scalar>, 2> >; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::KovasznayTest> { using type = Dumux::KovasznayTestProblem<TypeTag> ; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::KovasznayTest> { static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::KovasznayTest> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::KovasznayTest> { static constexpr bool value = true; };

template<class TypeTag>
struct UpwindSchemeOrder<TypeTag, TTag::KovasznayTest> { static constexpr int value = UPWINDSCHEMEORDER; };

//! Set the SimpleMassBalanceSummands
template<class TypeTag>
struct SimpleMassBalanceSummands<TypeTag, TTag::KovasznayTest> { using type = MySimpleMassBalanceSummands<GetPropType<TypeTag, Properties::CellCenterPrimaryVariables>, TypeTag>; };

//! Set the SimpleMomentumBalanceSummands
template<class TypeTag>
struct SimpleMomentumBalanceSummands<TypeTag, TTag::KovasznayTest> { using type = MySimpleMomentumBalanceSummands<GetPropType<TypeTag, Properties::FacePrimaryVariables>, TypeTag>; };

template<class TypeTag>
struct SimpleMomentumBalanceSummandsVector<TypeTag, TTag::KovasznayTest> { using type = std::vector<MySimpleMomentumBalanceSummands<GetPropType<TypeTag, Properties::FacePrimaryVariables>, TypeTag>>; };

//! The local residual
template<class TypeTag>
struct LocalResidual<TypeTag, TTag::KovasznayTest> { using type = SimpleNavierStokesResidual<TypeTag>; };

//! The flux variables
template<class TypeTag>
struct FluxVariables<TypeTag, TTag::KovasznayTest> { using type = SimpleNavierStokesFluxVariables<TypeTag>; };

//! Set the BaseLocalResidual to StaggeredLocalResidual
template<class TypeTag>
struct BaseLocalResidual<TypeTag, TTag::KovasznayTest> { using type = SimpleStaggeredLocalResidual<TypeTag>; };

//! The default grid geometry
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::KovasznayTest>
{
private:
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = typename GetPropType<TypeTag, Properties::Grid>::LeafGridView;
    using Traits = SimpleStaggeredFreeFlowDefaultFVGridGeometryTraits<GridView, upwindSchemeOrder>;
public:
    using type = SimpleStaggeredFVGridGeometry<GridView, enableCache, Traits>;
};
} // end namespace Properties

/*!
 * \ingroup NavierStokesTests
 * \brief  Test problem for the staggered grid (Kovasznay 1948, \cite Kovasznay1948)
 *
 * A two-dimensional Navier-Stokes flow with a periodicity in one direction
 * is considered. The set-up represents a wake behind a two-dimensional grid
 * and is chosen in a way such that an exact solution is available.
 */
template <class TypeTag>
class KovasznayTestProblem : public SimpleNavierStokesProblem<TypeTag>
{
    using ParentType = SimpleNavierStokesProblem<TypeTag>;

    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GetPropType<TypeTag, Properties::GridGeometry>::SubControlVolumeFace;
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using NumEqVector = Dumux::NumEqVector<GetPropType<TypeTag, Properties::PrimaryVariables>>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;

    static constexpr auto dimWorld = GridGeometry::GridView::dimensionworld;
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using VelocityVector = Dune::FieldVector<Scalar, dimWorld>;

    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();

public:
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    KovasznayTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        std::cout<< "upwindSchemeOrder is: " << GridGeometry::upwindStencilOrder() << "\n";
        rho_ = getParam<Scalar>("Component.LiquidDensity", 1.0);
        kinematicViscosity_ = getParam<Scalar>("Component.LiquidKinematicViscosity", 1.0);
        Scalar reynoldsNumber = 1.0 / kinematicViscosity_;
        lambda_ = 0.5 * reynoldsNumber
                        - std::sqrt(reynoldsNumber * reynoldsNumber * 0.25 + 4.0 * M_PI * M_PI);

        using CellArray = std::array<unsigned int, dimWorld>;
        const auto numCells = getParam<CellArray>("Grid.Cells");

        cellSizeX_ = (this->gridGeometry().bBoxMax()[0] - this->gridGeometry().bBoxMin()[0]) / numCells[0];
        cellSizeY_ = (this->gridGeometry().bBoxMax()[1] - this->gridGeometry().bBoxMin()[1]) / numCells[1];

        ParentType::createAnalyticalSolution();
    }

   /*!
     * \name Problem parameters
     */
    // \{

   /*!
     * \brief Returns the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 298.0; }

    // \}
   /*!
     * \name Boundary conditions
     */
    // \{

   /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        // set Dirichlet values for the velocity everywhere
        values.setDirichlet(Indices::velocityXIdx);
        values.setDirichlet(Indices::velocityYIdx);

        return values;
    }

    /*!
     * \brief Returns whether a fixed Dirichlet value shall be used at a given cell.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param scv The sub control volume
     * \param pvIdx The primary variable index in the solution vector
     */
    bool isDirichletCell(const Element& element,
                         const FVElementGeometry& fvGeometry,
                         const SubControlVolume& scv,
                         int pvIdx) const
    {
        // set fixed pressure in all cells at the left boundary
        auto isAtLeftBoundary = [&](const FVElementGeometry& fvGeometry)
        {
            if (fvGeometry.hasBoundaryScvf())
            {
                for (const auto& scvf : scvfs(fvGeometry))
                    if (scvf.boundary() && scvf.center()[0] < this->gridGeometry().bBoxMin()[0] + eps_)
                        return true;
            }
            return false;
        };
        return (isAtLeftBoundary(fvGeometry) && pvIdx == Indices::pressureIdx);
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume face (velocities)
     *
     * \param element The finite element
     * \param scvf the sub control volume face
     *
     * Concerning the usage of averaged velocity, see the explanation of the initial function. The average of the nondirection velocity along an scvf is required in the context of dirichlet boundary conditions for the functions getParallelVelocityFromBoundary_ and getParallelVelocityFromOtherBoundary_ within myfluxvariables.hh. There dirichlet is called for ghost faces built from the normalFace but the value then is taken in the the direction scvf.directionIndex, which is along that normalFace.
      */
    PrimaryVariables dirichlet(const Element& element, const SubControlVolumeFace& scvf) const
    {
        PrimaryVariables priVars(0.0);

        unsigned int dirIdx = scvf.directionIndex();
        priVars[Indices::velocity(dirIdx)] = this->analyticalVelocitySolutionAtPos(scvf);
        unsigned int nonDirIdx = (dirIdx == 0)?1:0;
        priVars[Indices::velocity(nonDirIdx)] = this->analyticalNonDirVelocitySolutionAtPos(scvf);

        return priVars;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume face (pressure)
     *
     * \param element The finite element
     * \param scv the sub control volume
     */
    PrimaryVariables dirichlet(const Element& element, const SubControlVolume& scv) const
    {
        PrimaryVariables priVars(0.0);
        priVars[Indices::pressureIdx] = this->analyticalPressureSolutionAtPos(scv);
        return priVars;
    }

   /*!
     * \brief Returns the analytical solution of the problem at a given position.
     *
     * \param globalPos The global position
     */
    PrimaryVariables analyticalSolutionAtPos(const GlobalPosition& globalPos) const
    {
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        PrimaryVariables values;
        values[Indices::pressureIdx] = rho_ * 0.5 * (1.0 - std::exp(2.0 * lambda_ * x));
        values[Indices::velocityXIdx] = 1.0 - std::exp(lambda_ * x) * std::cos(2.0 * M_PI * y);
        values[Indices::velocityYIdx] = 0.5 * lambda_ / M_PI * std::exp(lambda_ * x) * std::sin(2.0 * M_PI * y);

        return values;
    }

    // \}

   /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the initial value for a sub control volume face (velocities)
     *
     * Simply assigning the value of the analytical solution at the face center
     * gives a discrete solution that is not divergence-free. For small initial
     * time steps, this has a negative impact on the pressure solution
     * after the first time step. The flag UseVelocityAveraging triggers the
     * function averagedVelocity_ which uses a higher order quadrature formula to
     * bring the discrete solution sufficiently close to being divergence-free.
     */
    PrimaryVariables initial(const SubControlVolumeFace& scvf) const
    {
        const auto& globalPos = scvf.dofPosition();

        PrimaryVariables priVars(0.0);

        if (globalPos[0] < (this->gridGeometry().bBoxMin()[0] + eps_) ||
            globalPos[0] > (this->gridGeometry().bBoxMax()[0] - eps_) ||
            globalPos[1] < (this->gridGeometry().bBoxMin()[1] + eps_) ||
            globalPos[1] > (this->gridGeometry().bBoxMax()[1] - eps_))
        {
            priVars[Indices::velocityXIdx] = analyticalSolutionAtPos(globalPos)[Indices::velocityXIdx];
            priVars[Indices::velocityYIdx] = analyticalSolutionAtPos(globalPos)[Indices::velocityYIdx];
        }

        return priVars;
    }

    /*!
     * \brief Evaluates the initial value for a control volume (pressure)
     */
    PrimaryVariables initial(const SubControlVolume& scv) const
    {
        PrimaryVariables priVars(0.0);

        const auto& globalPos = scv.dofPosition();

        if (isLowerLeftCell_(globalPos))
            priVars[Indices::pressureIdx] = analyticalSolutionAtPos(globalPos)[Indices::pressureIdx];

        return priVars;
    }

private:
    bool isLowerLeftCell_(const GlobalPosition& globalPos) const
    {
        return globalPos[0] < (this->gridGeometry().bBoxMin()[0] + 0.5*cellSizeX_ + eps_);
    }

    static constexpr Scalar eps_=1e-6;

    Scalar rho_;
    Scalar kinematicViscosity_;
    Scalar lambda_;

    Scalar cellSizeX_;
    Scalar cellSizeY_;
};
} // end namespace Dumux

#endif
