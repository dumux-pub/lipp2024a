#!/bin/bash
mkdir -p datafiles
touch datafiles/fileproduction.log
bash ./produce_datafiles.sh > datafiles/fileproduction.log

cd datafiles
bash ../sort_datafiles.sh
cd ..

bash ./produce_texfiles.sh
bash ./copy_results.sh
