#include "setBoundaryMomentumStencils.hh"

template <int numVelocityDofs, int momentumOffset>
auto setMomentumStencils()
{
    std::array<MomentumStencil, numVelocityDofs> stencils;

    setBoundaryMomentumStencils<numVelocityDofs,momentumOffset>(stencils);

    double standardInterp = 1.;
    static const double standardDeltaX = Dumux::getParam<double>("Grid.StandardDeltaX");
    static const double standardDeltaY = Dumux::getParam<double>("Grid.StandardDeltaY");

///////////////////
    /*x*/
//////////////////
    double standardDeltaOppo = standardDeltaX;
    double standardDeltaScvf = standardDeltaY;

///////////////////
    /*scvfs in inner*/
///////////////////
    int i = 35;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({35,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({34,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({5,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({36,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({16,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({29,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({65,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({66,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({41,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({70,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({71,standardInterp});

///////////////////
    i = 36;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({36,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({35,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({16,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({37,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({20,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({30,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({66,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({67,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({42,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({71,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({72,standardInterp});

///////////////////
    i = 37;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({37,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({36,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({20,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({38,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({17,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({31,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({67,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({68,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({45,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({72,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({76,standardInterp});

///////////////////
    i = 38;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({38,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({37,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({17,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({39,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({6,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({32,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({68,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({69,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({46,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({76,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({77,standardInterp});

 ///////////////////
    i = 41;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({41,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({40,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({7,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({42,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({21,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({35,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({70,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({71,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({49,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({78,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({79,standardInterp});

///////////////////
    i = 43;
    stencils[i-momentumOffset].setScvfDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({43,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({42,1.},{36,0.125},{50,-0.125});//case d
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({24,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({45,1.},{37,0.125},{51,-0.125});//case d
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({25,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({36,9./16},{37,9./16},{35,-1./16},{38,-1./16});//case c
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({72,1.},{71,0.125},{76,-0.125});//case g
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({72,1.},{76,0.125},{71,-0.125});//case g

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({44,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({73,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({74,standardInterp});

///////////////////
    i = 42;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({42,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({41,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({21,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({43,16./30},{44,16./30},{36,-1./60},{37,-1./60},{50,-1./60},{51,-1./60});//case e
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({24,.5},{26,.5});//case b

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({36,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({71,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({72,1.},{71,0.125},{76,-0.125});//case f

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({50,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({79,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({75,1.},{79,0.125},{80,-0.125});//case f

///////////////////
    i = 44;
    stencils[i-momentumOffset].setScvfDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({44,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({42,1.},{50,0.125},{36,-0.125});//case d
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({26,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({45,1.},{51,0.125},{37,-0.125});//case d
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({27,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({43,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({73,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({74,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({50,9./16},{51,9./16},{49,-1./16},{52,-1./16});//case c
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({75,1.},{79,.125},{80,-0.125});//case g
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({75,1.},{80,.125},{79,-0.125});//case g

///////////////////
    i = 45;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({45,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({43,16./30},{44,16./30},{36,-1./60},{37,-1./60},{50,-1./60},{51,-1./60});//case e
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({25,.5},{27,.5});//case b

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({46,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({22,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({37,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({72,1.},{76,.125},{71,-0.125});//case f
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({76,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({51,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({75,1.},{80,.125},{79,-0.125});//case f
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({80,standardInterp});

///////////////////
    i = 46;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({46,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({45,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({22,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({47,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({8,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({38,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({76,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({77,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({52,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({80,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({81,standardInterp});

///////////////////
    i = 49;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({49,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({48,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({9,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({50,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({18,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({41,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({78,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({79,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({55,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({82,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({83,standardInterp});

///////////////////
    i = 50;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({50,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({49,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({18,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({51,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({23,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({42,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({79,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({75,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({56,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({83,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({84,standardInterp});

///////////////////
    i = 51;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({51,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({50,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({23,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({52,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({19,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({45,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({75,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({80,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({57,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({84,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({85,standardInterp});

///////////////////
    i = 52;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({52,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({51,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({19,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({53,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({10,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({46,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({80,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({81,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({58,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({85,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({86,standardInterp});

///////////////////
    /*y*/
//////////////////
    standardDeltaOppo = standardDeltaY;
    standardDeltaScvf = standardDeltaX;

///////////////////
    /*scvfs in inner*/
///////////////////
    i = 66;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({66,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({61,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({1,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({71,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({16,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({65,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({29,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({35,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({67,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({30,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({36,standardInterp});

///////////////////
    i = 67;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({67,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({62,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({2,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({72,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({20,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({66,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({30,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({36,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({68,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({31,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({37,standardInterp});

///////////////////
    i = 68;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({68,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({63,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({3,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({76,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({17,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({67,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({31,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({37,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({69,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({32,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({38,standardInterp});

///////////////////
    i = 71;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({71,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({66,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({16,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({79,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({21,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({70,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({35,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({41,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({72,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({36,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({42,standardInterp});

///////////////////
    i = 76;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({76,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({68,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({17,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({80,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({22,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({72,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({37,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({45,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({77,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({38,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({46,standardInterp});

///////////////////
    i = 79;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({79,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({71,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({21,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({83,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({18,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({78,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({41,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({49,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({75,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({42,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({50,standardInterp});

///////////////////
    i = 80;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({80,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({76,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({22,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({85,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({19,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({75,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({45,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({51,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({81,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({46,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({52,standardInterp});

///////////////////
    i = 83;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({83,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({79,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({18,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({88,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({12,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({82,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({49,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({55,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({84,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({50,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({56,standardInterp});

///////////////////
    i = 84;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({84,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({75,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({23,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({89,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({13,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({83,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({50,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({56,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({85,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({51,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({57,standardInterp});

///////////////////
    i = 85;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({85,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({80,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({19,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({90,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({14,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({84,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({51,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({57,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({86,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({52,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({58,standardInterp});

///////////////////
    i = 72;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({72,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({67,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({20,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({73,16./30},{74,16./30},{71,-1./60},{79,-1./60},{76,-1./60},{80,-1./60});//case e
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({24,.5},{25,.5});//case b

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({71,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({36,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({42,1.},{36,.125},{50,-0.125});//case f

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({76,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({37,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({45,1.},{37,.125},{51,-0.125});//case f

///////////////////
    i = 73;
    stencils[i-momentumOffset].setScvfDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({73,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({72,1.},{71,.125},{76,-0.125});//case d
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({24,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({75,1.},{79,.125},{80,-0.125});//case d
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({26,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({71,9./16},{79,9./16},{66,-1./16},{83,-1./16});//case c
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({42,1.},{36,.125},{50,-0.125});//case g
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({42,1.},{50,.125},{36,-0.125});//case g

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({74,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({43,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({44,standardInterp});

///////////////////
    i = 74;
    stencils[i-momentumOffset].setScvfDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({74,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({72,1.},{76,.125},{71,-0.125});//case d
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({25,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({75,1.},{80,.125},{79,-0.125});//case d
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({27,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({73,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({43,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({44,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({76,9./16},{80,9./16},{68,-1./16},{85,-1./16});//case c
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({45,1.},{37,.125},{51,-0.125});//case g
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({45,1.},{51,.125},{37,-0.125});//case g

///////////////////
    i = 75;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({75,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({73,16./30},{74,16./30},{71,-1./60},{79,-1./60},{76,-1./60},{80,-1./60});//case e
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({26,.5},{27,.5});//case b

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({84,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({23,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({79,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({42,1.},{50,0.125},{36,-0.125});//case f
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({50,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({80,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({45,1.},{51,0.125},{37,-0.125});//case f
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({51,standardInterp});

    return stencils;
}
