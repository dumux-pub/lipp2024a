struct OppoPair
{
    double delta = 0.;
    std::vector<std::pair<int,double>> velocityBuilder;
    std::vector<std::pair<int,double>> pressureBuilder;

    using Pair = typename std::pair<int,double>;

    void setVelocity (const Pair& pair1 = {-2,0},
                      const Pair& pair2 = {-2,0},
                      const Pair& pair3 = {-2,0},
                      const Pair& pair4 = {-2,0},
                      const Pair& pair5 = {-2,0},
                      const Pair& pair6 = {-2,0},
                      const Pair& pair7 = {-2,0},
                      const Pair& pair8 = {-2,0})
    {
        set(pair1, pair2, pair3, pair4, pair5, pair6, pair7, pair8, velocityBuilder);
    }

    void setPressure (const Pair& pair1 = {-2,0},
                      const Pair& pair2 = {-2,0},
                      const Pair& pair3 = {-2,0},
                      const Pair& pair4 = {-2,0},
                      const Pair& pair5 = {-2,0},
                      const Pair& pair6 = {-2,0},
                      const Pair& pair7 = {-2,0},
                      const Pair& pair8 = {-2,0})
    {
        set(pair1, pair2, pair3, pair4, pair5, pair6, pair7, pair8, pressureBuilder);
    }

    void setDelta (double deltaStuff)
    {
        delta = deltaStuff;
    }
};

struct LateralPair
{
    double delta = 0.;
    double boundaryParallelVelocity = 0.;

    std::vector<std::pair<int,double>> parallelVelocityBuilder;
    std::vector<std::pair<int,double>> rightOrUpVelocityBuilder;
    std::vector<std::pair<int,double>> leftOrDownVelocityBuilder;

    using Pair = typename std::pair<int,double>;

    void setParallelVelocity (const Pair& pair1 = {-2,0},
                      const Pair& pair2 = {-2,0},
                      const Pair& pair3 = {-2,0},
                      const Pair& pair4 = {-2,0},
                      const Pair& pair5 = {-2,0},
                      const Pair& pair6 = {-2,0},
                      const Pair& pair7 = {-2,0},
                      const Pair& pair8 = {-2,0})
    {
        set(pair1, pair2, pair3, pair4, pair5, pair6, pair7, pair8, parallelVelocityBuilder);
    }

    void setRightOrUpVelocity (const Pair& pair1 = {-2,0},
                      const Pair& pair2 = {-2,0},
                      const Pair& pair3 = {-2,0},
                      const Pair& pair4 = {-2,0},
                      const Pair& pair5 = {-2,0},
                      const Pair& pair6 = {-2,0},
                      const Pair& pair7 = {-2,0},
                      const Pair& pair8 = {-2,0})
    {
        set(pair1, pair2, pair3, pair4, pair5, pair6, pair7, pair8, rightOrUpVelocityBuilder);
    }

    void setLeftOrDownVelocity (const Pair& pair1 = {-2,0},
                      const Pair& pair2 = {-2,0},
                      const Pair& pair3 = {-2,0},
                      const Pair& pair4 = {-2,0},
                      const Pair& pair5 = {-2,0},
                      const Pair& pair6 = {-2,0},
                      const Pair& pair7 = {-2,0},
                      const Pair& pair8 = {-2,0})
    {
        set(pair1, pair2, pair3, pair4, pair5, pair6, pair7, pair8, leftOrDownVelocityBuilder);
    }

    void setDelta (double deltaStuff)
    {
        delta = deltaStuff;
    }

    void setParallelBoundaryVelocity (double val)
    {
        boundaryParallelVelocity = val;
    }
};

struct MomentumStencil
{
    using Pair = typename std::pair<int,double>;

    OppoPair leftOrDownOppoPair;
    OppoPair rightOrUpOppoPair;

    LateralPair leftOrDownLateralPair;
    LateralPair rightOrUpLateralPair;

    double scvfDelta = 0.;
    double boundarySelfVelocity = 0.;

    void setScvfDelta (double deltaStuff)
    {
        scvfDelta = deltaStuff;
    }

    void setBoundarySelfVelocity (double val)
    {
        boundarySelfVelocity = val;
    }

    std::vector<std::pair<int /*dof*/,double /*interpolation Factor*/>> midVelocityBuilder;

    void setMidVelocity (const Pair& pair1 = {-2,0},
                      const Pair& pair2 = {-2,0},
                      const Pair& pair3 = {-2,0},
                      const Pair& pair4 = {-2,0},
                      const Pair& pair5 = {-2,0},
                      const Pair& pair6 = {-2,0},
                      const Pair& pair7 = {-2,0},
                      const Pair& pair8 = {-2,0})
    {
        set(pair1, pair2, pair3, pair4, pair5, pair6, pair7, pair8, midVelocityBuilder);
    }
};

double leftNormal (double y);
double rightNormal (double y);
double upNormal (double x);
double downNormal (double x);

double leftTangential (double y);
double rightTangential (double y);
double upTangential (double x);
double downTangential (double x);

template <int numVelocityDofs, int momentumOffset>
auto setBoundaryMomentumStencils(std::array<MomentumStencil, numVelocityDofs>& stencils)
{
    double standardInterp = 1.;
    static const double standardDeltaX = Dumux::getParam<double>("Grid.StandardDeltaX");
    static const double standardDeltaY = Dumux::getParam<double>("Grid.StandardDeltaY");

///////////////////
    /*x*/
//////////////////
    double standardDeltaOppo = standardDeltaX;
    double standardDeltaScvf = standardDeltaY;

///////////////////
    /*boundary scvfs*/
///////////////////
    int i = 28;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(leftNormal(4.5*standardDeltaScvf));

///////////////////
    i = 34;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(leftNormal(3.5*standardDeltaScvf));

///////////////////
    i = 40;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(leftNormal(2.5*standardDeltaScvf));

///////////////////
    i = 48;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(leftNormal(1.5*standardDeltaScvf));

///////////////////
    i = 54;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(leftNormal(0.5*standardDeltaScvf));

///////////////////
    i = 33;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(rightNormal(4.5*standardDeltaScvf));

///////////////////
    i = 39;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(rightNormal(3.5*standardDeltaScvf));

///////////////////
    i = 47;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(rightNormal(2.5*standardDeltaScvf));

///////////////////
    i = 53;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(rightNormal(1.5*standardDeltaScvf));

///////////////////
    i = 59;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(rightNormal(0.5*standardDeltaScvf));

///////////////////
    /*scvfs with parallel at boundary*/
///////////////////
    i = 29;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({29,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({28,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({0,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({30,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({1,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({35,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({66,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({65,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(0.);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity();//boundary
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelBoundaryVelocity(upTangential(1*standardDeltaOppo));
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({61,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({60,standardInterp});

///////////////////
    i = 30;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({30,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({29,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({1,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({31,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({2,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({36,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({67,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({66,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(0.);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity();//boundary
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelBoundaryVelocity(upTangential(2*standardDeltaOppo));
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({62,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({61,standardInterp});

///////////////////
    i = 31;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({31,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({30,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({2,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({32,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({3,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(0.);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity();//boundary
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelBoundaryVelocity(upTangential(3*standardDeltaOppo));
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({62,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({63,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({37,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({67,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({68,standardInterp});

///////////////////
    i = 32;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({32,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({31,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({3,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({33,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({4,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(0.);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity();//boundary
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelBoundaryVelocity(upTangential(4*standardDeltaOppo));
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({63,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({64,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({38,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({68,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({69,standardInterp});

///////////////////
    i = 55;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({55,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({54,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({11,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({56,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({12,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({49,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({82,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({83,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(0.);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity();//boundary
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelBoundaryVelocity(downTangential(1*standardDeltaOppo));
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({87,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({88,standardInterp});

///////////////////
    i = 56;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({56,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({55,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({12,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({57,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({13,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({50,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({83,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({84,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(0.);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity();//boundary
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelBoundaryVelocity(downTangential(2*standardDeltaOppo));
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({88,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({89,standardInterp});

///////////////////
    i = 57;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({57,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({56,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({13,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({58,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({14,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({51,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({84,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({85,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(0.);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity();//boundary
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelBoundaryVelocity(downTangential(3*standardDeltaOppo));
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({89,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({90,standardInterp});

///////////////////
    i = 58;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({58,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({57,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({14,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({59,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({15,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({52,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({85,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({86,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(0.);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity();//boundary
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelBoundaryVelocity(downTangential(4*standardDeltaOppo));
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({90,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({91,standardInterp});

///////////////////
    /*y*/
//////////////////
    standardDeltaOppo = standardDeltaY;
    standardDeltaScvf = standardDeltaX;

    ///////////////////
    i = 60;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(upNormal(0.5*standardDeltaScvf));

///////////////////
    i = 61;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(upNormal(1.5*standardDeltaScvf));

///////////////////
    i = 62;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(upNormal(2.5*standardDeltaScvf));

///////////////////
    i = 63;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(upNormal(3.5*standardDeltaScvf));

///////////////////
    i = 64;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(upNormal(4.5*standardDeltaScvf));

///////////////////
    i = 87;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(downNormal(0.5*standardDeltaScvf));

///////////////////
    i = 88;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(downNormal(1.5*standardDeltaScvf));

///////////////////
    i = 89;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(downNormal(2.5*standardDeltaScvf));

///////////////////
    i = 90;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(downNormal(3.5*standardDeltaScvf));

///////////////////
    i = 91;
    stencils[i-momentumOffset].setMidVelocity();
    stencils[i-momentumOffset].setBoundarySelfVelocity(downNormal(4.5*standardDeltaScvf));

///////////////////
    /*scvfs with parallel at boundary*/
///////////////////
    i = 65;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({65,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({60,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({0,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({70,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({5,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(0.);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity();//boundary
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelBoundaryVelocity(leftTangential(4*standardDeltaOppo));
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({28,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({34,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({66,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({29,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({35,standardInterp});

///////////////////
    i = 70;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({70,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({65,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({5,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({78,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({7,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(0.);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity();//boundary
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelBoundaryVelocity(leftTangential(3*standardDeltaOppo));
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({34,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({40,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({71,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({35,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({41,standardInterp});

///////////////////
    i = 78;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({78,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({70,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({7,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({82,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({9,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(0.);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity();//boundary
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelBoundaryVelocity(leftTangential(2*standardDeltaOppo));
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({40,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({48,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({79,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({41,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({49,standardInterp});

///////////////////
    i = 82;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({82,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({78,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({9,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({87,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({11,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(0.);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity();//boundary
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelBoundaryVelocity(leftTangential(1*standardDeltaOppo));
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({48,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({54,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({83,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({49,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({55,standardInterp});

///////////////////
    i = 69;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({69,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({64,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({4,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({77,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({6,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({68,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({32,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({38,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(0.);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity();//boundary
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelBoundaryVelocity(leftTangential(4*standardDeltaOppo));
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({33,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({39,standardInterp});

///////////////////
    i = 77;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({77,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({69,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({6,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({81,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({8,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({76,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({38,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({46,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(0.);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity();//boundary
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelBoundaryVelocity(leftTangential(3*standardDeltaOppo));
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({39,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({47,standardInterp});

///////////////////
    i = 81;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({81,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({77,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({8,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({86,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({10,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({80,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({46,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({52,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(0.);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity();//boundary
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelBoundaryVelocity(leftTangential(2*standardDeltaOppo));
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({47,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({53,standardInterp});

///////////////////
    i = 86;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({86,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({81,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({10,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({91,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({15,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({85,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({52,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({58,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(0.);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity();//boundary
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelBoundaryVelocity(leftTangential(1*standardDeltaOppo));
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({53,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({59,standardInterp});
}
