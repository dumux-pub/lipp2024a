struct ContinuityStencil
{
    double deltax;
    double deltay;

    std::vector<std::pair<int /*dof*/,double /*interpolation Factor*/>> vuBuilder;
    std::vector<std::pair<int,double>> vdBuilder;

    std::vector<std::pair<int,double>> ulBuilder;
    std::vector<std::pair<int,double>> urBuilder;

    using Pair = typename std::pair<int,double>;

    void setVu (const Pair& pair1= {-2,0},
                      const Pair& pair2 = {-2,0},
                      const Pair& pair3 = {-2,0},
                      const Pair& pair4 = {-2,0},
                      const Pair& pair5 = {-2,0},
                      const Pair& pair6 = {-2,0},
                      const Pair& pair7 = {-2,0},
                      const Pair& pair8 = {-2,0})
    {
        set(pair1, pair2, pair3, pair4, pair5, pair6, pair7, pair8, vuBuilder);
    }

    void setVd (const Pair& pair1 = {-2,0},
                      const Pair& pair2 = {-2,0},
                      const Pair& pair3 = {-2,0},
                      const Pair& pair4 = {-2,0},
                      const Pair& pair5 = {-2,0},
                      const Pair& pair6 = {-2,0},
                      const Pair& pair7 = {-2,0},
                      const Pair& pair8 = {-2,0})
    {
        set(pair1, pair2, pair3, pair4, pair5, pair6, pair7, pair8, vdBuilder);
    }

    void setUl (const Pair& pair1= {-2,0},
                      const Pair& pair2 = {-2,0},
                      const Pair& pair3 = {-2,0},
                      const Pair& pair4 = {-2,0},
                      const Pair& pair5 = {-2,0},
                      const Pair& pair6 = {-2,0},
                      const Pair& pair7 = {-2,0},
                      const Pair& pair8 = {-2,0})
    {
        set(pair1, pair2, pair3, pair4, pair5, pair6, pair7, pair8, ulBuilder);
    }

    void setUr (const Pair& pair1= {-2,0},
                      const Pair& pair2 = {-2,0},
                      const Pair& pair3 = {-2,0},
                      const Pair& pair4 = {-2,0},
                      const Pair& pair5 = {-2,0},
                      const Pair& pair6 = {-2,0},
                      const Pair& pair7 = {-2,0},
                      const Pair& pair8 = {-2,0})
    {
        set(pair1, pair2, pair3, pair4, pair5, pair6, pair7, pair8, urBuilder);
    }

    void setDeltaX (double deltaStuff)
    {
        deltax = deltaStuff;
    }

    void setDeltaY (double deltaStuff)
    {
        deltay = deltaStuff;
    }
};

template <int numPDofs>
auto setContinuityStencils()
{
    double standardInterp = 1.;
    static const double standardDeltaX = Dumux::getParam<double>("Grid.StandardDeltaX");
    static const double standardDeltaY = Dumux::getParam<double>("Grid.StandardDeltaY");

    std::array<ContinuityStencil, numPDofs> contiStencils;

///////////////////
    /*Boundary*/
///////////////////
    int i = 0;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({60,standardInterp});
    contiStencils[i].setVd({65,standardInterp});
    contiStencils[i].setUl({28,standardInterp});
    contiStencils[i].setUr({29,standardInterp});

///////////////////
    i = 1;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({61,standardInterp});
    contiStencils[i].setVd({66,standardInterp});
    contiStencils[i].setUl({29,standardInterp});
    contiStencils[i].setUr({30,standardInterp});

///////////////////
    i = 2;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({62,standardInterp});
    contiStencils[i].setVd({67,standardInterp});
    contiStencils[i].setUl({30,standardInterp});
    contiStencils[i].setUr({31,standardInterp});

///////////////////
    i = 3;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({63,standardInterp});
    contiStencils[i].setVd({68,standardInterp});
    contiStencils[i].setUl({31,standardInterp});
    contiStencils[i].setUr({32,standardInterp});

///////////////////
    i = 4;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({64,standardInterp});
    contiStencils[i].setVd({69,standardInterp});
    contiStencils[i].setUl({32,standardInterp});
    contiStencils[i].setUr({33,standardInterp});

///////////////////
    i = 5;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({65,standardInterp});
    contiStencils[i].setVd({70,standardInterp});
    contiStencils[i].setUl({34,standardInterp});
    contiStencils[i].setUr({35,standardInterp});

///////////////////
    i = 6;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({69,standardInterp});
    contiStencils[i].setVd({77,standardInterp});
    contiStencils[i].setUl({38,standardInterp});
    contiStencils[i].setUr({39,standardInterp});

///////////////////
    i = 7;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({70,standardInterp});
    contiStencils[i].setVd({78,standardInterp});
    contiStencils[i].setUl({40,standardInterp});
    contiStencils[i].setUr({41,standardInterp});

///////////////////
    i = 8;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({77,standardInterp});
    contiStencils[i].setVd({81,standardInterp});
    contiStencils[i].setUl({46,standardInterp});
    contiStencils[i].setUr({47,standardInterp});

///////////////////
    i = 9;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({78,standardInterp});
    contiStencils[i].setVd({82,standardInterp});
    contiStencils[i].setUl({48,standardInterp});
    contiStencils[i].setUr({49,standardInterp});

///////////////////
    i = 10;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({81,standardInterp});
    contiStencils[i].setVd({86,standardInterp});
    contiStencils[i].setUl({52,standardInterp});
    contiStencils[i].setUr({53,standardInterp});

///////////////////
    i = 11;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({82,standardInterp});
    contiStencils[i].setVd({87,standardInterp});
    contiStencils[i].setUl({54,standardInterp});
    contiStencils[i].setUr({55,standardInterp});

///////////////////
    i = 12;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({83,standardInterp});
    contiStencils[i].setVd({88,standardInterp});
    contiStencils[i].setUl({55,standardInterp});
    contiStencils[i].setUr({56,standardInterp});

///////////////////
    i = 13;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({84,standardInterp});
    contiStencils[i].setVd({89,standardInterp});
    contiStencils[i].setUl({56,standardInterp});
    contiStencils[i].setUr({57,standardInterp});

///////////////////
    i = 14;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({85,standardInterp});
    contiStencils[i].setVd({90,standardInterp});
    contiStencils[i].setUl({57,standardInterp});
    contiStencils[i].setUr({58,standardInterp});

///////////////////
    i = 15;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({86,standardInterp});
    contiStencils[i].setVd({91,standardInterp});
    contiStencils[i].setUl({58,standardInterp});
    contiStencils[i].setUr({59,standardInterp});

///////////////////
    /*Corners around refinement region*/
///////////////////
    i = 16;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({66,standardInterp});
    contiStencils[i].setVd({71,standardInterp});
    contiStencils[i].setUl({35,standardInterp});
    contiStencils[i].setUr({36,standardInterp});

///////////////////
    i = 17;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({68,standardInterp});
    contiStencils[i].setVd({76,standardInterp});
    contiStencils[i].setUl({37,standardInterp});
    contiStencils[i].setUr({38,standardInterp});

///////////////////
    i = 18;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({79,standardInterp});
    contiStencils[i].setVd({83,standardInterp});
    contiStencils[i].setUl({49,standardInterp});
    contiStencils[i].setUr({50,standardInterp});

///////////////////
    i = 19;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({80,standardInterp});
    contiStencils[i].setVd({85,standardInterp});
    contiStencils[i].setUl({51,standardInterp});
    contiStencils[i].setUr({52,standardInterp});

///////////////////
    /*Neighbors around refinement region*/
///////////////////
    i = 20;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({67,standardInterp});
    contiStencils[i].setVd({72,.5},{94,.5});
    contiStencils[i].setUl({36,standardInterp});
    contiStencils[i].setUr({37,standardInterp});

///////////////////
    i = 21;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({71,standardInterp});
    contiStencils[i].setVd({79,standardInterp});
    contiStencils[i].setUl({41,standardInterp});
    contiStencils[i].setUr({92,.5},{42,.5});

///////////////////
    i = 22;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({76,standardInterp});
    contiStencils[i].setVd({80,standardInterp});
    contiStencils[i].setUl({93,.5},{45,.5});
    contiStencils[i].setUr({46,standardInterp});

///////////////////
    i = 23;

    contiStencils[i].setDeltaX(standardDeltaX);
    contiStencils[i].setDeltaY(standardDeltaY);

    contiStencils[i].setVu({75,.5},{95,.5});
    contiStencils[i].setVd({84,standardInterp});
    contiStencils[i].setUl({50,standardInterp});
    contiStencils[i].setUr({51,standardInterp});

///////////////////
    /*Refinement region*/
///////////////////
    i = 24;

    contiStencils[i].setDeltaX(.5*standardDeltaX);
    contiStencils[i].setDeltaY(.5*standardDeltaY);

    contiStencils[i].setVu({72,standardInterp});
    contiStencils[i].setVd({73,standardInterp});
    contiStencils[i].setUl({92,standardInterp});
    contiStencils[i].setUr({43,standardInterp});

///////////////////
    i = 25;

    contiStencils[i].setDeltaX(.5*standardDeltaX);
    contiStencils[i].setDeltaY(.5*standardDeltaY);

    contiStencils[i].setVu({94,standardInterp});
    contiStencils[i].setVd({74,standardInterp});
    contiStencils[i].setUl({43,standardInterp});
    contiStencils[i].setUr({93,standardInterp});

///////////////////
    i = 26;

    contiStencils[i].setDeltaX(.5*standardDeltaX);
    contiStencils[i].setDeltaY(.5*standardDeltaY);

    contiStencils[i].setVu({73,standardInterp});
    contiStencils[i].setVd({75,standardInterp});
    contiStencils[i].setUl({42,standardInterp});
    contiStencils[i].setUr({44,standardInterp});

///////////////////
    i = 27;

    contiStencils[i].setDeltaX(.5*standardDeltaX);
    contiStencils[i].setDeltaY(.5*standardDeltaY);

    contiStencils[i].setVu({74,standardInterp});
    contiStencils[i].setVd({95,standardInterp});
    contiStencils[i].setUl({44,standardInterp});
    contiStencils[i].setUr({45,standardInterp});

    return contiStencils;
}
