template<class SolutionType, class MomentumStencilType, class ContinuityStencilType>
void printL2norm (const SolutionType& var,
                   const MomentumStencilType& momentumStencils,
                   const ContinuityStencilType& continuityStencils,
                   std::string variablename)
{
//p
    double absolutePressureVar = 0.;

    double sumP = 0.;
    double totalVolumeP = 0.;

    for (int i = 0; i <= 27; ++i)
    {
        double diff = var[i];
        double squaredDiff = diff * diff;

        double deltax = continuityStencils[i].deltax;
        double deltay = continuityStencils[i].deltay;
        double volume = deltax * deltay;

        double weightedSquareDiff = squaredDiff*volume;

        totalVolumeP += volume;
        sumP += weightedSquareDiff;
    }

    absolutePressureVar = std::sqrt( sumP/totalVolumeP );

    std::cout << "absolute pressure L2 " << variablename << " = " << absolutePressureVar << std::endl;

//u
    double absoluteUVar = 0.;

    double sumU = 0.;

    for (const auto& i : xVelocityDofs())
    {
        double diff = var[i];
        double squaredDiff = diff * diff;

        double leftOrDownOppoDelta = momentumStencils[i-28].leftOrDownOppoPair.delta;
        double rightOrUpOppoDelta = momentumStencils[i-28].rightOrUpOppoPair.delta;
        double opposDelta = .5 * (leftOrDownOppoDelta + rightOrUpOppoDelta);
        double scvfDelta = momentumStencils[i-28].scvfDelta;
        double volume = opposDelta*scvfDelta;

        double weightedSquareDiff = squaredDiff*volume;

        sumU += weightedSquareDiff;
    }

    using GlobalPosition = Dune::FieldVector<double, 2>;
    const auto upperRight = Dumux::getParamFromGroup<GlobalPosition>("", "Grid.UpperRight");
    const auto lowerLeft = Dumux::getParamFromGroup<GlobalPosition>("", "Grid.LowerLeft");

    double totalVolume = (upperRight[1]-lowerLeft[1]) * (upperRight[0]-lowerLeft[0]);

    absoluteUVar = std::sqrt( sumU/totalVolume );
    //totalVolumeU would be 8, as the boundary Dirichlet dofs do not contribute. To have the same kind of calcualtion as in the reference, I set the total volume 10.

    std::cout << "absolute u L2 " << variablename << " = " << absoluteUVar << std::endl;

//v
    double absoluteVVar = 0.;

    double sumV = 0.;

    for (const auto& i : yVelocityDofs())
    {
        double diff = var[i];
        double squaredDiff = diff * diff;

        double leftOrDownOppoDelta = momentumStencils[i-28].leftOrDownOppoPair.delta;
        double rightOrUpOppoDelta = momentumStencils[i-28].rightOrUpOppoPair.delta;
        double opposDelta = .5 * (leftOrDownOppoDelta + rightOrUpOppoDelta);
        double scvfDelta = momentumStencils[i-28].scvfDelta;
        double volume = opposDelta*scvfDelta;

        double weightedSquareDiff = squaredDiff*volume;

        sumV += weightedSquareDiff;
    }

    absoluteVVar = std::sqrt( sumV/totalVolume );
    //totalVolumeU would be 8, as the boundary Dirichlet dofs do not contribute. To have the same kind of calcualtion as in the reference, I set the total volume 10.

    std::cout << "absolute v L2 " << variablename << " = " << absoluteVVar << std::endl;
}


