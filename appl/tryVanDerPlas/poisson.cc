// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Test for the staggered grid Navier-Stokes model (Kovasznay 1947)
 */

bool doFirstOrderLocalTruncErrorGlobalRefinement = false;

#include <config.h>

#include <ctime>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>
#include <dune/common/densematrix.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/staggeredrefinedfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>

#include <dumux/io/mystaggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>

#include <dumux/freeflow/navierstokes/model.hh>
#include <dumux/discretization/staggered/freeflow/fvgridgeometrytraits1.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/discretization/staggered/myfvgridgeometry.hh>
#include <dumux/freeflow/navierstokes/staggered/residualCalc.hh>

/*!
 * \brief Provides an interface for customizing error messages associated with
 *        reading in parameters.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
                    errorMessageOut += progName;
                    errorMessageOut += " [options]\n";
                    errorMessageOut += errorMsg;
                    errorMessageOut += "\n\nThe list of mandatory arguments for this program is:\n"
                                        "\t-TimeManager.TEnd               End of the simulation [s] \n"
                                        "\t-TimeManager.DtInitial          Initial timestep size [s] \n"
                                        "\t-Grid.File                      Name of the file containing the grid \n"
                                        "\t                                definition in DGF format\n"
                                        "\t-SpatialParams.LensLowerLeftX   x-coordinate of the lower left corner of the lens [m] \n"
                                        "\t-SpatialParams.LensLowerLeftY   y-coordinate of the lower left corner of the lens [m] \n"
                                        "\t-SpatialParams.LensUpperRightX  x-coordinate of the upper right corner of the lens [m] \n"
                                        "\t-SpatialParams.LensUpperRightY  y-coordinate of the upper right corner of the lens [m] \n"
                                        "\t-SpatialParams.Permeability     Permeability of the domain [m^2] \n"
                                        "\t-SpatialParams.PermeabilityLens Permeability of the lens [m^2] \n";

        std::cout << errorMessageOut
                  << "\n";
    }
}

namespace Dumux
{
namespace Properties
{
namespace TTag {
// Create new type tags
struct ChannelTestTypeTag { using InheritsFrom = std::tuple<NavierStokes, StaggeredFreeFlowModel>; };
} // end namespace TTag

//! The default fv grid geometry
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::ChannelTestTypeTag>
{
private:
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = typename GetPropType<TypeTag, Properties::Grid>::LeafGridView;
    using Traits = MyStaggeredFreeFlowDefaultGridGeometryTraits<GridView, upwindSchemeOrder>;
public:
    using type = MyStaggeredGridGeometry<GridView, enableCache, Traits>;
};

template<class TypeTag>
struct CVGridGeometry<TypeTag, TTag::ChannelTestTypeTag>
{
private:
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = typename Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >::LeafGridView;
    using Traits = MyStaggeredFreeFlowDefaultGridGeometryTraits<GridView, upwindSchemeOrder>;
public:
    using type = MyStaggeredGridGeometry<GridView, enableCache, Traits>;
};

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::ChannelTestTypeTag> { static constexpr bool value = true; };

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::ChannelTestTypeTag>
{
    using type = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >;
};
} // end namespace Properties
} // end namespace Dumux
/*
#include "output.hh"*/

double fFunc (double x, bool isChannel)
{
    if (isChannel)
        return 2.;
    else
        return 0.5*std::sin(4.*M_PI*x);
}

double pFunc(double x, bool isChannel)
{
    if (isChannel)
        return x*x-x;
    else
        return -1./(32.*M_PI*M_PI)*std::sin(4.*M_PI*x);
}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv, usage);

    bool isChannel = Dumux::getParam<bool>("Problem.IsChannel",false);

    static constexpr int N = 64;

    static constexpr double leftDelta = 1./(2.*N);
    static constexpr double rightDelta = 1./N;

    static constexpr double xInt = 0.5;

    static constexpr int numDofs = xInt/leftDelta + (1.-xInt)/rightDelta;
    unsigned int dofOfChange = xInt/leftDelta;

    Dune::FieldMatrix<double, numDofs, numDofs> A;
    Dune::FieldVector<double, numDofs> p;
    Dune::FieldVector<double, numDofs> f = {};
    Dune::FieldVector<double, numDofs> b = {};
    Dune::FieldVector<double, numDofs> pAnalytical = {};
    Dune::FieldVector<double, numDofs> deltaX = {};


    //x<0.5
    for (unsigned int i = 0; i < dofOfChange; ++i)
    {
        double xCentral = leftDelta * (i+0.5);
        deltaX[i] = leftDelta;
        pAnalytical[i] = pFunc(xCentral, isChannel);
        f[i] = fFunc(xCentral, isChannel);
    }
    //x>0.5
    for (unsigned int i = dofOfChange; i < numDofs; ++i)
    {
        double xCentral = xInt + rightDelta * (i-N/2+0.5);
        deltaX[i] = rightDelta;
        pAnalytical[i] = pFunc(xCentral, isChannel);
        f[i]= fFunc(xCentral, isChannel);
    }

    for (unsigned int i = 0; i < numDofs; ++i)
    {
        b[i]=f[i];

        bool minVersion = getParam<bool>("Problem.MinVersion");

        if (i != 0)
        {
            double deltaXiMinusHalfHat = 0.5*(deltaX[i] + deltaX[i-1]);
            if (minVersion)
                deltaXiMinusHalfHat = std::min(deltaX[i-1],deltaX[i]);

            A[i][i-1] += 1./(deltaX[i]*deltaXiMinusHalfHat);
            A[i][i]   -= 1./(deltaX[i]*deltaXiMinusHalfHat);
        }
        else
        {
            //don't add anything to b as p is zero at the boundary
            A[i][i] -= 2./(deltaX[i]*deltaX[i]);//gradient to boundary value is built with half of the standard distance, might want to leave this away for a zero gradient at the boundary, currently I assume a Dirichlet value of zero for p
        }

        if (i != (numDofs-1))
        {
             double deltaXiPlusHalfHat = .5*(deltaX[i] + deltaX[i+1]);

            if (minVersion)
                deltaXiPlusHalfHat = std::min(deltaX[i], deltaX[i+1]);

            A[i][i+1] += 1./(deltaX[i]*deltaXiPlusHalfHat);
            A[i][i]   -= 1./(deltaX[i]*deltaXiPlusHalfHat);
        }
        else
        {
            //don't add anything to b as p is zero at the boundary
            A[i][i] -= 2./(deltaX[i]*deltaX[i]);//gradient to boundary value is built with half of the standard distance, might want to leave this away for a zero gradient at the boundary, currently I assume a Dirichlet value of zero for p
        }
    }

    A.solve(p,b);

    Dune::FieldVector<double, numDofs> res = -b;
    A.umv(pAnalytical,res);

    Dune::FieldVector<double, numDofs> err;
    for (unsigned int i = 0; i < err.size(); ++i)
    {
        err[i] = p[i]-pAnalytical[i];
    }

    double absolutePressureVar = 0.;
    double sumP = 0.;
    double max = std::numeric_limits<double>::lowest();
    for (int i = 0; i < numDofs; ++i)
    {
        double diff = err[i];
        double squaredDiff = diff * diff;
        double weightedSquareDiff = squaredDiff*deltaX[i];
        sumP += weightedSquareDiff;

        max = std::max(max,err[i]);
    }
    //absolute L2 error
    absolutePressureVar = std::sqrt( sumP );

    std::cout << "L infty error = " << max << std::endl;
    std::cout << "absolute L2 error = " << absolutePressureVar << std::endl;

    std::cout << "for N= " << N << " local truncation error at left of interface = " << res[dofOfChange-1]  << std::endl;
    std::cout << "for N= " << N << " local truncation error at right of interface = " << res[dofOfChange]  << std::endl;

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
