void set (const typename std::pair<int,double>& pair1,
          const typename std::pair<int,double>& pair2,
          const typename std::pair<int,double>& pair3,
          const typename std::pair<int,double>& pair4,
          const typename std::pair<int,double>& pair5,
          const typename std::pair<int,double>& pair6,
          const typename std::pair<int,double>& pair7,
          const typename std::pair<int,double>& pair8,
          std::vector<std::pair<int,double>>& builder)
{
    builder.push_back(pair1);

    if (pair2.first != -2)
    {
        builder.push_back(pair2);
        if (pair3.first != -2)
        {
            builder.push_back(pair3);
            if (pair4.first != -2)
            {
                builder.push_back(pair4);
                if (pair5.first != -2)
                {
                    builder.push_back(pair5);
                    if (pair6.first != -2)
                    {
                        builder.push_back(pair6);
                        if (pair7.first != -2)
                        {
                            builder.push_back(pair7);
                            if (pair8.first != -2)
                                builder.push_back(pair8);
                        }
                    }
                }
            }
        }
    }
}
