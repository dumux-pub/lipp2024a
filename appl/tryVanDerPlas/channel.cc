// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Test for the staggered grid Navier-Stokes model (Kovasznay 1947)
 */

bool doFirstOrderLocalTruncErrorGlobalRefinement = false;

#include <config.h>

#include <ctime>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>
#include <dune/common/densematrix.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/staggeredrefinedfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>

#include <dumux/io/mystaggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>

#include <dumux/freeflow/navierstokes/model.hh>
#include <dumux/discretization/staggered/freeflow/fvgridgeometrytraits1.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/discretization/staggered/myfvgridgeometry.hh>
#include <dumux/freeflow/navierstokes/staggered/residualCalc.hh>

double leftNormal (double y)
{
    double uMax = Dumux::getParam<double>("Problem.MaximumVelocity");

    return 4 * uMax * y * (1-y);
}

double rightNormal (double y)
{
   double uMax = Dumux::getParam<double>("Problem.MaximumVelocity");

    return 4 * uMax * y * (1-y);
}

double upNormal (double x)
{
    return 0.;
}

double downNormal (double x)
{
    return 0.;
}

double leftTangential (double y)
{
    return 0.;
}

double rightTangential (double y)
{
    return 0.;
}

double upTangential (double x)
{
    return 0.;
}

double downTangential (double x)
{
    return 0.;
}

auto xVelocityDofs ()
{
#if VAN_DER_PLAS
    static constexpr int numDofs = 92;
#else
    static constexpr int numDofs = 96;
#endif

    static constexpr int numUDofs = (numDofs-28)*0.5;

    std::array<int,numUDofs> dofs;

    for (int i = 28; i <= 59; ++i)
        dofs[i-28] = i;
#if !VAN_DER_PLAS
    dofs[numUDofs-2] = 92;
    dofs[numUDofs-1] = 93;
#endif

    return dofs;
}

auto yVelocityDofs ()
{
#if VAN_DER_PLAS
    static constexpr int numDofs = 92;
#else
    static constexpr int numDofs = 96;
#endif

    static constexpr int numVDofs = (numDofs-28)*0.5;

    std::array<int,numVDofs> dofs;

    for (int i = 60; i <= 91; ++i)
        dofs[i-60] = i;
#if !VAN_DER_PLAS
    dofs[numVDofs-2] = 94;
    dofs[numVDofs-1] = 95;
#endif

    return dofs;
}

#include "setDofs.hh"
#include "set.hh"
#if VAN_DER_PLAS
#include "setVanDerPlasContinuityStencils.hh"
#include "setVanDerPlasMomentumStencils.hh"
#endif
#if MY_METHOD_CONTINUITY_LINEAR
#include "setMyMethodLinearContinuityStencils.hh"
#elif MY_METHOD_CONTINUITY_QUADRATIC
#include "setMyMethodQuadraticContinuityStencils.hh"
#endif
#if MY_METHOD_MOMENTUM_LINEAR
#include "setMyMethodLinearMomentumStencils.hh"
#elif MY_METHOD_MOMENTUM_LINEAR_PARALLELPOSVAR
#include "setMyMethodLinearParallelPosVarMomentumStencils.hh"
#elif MY_METHOD_MOMENTUM_LINEAR_NONDISTORTED
#include "setMyMethodLinearNonDistortedTransitionsalCVStencil.hh"
#elif MY_METHOD_MOMENTUM_QUADRATIC
#include "setMyMethodQuadraticMomentumStencils.hh"
#elif MY_METHOD_MOMENTUM_QUADRATIC_SKEWED
#include "setMyMethodQuadraticSkewedMomentumStencils.hh"
#elif MY_METHOD_MOMENTUM_QUADRATIC_PARALLELPOSVAR
#include "setMyMethodQuadraticParallelPosVarMomentumStencils.hh"
#elif MY_METHOD_MOMENTUM_QUADRATIC_NONDISTORTED
#include "setMyMethodQuadraticNonDistortedTransitionsalCVStencil.hh"
#endif
#include "print.hh"
#include "l2norm.hh"

/*!
 * \brief Provides an interface for customizing error messages associated with
 *        reading in parameters.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
                    errorMessageOut += progName;
                    errorMessageOut += " [options]\n";
                    errorMessageOut += errorMsg;
                    errorMessageOut += "\n\nThe list of mandatory arguments for this program is:\n"
                                        "\t-TimeManager.TEnd               End of the simulation [s] \n"
                                        "\t-TimeManager.DtInitial          Initial timestep size [s] \n"
                                        "\t-Grid.File                      Name of the file containing the grid \n"
                                        "\t                                definition in DGF format\n"
                                        "\t-SpatialParams.LensLowerLeftX   x-coordinate of the lower left corner of the lens [m] \n"
                                        "\t-SpatialParams.LensLowerLeftY   y-coordinate of the lower left corner of the lens [m] \n"
                                        "\t-SpatialParams.LensUpperRightX  x-coordinate of the upper right corner of the lens [m] \n"
                                        "\t-SpatialParams.LensUpperRightY  y-coordinate of the upper right corner of the lens [m] \n"
                                        "\t-SpatialParams.Permeability     Permeability of the domain [m^2] \n"
                                        "\t-SpatialParams.PermeabilityLens Permeability of the lens [m^2] \n";

        std::cout << errorMessageOut
                  << "\n";
    }
}

namespace Dumux
{
namespace Properties
{
namespace TTag {
// Create new type tags
struct ChannelTestTypeTag { using InheritsFrom = std::tuple<NavierStokes, StaggeredFreeFlowModel>; };
} // end namespace TTag

//! The default fv grid geometry
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::ChannelTestTypeTag>
{
private:
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = typename GetPropType<TypeTag, Properties::Grid>::LeafGridView;
    using Traits = MyStaggeredFreeFlowDefaultGridGeometryTraits<GridView, upwindSchemeOrder>;
public:
    using type = MyStaggeredGridGeometry<GridView, enableCache, Traits>;
};

template<class TypeTag>
struct CVGridGeometry<TypeTag, TTag::ChannelTestTypeTag>
{
private:
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = typename Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >::LeafGridView;
    using Traits = MyStaggeredFreeFlowDefaultGridGeometryTraits<GridView, upwindSchemeOrder>;
public:
    using type = MyStaggeredGridGeometry<GridView, enableCache, Traits>;
};

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::ChannelTestTypeTag> { static constexpr bool value = true; };

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::ChannelTestTypeTag>
{
    using type = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >;
};
} // end namespace Properties
} // end namespace Dumux

#include "output.hh"

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv, usage);

    static constexpr int numPDofs = 28;
#if VAN_DER_PLAS
    static constexpr int numDofs = 92;
#else
    static constexpr int numDofs = 96;
#endif

    const int continuityOffset = 0;
    const int momentumOffset = numPDofs;

    static constexpr int numVelocityDofs = numDofs - numPDofs;

    Dune::FieldMatrix<double, numDofs, numDofs> A;
    Dune::FieldVector<double, numDofs> x;
    Dune::FieldVector<double, numDofs> b = {};
    Dune::FieldVector<double, numDofs> cvsVector = {};

    std::array<std::pair<double, double>, numDofs> dofPositions;
    std::unordered_map<int, int> dofMapPressure;
    std::unordered_map<int, int> dofMapVelocity;
    setDofs(dofPositions, dofMapPressure, dofMapVelocity);

    Dune::FieldVector<double, numDofs> analyticalSol = {};
    Dune::FieldVector<double, numPDofs> analyticalPressure;
    Dune::FieldVector<double, numVelocityDofs> analyticalVelocity = {};

    for (int i = 0; i <= 27; ++i)
    {
        double x = dofPositions[i].first;

        analyticalPressure[i] = -8*x + 40;
        analyticalSol[i] = analyticalPressure[i];
    }

    for (const auto& i : xVelocityDofs())
    {
        double y = dofPositions[i].second;

        analyticalVelocity[i-28] = leftNormal(y);
        analyticalSol[i] = analyticalVelocity[i-28];
    }

    for (const auto& i : yVelocityDofs())
    {
        analyticalVelocity[i-28] = 0.;
        analyticalSol[i] = analyticalVelocity[i-28];
    }

    double dynamicVisosity = getParam<double>("Component.LiquidKinematicViscosity", 1.0) * getParam<double>("Component.LiquidDensity", 1.0);

    auto momentumStencils = setMomentumStencils<numVelocityDofs, momentumOffset>();

    for (int xIdx = 0; xIdx < momentumStencils.size(); ++xIdx)
    {
        double leftOrDownOppoDelta = momentumStencils[xIdx].leftOrDownOppoPair.delta;
        double rightOrUpOppoDelta = momentumStencils[xIdx].rightOrUpOppoPair.delta;
        double opposDelta = .5 * (leftOrDownOppoDelta + rightOrUpOppoDelta);
        double rightOrUpLateralDelta = momentumStencils[xIdx].rightOrUpLateralPair.delta;
        double scvfDelta = momentumStencils[xIdx].scvfDelta;
        double leftOrDownLateralDelta = momentumStencils[xIdx].leftOrDownLateralPair.delta;
        int dofIdx = xIdx+momentumOffset;

        cvsVector[dofIdx] = opposDelta * scvfDelta;

        for (const auto& midVelocitySet : momentumStencils[xIdx].midVelocityBuilder)
        {
            if (midVelocitySet.first != -2)
                A[dofIdx][midVelocitySet.first] += midVelocitySet.second*( 2*opposDelta/(rightOrUpLateralDelta+scvfDelta) + 2*opposDelta/(leftOrDownLateralDelta+scvfDelta) + 2*scvfDelta/leftOrDownOppoDelta + 2*scvfDelta/rightOrUpOppoDelta );
        }

        //boundary treatment
        if ( (momentumStencils[xIdx].midVelocityBuilder)[0].first == -2 )
        {
            A[dofIdx][dofIdx] = 1.;
            b[dofIdx] = momentumStencils[xIdx].boundarySelfVelocity;
            continue;
        }

        for (const auto& uuPair : momentumStencils[xIdx].rightOrUpLateralPair.parallelVelocityBuilder)
        {
            if (uuPair.first != -2)
                A[dofIdx][uuPair.first] += uuPair.second*( -2*opposDelta/(rightOrUpLateralDelta+scvfDelta) );
        }

        //boundary treatment
        const auto& uuPair = (momentumStencils[xIdx].rightOrUpLateralPair.parallelVelocityBuilder)[0];
        if ( uuPair.first == -2 )
        {
            b[dofIdx] -= -2*opposDelta/(rightOrUpLateralDelta+scvfDelta) * momentumStencils[xIdx].rightOrUpLateralPair.boundaryParallelVelocity;
        }

        for (const auto& udPair : momentumStencils[xIdx].leftOrDownLateralPair.parallelVelocityBuilder)
        {
            if (udPair.first != -2)
                A[dofIdx][udPair.first] += udPair.second*( -2*opposDelta/(leftOrDownLateralDelta+scvfDelta) );
        }

        //boundary treatment
        if ( (momentumStencils[xIdx].leftOrDownLateralPair.parallelVelocityBuilder)[0].first == -2 )
        {
            b[dofIdx] -= -2*opposDelta/(leftOrDownLateralDelta+scvfDelta)  * momentumStencils[xIdx].leftOrDownLateralPair.boundaryParallelVelocity;
        }

        for (const auto& ulPair : momentumStencils[xIdx].leftOrDownOppoPair.velocityBuilder)
        {
            if (ulPair.first != -2)
                A[dofIdx][ulPair.first] += ulPair.second*( -2*scvfDelta/leftOrDownOppoDelta );
        }
        for (const auto& urPair : momentumStencils[xIdx].rightOrUpOppoPair.velocityBuilder)
        {
            if (urPair.first != -2)
                A[dofIdx][urPair.first] += urPair.second*( -2*scvfDelta/rightOrUpOppoDelta );
        }

        for (const auto& vruPair : momentumStencils[xIdx].rightOrUpLateralPair.rightOrUpVelocityBuilder)
        {
            if (vruPair.first != -2)
                A[dofIdx][vruPair.first] += vruPair.second*(-1.);
        }
        for (const auto& vluPair : momentumStencils[xIdx].rightOrUpLateralPair.leftOrDownVelocityBuilder)
        {
            if (vluPair.first != -2)
                A[dofIdx][vluPair.first] += vluPair.second*(1.);
        }
        for (const auto& vrdPair : momentumStencils[xIdx].leftOrDownLateralPair.rightOrUpVelocityBuilder)
        {
            if (vrdPair.first != -2)
                A[dofIdx][vrdPair.first] += vrdPair.second*(1.);
        }
        for (const auto& vldPair : momentumStencils[xIdx].leftOrDownLateralPair.leftOrDownVelocityBuilder)
        {
            if (vldPair.first != -2)
                A[dofIdx][vldPair.first] += vldPair.second*(-1.);
        }
        for (const auto& plPair : momentumStencils[xIdx].leftOrDownOppoPair.pressureBuilder)
        {
            if (plPair.first != -2)
                A[dofIdx][plPair.first] += plPair.second*(-scvfDelta/dynamicVisosity);
        }
        for (const auto& prPair : momentumStencils[xIdx].rightOrUpOppoPair.pressureBuilder)
        {
            if (prPair.first != -2)
                A[dofIdx][prPair.first] += prPair.second*(scvfDelta/dynamicVisosity);
        }
    }

    auto continuityStencils = setContinuityStencils<numPDofs>();

    for (int pIdx = 0; pIdx < continuityStencils.size(); ++pIdx)
    {
        double deltax = continuityStencils[pIdx].deltax;
        double deltay = continuityStencils[pIdx].deltay;

        cvsVector[pIdx + continuityOffset] = deltax * deltay;

        if (pIdx != 11)
        {
            for (const auto& vuPair : continuityStencils[pIdx].vuBuilder)
            {
                if (vuPair.first != -2)
                    A[pIdx+continuityOffset][vuPair.first] += vuPair.second*( deltax );
            }
            for (const auto& vdPair : continuityStencils[pIdx].vdBuilder)
            {
                if (vdPair.first != -2)
                    A[pIdx+continuityOffset][vdPair.first] += vdPair.second*( -deltax );
            }

            for (const auto& urPair : continuityStencils[pIdx].urBuilder)
            {
                if (urPair.first != -2)
                    A[pIdx+continuityOffset][urPair.first] += urPair.second*( deltay );
            }
            for (const auto& ulPair : continuityStencils[pIdx].ulBuilder)
            {
                if (ulPair.first != -2)
                    A[pIdx+continuityOffset][ulPair.first] += ulPair.second*( -deltay );
            }
        }
        else
        {
            //fix middle pressure to zero
            A[pIdx+continuityOffset][pIdx+continuityOffset] = 1.;
            b[pIdx+continuityOffset] = analyticalPressure[pIdx];
        }
    }

    A.solve(x,b);

//     Dune::printvector(std::cout, x, "", "");
    Dune::FieldVector<double, numDofs> res = -b;
    A.umv(analyticalSol,res);

//     printMatrix(A, dofMapPressure, dofMapPressure);
//     printvector(res,dofMapPressure);

    for (unsigned int i = 0; i < res.size(); ++i)
    {
        if (cvsVector[i] != 0.)
            res[i] /= cvsVector[i];
    }

    Dune::FieldVector<double, numVelocityDofs> bFace;
    Dune::FieldMatrix<double, numVelocityDofs, numPDofs> AFaceCell;
    Dune::FieldMatrix<double, numVelocityDofs, numVelocityDofs> AFaceFace;
    for (int i = numPDofs; i<numDofs; ++i)
    {
        bFace[i-numPDofs] = b[i];

        for (int j = 0; j < numPDofs; ++j)
        {
            AFaceCell[i-numPDofs][j] = A[i][j];
        }
        for (int j = numPDofs; j<numDofs; ++j)
        {
            AFaceFace[i-numPDofs][j-numPDofs] = A[i][j];
        }
    }

    Dune::FieldVector<double, numVelocityDofs> velocitySol;
    Dune::FieldVector<double, numVelocityDofs> RHS = bFace;
    AFaceCell.mmv(analyticalPressure, RHS);

    AFaceFace.solve(velocitySol, RHS);

    // define the type tag for this problem
    using TypeTag = Properties::TTag::ChannelTestTypeTag;

    std::vector<Dune::FieldVector<double, numDofs>> outputVecs;
    Dune::FieldVector<double, numDofs> err;
    for (unsigned int i = 0; i < err.size(); ++i)
    {
        err[i] = x[i]-analyticalSol[i];
    }
    outputVecs.push_back(err);
    outputVecs.push_back(x);
    outputVecs.push_back(res);

    std::vector<std::string> names;
    names.push_back("error");
    names.push_back("sol");
    names.push_back("residual");

    output<TypeTag, std::vector<Dune::FieldVector<double, numDofs>>, std::array<std::pair<double, double>, numDofs>>(outputVecs, dofPositions, names);

    printL2norm(err, momentumStencils, continuityStencils, "error");
    printL2norm(res, momentumStencils, continuityStencils, "residual");

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
