#include "setBoundaryMomentumStencils.hh"

template <int numVelocityDofs, int momentumOffset>
auto setMomentumStencils()
{
    std::array<MomentumStencil, numVelocityDofs> stencils;

    setBoundaryMomentumStencils<numVelocityDofs,momentumOffset>(stencils);

    double standardInterp = 1.;
    static const double standardDeltaX = Dumux::getParam<double>("Grid.StandardDeltaX");
    static const double standardDeltaY = Dumux::getParam<double>("Grid.StandardDeltaY");

///////////////////
    /*x*/
//////////////////
    double standardDeltaOppo = standardDeltaX;
    double standardDeltaScvf = standardDeltaY;

///////////////////
    /*scvfs in inner*/
///////////////////
    int i = 35;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({35,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({34,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({5,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({36,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({16,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({29,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({65,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({66,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({41,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({70,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({71,standardInterp});

///////////////////
    i = 36;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({36,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({35,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({16,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({37,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({20,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({30,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({66,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({67,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({92,.5},{42,.5});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({71,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({72,.5},{94,.5});

///////////////////
    i = 37;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({37,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({36,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({20,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({38,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({17,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({31,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({67,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({68,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({93,.5},{45,.5});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({72,.5},{94,.5});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({76,standardInterp});

///////////////////
    i = 38;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({38,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({37,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({17,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({39,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({6,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({32,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({68,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({69,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({46,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({76,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({77,standardInterp});

 ///////////////////
    i = 41;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({41,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({40,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({7,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({92,.5},{42,.5});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({21,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({35,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({70,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({71,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({49,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({78,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({79,standardInterp});

///////////////////
    i = 92;
    stencils[i-momentumOffset].setScvfDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({92,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({41,0.375},{35,0.125},{92,.5});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({21,0.5},{16,1./6},{24,1./3});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({43,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({24,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({36,2./3},{92,1./3});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({71,2./3},{72,1./3});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({72,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({42,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({71,1./3},{79,1./3},{73,1./3});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({73,standardInterp});

///////////////////
    i = 43;
    stencils[i-momentumOffset].setScvfDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({43,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({92,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({24,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({93,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({25,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({36,1./3},{37,1./3},{43,1./3});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({72,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({94,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({44,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({73,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({74,standardInterp});

///////////////////
    i = 93;
    stencils[i-momentumOffset].setScvfDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({93,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({43,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({25,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({38,.125},{46,.375},{93,.5});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({17,1./6},{22,.5},{25,1./3});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({37,2./3},{93,1./3});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({94,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({76,2./3},{94,1./3});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({45,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({74,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({76,1./3},{80,1./3},{74,1./3});

///////////////////
    i = 42;
    stencils[i-momentumOffset].setScvfDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({42,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({41,.375},{49,.125},{42,.5});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({21,.5},{18,1./6},{26,1./3});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({44,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({26,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({92,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({71,1./3},{79,1./3},{73,1./3});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({73,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({50,2./3},{42,1./3});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({79,2./3},{75,1./3});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({75,standardInterp});

///////////////////
    i = 44;
    stencils[i-momentumOffset].setScvfDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({44,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({42,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({26,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({45,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({27,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({43,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({73,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({74,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({50,1./3},{51,1./3},{44,1./3});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({75,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({95,standardInterp});

///////////////////
    i = 45;
    stencils[i-momentumOffset].setScvfDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({45,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({44,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({27,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({46,.375},{52,.125},{45,.5});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({22,.5},{19,1./6},{27,1./3});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({93,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({74,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({76,1./3},{80,1./3},{74,1./3});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({51,2./3},{45,1./3});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({95,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({80,2./3},{95,1./3});

///////////////////
    i = 46;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({46,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({93,.5},{45,.5});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({22,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({47,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({8,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({38,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({76,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({77,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({52,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({80,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({81,standardInterp});

///////////////////
    i = 49;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({49,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({48,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({9,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({50,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({18,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({41,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({78,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({79,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({55,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({82,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({83,standardInterp});

///////////////////
    i = 50;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({50,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({49,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({18,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({51,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({23,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({42,.5},{92,.5});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({79,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({75,.5},{95,.5});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({56,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({83,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({84,standardInterp});

///////////////////
    i = 51;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({51,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({50,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({23,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({52,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({19,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({45,.5},{93,.5});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({75,.5},{95,.5});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({80,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({57,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({84,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({85,standardInterp});

///////////////////
    i = 52;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({52,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({51,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({19,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({53,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({10,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({46,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({80,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({81,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({58,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({85,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({86,standardInterp});


///////////////////
    /*y*/
//////////////////
    standardDeltaOppo = standardDeltaY;
    standardDeltaScvf = standardDeltaX;

///////////////////
    /*scvfs in inner*/
///////////////////
    i = 66;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({66,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({61,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({1,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({71,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({16,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({65,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({29,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({35,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({67,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({30,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({36,standardInterp});

///////////////////
    i = 67;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({67,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({62,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({2,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({72,.5},{94,.5});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({20,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({66,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({30,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({36,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({68,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({31,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({37,standardInterp});

///////////////////
    i = 68;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({68,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({63,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({3,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({76,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({17,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({67,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({31,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({37,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({69,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({32,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({38,standardInterp});

///////////////////
    i = 71;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({71,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({66,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({16,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({79,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({21,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({70,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({35,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({41,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({72,.5},{94,.5});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({36,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({92,.5},{42,.5});

///////////////////
    i = 76;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({76,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({68,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({17,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({80,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({22,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({94,.5},{72,.5});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({37,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({93,.5},{45,.5});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({77,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({38,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({46,standardInterp});

///////////////////
    i = 79;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({79,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({71,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({21,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({83,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({18,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({78,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({41,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({49,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({75,.5},{95,.5});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({92,.5},{42,.5});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({50,standardInterp});

///////////////////
    i = 80;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({80,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({76,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({22,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({85,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({19,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({95,.5},{75,.5});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({45,.5},{93,.5});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({51,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({81,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({46,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({52,standardInterp});

///////////////////
    i = 83;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({83,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({79,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({18,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({88,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({12,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({82,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({49,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({55,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({84,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({50,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({56,standardInterp});

///////////////////
    i = 84;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({84,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({75,.5},{95,.5});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({23,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({89,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({13,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({83,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({50,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({56,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({85,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({51,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({57,standardInterp});

///////////////////
    i = 85;
    stencils[i-momentumOffset].setScvfDelta(standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({85,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({80,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({19,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({90,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({14,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({84,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({51,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({57,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({86,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({52,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({58,standardInterp});

///////////////////
    i = 72;
    stencils[i-momentumOffset].setScvfDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({72,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({66,.125},{67,.375},{72,.5});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({16,1./6},{20,.5},{24,1./3});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({73,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({24,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({71,2./3},{72,1./3});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({36,2./3},{92,1./3});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({92,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({94,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({36,1./3},{37,1./3},{43,1./3});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({43,standardInterp});

///////////////////
    i = 94;
    stencils[i-momentumOffset].setScvfDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({94,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({68,.125},{67,.375},{94,.5});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({17,1./6},{20,.5},{25,1./3});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({74,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({25,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({72,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({36,1./3},{37,1./3},{43,1./3});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({43,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({76,2./3},{94,1./3});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({37,2./3},{93,1./3});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({93,standardInterp});

///////////////////
    i = 73;
    stencils[i-momentumOffset].setScvfDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({73,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({72,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({24,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({75,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({26,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({71,1./3},{79,1./3},{73,1./3});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({92,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({42,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({74,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({43,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({44,standardInterp});

///////////////////
    i = 74;
    stencils[i-momentumOffset].setScvfDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({74,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({94,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({25,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({95,standardInterp});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({27,standardInterp});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({73,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({43,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({44,standardInterp});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({76,1./3},{80,1./3},{74,1./3});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({93,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({45,standardInterp});

///////////////////
    i = 75;
    stencils[i-momentumOffset].setScvfDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({75,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({73,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({26,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({84,.375},{83,.125},{75,.5});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({23,.5},{18,1./6},{26,1./3});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({79,2./3},{75,1./3});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({42,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({50,2./3},{42,1./3});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({95,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({44,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({50,1./3},{51,1./3},{44,1./3});

///////////////////
    i = 95;
    stencils[i-momentumOffset].setScvfDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].setMidVelocity({95,standardInterp});

    stencils[i-momentumOffset].rightOrUpOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].rightOrUpOppoPair.setVelocity({74,standardInterp});
    stencils[i-momentumOffset].rightOrUpOppoPair.setPressure({27,standardInterp});

    stencils[i-momentumOffset].leftOrDownOppoPair.setDelta(.5*standardDeltaOppo);
    stencils[i-momentumOffset].leftOrDownOppoPair.setVelocity({84,.375},{85,.125},{95,.5});
    stencils[i-momentumOffset].leftOrDownOppoPair.setPressure({23,.5},{19,1./6},{27,1./3});

    stencils[i-momentumOffset].leftOrDownLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].leftOrDownLateralPair.setParallelVelocity({75,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setRightOrUpVelocity({44,standardInterp});
    stencils[i-momentumOffset].leftOrDownLateralPair.setLeftOrDownVelocity({50,1./3},{51,1./3},{44,1./3});

    stencils[i-momentumOffset].rightOrUpLateralPair.setDelta(.5*standardDeltaScvf);
    stencils[i-momentumOffset].rightOrUpLateralPair.setParallelVelocity({80,2./3},{95,1./3});
    stencils[i-momentumOffset].rightOrUpLateralPair.setRightOrUpVelocity({45,standardInterp});
    stencils[i-momentumOffset].rightOrUpLateralPair.setLeftOrDownVelocity({51,2./3},{45,1./3});

    return stencils;
}
