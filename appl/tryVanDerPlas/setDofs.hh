template<class DofPosType, class MapPType, class MapVType>
void setDofs (DofPosType& dofPositions, MapPType& dofMapP, MapVType& dofMapVelocity)
{
    using GlobalPosition = Dune::FieldVector<double, 2>;
    const auto upperRight = Dumux::getParamFromGroup<GlobalPosition>("", "Grid.UpperRight");
    const auto lowerLeft = Dumux::getParamFromGroup<GlobalPosition>("", "Grid.LowerLeft");

    auto setP = [&](int dof, double x, double y, int externalDof)
    {
        dofPositions[dof].first = x/10. * (upperRight[0] - lowerLeft[0]);
        dofPositions[dof].second = y * (upperRight[1] - lowerLeft[1]);
        dofMapP.emplace(externalDof,dof);
    };

    auto setVelocity = [&](int dof, double x, double y, int externalDof)
    {
        dofPositions[dof].first = x/10. * (upperRight[0] - lowerLeft[0]);
        dofPositions[dof].second = y * (upperRight[1] - lowerLeft[1]);
        dofMapVelocity.emplace(externalDof,dof);
    };

//pressures
    setP(0,1,.9,14);
    setP(1,3,.9,15);
    setP(2,5,.9,21);
    setP(3,7,.9,22);
    setP(4,9,.9,23);
    setP(5,1,.7,11);
    setP(16,3,.7,13);
    setP(20,5,.7,17);
    setP(17,7,.7,18);
    setP(6,9,.7,20);
    setP(7,1,.5,10);
    setP(21,3,.5,12);
    setP(24,4.5,.55,27);
    setP(25,5.5,.55,26);
    setP(26,4.5,.45,24);
    setP(27,5.5,.45,25);
    setP(22,7,.5,16);
    setP(8,9,.5,19);
    setP(9,1,.3,2);
    setP(18,3,.3,3);
    setP(23,5,.3,7);
    setP(19,7,.3,8);
    setP(10,9,.3,9);
    setP(11,1,.1,0);
    setP(12,3,.1,1);
    setP(13,5,.1,4);
    setP(14,7,.1,5);
    setP(15,9,.1,6);

//u
   setVelocity(28,0,.9,35);
   setVelocity(29,2,.9,36);
   setVelocity(30,4,.9,38);
   setVelocity(31,6,.9,50);
   setVelocity(32,8,.9,52);
   setVelocity(33,10,.9,54);
   setVelocity(34,0,.7,29);
   setVelocity(35,2,.7,30);
   setVelocity(36,4,.7,33);
   setVelocity(37,6,.7,42);
   setVelocity(38,8,.7,44);
   setVelocity(39,10,.7,48);
   setVelocity(40,0,.5,26);
   setVelocity(41,2,.5,27);
   setVelocity(43,5,.55,63);
   setVelocity(44,5,.45,57);
   setVelocity(46,8,.5,40);
   setVelocity(47,10,.5,46);
   setVelocity(48,0,.3,7);
   setVelocity(49,2,.3,8);
   setVelocity(50,4,.3,10);
   setVelocity(51,6,.3,21);
   setVelocity(52,8,.3,22);
   setVelocity(53,10,.3,24);
   setVelocity(54,0,.1,0);
   setVelocity(55,2,.1,1);
   setVelocity(56,4,.1,4);
   setVelocity(57,6,.1,12);
   setVelocity(58,8,.1,15);
   setVelocity(59,10,.1,18);

#if VAN_DER_PLAS
   setVelocity(42,4,.5,56);
   setVelocity(45,6,.5,60);
#else
   setVelocity(42,4,.45,56);
   setVelocity(45,6,.45,60);
   setVelocity(92,4,.55,66);
   setVelocity(93,6,.55,64);
#endif

//v
    setVelocity(60,1,1,37);
    setVelocity(61,3,1,39);
    setVelocity(62,5,1,51);
    setVelocity(63,7,1,53);
    setVelocity(64,9,1,55);
    setVelocity(65,1,.8,31);
    setVelocity(66,3,.8,34);
    setVelocity(67,5,.8,43);
    setVelocity(68,7,.8,45);
    setVelocity(69,9,.8,49);
    setVelocity(70,1,.6,28);
    setVelocity(71,3,.6,32);
    setVelocity(76,7,.6,41);
    setVelocity(77,9,.6,47);
    setVelocity(73,4.5,.5,59);
    setVelocity(74,5.5,.5,62);
    setVelocity(78,1,.4,9);
    setVelocity(79,3,.4,11);
    setVelocity(80,7,.4,23);
    setVelocity(81,9,.4,25);
    setVelocity(82,1,.2,3);
    setVelocity(83,3,.2,6);
    setVelocity(84,5,.2,14);
    setVelocity(85,7,.2,17);
    setVelocity(86,9,.2,20);
    setVelocity(87,1,0,2);
    setVelocity(88,3,0,5);
    setVelocity(89,5,0,13);
    setVelocity(90,7,0,16);
    setVelocity(91,9,0,19);

#if VAN_DER_PLAS
    setVelocity(72,5,.6,67);
    setVelocity(75,5,.4,58);
#else
    setVelocity(72,4.5,.6,67);
    setVelocity(75,4.5,.4,58);
    setVelocity(94,5.5,.6,65);
    setVelocity(95,5.5,.4,61);
#endif
}
