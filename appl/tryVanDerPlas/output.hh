#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dumux/io/grid/controlvolumegrids.hh>
#include <dumux/discretization/staggered/freeflow/mystaggeredgeometryhelper.hh>

template<class Vectors, class PairVector, class GridManager, class LeafGridView>
void outputOnStandardGrid(const Vectors& outputVecs, const PairVector& dofPositions, GridManager& hostGridManager, const LeafGridView& leafGridView, std::vector<std::string> names)
{
    std::array<double,2> bBoxMin_ = {std::numeric_limits<double>::max(),std::numeric_limits<double>::max()};
    std::array<double,2> bBoxMax_ = {-std::numeric_limits<double>::max(),-std::numeric_limits<double>::max()};

    for (const auto& element : elements(leafGridView))
    {
        for (int i=0; i<2; i++)
        {
            using std::min;
            using std::max;
            bBoxMin_[i] = min(bBoxMin_[i], element.geometry().corner(0)[i]);
            bBoxMin_[i] = min(bBoxMin_[i], element.geometry().corner(1)[i]);
            bBoxMin_[i] = min(bBoxMin_[i], element.geometry().corner(2)[i]);
            bBoxMin_[i] = min(bBoxMin_[i], element.geometry().corner(3)[i]);

            bBoxMax_[i] = max(bBoxMax_[i], element.geometry().corner(0)[i]);
            bBoxMax_[i] = max(bBoxMax_[i], element.geometry().corner(1)[i]);
            bBoxMax_[i] = max(bBoxMax_[i], element.geometry().corner(2)[i]);
            bBoxMax_[i] = max(bBoxMax_[i], element.geometry().corner(3)[i]);
        }
    }

    hostGridManager.grid().preAdapt();
    for (const auto& element : elements(leafGridView))
    {
        auto pos = element.geometry().center();

        auto x = pos[0];
        auto y = pos[1];

        if(.4*(bBoxMax_[0]-bBoxMin_[0]) < x && x < .6*(bBoxMax_[0]-bBoxMin_[0]) && y > .4*(bBoxMax_[1]-bBoxMin_[1]) && y < .6*(bBoxMax_[1]-bBoxMin_[1]))
        {
            hostGridManager.grid().mark( 1,  element);
        }
    }
    hostGridManager.grid().adapt();
    hostGridManager.grid().postAdapt();

    int numPDofs = leafGridView.size(0);

    std::vector<Dune::BlockVector<Dune::FieldVector<double, 1>, std::allocator<Dune::FieldVector<double, 1> > >> psVec;
    std::vector<Dune::BlockVector<Dune::FieldVector<double, 1>, std::allocator<Dune::FieldVector<double, 1> > >> usVec;
    std::vector<Dune::BlockVector<Dune::FieldVector<double, 1>, std::allocator<Dune::FieldVector<double, 1> > >> vsVec;

    Dune::BlockVector<Dune::FieldVector<double, 1>, std::allocator<Dune::FieldVector<double, 1> > > dummyVec;
    dummyVec.resize(numPDofs);

    for (unsigned int i = 0; i < names.size(); ++i)
    {
        psVec.push_back(dummyVec);
        usVec.push_back(dummyVec),
        vsVec.push_back(dummyVec);
    }

    using CVsGridView = std::decay_t<decltype(leafGridView)>;
    Dune::VTKWriter<CVsGridView> vtkWriter (leafGridView);

    for (unsigned int i = 0; i < names.size(); ++i)
    {
        vtkWriter.addCellData(psVec[i], names[i] + "_" + "p");
#if !VAN_DER_PLAS //for van der Plas the intersections do not in general correspond to dofPositions
        vtkWriter.addCellData(usVec[i], names[i] + "_" + "u");
        vtkWriter.addCellData(vsVec[i], names[i] + "_" + "v");
#endif
    }

    for (const auto& element : elements(leafGridView))
    {
        //p
        double minDiff = std::numeric_limits<double>::max();
        unsigned int pIndex = 0;

        for (unsigned int i = 0; i < dofPositions.size(); ++i)
        {
            double diffX = dofPositions[i].first  - element.geometry().center()[0];
            double diffY = dofPositions[i].second - element.geometry().center()[1];

            if (diffX*diffX + diffY*diffY < minDiff)
            {
                pIndex = i;
                minDiff = diffX*diffX + diffY*diffY;
            }
        }

        for (unsigned int i = 0; i < names.size(); ++i)
            psVec[i][leafGridView.indexSet().index(element)] = outputVecs[i][pIndex];

        //u
        double sumAreasU = 0.;
        double sumAreasV = 0.;
        for (auto&& intersection : intersections(leafGridView, element))
        {
            unsigned int index = 0;
            for (unsigned int i = 0; i < dofPositions.size(); ++i)
            {
                double diffX = dofPositions[i].first  - intersection.geometry().center()[0];
                double diffY = dofPositions[i].second - intersection.geometry().center()[1];

                if (diffX*diffX + diffY*diffY < 1e-10)
                {
                    index = i;
                }
            }

            if (Dumux::directionIndex(intersection.centerUnitOuterNormal()) == 0)
            {
                sumAreasU += intersection.geometry().volume();

                for (unsigned int i = 0; i < names.size(); ++i)
                    usVec[i][leafGridView.indexSet().index(element)] += intersection.geometry().volume()*(outputVecs[i][index]);
            }
            else if (Dumux::directionIndex(intersection.centerUnitOuterNormal()) == 1)
            {
                sumAreasV += intersection.geometry().volume();
                for (unsigned int i = 0; i < names.size(); ++i)
                    vsVec[i][leafGridView.indexSet().index(element)] += intersection.geometry().volume()*(outputVecs[i][index]);
            }
        }
        for (unsigned int i = 0; i < names.size(); ++i)
        {
            usVec[i][leafGridView.indexSet().index(element)] /= sumAreasU;
            vsVec[i][leafGridView.indexSet().index(element)] /= sumAreasV;
        }
    }

    vtkWriter.write("contiCVs");
}

template<class TypeTag, class Vectors, class PairVector, class LeafGridView>
void outputOnCVGrid (const Vectors& outputVecs, const PairVector& dofPositions, const LeafGridView& leafGridView, std::string paramGroup /*"xCVs" for x, "yCVs" for y*/, std::vector<std::string> names, int directionIndex /*0 for x, 1 for y*/)
{
    using namespace Dumux;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition =  typename Element::Geometry::GlobalCoordinate;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using CVGridGeometry = GetPropType<TypeTag, Properties::CVGridGeometry>;

    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);

    // create control volume grids and vtk output
    generateCVsDGFfile<GridGeometry, GlobalPosition>(paramGroup, *gridGeometry,directionIndex, VAN_DER_PLAS);

    using Grid = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >;
    using GridManager = GridManager<Grid>;

    GridManager cvsGridManager;
    cvsGridManager.init(paramGroup);
    const auto& cvsLeafGridView = cvsGridManager.grid().leafGridView();

    std::vector<Scalar> dummyVec;
    dummyVec.resize(cvsLeafGridView.size(0));

    std::vector<std::vector<Scalar>> vtkVecs;

    using CVsGridView = std::decay_t<decltype(cvsLeafGridView)>;
    Dune::VTKWriter<CVsGridView> cvsWriter (cvsLeafGridView);

    for (unsigned int i = 0; i < names.size(); ++i)
        vtkVecs.push_back(dummyVec);

    for (unsigned int i = 0; i < names.size(); ++i)
        cvsWriter.addCellData(vtkVecs[i], names[i]);

    auto cvsGridGeometry = std::make_shared<CVGridGeometry>(cvsLeafGridView);

    for (const auto& cv : elements(cvsLeafGridView))
    {
        std::vector<double> xValues;
        std::vector<double> yValues;

        for (unsigned int i=0; i < cv.geometry().corners(); ++i)
        {
            const auto& corner = cv.geometry().corner(i);

            xValues.push_back(corner[0]);
            yValues.push_back(corner[1]);
        }

        std::sort(xValues.begin(),xValues.end());
        std::sort(yValues.begin(),yValues.end());

        double left = xValues[0];
        double right = xValues[3];
        double down = yValues[0];
        double up = yValues[3];

        unsigned int index = 0;

        const auto directionVelocityDofs = (directionIndex == 0) ? xVelocityDofs() : yVelocityDofs();

        for (const auto& i : directionVelocityDofs)
        {
            if ((left < dofPositions[i].first || scalarCmp(left, dofPositions[i].first)) &&
            (dofPositions[i].first < right || scalarCmp(right, dofPositions[i].first)) &&
            (down < dofPositions[i].second || scalarCmp(down, dofPositions[i].second)) &&
            (dofPositions[i].second < up || scalarCmp(up, dofPositions[i].second)))
            {
                index = i;
            }
        }

        for (unsigned int i = 0; i < names.size(); ++i)
            vtkVecs[i][cvsLeafGridView.indexSet().index(cv)] = outputVecs[i][index];
    }

    cvsWriter.write("mom"+paramGroup);
}

template<class TypeTag, class Vectors, class PairVector, class LeafGridView>
void outputOnCVGrids(const Vectors& outputVecs, const PairVector& dofPositions, const LeafGridView& leafGridView, std::vector<std::string> names)
{
    outputOnCVGrid<TypeTag, Vectors, PairVector, LeafGridView> (outputVecs, dofPositions, leafGridView, "XCVs", names, 0);
    outputOnCVGrid<TypeTag, Vectors, PairVector, LeafGridView> (outputVecs, dofPositions, leafGridView, "YCVs", names, 1);
}

template<class TypeTag, class Vectors, class PairVector>
void output(const Vectors& outputVecs, const PairVector& dofPositions, std::vector<std::string> names)
{
    if (names.size() != outputVecs.size())
        DUNE_THROW(Dune::InvalidStateException, "names and outputVecs must be of same size.");

    using Grid = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >;
    using GridManager = Dumux::GridManager<Grid>;

    GridManager hostGridManager;
    hostGridManager.init("Output");
    const auto& leafGridView = hostGridManager.grid().leafGridView();

    using GridView = typename Grid::LeafGridView;

    outputOnStandardGrid(outputVecs, dofPositions, hostGridManager, leafGridView, names);
    outputOnCVGrids<TypeTag, Vectors, PairVector, GridView>(outputVecs, dofPositions, leafGridView, names);

    return;
}

