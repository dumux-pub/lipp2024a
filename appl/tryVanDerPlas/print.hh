#include <cmath>
#include <complex>
#include <limits>
#include <ios>
#include <iomanip>
#include <fstream>
#include <string>

template<class Matrix>
void printMatrix(const Matrix& matrix, const std::unordered_map<int, int>& dofMapA, const std::unordered_map<int, int>& dofMapB)
{
    std::ostream& s = std::cout;
    std::string rowtext = "";
    int width=3;
    int precision=3;

    std::ios_base::fmtflags oldflags = s.flags();
    // set the output format
    s.setf(std::ios_base::scientific, std::ios_base::floatfield);
    int oldprec = s.precision();
    s.precision(precision);

    for (int i = 0; i<dofMapA.size(); ++i)
    {
        int skipcols=0;
        int count=0;

        for (int j = 0; j<dofMapB.size(); ++j)
        {
            double entry = matrix[dofMapA.at(i)][dofMapB.at(j)];
            if (entry != 0){
                if(count<skipcols)
                continue;
                if(count>=skipcols+width)
                break;

                if(count==skipcols) {
                    s << rowtext;           // start a new row
                    s << " ";               // space in front of each entry
                    s.width(4);             // set width for counter
                    s << i<<": "; // number of first entry in a line
                }
                s.width(4);
                s<<j<<": |";

                s.width(9);
                s<<entry<<" ";

                s<<"|";
                ++count;

                if (count == 3 || count == 6 || count == 9 || count == 12)
                {
                    skipcols += width;
                    s << std::endl;
                    s << std::endl;
                }
            }
        }
        s << std::endl;
        s << std::endl;
    }

    // reset the output format
    s.flags(oldflags);
    s.precision(oldprec);
}

  template<class V>
  void printvector (const V& v, const std::unordered_map<int, int>& dofMap)
  {
    std::ostream& s = std::cout;
    std::string title = "";
    std::string rowtext = "";
    int columns=1;
    int width=10;
    int precision=2;

    // count the numbers printed to make columns
    int counter=0;

    // remember old flags
    std::ios_base::fmtflags oldflags = s.flags();

    // set the output format
    s.setf(std::ios_base::scientific, std::ios_base::floatfield);
    int oldprec = s.precision();
    s.precision(precision);

    // print title
    s << title << " [blocks=" << v.N() << ",dimension=" << v.dim() << "]"
      << std::endl;

      for (int i = 0; i < dofMap.size(); ++i)
      {
          s << rowtext; // start a new row
          s << " ";     // space in front of each entry
          s.width(4);   // set width for counter
          s << counter; // number of first entry in a line

        s << " ";         // space in front of each entry
        s.width(width);   // set width for each entry anew
        s << v[dofMap.at(i)];        // yeah, the number !
        counter++;        // increment the counter
          s << std::endl; // start a new line
      }

    // reset the output format
    s.flags(oldflags);
    s.precision(oldprec);
  }
