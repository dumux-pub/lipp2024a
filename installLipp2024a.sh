#!/bin/sh

### Create a folder for the dune and dumux modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### Clone the necessary modules
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git dumux
git clone https://gitlab.dune-project.org/extensions/dune-alugrid.git
git clone https://gitlab.dune-project.org/core/dune-common.git
git clone https://gitlab.dune-project.org/core/dune-geometry.git
git clone https://gitlab.dune-project.org/core/dune-grid.git
git clone https://gitlab.dune-project.org/core/dune-istl.git
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone https://gitlab.dune-project.org/extensions/dune-subgrid/
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git
git clone https://git.iws.uni-stuttgart.de/dumux-pub/lipp2024a.git

### Go to specific commits
cd dumux && git checkout 2ec8f9159a7211085cb4331d8a679dda103f3ffb && cd ..
cd dune-alugrid && git checkout 40f4a8b6ce5dc726c15ad0087f2def8d035d7101 && cd ..
cd dune-common && git checkout 14766f4fa2d49396bbf4a74412f245d4465aba7c && cd ..
cd dune-geometry && git checkout 08c1ff10225bc57cf4faf35a97bef348f3bc6689 && cd ..
cd dune-grid && git checkout 5ea4a064aace1ed9550094258b7ee3345d025434 && cd ..
cd dune-istl && git checkout 5918848289cf7f8cd924dae93405b4359bcabfa3 && cd ..
cd dune-localfunctions && git checkout 769fb8eeaf4931149a3cf1d29f18f9c7b34f5dca && cd ..
cd dune-subgrid && git checkout a08c72692ca714ee02d8ee1b806be40a989b034a && cd ..
cd dune-foamgrid && git checkout 4845fbcac1bb814fc65173dfd2f4d1d58ce33863 && cd ..
cd lipp2024a && git checkout next && cd ..

### Run dunecontrol
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all
