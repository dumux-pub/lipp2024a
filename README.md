SUMMARY
=======
This is the DuMuX module containing the code for producing the results for the Doctoral Thesis "Capturing local details in fluid-flow simulations: options, challenges and applications using marker-and-cell schemes" of Melanie Lipp.

Installation
============

The easiest way to install this module and its dependencies is to create a new
directory and clone this module:

```
mkdir New_Folder
cd New_Folder
git clone https://git.iws.uni-stuttgart.de/dumux-pub/lipp2024a.git
```

After that, execute the file [installLipp2024a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/lipp2024a/-/blob/next/installLipp2024a.sh)

```
cd lipp2024a
git checkout next
cd ..
chmod u+x lipp2024a/installLipp2024a.sh
./lipp2024a/installLipp2024a.sh
```

This should automatically download all necessary modules and check out the correct versions.

Applications
============


__Building from source__:

In order to produce all results navigate to the folder

```bash
cd lipp2024a/build-cmake/appl/
```

And run:

```bash
bash ./produce_data_and_tex_files.sh
```

Results can then be found in the folders `datafiles` and `texfiles`.
